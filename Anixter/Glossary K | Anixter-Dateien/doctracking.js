$(document).ready(function() {
	$("a").bind(
			"click",
			function() {
				if (typeof dataLayer !== 'undefined') {
					var url = this.href.toUpperCase();
					if (url.indexOf(".PDF") > -1 || url.indexOf(".DOC") > -1
						|| url.indexOf(".XLS") > -1 || url.indexOf(".ZIP") > -1
						|| url.indexOf(".PPT") > -1) {
						var urlparts = url.split('/');
						var file_name = urlparts[urlparts.length - 1];
						var folder_name = urlparts[urlparts.length - 2];
						if (folder_name == '$FILE') {
							folder_name = 'Lotus Notes Document Library';
						} else if (folder_name.indexOf("OBJECTS.") > -1) {
							folder_name = 'Spec Sheet';
						} else {
							folder_name = unescape(folder_name);
						}

						dataLayer.push({
							'data-eventvar': folder_name,
							'data-actionvar': 'download',
							'data-labelvar': file_name,
							'data-categoryvar': 'document-download'
						});

						return true;
					} else {
						return true;
					}
				}
			});
});