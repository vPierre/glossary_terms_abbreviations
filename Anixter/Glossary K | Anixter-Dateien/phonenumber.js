//pass through the phone number and return the properly formatted number
function stripPhoneNumber(phoneNumber)
{
  //regular expression to remove white spaces from phone number
  phoneNumber = phoneNumber.replace(/\s+/g, "");

  //regular expression to remove alpha characters from phone number
  phoneNumber = phoneNumber.replace(/[^0-9]/g, "");

  return phoneNumber;
}