
$jq7(document).ready(function(){
    $jq7('body').on('click', '#mobileCSDialog div a', function(event) {
        //execute Ajax call to clear shopping cart when switching to a different country
        var regionCode=$jq7(this).attr('data-region');
        $jq7.cookie('defaultSite',regionCode,{expires:365,path:'/'});
    	if ($jq7(".dialog-country-select").data("currentcountrycode") != $jq7(this).attr('data-country')) {
            $jq7.ajax({
                url: '/bin/shoppingCart/clear',
                type: 'POST',
                async: false
            });
        }
    });
});





