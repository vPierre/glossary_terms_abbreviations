window.DigitalFeedback['https://digitalfeedback.us.confirmit.com/api/digitalfeedback/loader/prod/scenario?programKey=usK4wj&scenarioId=867&programVersion=53'] = function (api) {
var showLog = true;

ConfirmitLog(showLog,"CONF - v5");

var loc =  document.location.href;
var loclc = loc.toLowerCase();

var pages = ['en_us','en_ca','fr_ca'];

var isOnPage = false;

for(var i=0;i<pages.length;++i)
{
	if(loclc.indexOf(pages[i].toLowerCase()) != -1)
	{
		isOnPage = true;
		break;
	}
}

if(!isOnPage)
{
	ConfirmitLog(showLog,"CONF - not on page.  Exiting.");
	return;
}

var ctx = api()
  .invite('iVFEEDback')
  .container('Vfeedback')
  .survey('p3094147293')
  .data({'purl': loc})
  .show();

  ctx.events.acceptInvite.on(
  function() { 
    console.log('invite accepted');
    ctx.show()
  });



};