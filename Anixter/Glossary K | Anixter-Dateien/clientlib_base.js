var campaignregistration = {
    handlers: {
        changeCountry:function() {
            /*
             * Setup the State selection
             */
            common.utility.form.populateStateSelection($("#campaign-registration-state-input-field-container"), {
                selectedCountry: $(this).val(),
                selectedLanguage: campaignregistration.info.currentLanguage,
                stateInputValue: (($("#state").is("input")) ? $("#state").val() : ""),
                required: true
            });
            //populateStateSelection( selectedCountry, selectedLanguage );
        },
        clickReset: function(){
            /*
             * Clear error container when reset is pressed
             */
            var validator = $("form#campaignregistration").validate();
            validator.resetForm();
        }
    }
};

$jq7("document").ready( function($) {
    if ($("#campaignregistration").length > 0) {
        $(document).on("change.countryInput", "#campaign-registration-country-input-field", campaignregistration.handlers.changeCountry);

        $(document).on("click.btnReset", "#btnReset", campaignregistration.handlers.clickReset);

        campaignregistration.errors = $("#campaignregistration-form-error-container").data();

        campaignregistration.info = $("#campaignregistration").data();

        $jq7.validator.addMethod("phone", function(phone_number, element) {
            phone_number = phone_number.replace(/\s+/g, "");
            var numberCount = 0;
            if (this.optional(element)) {
                return true;
            }
            if (phone_number.length > 0) {
                var numberArray = phone_number.split("");
                for (var i=0;i<numberArray.length;i++) {
                    if(!isNaN(numberArray[i])) {
                        numberCount++;
                    }
                }
            }
            return (numberCount >= 10);
        }, campaignregistration.errors.phoneNumberFormatMessage);

        $( "form#campaignregistration" ).validate({
            errorContainer: "#campaignregistration-form-error-container",
            errorLabelContainer : "#campaignregistration-form-error-container-error-list",
            wrapper : "li",
            messages : {
                firstName: campaignregistration.errors.firstNameMissingMessage,
                lastName: campaignregistration.errors.lastNameMissingMessage,
                companyName: campaignregistration.errors.companyNameMissingMessage,
                emailAddress: {
                    required: campaignregistration.errors.emailAddressMissingMessage,
                    email: campaignregistration.errors.emailAddressEmailFormatMessage
                },
                streetAddress1: campaignregistration.errors.streetAddress1MissingMessage,
                city: campaignregistration.errors.cityMissingMessage,
                state: campaignregistration.errors.stateMissingMessage,
                country: campaignregistration.errors.countryMissingMessage,
                phone: campaignregistration.errors.phoneNumberFormatMessage
            },
            rules: {
                phoneNumber: {
                    required: false,
                    phone: true
                }
            },
            focusInvalid: false,
            invalidHandler : function(form, validator) {
                var errorPane = $( "#campaignregistration-form-error-container");
                errorPane.show();
                if (!validator.numberOfInvalids()) return;

                $('html, body').animate({
                    scrollTop: errorPane.offset().top - 20
                }, 1000);
                setTimeout(function() { $(validator.errorList[0].element).focus(); }, 0);
            }
        });

        common.utility.form.populateStateSelection($("#campaign-registration-state-input-field-container"), {
            selectedCountry: $("#campaign-registration-country-input-field").val(),
            selectedLanguage: campaignregistration.info.currentLanguage,
            stateInputValue: (($("#state").is("input")) ? $("#state").val() : ""),
            required: true
        });
//		populateStateSelection( $( "#campaign-registration-country-input-field" ).val(), campaignregistration.info.currentLanguage );
    }
});
/*
 * Copyright 1997-2010 Day Management AG
 * Barfuesserplatz 6, 4001 Basel, Switzerland
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * Day Management AG, ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Day.
 */
(function($) {
    $(function () {
        // Used to output caught errors
        function errorLog(error, message) {
            try {
                if ($.cq.isAuthor() || window.location.hash == '#debug') {
                    if (typeof console != 'undefined' && typeof console.log  != 'undefined') {
                        console.log(error);
                        console.log(message);
                    }
                    alert(error.name+':\n'+error.message+'.\n'+message+'.');
                }
            } catch (e) { }
        }

        try {
            // Opacity fading conflicts in IE8 with the PNG fix and text anti-aliasing
            var fadingSpeed = $.browser.msie ? 0 : 250;

            // Removes the URL hash if it corresponds to the id of an element in the given context
            function removeHash(context) {
                try {
                    if (window.location.hash.length > 0 && $(window.location.hash, context).length > 0) {
                        window.location = (window.location+'').replace(window.location.hash, '');
                    }
                } catch (e) {
                    errorLog(e, 'Could not remove hash');
                }
            }

            // carousel code
            try {
                $('.cq-carousel').each(function () {
                    var carousel = $(this);
                    var playDelay = +$("var[title='play-delay']", this).text();
                    if (!playDelay) {
                        playDelay = 6000;
                    }
                    var slidingSpeed = +$("var[title='transition-time']", this).text();
                    if (!slidingSpeed) {
                        slidingSpeed = 1000;
                    }
                    var banners = $('.cq-carousel-banners', this);
                    //do not why, but
                    // var links = $('.cq-carousel-banner-switch a', this);
                    //returns more links than expected after component reload. Changed to "find" = works......
                    var switcher = $('.cq-carousel-banner-switch', this);
                    var links = switcher.find('a');
                    var items = $('.cq-carousel-banner-item', this);
                    var width = items.outerWidth();
                    var itemActive = items.filter(':first');
                    var itemPrevious = null;
                    var interval = null;
                    var i = 0;

                    var ctlPrev = $('a.cq-carousel-control-prev', this);
                    ctlPrev.click(function() {
                        if (ctlPrev.is('.cq-carousel-active')) {
                            $(links[(i+links.length-1)%links.length]).click();
                        }
                        return false;
                    });
                    var ctlNext = $('a.cq-carousel-control-next', this);
                    ctlNext.click(function() {
                        if (ctlNext.is('.cq-carousel-active')) {
                            $(links[(i+1)%links.length]).click();
                        }
                        return false;
                    });
                    if (links.length > 1) {
                        ctlNext.addClass('cq-carousel-active');
                    }
                    function play() {
                        stop();
                        if( playDelay > 0) {
                            interval = setInterval(function () {
                                $(links[(i+1)%links.length]).click();
                            }, playDelay);
                        }
                    }
                    function stop() {
                        if (interval !== null) {
                            clearInterval(interval);
                            interval = null;
                        }
                    }

                    // Show first item (needed for browsers that don't support CSS3 selector :first-of-type)
                    if (fadingSpeed || $.browser.version > 6) {
                        itemActive.css('left', 0);
                    } else {
                        itemActive.show();
                    }

                    links
                        .click(function () {
                            var link = $(this);
                            var itemNew = items.filter(link.attr('href'));
                            var j = itemNew.prevAll().length;
                            var direction = (j > i || interval !== null) ? 1 : -1;

                            if (!link.is('.cq-carousel-active')) {
                                links.removeClass('cq-carousel-active');
                                link.addClass('cq-carousel-active');

                                if (itemActive.is(':animated')) {
                                    itemActive.stop(true, true);
                                    itemPrevious.stop(true, true);
                                }

                                if (fadingSpeed) {
                                    itemNew.css({'left': direction*width}).animate({'left': 0, 'opacity': 1}, slidingSpeed);
                                    itemActive.animate({'left': -direction*width, 'opacity': 0}, slidingSpeed);
                                } else if ($.browser.version > 6) {
                                    itemNew.css({'left': direction*width}).animate({'left': 0}, slidingSpeed);
                                    itemActive.animate({'left': -direction*width}, slidingSpeed);
                                } else {
                                    itemNew.fadeIn();
                                    itemActive.fadeOut();
                                }

                                itemPrevious = itemActive;
                                itemActive = itemNew;
                                i = j;
                                if (i > 0) {
                                    ctlPrev.addClass('cq-carousel-active');
                                } else {
                                    ctlPrev.removeClass('cq-carousel-active');
                                }
                                if (i < links.length-1) {
                                    ctlNext.addClass('cq-carousel-active');
                                } else {
                                    ctlNext.removeClass('cq-carousel-active');
                                }
                            }

                            return false;
                        })
                        .each(function () {
                            var link = $(this);

                            link.attr('title', link.text());
                        })
                        .filter(':first').addClass('cq-carousel-active');

                    play();
                    carousel.hover(
                            function() {
                                stop();
                                ctlPrev.fadeIn();
                                ctlNext.fadeIn();
                            },
                            function() {
                                play();
                                ctlPrev.fadeOut();
                                ctlNext.fadeOut();
                            }
                    );

                    // Accessing the page with the anchor of a banner in the URL can break the layout
                    removeHash(this);
                });
            } catch (e) {
                errorLog(e, 'Could not initialize the banners');
            }
        } catch (e) {
            errorLog(e, 'Init failed');
        }
    });
})($CQ || $);
var feedbackform = {
    handlers: {
        clickReset: function(){
            /*
             * Clear error container when reset is pressed
             */
            var validator = $("form#feedback-form").validate();
            validator.resetForm();
        }
    },
    validators: {
        phone: function(phone_number, element) {
            phone_number = phone_number.replace(/\s+/g, "");
            var numberCount = 0;
            if (this.optional(element)) {
                return true;
            }
            if (phone_number.length > 0) {
                var numberArray = phone_number.split("");
                for (i=0;i<numberArray.length;i++) {
                    if(!isNaN(numberArray[i])) {
                        numberCount++;
                    }
                }
            }
            return (numberCount >= 10);
        }
    }
};

$jq7("document").ready( function($) {
    if ($("#feedback-form").length > 0) {
        $(document).on("click.btnReset", "#btnReset", feedbackform.handlers.clickReset);

        feedbackform.errors = $("#feedback-form-error-container").data();

        feedbackform.info = $("#feedback-form").data();

        $jq7.validator.addMethod("phone", feedbackform.validators.phone, feedbackform.errors.phoneNumberFormatMessage);

        $( "form#feedback-form" ).validate({
            errorContainer: "#feedback-form-error-container",
            errorLabelContainer : "#feedback-form-error-container-error-list",
            wrapper : "li",
            messages : {
                firstName: feedbackform.errors.firstNameMissingMessage,
                lastName: feedbackform.errors.lastNameMissingMessage,
                emailAddress: {
                    required: feedbackform.errors.emailMissingMessage,
                    email: feedbackform.errors.emailAddressEmailFormatMessage
                },
                phone: feedbackform.errors.phoneNumberFormatMessage
            },
            rules: {
                phoneNumber: {
                    required: false,
                    phone: true
                }
            },
            focusInvalid: false,
            invalidHandler : function(form, validator) {
                var errorPane = $( "#feedback-form-error-container");
                errorPane.show();
                if (!validator.numberOfInvalids()) return;

                $('html, body').animate({
                    scrollTop: errorPane.offset().top - 20
                }, 1000);
                setTimeout(function() { $(validator.errorList[0].element).focus(); }, 0);
            }
        });
    }
});
$(document).ready(function(){
    if ($("#governmentContacts").length > 0) {
        $("#button").click(function(){
            var stateIndex = document.getElementById("states").selectedIndex;
            var stateElement = document.getElementById("states").options;
            if (stateIndex != 0){
                window.open(stateElement[stateIndex].value,'_self');
            }
            return false;
        });
    }
});
$jq7( "#large-content-banner" ).ready( function($) {

    var hero = $('.carousel-hero-container');
    var dots = $('.carousel-controls a');
    var slides = hero.find('.slide');

    if (typeof hero !== undefined) {
        $('.carousel-hero-container .slide').css('width', $('body').outerWidth());

        hero.carousel({
            autoScroll: true,
            pause:8000,
            loop:true,
            continuous:true,
            pagination: false,
            nextPrevActions: false,
            itemsPerPage:1,
            itemsPerTransition:1,
            before: function(e, data){
                dots.removeClass('on');
                dots.eq( hero.carousel('getPage')-1 ).addClass('on');
            }
        });

        dots.click(function(e){
            hero.carousel('goToPage', dots.index(this)+1);
            e.preventDefault();
        });

        $(window).resize(function() {
            var currPage = hero.carousel('getPage');
            hero.carousel('goToPage', currPage, false);
            $('.carousel-hero-container .slide').css('width', $('body').outerWidth());
        });
    }
    
    //re-apply the background-image for each slide
    $(".slide").each(function() {
    	var imgUrl = "url('" + location.protocol + "//" + location.host + $(this).attr("data-imgSrc") + "')"; 
    	$(this).css("background-image", imgUrl);
    });

});
var locationfinder = locationfinder || {};

locationfinder.requestLocationsForRegions = function( requestURI, locationPath, container, success, failure ) {
	$.ajax( requestURI + "?locationPath=" + locationPath, {
        accepts : "application/json",
        dataType : "json",
        error : function( jqXHR, textStatus, errorThrown) {
            if ( failure ) {
                return failure.apply (
                    container,
                    [{
	                    success : false,
	                    returnCode : jqXHR.status,
	                    response : { message : errorThrown }
                    }]
                );
            }
        },
        success : function( data, textStatus, jqXHR ) {
            if ( ! data || ! data.success ) {
                if ( failure ) {
                    return failure.apply (
                        container,
                        [{
                            success : false,
                            returnCode : 0,
                            response : data
                        }]
                    );
                }
            }

            success.apply( container, [data] );

        }
    });
};

$(document).ready(function() {
    var overlay = $('#locations-map .region-overlay');
    $('#locations-map area').hover(function (e) {
        var area = $(this);
        overlay.addClass('on').addClass('region-' + area.attr('class'));
    }, function (e) {
        overlay.attr('class', 'region-overlay');
    });


    $('area').click(function() {
        var locationPath = $(this).attr('id');
        var title = $(this).attr('alt');

        locationfinder.requestLocationsForRegions(
            "/bin/locationList",
            locationPath,
            $('#locationList'),
            function getLocationContainer( data ) {
                var locationList = data;
                var locationCount = locationList.length;
                var columnCount = 15;
                var locationListContainer = $('<div class="dialog-columns-4 clearfix">');

                if ( locationList && locationList.length ) {

                    var i;

                    /*  First Column */
                    var curLocationContainer1 = $('<div class="col">');
                    curLocationContainer1.append("<ul/>");
                    for( i=0; i<columnCount && i<locationCount; i++ ) {
                        var curLocation1 = locationList[i];
                        var locationLinkTitle1 = curLocation1.title;
                        var locationLinkHref1 = curLocation1.href;
                        curLocationContainer1.append( "<li><a href=" + locationLinkHref1 + ">" + locationLinkTitle1 + "</a></li>" );
                        locationListContainer.append( curLocationContainer1 );
                    }
                    locationListContainer.append("</div>");

                    /*  Second Column */
                    var curLocationContainer2 = $('<div class="col">');
                    curLocationContainer2.append("<ul/>");
                    for( ; i<columnCount * 2 && i<locationCount; i++ ) {
                        var curLocation2 = locationList[i];
                        var locationLinkTitle2 = curLocation2.title;
                        var locationLinkHref2 = curLocation2.href;
                        curLocationContainer2.append( "<li><a href=" + locationLinkHref2 + ">" + locationLinkTitle2 + "</a></li>" );
                        locationListContainer.append( curLocationContainer2 );
                    }
                    locationListContainer.append("</div>");

                    /*  Third Column */
                    var curLocationContainer3 = $('<div class="col">');
                    curLocationContainer3.append("<ul/>");
                    for( ; i<columnCount * 3 && i<locationCount; i++ ) {
                        var curLocation3 = locationList[i];
                        var locationLinkTitle3 = curLocation3.title;
                        var locationLinkHref3 = curLocation3.href;
                        curLocationContainer3.append( "<li><a href=" + locationLinkHref3 + ">" + locationLinkTitle3 + "</a></li>" );
                        locationListContainer.append( curLocationContainer3 );
                    }
                    locationListContainer.append("</div>");

                    /*  Fourth Column */
                    var curLocationContainer4 = $('<div class="col">');
                    curLocationContainer4.append("<ul/>");
                    for( ; i<columnCount * 4 && i<locationCount; i++ ) {
                        var curLocation4 = locationList[i];
                        var locationLinkTitle4 = curLocation4.title;
                        var locationLinkHref4 = curLocation4.href;
                        curLocationContainer4.append( "<li><a href=" + locationLinkHref4 + ">" + locationLinkTitle4 + "</a></li>" );
                        locationListContainer.append( curLocationContainer4 );
                    }
                    locationListContainer.append("</div>");
                }

                $('#locationList').html('<div class="selectCountryTitle"><h2>'+ title + '</h2></div>');
                $('#locationList').append( locationListContainer );
                $('#locationList').append("</div>");
            }
        );
    });
});
var newslettersignup = newslettersignup || {
    handlers: {
        clickReset: function(){
            /*
             * Clear error container when reset is pressed
             */
            var validator = $("form#newslettersignup").validate();
            validator.resetForm();
        }
    }
};

$jq7("document").ready( function($) {
    if($("#newslettersignup").length > 0) {
        $(document).on("click.btnReset", "#btnReset", newslettersignup.handlers.clickReset);

        newslettersignup.errors = $("#newslettersignup-form-error-container").data();

        newslettersignup.info = $("#newslettersignup").data();

        $( "form#newslettersignup" ).validate({
            errorContainer: "#newslettersignup-form-error-container",
            errorLabelContainer : "#newslettersignup-form-error-container-error-list",
            wrapper : "li",
            messages : {
                firstName: newslettersignup.errors.firstNameMissingMessage,
                lastName: newslettersignup.errors.lastNameMissingMessage,
                companyName: newslettersignup.errors.companyNameMissingMessage,
                emailAddress: {
                    required: newslettersignup.errors.emailAddressMissingMessage,
                    email: newslettersignup.errors.emailAddressEmailFormatMessage
                },
                areasOfInterest: {
                    required: newslettersignup.errors.areasOfInterestRequiredMessage
                }
            },
            rules: {
                areasOfInterest: {
                    required: true,
                    minlength: 1
                }
            },
            focusInvalid: false,
            invalidHandler : function(form, validator) {
                var errorPane = $( "#newslettersignup-form-error-container");
                errorPane.show();
                if (!validator.numberOfInvalids()) return;

                $('html, body').animate({
                    scrollTop: errorPane.offset().top - 20
                }, 1000);
                setTimeout(function() { $(validator.errorList[0].element).focus(); }, 0);
            }
        });
    }
});
function openLink(){
    var languageIndex = document.getElementById("language").selectedIndex;
    var languageElement = document.getElementById("language").options;
    if (languageIndex == 0){
        CQ.Ext.Msg.alert(CQ.I18n.getMessage("Warning"), CQ.I18n.getMessage("Please select a state from dropdown list."));
    } else {
        window.open(languageElement[languageIndex].value,'_self');
    }
    return false;
}
var requestinformation = {
    handlers: {
        changeCountry: function() {
            /*
             * Setup the State selection
             */
            common.utility.form.populateStateSelection($("#rfi-state-input-field-container"), {
                selectedCountry: $(this).val(),
                selectedLanguage: requestinformation.info.currentLanguage,
                stateInputValue: (($("#state").is("input")) ? $("#state").val() : ""),
                required: true
            });
//			populateStateSelection( selectedCountry, selectedLanguage );
        },
        clickReset: function(){
            /*
             * Clear error container when reset is pressed
             */
            var validator = $("form#requestinformation").validate();
            validator.resetForm();
        }
    },
    validators: {
        phone: function(phone_number, element) {
            phone_number = phone_number.replace(/\s+/g, "");
            var numberCount = 0;
            if (this.optional(element)) {
                return true;
            }
            if (phone_number.length > 0) {
                var numberArray = phone_number.split("");
                for (var i=0;i<numberArray.length;i++) {
                    if(!isNaN(numberArray[i])) {
                        numberCount++;
                    }
                }
            }
            return (numberCount >= 10);
        }
    }
};

$jq7("document").ready( function($) {
    if ($("#requestinformation").length > 0) {
        $(document).on("change.country", "#rfi-country-input-field", requestinformation.handlers.changeCountry);

        $(document).on("click.btnReset", "#btnReset", requestinformation.handlers.clickReset);

        requestinformation.info = $("#requestinformation").data();
        requestinformation.errors = $("#requestinformation-form-error-container").data();

        $jq7.validator.addMethod("phone", requestinformation.validators.phone, requestinformation.errors.phoneNumberFormatMessage);

        $( "form#requestinformation" ).validate({
            errorContainer: "#requestinformation-form-error-container",
            errorLabelContainer : "#requestinformation-form-error-container-error-list",
            wrapper : "li",
            messages : {
                firstName: requestinformation.errors.firstNameMissingMessage,
                lastName: requestinformation.errors.lastNameMissingMessage,
                companyName: requestinformation.errors.companyNameMissingMessage,
                emailAddress: {
                    required: requestinformation.errors.emailAddressMissingMessage,
                    email: requestinformation.errors.emailAddressEmailFormatMessage
                },
                streetAddress1: requestinformation.errors.streetAddress1MissingMessage,
                city: requestinformation.errors.cityMissingMessage,
                state: requestinformation.errors.stateMissingMessage,
                country: requestinformation.errors.countryMissingMessage,
                phoneNumber: requestinformation.errors.phoneNumberFormatMessage,
                areasOfInterest: requestinformation.errors.areasOfInterestRequiredMessage
            },
            rules: {
                phoneNumber: {
                    required: false,
                    phone: true
                },
                areasOfInterest: {
                    required: true,
                    minlength: 1
                }
            },
            focusInvalid: false,
            invalidHandler : function(form, validator) {
                var errorPane = $( "#requestinformation-form-error-container");
                errorPane.show();
                if (!validator.numberOfInvalids()) return;

                $('html, body').animate({
                    scrollTop: errorPane.offset().top - 20
                }, 1000);
                setTimeout(function() { $(validator.errorList[0].element).focus(); }, 0);
            }
        });

        common.utility.form.populateStateSelection($("#rfi-state-input-field-container"), {
            selectedCountry: $("#rfi-country-input-field").val(),
            selectedLanguage: requestinformation.info.currentLanguage,
            stateInputValue: (($("#state").is("input")) ? $("#state").val() : ""),
            required: true
        });
//    	populateStateSelection( $( "#rfi-country-input-field" ).val(), requestinformation.info.currentLanguage );
    }
});
$jq7( "#small-content-banner" ).ready( function($) {

    var hero = $('.small-carousel-hero-container');
    var dots = $('.small-carousel-controls a');
    var slides = hero.find('.slide');

    if (typeof hero !== 'undefined') {
        $('.small-carousel-hero-container .slide').css('width', $('body').outerWidth());

        hero.carousel({
            autoScroll: true,
            pause:8000,
            loop:true,
            continuous:true,
            pagination: false,
            nextPrevActions: false,
            itemsPerPage:1,
            itemsPerTransition:1,
            before: function(e, data){
                dots.removeClass('on');
                dots.eq( hero.carousel('getPage')-1 ).addClass('on');
            }
        });

        dots.click(function(e){
            hero.carousel('goToPage', dots.index(this)+1);
            e.preventDefault();
        });

        $(window).resize(function() {
            var currPage = hero.carousel('getPage');
            hero.carousel('goToPage', currPage, false);
            $('.small-carousel-hero-container .slide').css('width', $('body').outerWidth());
        });
    }
});

var livetwitterfeed = livetwitterfeed || {};

(function( $ ) {

    var methods = {

        /**
         * Makes an AJAX request to the RESTful Twitter user timeline service.
         *
         * @param screenName The non @ prefixed screen name to request from Twitter
         * @param options An object of options.  The following options are supported
         *   <ul>
         *     <li>count : The number of posts to attempt to retrieve.</li>
         *     <li>excludeReplies : Boolean indicating whether replies should be excluded from the retrieved timeline.</li>
         *     <li>contributorDetails : Boolean indicating whether contributor details should be requested.</li>
         *     <li>includeEntities : Boolean indicating whether entity meta data should be requested.</li>
         *     <li>sinceId : The ID of the tweet to request tweets since.</li>
         *     <li>success : Function to call upon successful completion of the Twitter request</li>
         *   </ul>
         */
        makeTwitterRequest : function( screenName, options ) {

            var getParameters = {};

            getParameters.screen_name = screenName;

            if( typeof options.count === "number" ) {
                getParameters.count = options.count;
            }

            if( typeof options.excludeReplies === "boolean" ) {
                getParameters.exclude_replies = options.excludeReplies;
            }

            if( typeof options.contributorDetails === "boolean" ) {
                getParameters.contributor_details = options.contributorDetails;
            }

            if( typeof options.includeEntities === "boolean" ) {
                getParameters.include_entities = options.includeEntities;
            }

            if( typeof options.includeRts === "boolean" ) {
                getParameters.include_rts = options.includeRts;
            }

            if( options.sinceId ) {
                getParameters.since_id = options.since_id;
            }

            var context = this;

            $.ajax(
                "//api.twitter.com/1/statuses/user_timeline.json",
                {
                    context : context,
                    data : getParameters,
                    dataType : "jsonp",
                    type : "GET",
                    success : options.success || null
                }
            );
        },

        /**
         * The assumed context is the DOM element on which makeTwitterFeed was called
         *
         * @param curUser An object representing the current user
         */
        buildTwitterContainerHeader : function( curUser ) {

            var headerDom = this.find( "header" ).first();

            headerDom.html( "" );

            var headerImage = $( "<img src='" + curUser.profile_image_url + "' width='38' height='38'>" );
            var headerUserLink = $( "<h1 />" ).append( methods._buildUserLinkFromUser( curUser ) );
            var headerParagraph = $( "<p>On Twitter <br />@" + curUser.screen_name + "</p>" );

            headerDom.append( headerImage );
            headerDom.append( headerUserLink );
            headerDom.append( headerParagraph );

        },

        /**
         * The assumed context is the DOM element on which makeTwitterFeed was called
         *
         * @param tweetArray An array of Twitter tweet objects
         */
        buildTwitterContainerBody : function( tweetArray ) {

            var bodyListDom = this.find( "ul" ).first();

            bodyListDom.html( "" );

            for( var i=0; i<tweetArray.length; i++ ) {

                var curTweet = tweetArray[i];

                var tweetDom = methods.buildTweet( curTweet );

                bodyListDom.append( tweetDom );

            }
        },

        _constructEntitySetForEntities : function( entities ) {

            var retArray = Array();

            if( entities ) {
                if( entities.media && entities.media.length ) {
                    retArray = retArray.concat( methods._constructEntitySetForMediaEntities( entities.media ) );
                }
                if( entities.urls && entities.urls.length ) {
                    retArray = retArray.concat( methods._constructEntitySetForUrlEntities( entities.urls ) );
                }
                if( entities.user_mentions && entities.user_mentions.length ) {
                    retArray = retArray.concat( methods._constructEntitySetForUserMentionEntities( entities.user_mentions ) );
                }
                if ( entities.hashtags && entities.hashtags.length ) {
                    retArray = retArray.concat( methods._constructEntitySetForHashTagEntities( entities.hashtags ) );
                }
            }
            return retArray;
        },

        _constructEntitySetForMediaEntities : function( mediaEntities ) {

            var retArray = Array();

            for( var curMediaEntity in mediaEntities ) {
                if ( mediaEntities.hasOwnProperty( curMediaEntity ) &&
                     mediaEntities[curMediaEntity].url &&
                     mediaEntities[curMediaEntity].expanded_url &&
                     mediaEntities[curMediaEntity].indices ) {
                    retArray.push( {
                        extractedString : mediaEntities[curMediaEntity].url,
                        fullUrl : mediaEntities[curMediaEntity].expanded_url,
                        indices : mediaEntities[curMediaEntity].indices
                    });
                }
            }

            return retArray;
        },

        _constructEntitySetForUrlEntities : function( urlEntities ) {

            var retArray = Array();

            for( var curUrlEntity in urlEntities ) {
                if ( urlEntities.hasOwnProperty( curUrlEntity ) &&
                     urlEntities[curUrlEntity].url &&
                     urlEntities[curUrlEntity].indices ) {
                    retArray.push( {
                        extractedString : urlEntities[curUrlEntity].url,
                        fullUrl : urlEntities[curUrlEntity].expanded_url || urlEntities[curUrlEntity].url,
                        indices : urlEntities[curUrlEntity].indices
                    });
                }
            }

            return retArray;
        },

        _constructEntitySetForUserMentionEntities : function( userMentions ) {

            var retArray = Array();

            for( var curUserMentionEntity in userMentions ) {
                if ( userMentions.hasOwnProperty( curUserMentionEntity ) &&
                     userMentions[curUserMentionEntity].id_str &&
                     userMentions[curUserMentionEntity].screen_name &&
                     userMentions[curUserMentionEntity].indices ) {
                    retArray.push( {
                        extractedString : userMentions[curUserMentionEntity].screen_name.indexOf( "@" ) === 0 ? userMentions[curUserMentionEntity].screen_name : "@" + userMentions[curUserMentionEntity].screen_name,
                        fullUrl : "//twitter.com/" + userMentions[curUserMentionEntity].screen_name,
                        indices : userMentions[curUserMentionEntity].indices
                    });
                }
            }

            return retArray;

        },

        _constructEntitySetForHashTagEntities : function( hashTagEntities ) {

            var retArray = Array();

            for( var curHashTagEntity in hashTagEntities ) {

                if ( hashTagEntities.hasOwnProperty( curHashTagEntity ) &&
                     hashTagEntities[curHashTagEntity].text &&
                     hashTagEntities[curHashTagEntity].indices ) {

                    var hashlessText = hashTagEntities[curHashTagEntity].text.indexOf( "#" ) === 0 ? hashTagEntities[curHashTagEntity].text.substring(1) : hashTagEntities[curHashTagEntity].text;

                    retArray.push( {
                        extractedString : "#" + hashlessText,
                        fullUrl : "https://twitter.com/search/#" + hashlessText,
                        indices : hashTagEntities[curHashTagEntity].indices
                    });

                }
            }

            return retArray;
        },

        _enhanceTweetWithEntities : function( originalTweet, entities ) {

            var retString = originalTweet;

            if ( typeof originalTweet === "string" && entities.sort ) {
                entities.sort( function(o1,o2) {
                    var i1 = o1.indices[0];
                    var i2 = o2.indices[0];

                    return i2-i1;
                });

                for( var i=0; i<entities.length; i++ ) {
                    var curEntity = entities[i];

                    retString = methods._addLinkAtIndex( retString, curEntity.fullUrl, curEntity.indices );
                }
            }

            return retString;
        },

        _addLinkAtIndex : function( originalString, url, indices ) {

            if ( originalString && url && indices && indices.length && indices.length == 2 ) {
                return originalString.substring(0, indices[0]) +
                       "<a href='" + url + "' target='_twitter'>" +
                       originalString.substring(indices[0], indices[1]) +
                       "</a>" + originalString.substring(indices[1]);
            }

            return originalString;
        },

        _buildUserLinkFromUser : function( userObject ) {
            if ( userObject ) {
                return $( "<a href='//www.twitter.com/" + userObject.screen_name + "' target='_twitter' class='twitter-user-name' >" + userObject.name + "</a>" );
            }
            return "";
        },

        _buildTimeStringFromPostDate : function( postDate ) {
            var milisecondsInADay = 86400000;
            var milisecondsInAnHour = 3600000;
            var milisecondsInAMinute = 60000;

            var postDate = new Date( postDate );
            var postTimeMiliseconds = postDate.valueOf();
            var curMiliseconds = (new Date()).valueOf();
            var differenceInMiliseconds = curMiliseconds - postTimeMiliseconds;

            if ( differenceInMiliseconds < milisecondsInAMinute ) {
                return "just now";
            }
            if ( differenceInMiliseconds < milisecondsInAnHour ) {
                return Math.ceil( differenceInMiliseconds / milisecondsInAMinute ) + "m";
            }
            if ( differenceInMiliseconds < milisecondsInADay ) {
                return Math.ceil( differenceInMiliseconds / milisecondsInAnHour ) + "h";
            }

            return postDate.getDate() + " " + methods._buildMonthForInt( postDate.getMonth() );
        },

        _buildMonthForInt : function( monthInt ) {
            switch( monthInt ) {
            case 0:
                return "Jan";
            case 1:
                return "Feb";
            case 2:
                return "Mar";
            case 3:
                return "Apr";
            case 4:
                return "May";
            case 5:
                return "Jun";
            case 6:
                return "Jul";
            case 7:
                return "Aug";
            case 8:
                return "Sep";
            case 9:
                return "Oct";
            case 10:
                return "Nov";
            case 11:
                return "Dec";
            default:
                return "";
            }
        },






        /**
         * Create the DOM for a Tweet given the tweet object
         *
         * @param curTweet The object representing the tweet in question
         */
        buildTweet : function( curTweet ) {

            var tweetId = curTweet.id_str;

            var tweetContainer = $( "<li class='twitter-tweet-container' id='tweet-'" + tweetId + "' />" );

            var tweetString = curTweet.text;

            if ( curTweet.entities ) {
                var entitySet = methods._constructEntitySetForEntities( curTweet.entities )
                var tweetString = methods._enhanceTweetWithEntities( curTweet.text, entitySet );
            }

            var replyControl = $( "<a href='https://twitter.com/intent/tweet?in_reply_to=" + tweetId + "' target='_twitter'>Reply</a>" );
            var retweetControl = $( "<a href='https://twitter.com/intent/retweet?tweet_id=" + tweetId + "' target='_twitter'>Retweet</a>" );
            var favoriteControl = $( "<a href='https://twitter.com/intent/favorite?tweet_id=" + tweetId + "' target='_twitter'>Favorite</a>" );

            var userLink = methods._buildUserLinkFromUser( curTweet.user );

            var postTime = methods._buildTimeStringFromPostDate( curTweet.created_at );

            var tweetParagraph = $( "<p />" );
            tweetParagraph.append( tweetString );
            tweetParagraph.append( " " );
            tweetParagraph.append( postTime );
            tweetParagraph.append( " " );
            tweetParagraph.append( replyControl );
            tweetParagraph.append( " " );
            tweetParagraph.append( retweetControl );
            tweetParagraph.append( " " );
            tweetParagraph.append( favoriteControl );

            tweetContainer.append( $( "<h2 />" ).append( userLink ) );
            tweetContainer.append( tweetParagraph );

            return tweetContainer;

        }
    };

    /**
     *
     * @param screenName The non @ prefixed screen name to request from twitter
     * @param options An object of options.  The following options are supported
     *   <ul>
     *     <li>count : The number of posts to attempt to initially retrieve. Defaults to 4</li>
     *     <li>excludeReplies : Whether replies should be excluded in the retrieved tweets. Defaults to true</li>
     *     <li>polling : Boolean indicating whether polling should be enabled. Defaults to false</li>
     *     <li>pollingInterval : int indicating how often Twitter should be polled if polling is enabled.  This is required if polling is set to true</li>
     *   </ul>
     */
    $.fn.makeTwitterFeed = function( screenName, options ) {

        /*
         * Augment the options with a success function as long as one is not already specified
         */
        if( ! options.success ) {
            /*
             * The success function presumes that its context is the DOM element on which the
             * makeTwitterFeed method was called
             */
            options.success = function( data ) {
                if ( data.length ) {
                    var tweet1 = data[0];

                    /*
                     * Build the header if we have user data
                     */
                    if( tweet1 && tweet1.user ) {

                        methods.buildTwitterContainerHeader.apply( this, [tweet1.user] );

                    }

                    methods.buildTwitterContainerBody.apply( this, [data] );

                    /*
                     * Establish polling if the options call for it
                     */
                    if ( options.polling ) {
//                        console.log( "polling on" );
                    }
                }
            }
        }
        /*
         * Make the initial twitter request
         */
        methods.makeTwitterRequest.apply( this, [screenName, options] );
    }

})( $jq7 );
var twitterfeed = twitterfeed || {};

$jq7( "document" ).ready( function( $ ) {
	if ($(".sidebar-twitter").length > 0) {
		twitterfeed.info = $(".sidebar-twitter").data();
		
		if (twitterfeed.info.hasScreenName === "true") {
			var config = {
				count : twitterfeed.info.count, 
				excludeReplies : true, 
				includeEntities : true, 
				includeRts : true,
				polling : twitterfeed.info.polling
			};
			if (twitterfeed.info.polling === "true") {
				config.interval = twitterfeed.info.pollingInterval;
			}
			$( "#" + twitterfeed.info.name + "-twitter-feed" ).makeTwitterFeed( twitterfeed.info.screenName, config);
		}		
	}
});

    
    +function ($) {   
    /**
    /**
     *   InlineTypeahead Script Description
     */
    'use strict';

    var setUpMfrTypeAhead = {

        init: function() {
            var $searchElem,
                options,
                typeAheadService,
                minCharactersBeforeRequest,
                waitTimeBeforeRequest;

            $searchElem = $('#mfrSearchBox');


            options = $.parseJSON($searchElem.attr('data-options'));

            typeAheadService = options.autocompleteUrl;
            waitTimeBeforeRequest = options.waitTimeBeforeRequest;

            //https://stackoverflow.com/questions/1909441/how-to-delay-the-keyup-handler-until-the-user-stops-typing
            var delay = (function(){
                var timer = 0;
                return function (callback, ms) {
                    clearTimeout(timer);
                    timer = setTimeout(callback, ms)
                }
            })();

            $('#mfrSearchBox.mfrSearchInput').keydown(function (e) {
                var keycode = (e.keyCode ? e.keyCode : e.which);
                if (keycode === 13) {
                    delay(function () {
                    }, waitTimeBeforeRequest);
                }
            });

            $('#mfrSearchBox.mfrSearchInput').bind("paste",function (e) {
            	setTimeout(function () {
            		$('#mfrSearchBox.mfrSearchInput').val($('#mfrSearchBox.mfrSearchInput').val().replace(/[\t\r\n]|\s+$/g,""));
            	}, 100);
            	});

            $('.button.button__search').click(function () {
            	$('#mfrSearchBox.mfrSearchInput').val($('#mfrSearchBox.mfrSearchInput').val().replace(/[\t\r\n]|\s+$/g,""));
                delay(function () {
                }, waitTimeBeforeRequest);
            });

            $searchElem.keyup(function (e) {

                var keycode = (e.keyCode ? e.keyCode : e.which);

                //minimum number of characters to start the search
                minCharactersBeforeRequest = $searchElem.val().startsWith("\"") && !$searchElem.val().endsWith("\"") ? options.minCharactersBeforeRequest+1 : options.minCharactersBeforeRequest;
                if($searchElem.val().length >= minCharactersBeforeRequest && keycode !== 13) {
                    delay(setUpMfrTypeAhead.performTypeAheadSearch($searchElem, typeAheadService, options), waitTimeBeforeRequest);
                } else {
                    setUpMfrTypeAhead.closePopover();
                }

            });

            $searchElem.focusin(function (e) {
                minCharactersBeforeRequest = $searchElem.val().startsWith("\"") && !$searchElem.val().endsWith("\"") ? options.minCharactersBeforeRequest+1 : options.minCharactersBeforeRequest;
                if($searchElem.val().length >= minCharactersBeforeRequest) {
                    delay(setUpMfrTypeAhead.performTypeAheadSearch($searchElem, typeAheadService, options), waitTimeBeforeRequest);
                } else {
                    setUpMfrTypeAhead.closePopover();
                }

            });

            setUpMfrTypeAhead.hideLostFocus();
        }
        , hideLostFocus: function () {

            //https://stackoverflow.com/questions/11703093/how-to-dismiss-a-twitter-bootstrap-popover-by-clicking-outside
            $('body').on('click', function (e) {
                //did not click a popover toggle or popover
                if(!($(e.target).hasClass('mfrSearchInput'))){
                    if ($(e.target).data('toggle') !== 'popover'
                        && $(e.target).parents('.popover.in').length === 0) {
                        setUpMfrTypeAhead.closePopover();
                    }
                }
            });
        },

        closePopover : function () {

            var $searchElem = $('#mfrSearchBox');

            //only hide the popover if it was displayed once
            if($("#mfrSearchBox").data('bs.popover')) {
                $searchElem.popover('hide');
            }
        },

        performTypeAheadSearch: function($searchElem, typeAheadService, options) {

            $.ajax({
                url : typeAheadService.replace('%QUERY', $searchElem.val()),
                type: 'GET',
                cache: false,
                dataType: 'json'
            }).done(function (response) {
                //response.products = response.products.slice(0,3);
                var mfrHtml =  $('#' + options.mfrTemplate).tmpl(response);
                var html=$("<div class=\"row\">").html(mfrHtml);

                //refresh only the content of the popover
                if( $("#mfrSearchBox").data('bs.popover')) {
                    $("#mfrSearchBox").data('bs.popover').options.content = html;
                } else {

                    //instance the popover when is invoked the first time
                    $("#mfrSearchBox").popover({
                        html: true,
                        container : '.mfr-search',
                        content: html,
                        placement: 'bottom',
                        trigger: 'manual'
                    });
                }

                $searchElem.popover('show');
                
               // $('suggestion-detail-header, .suggestion-detail-info, .category-container, .manufacturer-container').wrapInTag({"words" : [$('#search').val()]});

           
                
            })


        }
    }
    
    $(function() {
    	  if (typeof $('#mfrSearchBox').attr('data-options') !== 'undefined'){
    		  setUpMfrTypeAhead.init();
    	  }

        // http://stackoverflow.com/a/9795091
        $.fn.wrapInTag = function (opts) {
            // http://stackoverflow.com/a/1646618
            function getText(obj) {
                return obj.textContent ? obj.textContent : obj.innerText;
            }

            var tag = opts.tag || 'strong',
                words = opts.words || [],
                regex = RegExp(words.join('|'), 'gi'),
                replacement = '<' + tag + '>$&</' + tag + '>';

            // http://stackoverflow.com/a/298758
            $(this).contents().each(function () {
                if (this.nodeType === 3) //Node.TEXT_NODE
                {
                    // http://stackoverflow.com/a/7698745
                    $(this).replaceWith(getText(this).replace(regex, replacement));
                }
                else if (!opts.ignoreChildNodes) {
                    $(this).wrapInTag(opts);
                }
            });
        };
    });
    }($jq7);

    
function eventListingv2() {
	var evntlstUrl;
	var cardParent;
	var cardTemplate;
	var evntlstmonth;
	
	function evntlstInit() {
		var parentElement = document.getElementById("evntlst-container");
		cardParent = document.querySelector("#evntlst-cardtarget");
		cardTemplate = document.querySelector(".evntlst-card-parent");
		cardTemplate.remove();
		evntlstUrl = parentElement.dataset.evntlstPath;
		
		//attach listeners to the month buttons
		var monthButtonList = parentElement.querySelectorAll('.evntlst-btn-month'); //all 12 month buttons
		$(monthButtonList).click(evntlstMonthListener);
	
	    //atach listeners to the other buttons
	    var otherButtonList = parentElement.querySelectorAll('.evntlst-btn-other'); //other filter buttons
		$(otherButtonList).click(evntlstOtherListener);
	
	    $("#evntlst-yr-minus").click(evntlstYearListener);
	    $("#evntlst-yr-plus").click(evntlstYearListener);
	
	    var today = new Date();
	    
		evntlstMonth = evntlstGetCurrentMonth();
	    $("#evntlst-btn-" + evntlstMonth).addClass("evntlst-btn-month--selected");
	
		evntlstUpdatePage(evntlstMonth, $("#evntlst-yr-show").text(), null);
	}
	
	function evntlstGetCurrentMonth(){
		var today = new Date();
		var evntlstMonthArray = ['january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december'];
		return evntlstMonthArray[today.getMonth()];
	}
	
	function evntlstMonthListener(){
		$('.evntlst-btn-month').removeClass("evntlst-btn-month--selected");
		$(this).addClass("evntlst-btn-month--selected");
		$('.evntlst-btn-other').removeClass("evntlst-btn-other--selected");
	
	    month = this.id.slice(12);
	    evntlstMonth = month;
	    evntlstUpdatePage(month, $("#evntlst-yr-show").text(), null);
	}
	
	function evntlstOtherListener(){
	    if ($(this).hasClass("evntlst-btn-other--selected")){
			$('.evntlst-btn-other').removeClass("evntlst-btn-other--selected");
			evntlstUpdatePage(evntlstMonth, $("#evntlst-yr-show").text(), null);
	    } else {
	        $('.evntlst-btn-other').removeClass("evntlst-btn-other--selected");
	        $(this).addClass("evntlst-btn-other--selected");
	        type = this.id.slice(12);
	        evntlstUpdatePage(evntlstMonth, $("#evntlst-yr-show").text(), type);
		}
	}
	
	function evntlstYearListener(){
		today = new Date();
		year = parseInt(document.querySelector("#evntlst-yr-show").innerText);
	    if (this.id == 'evntlst-yr-minus') {	
	        year = year - 1;
	    }else {
	        year = year + 1;
	    }
	    $('.evntlst-btn-other').removeClass("evntlst-btn-other--selected");
	    
	    if (year > today.getFullYear()){
	    	evntlstMonth = 'january';
	    }else if (year == today.getFullYear()){
	    	evntlstMonth = evntlstGetCurrentMonth();
	    }else {
	    	evntlstMonth = 'december';
	    }
	    $('.evntlst-btn-month').removeClass("evntlst-btn-month--selected");
	    $("#evntlst-btn-" + evntlstMonth).addClass("evntlst-btn-month--selected");
	    
	    $("#evntlst-yr-show").text(year);
	    evntlstUpdatePage(evntlstMonth, year, null);
	}
	
	function evntlstUpdatePage(month, year, filter){
		
	    var urlParms = "?tag=" + encodeURIComponent("resource:event/" + year);
	    urlParms = urlParms + "&tag=" +  encodeURIComponent("resource:event/" + month);
	    if(filter) {
			urlParms = urlParms + "&tag=" +  encodeURIComponent("resource:event/" + filter);
	    }
	
		removeAllChildNodes(cardParent);
	    $.ajax({
	    	url: evntlstUrl + urlParms,
	    	dataType: json,
	    	success: function(result){
	    		if (!result.error){
		    		for(var i = 0; i < result.events.length; i++){
		    			card = cardTemplate.cloneNode(true);
		    			cardData = result.events[i];
		    			card.querySelector(".evntlst-card-title").innerText = cardData.title;
		    			card.querySelector(".evntlst-card-descrip").innerHTML = cardData.description;
		    			card.querySelector(".evntlst-card-datelink").innerText = cardData.pageDate;
		    			tagContainer = card.querySelector(".evntlst-card-tagcontainer");
		    			removeAllChildNodes(tagContainer);
		    			for (var j = 0; j < cardData.tags.length; j++){
		    				tagDiv = document.createElement("div");
		    				tagDiv.classList.add("evntlst-card-tag");
		    				tagDiv.innerText = cardData.tags[j];
		    				tagContainer.appendChild(tagDiv);
		    			}
		           		var linkTarget = ""
						if (cardData.redirectTarget != null){	
							linkTarget = cardData.redirectTarget;
		    			}else {
							linkTarget = cardData.path;
						}
		           		
		           		link = document.createElement("a");
						link.href = linkTarget;
						link.innerText = "More";
						card.querySelector(".evntlst-card-morelink").innerText = "";
						card.querySelector(".evntlst-card-morelink").appendChild(link);
		           		
						if(cardData.image !=null){
							imageElement = document.createElement("img");
							imageElement.src = cardData.image;
							imageElement.className = "evntlst-card-img";
							imageContainer = card.querySelector(".evntlst-card-heroimgcontainer");
							removeAllChildNodes(imageContainer);
							imageContainer.appendChild(imageElement);
						} else {
							imgContainer = card.querySelector(".evntlst-card-heroimgcontainer")
							imgContainer.remove();
						}
						
		    			cardParent.appendChild(card);
		    		}
		    		cards = document.querySelectorAll(".evntlst-card-parent")
		    		var i = 0;
		
		    		while (i < cards.length){
		    			row = document.createElement("div");
		    			row.className = "row";
		    			for (var j = 0; j < 2 && i < cards.length; j++){
		    				row.appendChild(cards[i]);
		    				i++;
		    			}
		    			cardParent.appendChild(row);
		    		}
		    	} else {
					console.log("Unable to update page: " + result.error);
					cardParent.appendChild(cardTemplate);
					//card.querySelector(".evntlst-card-descrip").innerText = "Lorem ipsum dolor sit amet.";
				}
	    	}
	    });
	}
	
	function removeAllChildNodes(parent) {
	    while (parent.firstChild) {
	        parent.removeChild(parent.firstChild);
	    }
	}
	evntlstInit();
}

if (document.querySelector(".eventlistingv2") && document.getElementById("evntlst-container")){
	eventListingv2();
}
(function (window, undefined) {
    var MapsLib = function (options) {
        var self = this;

        options = options || {};

        this.recordName = options.recordName || "result"; //for showing a count of results
        this.recordNamePlural = options.recordNamePlural || "results";
        this.searchRadius = options.searchRadius || 80467; //in meters ~ 1/2 mile

        // the encrypted Table ID of your Fusion Table (found under File => About)
        this.fusionTableId = options.fusionTableId || "1znwIy4SnZ-uqcFZswWkWelNC4IOH0FJUeHBHPQDG",

        // Found at https://console.developers.google.com/
        // Important! this key is for demonstration purposes. please register your own.
        this.googleApiKey = options.googleApiKey || "AIzaSyCIHmkOzyEp4fDWPGHhoVzRKMl8rtkdBfM",
        
        // name of the location column in your Fusion Table.
        // NOTE: if your location column name has spaces in it, surround it with single quotes
        // example: locationColumn:     "'my location'",
        this.locationColumn = options.locationColumn || "Lat";
        
        // appends to all address searches if not present
        this.locationScope = options.locationScope || "";

        // zoom level when map is loaded (bigger is more zoomed in)
        this.defaultZoom = options.defaultZoom || 11; 

        // center that your map defaults to
        this.map_centroid = new google.maps.LatLng(options.map_center[0], options.map_center[1]);
        
        // marker image for your searched address
        if (typeof options.addrMarkerImage !== 'undefined') {
            if (options.addrMarkerImage != "")
                this.addrMarkerImage = options.addrMarkerImage;
            else
                this.addrMarkerImage = ""
        }
        else
            this.addrMarkerImage = "/etc.clientlibs/settings/wcm/designs/common/images/resources/img/blue-pushpin.png"

    	this.currentPinpoint = null;
    	$("#result_count").html("");
        
        this.myOptions = {
            zoom: this.defaultZoom,
            center: this.map_centroid,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        this.geocoder = new google.maps.Geocoder();
        this.map = new google.maps.Map($("#map_canvas")[0], this.myOptions);
        
        // maintains map centerpoint for responsive design
        google.maps.event.addDomListener(self.map, 'idle', function () {
            self.calculateCenter();
        });
        google.maps.event.addDomListener(window, 'resize', function () {
            self.map.setCenter(self.map_centroid);
        });
        self.searchrecords = null;

        //reset filters
        $("#search_address").val(self.convertToPlainString($.address.parameter('address')));
        var loadRadius = self.convertToPlainString($.address.parameter('radius'));
        if (loadRadius != "") 
            $("#search_radius").val(loadRadius);
        else 
            $("#search_radius").val(self.searchRadius);
        
        $(":checkbox").prop("checked", "checked");
        $("#result_box").hide();

        //-----custom initializers-----
        //-----end of custom initializers-----

        //run the default search when page loads
        self.doSearch();
        if (options.callback) options.callback(self);
    };

    //-----custom functions-----
    //-----end of custom functions-----

    MapsLib.prototype.submitSearch = function (whereClause, map) {
        var self = this;
        //get using all filters
        //NOTE: styleId and templateId are recently added attributes to load custom marker styles and info windows
        //you can find your Ids inside the link generated by the 'Publish' option in Fusion Tables
        //for more details, see https://developers.google.com/fusiontables/docs/v1/using#WorkingStyles
        self.searchrecords = new google.maps.FusionTablesLayer({
            query: {
                from: self.fusionTableId,
                select: self.locationColumn,
                where: whereClause
            },
            styleId: 2,
            templateId: 2
        });
        self.fusionTable = self.searchrecords;
        self.searchrecords.setMap(map);
        self.getCount(whereClause);
        self.getList(whereClause);
    };




    MapsLib.prototype.getgeoCondition = function (address, callback) {
        var self = this;



        if (address !== "") {
            if (address.toLowerCase().indexOf(self.locationScope) === -1) {
                address = address + " " + self.locationScope;
            }
            self.geocoder.geocode({
                'address': address
            }, function (results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    self.currentPinpoint = results[0].geometry.location;
                    var map = self.map;

                    $.address.parameter('address', encodeURIComponent(address));
                    $.address.parameter('radius', encodeURIComponent(self.searchRadius));
                    map.setCenter(self.currentPinpoint);
                    // set zoom level based on search radius
                    if (self.searchRadius >= 1610000) map.setZoom(4); // 1,000 miles
                    else if (self.searchRadius >= 805000) map.setZoom(5); // 500 miles
                    else if (self.searchRadius >= 402500) map.setZoom(6); // 250 miles
                    else if (self.searchRadius >= 161000) map.setZoom(7); // 100 miles
                    else if (self.searchRadius >= 80500) map.setZoom(8); // 100 miles
                    else if (self.searchRadius >= 40250) map.setZoom(9); // 100 miles
                    else if (self.searchRadius >= 16100) map.setZoom(11); // 10 miles
                    else if (self.searchRadius >= 8050) map.setZoom(12); // 5 miles
                    else if (self.searchRadius >= 3220) map.setZoom(13); // 2 miles
                    else if (self.searchRadius >= 1610) map.setZoom(14); // 1 mile
                    else if (self.searchRadius >= 805) map.setZoom(15); // 1/2 mile
                    else if (self.searchRadius >= 400) map.setZoom(16); // 1/4 mile
                    else self.map.setZoom(17);

                    if (self.addrMarkerImage != '') {
                        self.addrMarker = new google.maps.Marker({
                            position: self.currentPinpoint,
                            map: self.map,
                            icon: self.addrMarkerImage,
                            animation: google.maps.Animation.DROP,
                            title: address
                        });
                    }
                    var geoCondition = " AND ST_INTERSECTS(" + self.locationColumn + ", CIRCLE(LATLNG" + self.currentPinpoint.toString() + "," + self.searchRadius + "))";
                    callback(geoCondition);
                    self.drawSearchRadiusCircle(self.currentPinpoint);
                } else {
                    alert("We could not find your address: " + status);
                    callback('');
                }
            });
        } else {
            callback('');
        }
    };

    MapsLib.prototype.doSearch = function () {
        var self = this;
        self.clearSearch();
        var address = $("#search_address").val();
        self.searchRadius = $("#search_radius").val();
        self.whereClause = self.locationColumn + " not equal to ''";
        
        //-----custom filters-----
        //-----end of custom filters-----

        self.getgeoCondition(address, function (geoCondition) {
            self.whereClause += geoCondition;
            self.submitSearch(self.whereClause, self.map);
        });

    };

    MapsLib.prototype.reset = function () {
        $.address.parameter('address','');
        $.address.parameter('radius','');
        window.location.reload();
    };


    MapsLib.prototype.getInfo = function (callback) {
        var self = this;
        $jq7.ajax({
            url: 'https://www.googleapis.com/fusiontables/v1/tables/' + self.fusionTableId + '?key=' + self.googleApiKey,
            dataType: 'json'
        }).done(function (response) {
            if (callback) callback(response);
        });
    };

    MapsLib.prototype.addrFromLatLng = function (latLngPoint) {
        var self = this;
        self.geocoder.geocode({
            'latLng': latLngPoint
        }, function (results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                if (results[1]) {
                    $('#search_address').val(results[1].formatted_address);
                    $('.hint').focus();
                    self.doSearch();
                }
            } else {
                alert("Geocoder failed due to: " + status);
            }
        });
    };

    MapsLib.prototype.drawSearchRadiusCircle = function (point) {
        var self = this;
        var circleOptions = {
            strokeColor: "#4b58a6",
            strokeOpacity: 0.3,
            strokeWeight: 1,
            fillColor: "#4b58a6",
            fillOpacity: 0.05,
            map: self.map,
            center: point,
            clickable: false,
            zIndex: -1,
            radius: parseInt(self.searchRadius)
        };
        self.searchRadiusCircle = new google.maps.Circle(circleOptions);
    };

    MapsLib.prototype.query = function (query_opts, callback) {
        var queryStr = [],
            self = this;
        queryStr.push("SELECT " + query_opts.select);
        queryStr.push(" FROM " + self.fusionTableId);
        // where, group and order clauses are optional
        if (query_opts.where && query_opts.where != "") {
            queryStr.push(" WHERE " + query_opts.where);
        }
        if (query_opts.groupBy && query_opts.roupBy != "") {
            queryStr.push(" GROUP BY " + query_opts.groupBy);
        }
        if (query_opts.orderBy && query_opts.orderBy != "") {
            queryStr.push(" ORDER BY " + query_opts.orderBy);
        }
        if (query_opts.offset && query_opts.offset !== "") {
            queryStr.push(" OFFSET " + query_opts.offset);
        }
        if (query_opts.limit && query_opts.limit !== "") {
            queryStr.push(" LIMIT " + query_opts.limit);
        }
        var theurl = {
            base: "https://www.googleapis.com/fusiontables/v1/query?sql=",
            queryStr: queryStr,
            key: self.googleApiKey
        };
        $.ajax({
            url: [theurl.base, encodeURIComponent(theurl.queryStr.join(" ")), "&key=", theurl.key].join(''),
            dataType: "json"
        }).done(function (response) {
            //console.log(response);
            if (callback) callback(response);
        }).fail(function(response) {
            self.handleError(response);
        });
    
	};

    MapsLib.prototype.handleError = function (json) {
        if (json.error !== undefined) {
            var error = json.responseJSON.error.errors;
            console.log("Error in Fusion Table call!");
            for (var row in error) {
                console.log(" Domain: " + error[row].domain);
                console.log(" Reason: " + error[row].reason);
                console.log(" Message: " + error[row].message);
            }
        }
    };
    MapsLib.prototype.getCount = function (whereClause) {
        var self = this;
        var selectColumns = "Count()";
        self.query({
            select: selectColumns,
            where: whereClause
        }, function (json) {
            self.displaySearchCount(json);
        });
    };

    MapsLib.prototype.displaySearchCount = function (json) {
        var self = this;
        
        var numRows = 0;
        if (json["rows"] != null) {
            numRows = json["rows"][0];
        }
        var name = self.recordNamePlural;
        if (numRows == 1) {
            name = self.recordName;
        }
        $("#result_box").fadeOut(function () {
            $("#result_count").html(self.addCommas(numRows) + " " + name + " found");
        });
        $("#result_box").fadeIn();
    };

     MapsLib.prototype.getList = function(whereClause) {
    var self = this;
    var selectColumns = 'Description, Address1, Address2, Phone, Lat, Lng, Number ';

    self.query({ 
      select: selectColumns, 
      where: whereClause 
    }, function(response) { 
      self.displayList(response);
    });
  },

  MapsLib.prototype.displayList = function(json) {
    var self = this;

    var data = json['rows'];
    var template = '';

    var results = $('#results_list');
    results.hide().empty(); //hide the existing list and empty it out first

    if (data == null) {
      //clear results list
      results.append("<li><span class='lead'>No results found</span></li>");
    }
    else {
      for (var row in data) {
    	  if(data[row].constructor === Array){
	        template = "\
	          <div class='row-fluid item-list'>\
	            <div class='span12'>\
	              <strong>" + data[row][0] + "</strong> \
	              <br />\
	              " + data[row][1] + "\
	              <br />\
	              " + data[row][2] + "\
	              <br />\
	              " + data[row][3] + "\
	              <br />\
	              Branch Number: " + data[row][6] + "\
	              <br />\
	              <a target='_blank' class='' href='https://maps.google.com?daddr=" + data[row][1] + " , " + data[row][2] + "'><img src='/etc.clientlibs/settings/wcm/designs/common/images/resources/img/directions-icon.png' /> Get Directions</a> \
	              <br />\
	              <br />\
	            </div>\
	          </div>";
	        results.append(template);
    	  }
      }
    }
    results.fadeIn();
  },

    MapsLib.prototype.addCommas = function (nStr) {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    };

    // maintains map centerpoint for responsive design
    MapsLib.prototype.calculateCenter = function () {
        var self = this;
        center = self.map.getCenter();
    };

    //converts a slug or query string in to readable text
    MapsLib.prototype.convertToPlainString = function (text) {
        if (text === undefined) return '';
        return decodeURIComponent(text);
    };

    MapsLib.prototype.clearSearch = function () {
        var self = this;
        if (self.searchrecords && self.searchrecords.getMap) 
            self.searchrecords.setMap(null);
        if (self.addrMarker && self.addrMarker.getMap) 
            self.addrMarker.setMap(null);
        if (self.searchRadiusCircle && self.searchRadiusCircle.getMap) 
            self.searchRadiusCircle.setMap(null);
    };

    

    MapsLib.prototype.findMe = function () {
        var self = this;
        var foundLocation;
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var latitude = position.coords.latitude;
                var longitude = position.coords.longitude;
                var accuracy = position.coords.accuracy;
                var coords = new google.maps.LatLng(latitude, longitude);
                self.map.panTo(coords);
                self.addrFromLatLng(coords);
                self.map.setZoom(14);
                $jq7('#map_canvas').append('<div id="myposition"><i class="fontello-target"></i></div>');
                setTimeout(function () {
                    $jq7('#myposition').remove();
                }, 3000);
            }, function error(msg) {
                alert('Please enable your GPS position future.');
            }, {
                //maximumAge: 600000,
                //timeout: 5000,
                enableHighAccuracy: true
            });
        } else {
            alert("Geolocation API is not supported in your browser.");
        }
    };
    if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
        module.exports = MapsLib;
    } else if (typeof define === 'function' && define.amd) {
        define(function () {
            return MapsLib;
        });
    } else {
        window.MapsLib = MapsLib;
    }

})(window);


function followUsClick(socialNetwork) {
		digitalData.linkName = 'social follow:' + socialNetwork;
		digitalData.linkType = 'exit link';
		digitalData.linkCategory = 'social follow';
		_satellite.track("click tracking");
};





function trackBanner(campaignName) {
    
    dataLayer.push({'data-eventvar':'banner','data-actionvar':'click','data-labelvar':campaignName,'data-categoryvar':'banner' });
    digitalData.linkName=campaignName;
    digitalData.linkType="custom link";
    digitalData.linkCategory="internal campaign";
    _satellite.track("click tracking")
    return true;
}


(function(j,f){function s(a,b){var c=a.createElement("p"),m=a.getElementsByTagName("head")[0]||a.documentElement;c.innerHTML="x<style>"+b+"</style>";return m.insertBefore(c.lastChild,m.firstChild)}function o(){var a=d.elements;return"string"==typeof a?a.split(" "):a}function n(a){var b=t[a[u]];b||(b={},p++,a[u]=p,t[p]=b);return b}function v(a,b,c){b||(b=f);if(e)return b.createElement(a);c||(c=n(b));b=c.cache[a]?c.cache[a].cloneNode():y.test(a)?(c.cache[a]=c.createElem(a)).cloneNode():c.createElem(a);
return b.canHaveChildren&&!z.test(a)?c.frag.appendChild(b):b}function A(a,b){if(!b.cache)b.cache={},b.createElem=a.createElement,b.createFrag=a.createDocumentFragment,b.frag=b.createFrag();a.createElement=function(c){return!d.shivMethods?b.createElem(c):v(c,a,b)};a.createDocumentFragment=Function("h,f","return function(){var n=f.cloneNode(),c=n.createElement;h.shivMethods&&("+o().join().replace(/\w+/g,function(a){b.createElem(a);b.frag.createElement(a);return'c("'+a+'")'})+");return n}")(d,b.frag)}
function w(a){a||(a=f);var b=n(a);if(d.shivCSS&&!q&&!b.hasCSS)b.hasCSS=!!s(a,"article,aside,figcaption,figure,footer,header,hgroup,nav,section{display:block}mark{background:#FF0;color:#000}");e||A(a,b);return a}function B(a){for(var b,c=a.attributes,m=c.length,f=a.ownerDocument.createElement(l+":"+a.nodeName);m--;)b=c[m],b.specified&&f.setAttribute(b.nodeName,b.nodeValue);f.style.cssText=a.style.cssText;return f}function x(a){function b(){clearTimeout(d._removeSheetTimer);c&&c.removeNode(!0);c=null}
var c,f,d=n(a),e=a.namespaces,j=a.parentWindow;if(!C||a.printShived)return a;"undefined"==typeof e[l]&&e.add(l);j.attachEvent("onbeforeprint",function(){b();var g,i,d;d=a.styleSheets;for(var e=[],h=d.length,k=Array(h);h--;)k[h]=d[h];for(;d=k.pop();)if(!d.disabled&&D.test(d.media)){try{g=d.imports,i=g.length}catch(j){i=0}for(h=0;h<i;h++)k.push(g[h]);try{e.push(d.cssText)}catch(n){}}g=e.reverse().join("").split("{");i=g.length;h=RegExp("(^|[\\s,>+~])("+o().join("|")+")(?=[[\\s,>+~#.:]|$)","gi");for(k=
"$1"+l+"\\:$2";i--;)e=g[i]=g[i].split("}"),e[e.length-1]=e[e.length-1].replace(h,k),g[i]=e.join("}");e=g.join("{");i=a.getElementsByTagName("*");h=i.length;k=RegExp("^(?:"+o().join("|")+")$","i");for(d=[];h--;)g=i[h],k.test(g.nodeName)&&d.push(g.applyElement(B(g)));f=d;c=s(a,e)});j.attachEvent("onafterprint",function(){for(var a=f,c=a.length;c--;)a[c].removeNode();clearTimeout(d._removeSheetTimer);d._removeSheetTimer=setTimeout(b,500)});a.printShived=!0;return a}var r=j.html5||{},z=/^<|^(?:button|map|select|textarea|object|iframe|option|optgroup)$/i,
y=/^<|^(?:a|b|button|code|div|fieldset|form|h1|h2|h3|h4|h5|h6|i|iframe|img|input|label|li|link|ol|option|p|param|q|script|select|span|strong|style|table|tbody|td|textarea|tfoot|th|thead|tr|ul)$/i,q,u="_html5shiv",p=0,t={},e;(function(){try{var a=f.createElement("a");a.innerHTML="<xyz></xyz>";q="hidden"in a;var b;if(!(b=1==a.childNodes.length)){f.createElement("a");var c=f.createDocumentFragment();b="undefined"==typeof c.cloneNode||"undefined"==typeof c.createDocumentFragment||"undefined"==typeof c.createElement}e=
b}catch(d){e=q=!0}})();var d={elements:r.elements||"abbr article aside audio bdi canvas data datalist details figcaption figure footer header hgroup mark meter nav output progress section summary time video",shivCSS:!1!==r.shivCSS,supportsUnknownElements:e,shivMethods:!1!==r.shivMethods,type:"default",shivDocument:w,createElement:v,createDocumentFragment:function(a,b){a||(a=f);if(e)return a.createDocumentFragment();for(var b=b||n(a),c=b.frag.cloneNode(),d=0,j=o(),l=j.length;d<l;d++)c.createElement(j[d]);
return c}};j.html5=d;w(f);var D=/^$|\b(?:all|print)\b/,l="html5shiv",C=!e&&function(){var a=f.documentElement;return!("undefined"==typeof f.namespaces||"undefined"==typeof f.parentWindow||"undefined"==typeof a.applyElement||"undefined"==typeof a.removeNode||"undefined"==typeof j.attachEvent)}();d.type+=" print";d.shivPrint=x;x(f)})(this,document);

/*
    json2.js
    2011-10-19

    Public Domain.

    NO WARRANTY EXPRESSED OR IMPLIED. USE AT YOUR OWN RISK.

    See http://www.JSON.org/js.html


    This code should be minified before deployment.
    See http://javascript.crockford.com/jsmin.html

    USE YOUR OWN COPY. IT IS EXTREMELY UNWISE TO LOAD CODE FROM SERVERS YOU DO
    NOT CONTROL.


    This file creates a global JSON object containing two methods: stringify
    and parse.

        JSON.stringify(value, replacer, space)
            value       any JavaScript value, usually an object or array.

            replacer    an optional parameter that determines how object
                        values are stringified for objects. It can be a
                        function or an array of strings.

            space       an optional parameter that specifies the indentation
                        of nested structures. If it is omitted, the text will
                        be packed without extra whitespace. If it is a number,
                        it will specify the number of spaces to indent at each
                        level. If it is a string (such as '\t' or '&nbsp;'),
                        it contains the characters used to indent at each level.

            This method produces a JSON text from a JavaScript value.

            When an object value is found, if the object contains a toJSON
            method, its toJSON method will be called and the result will be
            stringified. A toJSON method does not serialize: it returns the
            value represented by the name/value pair that should be serialized,
            or undefined if nothing should be serialized. The toJSON method
            will be passed the key associated with the value, and this will be
            bound to the value

            For example, this would serialize Dates as ISO strings.

                Date.prototype.toJSON = function (key) {
                    function f(n) {
                        // Format integers to have at least two digits.
                        return n < 10 ? '0' + n : n;
                    }

                    return this.getUTCFullYear()   + '-' +
                         f(this.getUTCMonth() + 1) + '-' +
                         f(this.getUTCDate())      + 'T' +
                         f(this.getUTCHours())     + ':' +
                         f(this.getUTCMinutes())   + ':' +
                         f(this.getUTCSeconds())   + 'Z';
                };

            You can provide an optional replacer method. It will be passed the
            key and value of each member, with this bound to the containing
            object. The value that is returned from your method will be
            serialized. If your method returns undefined, then the member will
            be excluded from the serialization.

            If the replacer parameter is an array of strings, then it will be
            used to select the members to be serialized. It filters the results
            such that only members with keys listed in the replacer array are
            stringified.

            Values that do not have JSON representations, such as undefined or
            functions, will not be serialized. Such values in objects will be
            dropped; in arrays they will be replaced with null. You can use
            a replacer function to replace those with JSON values.
            JSON.stringify(undefined) returns undefined.

            The optional space parameter produces a stringification of the
            value that is filled with line breaks and indentation to make it
            easier to read.

            If the space parameter is a non-empty string, then that string will
            be used for indentation. If the space parameter is a number, then
            the indentation will be that many spaces.

            Example:

            text = JSON.stringify(['e', {pluribus: 'unum'}]);
            // text is '["e",{"pluribus":"unum"}]'


            text = JSON.stringify(['e', {pluribus: 'unum'}], null, '\t');
            // text is '[\n\t"e",\n\t{\n\t\t"pluribus": "unum"\n\t}\n]'

            text = JSON.stringify([new Date()], function (key, value) {
                return this[key] instanceof Date ?
                    'Date(' + this[key] + ')' : value;
            });
            // text is '["Date(---current time---)"]'


        JSON.parse(text, reviver)
            This method parses a JSON text to produce an object or array.
            It can throw a SyntaxError exception.

            The optional reviver parameter is a function that can filter and
            transform the results. It receives each of the keys and values,
            and its return value is used instead of the original value.
            If it returns what it received, then the structure is not modified.
            If it returns undefined then the member is deleted.

            Example:

            // Parse the text. Values that look like ISO date strings will
            // be converted to Date objects.

            myData = JSON.parse(text, function (key, value) {
                var a;
                if (typeof value === 'string') {
                    a =
/^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}(?:\.\d*)?)Z$/.exec(value);
                    if (a) {
                        return new Date(Date.UTC(+a[1], +a[2] - 1, +a[3], +a[4],
                            +a[5], +a[6]));
                    }
                }
                return value;
            });

            myData = JSON.parse('["Date(09/09/2001)"]', function (key, value) {
                var d;
                if (typeof value === 'string' &&
                        value.slice(0, 5) === 'Date(' &&
                        value.slice(-1) === ')') {
                    d = new Date(value.slice(5, -1));
                    if (d) {
                        return d;
                    }
                }
                return value;
            });


    This is a reference implementation. You are free to copy, modify, or
    redistribute.
*/

/*jslint evil: true, regexp: true */

/*members "", "\b", "\t", "\n", "\f", "\r", "\"", JSON, "\\", apply,
    call, charCodeAt, getUTCDate, getUTCFullYear, getUTCHours,
    getUTCMinutes, getUTCMonth, getUTCSeconds, hasOwnProperty, join,
    lastIndex, length, parse, prototype, push, replace, slice, stringify,
    test, toJSON, toString, valueOf
*/


// Create a JSON object only if one does not already exist. We create the
// methods in a closure to avoid creating global variables.

var JSON;
if (!JSON) {
    JSON = {};
}

(function () {
    'use strict';

    function f(n) {
        // Format integers to have at least two digits.
        return n < 10 ? '0' + n : n;
    }

    if (typeof Date.prototype.toJSON !== 'function') {

        Date.prototype.toJSON = function (key) {

            return isFinite(this.valueOf())
                ? this.getUTCFullYear()     + '-' +
                    f(this.getUTCMonth() + 1) + '-' +
                    f(this.getUTCDate())      + 'T' +
                    f(this.getUTCHours())     + ':' +
                    f(this.getUTCMinutes())   + ':' +
                    f(this.getUTCSeconds())   + 'Z'
                : null;
        };

        String.prototype.toJSON      =
            Number.prototype.toJSON  =
            Boolean.prototype.toJSON = function (key) {
                return this.valueOf();
            };
    }

    var cx = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
        escapable = /[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
        gap,
        indent,
        meta = {    // table of character substitutions
            '\b': '\\b',
            '\t': '\\t',
            '\n': '\\n',
            '\f': '\\f',
            '\r': '\\r',
            '"' : '\\"',
            '\\': '\\\\'
        },
        rep;


    function quote(string) {

// If the string contains no control characters, no quote characters, and no
// backslash characters, then we can safely slap some quotes around it.
// Otherwise we must also replace the offending characters with safe escape
// sequences.

        escapable.lastIndex = 0;
        return escapable.test(string) ? '"' + string.replace(escapable, function (a) {
            var c = meta[a];
            return typeof c === 'string'
                ? c
                : '\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
        }) + '"' : '"' + string + '"';
    }


    function str(key, holder) {

// Produce a string from holder[key].

        var i,          // The loop counter.
            k,          // The member key.
            v,          // The member value.
            length,
            mind = gap,
            partial,
            value = holder[key];

// If the value has a toJSON method, call it to obtain a replacement value.

        if (value && typeof value === 'object' &&
                typeof value.toJSON === 'function') {
            value = value.toJSON(key);
        }

// If we were called with a replacer function, then call the replacer to
// obtain a replacement value.

        if (typeof rep === 'function') {
            value = rep.call(holder, key, value);
        }

// What happens next depends on the value's type.

        switch (typeof value) {
        case 'string':
            return quote(value);

        case 'number':

// JSON numbers must be finite. Encode non-finite numbers as null.

            return isFinite(value) ? String(value) : 'null';

        case 'boolean':
        case 'null':

// If the value is a boolean or null, convert it to a string. Note:
// typeof null does not produce 'null'. The case is included here in
// the remote chance that this gets fixed someday.

            return String(value);

// If the type is 'object', we might be dealing with an object or an array or
// null.

        case 'object':

// Due to a specification blunder in ECMAScript, typeof null is 'object',
// so watch out for that case.

            if (!value) {
                return 'null';
            }

// Make an array to hold the partial results of stringifying this object value.

            gap += indent;
            partial = [];

// Is the value an array?

            if (Object.prototype.toString.apply(value) === '[object Array]') {

// The value is an array. Stringify every element. Use null as a placeholder
// for non-JSON values.

                length = value.length;
                for (i = 0; i < length; i += 1) {
                    partial[i] = str(i, value) || 'null';
                }

// Join all of the elements together, separated with commas, and wrap them in
// brackets.

                v = partial.length === 0
                    ? '[]'
                    : gap
                    ? '[\n' + gap + partial.join(',\n' + gap) + '\n' + mind + ']'
                    : '[' + partial.join(',') + ']';
                gap = mind;
                return v;
            }

// If the replacer is an array, use it to select the members to be stringified.

            if (rep && typeof rep === 'object') {
                length = rep.length;
                for (i = 0; i < length; i += 1) {
                    if (typeof rep[i] === 'string') {
                        k = rep[i];
                        v = str(k, value);
                        if (v) {
                            partial.push(quote(k) + (gap ? ': ' : ':') + v);
                        }
                    }
                }
            } else {

// Otherwise, iterate through all of the keys in the object.

                for (k in value) {
                    if (Object.prototype.hasOwnProperty.call(value, k)) {
                        v = str(k, value);
                        if (v) {
                            partial.push(quote(k) + (gap ? ': ' : ':') + v);
                        }
                    }
                }
            }

// Join all of the member texts together, separated with commas,
// and wrap them in braces.

            v = partial.length === 0
                ? '{}'
                : gap
                ? '{\n' + gap + partial.join(',\n' + gap) + '\n' + mind + '}'
                : '{' + partial.join(',') + '}';
            gap = mind;
            return v;
        }
    }

// If the JSON object does not yet have a stringify method, give it one.

    if (typeof JSON.stringify !== 'function') {
        JSON.stringify = function (value, replacer, space) {

// The stringify method takes a value and an optional replacer, and an optional
// space parameter, and returns a JSON text. The replacer can be a function
// that can replace values, or an array of strings that will select the keys.
// A default replacer method can be provided. Use of the space parameter can
// produce text that is more easily readable.

            var i;
            gap = '';
            indent = '';

// If the space parameter is a number, make an indent string containing that
// many spaces.

            if (typeof space === 'number') {
                for (i = 0; i < space; i += 1) {
                    indent += ' ';
                }

// If the space parameter is a string, it will be used as the indent string.

            } else if (typeof space === 'string') {
                indent = space;
            }

// If there is a replacer, it must be a function or an array.
// Otherwise, throw an error.

            rep = replacer;
            if (replacer && typeof replacer !== 'function' &&
                    (typeof replacer !== 'object' ||
                    typeof replacer.length !== 'number')) {
                throw new Error('JSON.stringify');
            }

// Make a fake root object containing our value under the key of ''.
// Return the result of stringifying the value.

            return str('', {'': value});
        };
    }


// If the JSON object does not yet have a parse method, give it one.

    if (typeof JSON.parse !== 'function') {
        JSON.parse = function (text, reviver) {

// The parse method takes a text and an optional reviver function, and returns
// a JavaScript value if the text is a valid JSON text.

            var j;

            function walk(holder, key) {

// The walk method is used to recursively walk the resulting structure so
// that modifications can be made.

                var k, v, value = holder[key];
                if (value && typeof value === 'object') {
                    for (k in value) {
                        if (Object.prototype.hasOwnProperty.call(value, k)) {
                            v = walk(value, k);
                            if (v !== undefined) {
                                value[k] = v;
                            } else {
                                delete value[k];
                            }
                        }
                    }
                }
                return reviver.call(holder, key, value);
            }


// Parsing happens in four stages. In the first stage, we replace certain
// Unicode characters with escape sequences. JavaScript handles many characters
// incorrectly, either silently deleting them, or treating them as line endings.

            text = String(text);
            cx.lastIndex = 0;
            if (cx.test(text)) {
                text = text.replace(cx, function (a) {
                    return '\\u' +
                        ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
                });
            }

// In the second stage, we run the text against regular expressions that look
// for non-JSON patterns. We are especially concerned with '()' and 'new'
// because they can cause invocation, and '=' because it can cause mutation.
// But just to be safe, we want to reject all unexpected forms.

// We split the second stage into 4 regexp operations in order to work around
// crippling inefficiencies in IE's and Safari's regexp engines. First we
// replace the JSON backslash pairs with '@' (a non-JSON character). Second, we
// replace all simple value tokens with ']' characters. Third, we delete all
// open brackets that follow a colon or comma or that begin the text. Finally,
// we look to see that the remaining characters are only whitespace or ']' or
// ',' or ':' or '{' or '}'. If that is so, then the text is safe for eval.

            if (/^[\],:{}\s]*$/
                    .test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, '@')
                        .replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']')
                        .replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {

// In the third stage we use the eval function to compile the text into a
// JavaScript structure. The '{' operator is subject to a syntactic ambiguity
// in JavaScript: it can begin a block or an object literal. We wrap the text
// in parens to eliminate the ambiguity.

                j = eval('(' + text + ')');

// In the optional fourth stage, we recursively walk the new structure, passing
// each name/value pair to a reviver function for possible transformation.

                return typeof reviver === 'function'
                    ? walk({'': j}, '')
                    : j;
            }

// If the text is not JSON parseable, then a SyntaxError is thrown.

            throw new SyntaxError('JSON.parse');
        };
    }
}());

$(function() {

  
    //Dialogs
    $('.dialog-1').click(function(e){
        $('.dialog-no-results').dialog({position:'center', width:500, height: 260, modal: true, resizable: false});
        e.preventDefault();
    });
    
    $('.dialog-2').click(function(e){
    	$('body').addClass('country-selector');
        var clsUrl= $('.dialog-2').data('clsUrl');
        var windowWidth = window.outerWidth;
        var modalWidth;
        
        if (windowWidth <= 425){
        	//Mobile
            modalWidth = "100%";
        }else if (windowWidth >= 768 && windowWidth <= 1024){
        	//Tablet and short laptop
            modalWidth = "80%";
        }else if (windowWidth >= 1025){
        	//Large laptop
            modalWidth = "50%";
        }
        
        $.ajax({
        	url: clsUrl,
            success: function(data) {
            $( ".countryLanguageSelectorDialog" ).html(data);
            
            handleChangeContinent();
            
            var visibleDesktopContainer = null;
                    
            $('.js-country-container').each(function( index ) {
            	var dataValue = $('.js-continent-select')[index].getAttribute('data-continent-id');
                $(this).attr('data-continent-target',dataValue);
            });

            $('.js-country-container .js-country-language').each(function() {
                         
            	var language = $(this).attr("href").split('/')[1];
            	var countryContainer = $(this).parent().parent().parent().parent();
                         
            	if( !countryContainer.hasClass("hidden")){
            		countryContainer.addClass('hidden');
            	}

                if (countryContainer.find('h3:first').text() === "Latin America") {
                	visibleDesktopContainer = countryContainer; 
                }
                
                if(visibleDesktopContainer !== null){
                	var continentDesktopSelector = $("[data-continent-id=" + visibleDesktopContainer.data("continentTarget")+"]" + " .js-continent-img");
                    
                    var continentDesktopSelectedClass = visibleDesktopContainer.data("continentTarget") + "-selected";

                    continentDesktopSelector.addClass(continentDesktopSelectedClass);                
                    visibleDesktopContainer.removeClass('hidden');        
                
                }
                     
            });
            
            $jq7.colorbox({
            	inline:true,                  
            	width:modalWidth,                
            	href:".countryLanguageSelectorDialog",           
            	position:"center",              
            	closeButton:false,            
            	onComplete: function () {
                             
            		$('#cboxLoadedContent').css('backgroundColor', 'white');
            		$('#colorbox').addClass('cls-modal-height');
            		$('#cboxWrapper').addClass('cls-modal-height');
            		$('#cboxContent').addClass('cls-modal-height');
            		$('#cboxLoadedContent').addClass('cls-modal-height');
            		$('#cboxClose').hide();
            		$('.countryLanguageSelectorDialog').removeClass('hidden');
                    $("#hybriscss1").removeAttr("disabled");
                    $("#hybriscss2").removeAttr("disabled");
                    $jq7.colorbox.resize();   
                    
                    $('.close-modal span').click(function() {
                    	$jq7.colorbox.close();
                    });
                    
                    $('.yes').click(function() {
                    	$('.countryLanguageSelectorDialog').removeClass('hidden');
                        $jq7.colorbox.resize();
                    });

            	},
                    
            	onClosed:function(){          
            		$("#hybriscss1").attr("disabled","true");               
            		$("#hybriscss2").attr("disabled","true");                    
            		$('body').removeClass('country-selector'); 
            	}
            });
            }        
        });
    });


    $('.dialog-4').click(function(e){
        $('.dialog-accessories').dialog({position:'center', width:500, height: 630, modal: true, resizable: false});
        e.preventDefault();
    });

    $('#locations-map area').click(function (e) {
        var offset = $('#locations-map').offset();
        $('.dialog-location-select').dialog({position: [offset.left+30, 'center'], width: 725, height: 550, modal: false, resizable: false});
        e.preventDefault();
    });

    //product title tooltips
    $jq7('.truncated').tooltip();

    //tabs
    //$('.tabs').tabs();

    $(".help-modal-link").click(function() {
        $('#' + $(this).attr('id') + '-dialog').dialog({position: "center",
            width: 800,
            height: 500,
            modal: true,
            resizable: false,
            autoOpen: true,
            draggable: false,
            zIndex: 9999});
    });

});


function getAccountData(accountUrl,custLbl){
  
         var selectedAcctURL = $(".selectedAcctURL").filter(":first").text();
         var accountId=""; 
          $.getJSON( selectedAcctURL , function(json) {  
        	  
        	     var continent = 'na';
        	     if (typeof json.hemisphere != 'undefined'){
        	    	 continent = json.hemisphere.code;
        	     }
        	     
        	     var customerTypeCode = 'na';
            	 if (typeof json.customer != 'undefined'){
            		 customerTypeCode = json.customer.customerType;
            	 }	 
        	  
                 accountId = json.uid;
                 switchAccount.elements.accountNumber= accountId,
                 switchAccount.elements.accountName=json.name;
                 switchAccount.elements.customerNumber=json.customerNumber;
                 switchAccount.elements.continentCode=continent;
                 switchAccount.elements.customerTypeCode=customerTypeCode;
                 switchAccount.elements.accountPrimaryKey= json.pk;
                //console.log('Account id : ', accountId);
            })
            .done(function() {
	         $.ajax({
	                dataType: "json",
	                url: accountUrl + "?t=" + Math.floor(Math.random() * 101) * (new Date().getTime()),
	                success: function (data){
	                       
	                        var s=$("#accountSelectForm select[name='company']");
	                        s.hide();
	                        var numAccts=0;
	                        $.each(data, function(index, element) {
	                            if(typeof element.customerNumber!=="undefined"){
	                                var accountText=element.name + " ("+ custLbl + " \u00A0" + element.customerNumber + ")";
	                                var o=$("<option />", {value: element.uid, text: accountText});
	                                o.appendTo(s);
	                                numAccts++;
	                            }
	                        });
	                        //switchAccount.getSelectedAcct();
	                        if(numAccts>0){
	                        	s.val(accountId);
	                        	s.show();
	                        }else{
	                        	s.hide();
	                        }
	                  }
	            })//end ajax to account service
	            .always(function() {
                         new InlineHeader();
                })
	             .fail(function(xhr, textStatus, errorThrown) {
	                     var s=$("#accountSelectForm select[name='company']");
	                     s.hide();
	            });//fail for account service
            }) //end done for selected account
             .fail(function(xhr, textStatus, errorThrown) {
                     var s=$("#accountSelectForm select[name='company']");
                     s.hide();
            });//fail for selected account
        
}

'use strict';

var switchAccount = {
  elements: {
    $accountSelectForm: $('#accountSelectForm'),
    count: "0",
    price: "0",
    name: "",
    userId: "",
    softLogin: "",
    customerTypeCode: "na",
    customerType: "na",
    loginStatus: "na",
    continentCode: "na",
    customerNumber: "na",
    accountNumber: "na",
    accountName: "na",
    userPrimaryKey:"na",
    userType: "na",
    engagementStatus: "na",
    overallStatus: "unrecognized"
  },
  init: function() {
    this.handleAccountChangeForm();

  },
  setAdobeAnalyticsUser: function(){
	 var accountId = accountName = accountNumber = customerType = customerTypeCode = continent = customerNumber = accountPk = userPrimaryKey = 'na';
 	 	 
	 if (switchAccount.elements.loginStatus != 'logged-out'){
 		 accountId =  switchAccount.elements.accountNumber;
 		 accountName = switchAccount.elements.accountName;
 		 customerType = switchAccount.elements.customerType;
 		 customerTypeCode = switchAccount.elements.customerTypeCode;
 		 customerNumber = switchAccount.elements.customerNumber;
 		 accountNumber = switchAccount.elements.accountNumber;
 		 userPrimaryKey=switchAccount.elements.userPrimaryKey;
 		 accountPk = switchAccount.elements.accountPrimaryKey;
 		continent = switchAccount.elements.continentCode;
 	 }
     digitalData.user = {
  	       customerTypeCode: customerTypeCode,
  	       customerType:  customerType,
  	       loginStatus:  switchAccount.elements.loginStatus,
  	       continentCode:  continent,
  	       customerNumber:  customerNumber,
  	       hybris:
  	       {
  	              accountNumber: accountNumber,
  	              accountName: accountName,
  	              userPrimaryKey: userPrimaryKey,
  	              accountPrimaryKey: accountPk
  	       },
  	       userType:  switchAccount.elements.userType,
  	       engagementStatus:  switchAccount.elements.engagementStatus,
  	       overallStatus:  switchAccount.elements.overallStatus
  	};
     _satellite.track("page_load");
  },
  
  handleAccountChangeForm: function() {
    this.elements.$accountSelectForm.submit(function(e) {
      e.preventDefault();
      return false;
    });
  },
  switchAccountNoConfirm: function( isMobile) {
	  if(isMobile){
		  accountId = $('.accountSelectFormMobile option:selected').val();
	  }else{
      	accountId = $('#accountSelectForm option:selected').val();
  	  }
      var switchAcctURL = $(".switchAcctURL").text()+ accountId;
    
    $.get(switchAcctURL, function( data ) {
       //console.log( 'Load was performed.' + switchAcctURL );
      //window.location.reload();
    })
      .done(function() {
          ACC.minicart.$layer.data("needRefresh", true);    
          var minicartURL=$(".updatePricingURL").text();
                   $.getJSON( minicartURL , function(json) {
                      
                       switchAccount.elements.price = json.miniCartPrice;
                       switchAccount.elements.count = json.miniCartCount;
                       switchAccount.elements.name  = json.userName;
                       switchAccount.elements.userId = json.userId;
                       switchAccount.elements.loginStatus=json.loginStatus;
                       switchAccount.elements.userType=json.userType;
                       switchAccount.elements.softLogin = json.softLoggedInAccountType;
                       switchAccount.elements.miniCartPriceZero = json.miniCartPriceZero;
                       switchAccount.elements.punchout = json.isPunchOutSession;
                       switchAccount.elements.b2b = json.isB2bOnly;
                       switchAccount.elements.accountOnly = json.isAccountOnly;
                       switchAccount.elements.userPrimaryKey=json.userId;
                  })
                .done(function() {
                   var price=switchAccount.elements.price;
                   var count = switchAccount.elements.count;
                   var name = switchAccount.elements.name;
                   var userId= switchAccount.elements.userId;
                   var softLogin= switchAccount.elements.softLogin;
                   var miniCartPriceZero = switchAccount.elements.miniCartPriceZero;
                   var punchoutSession = switchAccount.elements.punchout;
                   var punchoutSession = "na";
                   punchoutSession = switchAccount.elements.punchout;
                   var b2bSession = switchAccount.elements.b2b;
                   var accountOnlySession = switchAccount.elements.accountOnly;
                   
                   if (punchoutSession == 'false') {
   						$('.punchout').removeClass("punchoutHidden");
   	               } else {
	   					$('.punchout').addClass("punchoutHidden");
	   					$(".punchout").removeClass("visible-xs-inline-block");
   	               }
                    
                    if (b2bSession == 'false') {
    					$('.b2b').removeClass("b2bHidden");
    				} else {
    					$('.b2b').addClass("b2bHidden");
    					$(".b2b").removeClass("visible-xs-inline-block");
    				}
                    
                    if (accountOnlySession == 'true') {
    					$('.account-only').removeClass("accountOnlyHidden");
    				} else {
    					$('.account-only').addClass("accountOnlyHidden");
    					$(".account-only").removeClass("visible-xs-inline-block");
    				}
                   
                   //if(miniCartPriceZero == 'true') {
                	   $(".miniCart .price").html('');
                       $(".miniCart .price").addClass("hidden");
                  /* }else{
                       $(".miniCart .price").html(price);
                       $(".miniCart .price").removeClass("hidden");
                   }*/
                   
                    $(".miniCart .count").html("(" + count + ")");
                    if(parseInt(count)>0){
                        $('.badge-mobile').html(count);
                        $('.badge-mobile').css('display', 'inline-block');
                    }
                    
                    //set Name
                    if((name !="Anonymous") && (name != "")) {
                    	
                        var welcomeText = $('#js_user-name').text().replace('{userid}', name);
                        $('#js_user-name').html(welcomeText);
                        if($('#js_user-name-top').html()!=null){
                        	var welcomeHeaderTopText = $('#js_user-name-top').html().replace('{userid}', name);             
                        	$('#js_user-name-top').html(welcomeHeaderTopText);
                        }
                        if($('#js_user-name_footer').html()!=null){
                        	var welcomeFooterText = $('#js_user-name_footer').html().replace('{userid}', name);
                        	$('#js_user-name_footer').html(welcomeFooterText);
                        }
                        $('.authenticated').removeClass('hidden');
                        $('.unauthenticated').remove();
                        } else {
                          
                        	
                      	  $('#js_user-name').html(''); 
                      	  $('#js_user-name-top').html('');
                      	  $('#js_user-name_footer').html('');
                      	  $('.unauthenticated').removeClass('hidden');
                            $('.authenticated').remove();  
                    }
                    
                    //check login
                    var loggedIn=((switchAccount.elements.loginStatus!=null)&&(switchAccount.elements.loginStatus.indexOf("in")>-1));
                  
                    if(loggedIn){
                    	
                    	switchAccount.elements.overallStatus="customer";	
                        dataLayer.push({
                            'data-eventvar' : 'login',
                            'data-actionvar' : 'site-entry',
                            'data-labelvar' : userId,
                            'data-categoryvar' : 'login',
                            'data-loginvar' : 'Logged In',
                            'data-customerType': softLogin,
                            'event' : 'login-click'
                        }); 

                    }else{
                    	switchAccount.elements.overallStatus="unrecognized";	
                    }
                    
                    switchAccount.setAdobeAnalyticsUser();
                  
                });
        })
        .done(function() {
        	var selectedAcctURL = $(".selectedAcctURL").filter(":first").text();
    
        	$.getJSON( selectedAcctURL , function(json) {
        	//console.log( "success" );
            //console.log(selectedAcctURL);
            var accountId = json.uid;
            switchAccount.elements.accountNumber= accountId,
            switchAccount.elements.accountName=json.name;
            switchAccount.elements.customerNumber=json.customerNumber;
            switchAccount.elements.continentCode=json.hemisphere.code;
            switchAccount.setAdobeAnalyticsUser();
            //console.log('Account id : ', accountId);
            $('select[name=company]').val(accountId);
            $('.currentSelectedAcct').text(accountId);
            $("select[name=company] option[value='" + accountId +"']").prop("selected","selected").siblings().removeAttr('selected'); 
           
        	})
        	.done(function() {
            	 setTimeout(function(){hybrisProductMenu.repopulateProductMenu();
            	 }, 2000);
            })
            .done(function() {
                setTimeout(function(){
                    ACC.minicart.$layer.data("needRefresh", true);  
                }, 2000);
            });
        });
      
},
  getSelectedAcct: function() {
	  var selectedAcctURL = $(".selectedAcctURL").filter(":first").text();
      
	  $.getJSON( selectedAcctURL , function(json) {
			//console.log( "success" );
			//console.log(selectedAcctURL);
			 var accountId = json.uid;
			 
       	     var continent = 'na';
    	     if (typeof json.hemisphere != 'undefined'){
    	    	 continent = json.hemisphere.code;
    	     }
    	     
    	     var customerTypeCode = 'na';
        	 if (typeof json.customer != 'undefined'){
        		 customerTypeCode = json.customer.customerType;
        	 }	 
			 
			 switchAccount.elements.accountNumber= accountId,
             switchAccount.elements.accountName=json.name;
             switchAccount.elements.customerNumber=json.customerNumber;
             switchAccount.elements.continentCode=continent;
             switchAccount.elements.customerTypeCode=customerTypeCode;
             
             switchAccount.setAdobeAnalyticsUser();
		    //console.log('Account id : ', accountId);
			$('select[name=company]').val(accountId);
			$('.currentSelectedAcct').text(accountId);
			$("select[name=company] option[value='" + accountId +"']").prop("selected","selected").siblings().removeAttr('selected');
		 
		})
		  .done(function() {
		    //console.log( "Load was performed." );

		  })
		  .fail(function(xhr, textStatus, errorThrown) {
		     //console.log( "was unable to retreive selected account." );
		     switchAccount.ajaxGetFailure(xhr, textStatus, errorThrown);
		})
		  .always(function() {
		    //console.log( "selectaccount done" );
		  });
  },
  ajaxGetFailure:function(xhr, textStatus, errorThrown) {
      var responseText;
      try {
          responseText = $jq7.parseJSON(xhr.responseText);
          
      } catch (e) {
          responseText = xhr.responseText;
          //console.log(responseText);
      }
  },
  confirmAccountSwitch: function() {
    var accountId = $('#accountSelectForm option:selected').val();

    var cartItemsCount = $('.miniCart .count').text();
    
    if (cartItemsCount != "0" && cartItemsCount!="(0)") {
      $jq7.colorbox({
          width: '300px',
          overlayClose: true,
          html:function(){ 
        	  dlgHtml=$(".switchAcctDialog").html();
        	  return dlgHtml;
          },
          onOpen: function () {
          },
          onComplete: function () {
              $('#cboxClose').hide();
              // ajax to switch accounts
              $('.modal-content #confirmSwitchButton').click(function() {
            	//Yes button  Account Switch Dialog
            	  accountId = $('#accountSelectForm option:selected').val();
            	  var switchAcctURL = $(".switchAcctURL").text()+ accountId;
                
                $.get(switchAcctURL , function( data ) {
                	  window.location.reload();

                	  })
                	  .done(function() {
                	      //console.log( "account switched" );

                	   })
                	   .fail(function(xhr, textStatus, errorThrown) {
                		   switchAccount.ajaxGetFailure(xhr, textStatus, errorThrown);
                	                 
                	  })
                	  .always(function() {
                	      $jq7.colorbox.close();
                	  });
              });
              $('.modal-content #cancelSwitchButton').click(function(event) {
            	  //Cancel Account Switch Dialog
            	  event.preventDefault();
            	  accountId=$('.currentSelectedAcct').text();
            	  $('select[name=company]').val(accountId);
            	  $("select[name=company] option[value='" + accountId +"']").prop("selected","selected").siblings().removeAttr('selected');
         		 
            	  $jq7.colorbox.close();
              });
          },
          onClosed: function () {
            $('#cboxClose').show();
          }
      });
    } else {
    	accountId = $('#accountSelectForm option:selected').val();
  	  	var switchAcctURL = $(".switchAcctURL").text()+ accountId;
      
      $.get(switchAcctURL, function( data ) {
        // console.log( 'Load was performed.' );
        window.location.reload();
      })
      .always(function() {
	      $jq7.colorbox.close();
	  });
    }

  }
};

$(function() {
    switchAccount.init();
});
function populatingMobileSelects(language, visibleContainer) {
        var numContainers =  $('.js-country-container').length ;
        $('.js-country-container').each(function (containerIndex) {
            var isContainerSelected = false;

            if(containerIndex > numContainers-1){
                return false;
            }else{
                if($(this).data("continentTarget") === visibleContainer.data("continentTarget")){
                    isContainerSelected = true;
                }
                var continentName = $($(this).children().get(0)).text();
                $(this).children().each(function (countryIndex) {
                    if(countryIndex < 1){
                        var continentMobileOption;
                        if(isContainerSelected){
                            continentMobileOption = "<option class='js-continent-option visible-xs' value='"+ continentName + "' selected='selected'>" + continentName+ "</option>";
                        }else{
                            continentMobileOption = "<option class='js-continent-option visible-xs' value='"+ continentName + "'>" + continentName + "</option>";
                        }
                        $('.js-mobile-select-continent').append(continentMobileOption);
                    }else{
                        $(this).children().each(function(){
                            $(this).children().each(function(){
                                $(this).children().each(function(){
                                    if($(this).hasClass("js-country-language")){
                                        var languageMobileOption;
                                        if(isContainerSelected){
                                            if($(this).attr("href").includes(language)){
                                                languageMobileOption = "<option class='js-language-option visible-xs' value='" + $(this).attr("href") + "' data-visible-for='"+continentName+"' selected='selected'>" + $(this).data("countryName") + " " + "(" + $.trim($(this).text()) + ")" + "</option>";
                                            }else{
                                                languageMobileOption = "<option class='js-language-option visible-xs' value='" + $(this).attr("href") + "' data-visible-for='"+continentName+"'>" + $(this).data("countryName")+" "+ "(" + $.trim($(this).text()) + ")" + "</option>";
                                            }

                                        }else{
                                            languageMobileOption = "<option class='js-language-option hidden-xs' value='" + $(this).attr("href") + "' data-visible-for='"+continentName+"'>" + $(this).data("countryName")+ " " + "(" + $.trim($(this).text()) + ")" + "</option>";
                                        }
                                        var optionSelectorByAttr = $("option" + "[value='" +$(this).attr("href") + "']");
                                        if($(this).attr("href") !== "/es_la" && optionSelectorByAttr.length === 0) {
                                              $('.js-mobile-select-language').append(languageMobileOption);
                                        }else{
                                            if($(this).attr("href") === "/es_la"){
                                                var isLatinCountryDuplicated = false;
                                                optionSelectorByAttr.each(function () {
                                                    if($(this).text() === $(languageMobileOption).text()){
                                                        isLatinCountryDuplicated = true;
                                                        return false;
                                                    }
                                                });
                                                if(!isLatinCountryDuplicated){
                                                    $('.js-mobile-select-language').append(languageMobileOption);
                                                }
                                            }
                                        }
                                    }
                                });
                            });
                        });
                    }
                });
            }
        });
    }

    function handleChangeContinentMobileSelect()    {
        $('body').on('change', '.js-mobile-select-continent', $.proxy(function (e) {
            var selectedValue = e.currentTarget.options[e.currentTarget.selectedIndex].value;
            var firstLanguageFound = false;

            $(".js-countries-containers-wrapper select option").each(function () {
                if($(this).hasClass("visible-xs")){
                    $(this).removeClass("visible-xs");
                    $(this).addClass("hidden-xs");
                    if($(this).is(":selected")){
                        $(this).removeAttr("selected");
                    }
                }
                if($(this).data("visibleFor") === selectedValue && $(this).hasClass("hidden-xs")){
                    $(this).removeClass("hidden-xs");
                    $(this).addClass("visible-xs");
                    if(!firstLanguageFound){
                        var optionValue = $(this).val();
                        $(this).attr("selected","selected");
                        $(".js-countries-containers-wrapper > select").each(function (){
                            $(this).attr("selected","selected");
                            $(this).val(optionValue);
                        });
                        firstLanguageFound = true;
                    }
                }
            });
        }, this));
    }

    function handleContinueButtonMobileSelect(){
        $('body').on('click', '.js-cl-continue-button', $.proxy(function () {
            window.location.replace($($(".js-countries-containers-wrapper > select").get(1)).val());
        }, this));
    }
    
    $('.mobiledialog-2').click(function(e){
          $('body').addClass('country-selector');
          var clsUrl= $('.mobiledialog-2').data('clsUrl');
          var windowWidth = window.outerWidth;
          var modalWidth;
          if (windowWidth <= 425){
              //Mobile
              modalWidth = "100%";
          }else if (windowWidth >= 768 && windowWidth <= 1024){
              //Tablet and short laptop
              modalWidth = "80%";
          }else if (windowWidth >= 1025){
              //Large laptop
              modalWidth = "50%";
          }
        $.ajax({
            url: clsUrl,
            success: function(data) {
            $( ".countryLanguageSelectorDialog" ).html(data); 
            //var currentCountry = document.location.pathname.split('/')[1] !== "" ? document.location.pathname.split('/')[1] : "en_us";
            var currentCountry = $( ".countryLanguageSelectorDialog" ).data('country');
            var visibleMobileContainer = null;

            if(currentCountry.includes(".")){
                currentCountry = currentCountry.split(".")[0];
            }

            $('.js-country-container').each(function( index ) {
                var dataValue =  $('.js-continent-select')[index].getAttribute('data-continent-id');
                $(this).attr('data-continent-target',dataValue);
            });

            $('.js-country-container .js-country-language').each(function() {
                var language = $(this).attr("href").split('/')[1];
                var countryContainer = $(this).parent().parent().parent().parent();

                if( !countryContainer.hasClass("hidden")){
                    countryContainer.addClass('hidden');
                }

                if (language === currentCountry) {
                    
                        visibleMobileContainer = countryContainer;
                    
                }
            });
            populatingMobileSelects(currentCountry, visibleMobileContainer);
            handleChangeContinentMobileSelect();
            handleContinueButtonMobileSelect();

            

            if(visibleMobileContainer !== null){
                visibleMobileContainer.removeClass('hidden');
            }

           var modalContent = $('.countryLanguageSelectorDialog').wrap('<p/>').parent().html();

            $jq7.colorbox({
            width:modalWidth,
            html:
                '<div>' +
                    modalContent +
                '</div>',
             onComplete: function () {
               $('#cboxLoadedContent').css('backgroundColor', 'white');
                   $('#colorbox').addClass('cls-modal-height');
                   $('#cboxWrapper').addClass('cls-modal-height');
                   $('#cboxContent').addClass('cls-modal-height');
                   $('#cboxLoadedContent').addClass('cls-modal-height');
                   $('#cboxClose').hide();
                   $('.countryLanguageSelectorDialog').removeClass('hidden');

                   $jq7.colorbox.resize();

                   $('.close-modal span').click(function() {
                       $jq7.colorbox.close();
                   });
                   $('.yes').click(function() {
                       $('.countryLanguageSelectorDialog').removeClass('hidden');
                       $jq7.colorbox.resize();
                   });

             },
            onClosed:function(){
                 $('body').removeClass('country-selector');
                }           
          });
        }
        });
    });

    function handleChangeContinent() {
        $('body').on('click', '.js-continent-select', $.proxy(function (e) {
            var countryContainerSelected = e.currentTarget.getAttribute('data-continent-id');
            var countryContainerSelectedClass = countryContainerSelected + "-selected";

            $(".js-tabs-list li div").each(function () {
                $(this).removeClass($(this).parent().attr("data-continent-id") + "-selected");
            });

            e.currentTarget.children[0].setAttribute("class",countryContainerSelectedClass);

            $(".js-countries-containers-wrapper > div").each(function () {
                var countryContainer = $(this);
                if (countryContainer.data("continentTarget") !== countryContainerSelected && !countryContainer.hasClass("hidden")) {
                    countryContainer.addClass('hidden');
                } else {
                    if(countryContainer.data("continentTarget") === countryContainerSelected){
                        countryContainer.removeClass('hidden');
                    }
                }
            });
        }, this));
    };
    
$jq7( "document" ).ready( function($) {
    var newsSpinner = $( "section.news-spinner .news-items-container" );

    var prevControl = $( "section.news-spinner .back-control" ).first();
    var nextControl = $( "section.news-spinner .forward-control" ).first();
    var interval = $("section.news-spinner").data("interval");
    var direction = $("section.news-spinner").data("direction");

    newsSpinner.marquee( {
        nextControl : nextControl,
        prevControl : prevControl,
        interval : interval,
        direction : direction
    });

});
function createCookie(name,value,days) {
  if (days) {
    var date = new Date();
    date.setTime(date.getTime()+(days*24*60*60*1000));
    var expires = "; expires="+date.toGMTString();
  }
  else var expires = "";
  document.cookie = name+"="+value+expires+"; path=/";
}
function setDefaultLanguage(lang){
	createCookie("googtrans",lang);
	
}
var varIsHome=(isHomepage==null)||(isHomepage==true)?true:false;
var isSplashEnabled=(splashEnabled==null)||(splashEnabled==true)?true:false;
var isAuthor=false;
var isBot=false;

function isPublish(){
	var ispublish=$('.isPublish').attr('ispublish');
	return (ispublish==="true");
}

function isMobilePreview(){
	return (typeof CQ.WCM.isEditMode==='undefined');
}


if(!isPublish() && !isMobilePreview()) {
	  if (CQ.WCM.isEditMode(true) || CQ.WCM.isDesignMode(true)){
		  isAuthor=true;
	  }
}

if ((/bot|googlebot|crawler|spider|robot|crawling/i).test(navigator.userAgent)) {
	isBot=true;
}

if (isSplashEnabled && varIsHome && !isBot) {
	if(!isAuthor){
	$.ajax({
		async : false,
		url : "/bin/getSplashPage",
		cache : false,
		dataType : "json",
		success : function(data) {
			url = data.url;
			// if url not equal null or spaces then redirect
			if ((url != null) && ($.trim(url) != "")) {
				if (url.indexOf(".html") < 0) {
					var lastLetter = url.slice(-1);
					if (lastLetter == "/") {
						url = url.substring(0, url.lastIndexOf("/"));
					}
					url += ".html";
				}
				window.location.href = url;
			}
		},
		error : function(request, status, error) {
		}
	});
	}
}

$CQ(document).ready(function() {

	loadHeader();
	
	
    $jq7(document).ready(function ($) { 
        $("img.lazy").lazyload({
        	skip_invisible : false,
            threshold : 200
        }); 
        $('body,html').scroll();
        
        var template= $('.headerHtml').attr('template');
        
        if(template==="common"){
        	bindLink();
        	pageAnalytics();
        }
    });
    
  

});

function bindLink(){
	$CQ("a").bind("click", function(){
        var url = this.href.toUpperCase();
        if (url.indexOf(".PDF") > -1 || url.indexOf(".DOC") > -1 || url.indexOf(".XLS") > -1 || url.indexOf(".ZIP") > -1 || url.indexOf(".PPT") > -1) {
            var urlparts = url.split('/');
            var file_name = urlparts[urlparts.length - 1];
            var folder_name = urlparts[urlparts.length - 2];
            if (folder_name == '$FILE') {
                folder_name = 'Lotus Notes Document Library';
            }  else {
                folder_name = unescape(folder_name);
            }
            dataLayer.push({'data-eventvar': folder_name, 'data-actionvar' : 'download', 'data-labelvar' : file_name, 'data-categoryvar': 'document-download' });
            
            digitalData.linkName = file_name;
            digitalData.linkType = "download link";
            digitalData.linkCategory = 'download';
            _satellite.track("click tracking");
            
            return true;
        } else {
            return true;
        }
    });
    
	
}

function loadHeader() {
	
	var navUrl= $('.headerHtml').attr('headerservice');
	var hybrisEndpoint=$('.headerHtml').attr('hybrisendpoint');
	var siteBrandCssClass=$('.headerHtml').attr('siteBrandCssClass');
	if(siteBrandCssClass !=null && siteBrandCssClass!=""){
		$jq7( "body" ).first().addClass(siteBrandCssClass);	
	} 
	if (navUrl !="" && navUrl !=null) {
		
		$.ajax({
			url: navUrl,
			success: function(data) {
					
				var available = false;
				
				if (data.indexOf("error404") == -1) {
					available = true;
				}
				
				  if (available) {
					  
					var windowWidth = window.outerWidth;
					var parser = new DOMParser();
		            if (!$( ".globalheader" ).is(':hidden') && (windowWidth > 767.99)) {
		            	var updatedData = data.replace(/\/(_ui\/|medias\/|en_us\/)/g, hybrisEndpoint + '$1');
		            	var updatedHtml= parser.parseFromString(updatedData, "text/html");
		            	
		            	if(isAuthor){
			            	$jq7(updatedHtml).find("script:contains('var ACC = ACC')").appendTo(".globalheader");
			            	$jq7(updatedHtml).find("body").appendTo(".globalheader");
		            	}else{
		            		
		            		$jq7( ".globalheader" ).html(data);
		            	}
		            	
		            	if (window.$jq7 && typeof globalHeader !== "undefined") {
		            		globalHeader.init();
		                }  
		            }
		                    
		        	
					var headerHtml = parser.parseFromString(data, "text/html");
					
					var punchoutSession = $(headerHtml).find(".global_header_cq").data("is-punchout");
					var b2bUser = $(headerHtml).find(".global_header_cq").data("is-user-b2b");
					var isAnonymousUser = $(headerHtml).find(".global_header_cq").data("is-anonymous-user");
					var b2bOnlySite = $(headerHtml).find(".global_header_cq").data("is-b2b-only");					
					
					if (isAnonymousUser) {
						$('.unauthenticated').removeClass('hidden');
						$('.authenticated').remove();  						
					} else {
						$('.authenticated').removeClass('hidden');
						$('.unauthenticated').remove();
					}
					
					if (punchoutSession) {
	 					$('.punchout').addClass("punchoutHidden");
	 					$(".punchout").removeClass("visible-xs-inline-block");						
					} else {
	 					$('.punchout').removeClass("punchoutHidden");						
					}
					
					if (b2bOnlySite) {
	  					$('.b2b').addClass("b2bHidden");
	  					$(".b2b").removeClass("visible-xs-inline-block");						
					} else {
	  					$('.b2b').removeClass("b2bHidden");						
					}
	                  
	                  
	                if (b2bUser) {
	                	$('.account-only').removeClass("accountOnlyHidden");
	    			} else {
	    				$('.account-only').addClass("accountOnlyHidden");
	    				$(".account-only").removeClass("visible-xs-inline-block");
	    			}
	                  
				}
			},
            error: function(XMLHttpRequest, textStatus, errorThrown) { 
                //alert("Status: " + textStatus); alert("Error: " + errorThrown); 
            },   
			async:false
		});
	}
	
}	

function pageAnalytics() {
	var banners = setBanners();
	
	var path = window.location.pathname;
	
	var locale = $("#localeAnalytics").data("locale");
	var language = (typeof locale != 'undefined') ? locale.toLowerCase() : 'na';
	if (path.indexOf("404") >= 0){
		var pageName= '404';
	} else{
		var pageName = $("#breadcrumbAnalytics").data("pagename");
		if (typeof pageName == 'undefined') {
			pageName = 'na';
		} else {
			pageName = pageName.replace('home/', '');
		}		
	}
	var categoryList = pageName.split('/');
	var vendor = 'na';
	var pageType = categoryList[0];

	if (pageType == '') {
		pageType = "Home";
	} else if (pageType == 'manufacturers' && categoryList[1] != undefined){
		vendor = categoryList[1];
	}	
	
	var categoryRow = {};
	var categoryGroup = "";
	for (var i=0; i < 6; i++ ) {

		if (typeof categoryList[i] == 'undefined'){
			var catName = 'na';
			var categoryGroup = 'na';	
	    } else {
	    	catName = categoryList[i];
	    	if (categoryGroup == "") {
		    	categoryGroup = categoryList[i];
	    	} else {
		    	categoryGroup = categoryGroup + ':' + categoryList[i];		    		
	    	}
	    }
		
        if (i == 0) {
        	categoryRow["primaryCategory"] = catName;
	    } else {
        	var subCategoryLabel = 'subCategory' + (i);
	    	categoryRow[subCategoryLabel] = catName;
			var categoryGroupLabel = 'categorygroup' + (i);
			categoryRow[categoryGroupLabel] = (categoryGroup.replace(/\s/g,'')).toLowerCase();
	    }
	}
	
	var pageNameFinal = language + '/' + (pageName.replace(/\s/g,'-')).toLowerCase();
	 
	digitalData.page = {
			pageInfo:
			{
				pageName: pageNameFinal,
				pageType: pageType
			 },
			 category: categoryRow, 
			 attributes:
	         {
				  server: window.location.hostname,
	              language: language,
	              vendorName: vendor,
	              banners: banners != "" ? banners: 'na'
	         }
   };
	

	if (path.indexOf('search-results') != -1) {
		var searchTerm = $('#globalSearch input[name="searchTerms"]').val();
		if (typeof searchTerm==='undefined') {
			 searchTerm = $('#globalHybrisSearch input[name="text"]').val();
		} else {
			searchTerm = 'na';
		}
		var searchRedirect = 'na';
		setSearchAnalytics(undefined, searchTerm, searchRedirect);
	}
	
	else if (window.location.search.indexOf('?term=') != -1) {
		var searchSuccess = 'successful search';
		var searchTerm = (window.location.search.replace('?term=','')).replace(/%20/g, " ");
		var searchRedirect = 'redirect';
		setSearchAnalytics(searchSuccess, searchTerm, searchRedirect);
	}
	
	
}




function setBanners() {
	var bannerList = "";
	$(".bannerAnalytics").each( function() {
		var banner = $(this).data("campaign");
		if (banner != "") {
			if (bannerList != "") {
				bannerList = bannerList + ',' + banner;			
			} else {
				bannerList = banner;
			}			
		}
	});
	return bannerList;
}

function setSearchAnalytics(searchSuccess, searchTerm, searchRedirect) {
	count = $('#numResults').text();
	if (count == ''){
		count = 'na';
	}
	
	if (typeof searchSuccess==='undefined') {
		searchSuccess = count > 0 ? "successful search" : "null Search";
	}
	var searchPagination= $('#updateResultsForm .hidden input[name="pageNumber"]').val();
	
	if (typeof searchPagination==='undefined'){
		searchPagination = 'na';
	}
	var searchResultList =  $('#productList').data('product-number-list');
	if (typeof searchResultList==='undefined'){
		searchResultList = 'na';
	}
	
	digitalData.page.event = "search";
	digitalData.search = {
			searchTerm: searchTerm,
			searchResults: count,
			searchEvent: 'new search',
			searchFilter: "na",
			searchSort: "na",
			searchType: "standard",
			searchSuccess: searchSuccess,
			searchTermRecommended: "na",
			searchTermTypeAhead: "na",
			searchPagination: searchPagination,
			searchRedirect: searchRedirect,
			searchLandingPage: ((window.location.pathname.replace('/content/', '')).replace('.html','')),
			searchResultList: searchResultList
	};	
	 _satellite.track("searchInfo_load ");	
} 




/*var hybrisProductMenu = {
    elements: {
        productMenuSpan: $jq7('.hybrisProductMenuHtml'),
    },

    init: function() {
    	var menuPlaceHolder=this.elements.productMenuSpan;
    	if (typeof menuPlaceHolder != 'undefined') {
    		hybrisProductMenu.getProductMenu();
    	} else {
    		getAccountData(accountServiceURL,accountCustLabel);
    	}
    },
    repopulateProductMenu: function() {
        
	    //get hybris product menu
	    var productMenuUrl= this.elements.productMenuSpan.attr('hybris-productmenuservice');
	   
        if (productMenuUrl!="" && productMenuUrl!=null) {
          
        	$jq7.get(productMenuUrl , function(data) {
                var PageNotFound=false;
                
                 if(data.indexOf("section-secondary-nav") == -1) {
                	 PageNotFound = true;
                 } else if (data.indexOf("error404") > -1){
                	 PageNotFound = true;
                 } else if (data.indexOf("404.jpg") > -1){
                	 PageNotFound = true;
                 }
        	             
                 if(PageNotFound){
        			 new InlineHeader();
                 } else {
                     hybrisProductMenu.elements.productMenuSpan.html(data);                	 
                 }
    		 
    		})
   		     .done(function() {
              //if done do something
   		    	 headerNavigation.reInitialize();
                
              })
    		 
    		 .fail(function(xhr, textStatus, errorThrown) {
    			 if (typeof console != 'undefined' && typeof console.log  != 'undefined') {
    				 console.log( "Unable to retrieve product menu nav." );  
    			 }
    			 new InlineHeader();
    		});
    		 
        }

    },
    getProductMenu: function() {
         
	    //get hybris product menu
	    var productMenuUrl= this.elements.productMenuSpan.attr('hybris-productmenuservice');
	    var isHybrisEnabled=this.elements.productMenuSpan.attr('hybris-enabled');
	    var useCacheMenu=this.elements.productMenuSpan.attr('cache-menu');
	    var accountServiceURL=$jq7(".accountServiceURL").text();
	    var accountCustLabel=$jq7(".accountCustLabel").text();

        if (isHybrisEnabled=="true" && useCacheMenu=="false" && productMenuUrl!="" && productMenuUrl!=null) {
          
        	$jq7.get(productMenuUrl , function(data) {
                var PageNotFound=false;
                
                 if(data.indexOf("section-secondary-nav") == -1) {
                	 PageNotFound = true;
                 } else if (data.indexOf("error404") > -1){
                	 PageNotFound = true;
                 } else if (data.indexOf("404.jpg") > -1){
                	 PageNotFound = true;
                 }
        	             
                 if(PageNotFound){
        			 new InlineHeader();
                 } else {
                     hybrisProductMenu.elements.productMenuSpan.html(data);                	 
                 }
    		 
    		})
   		     .done(function() {
              //if done do something
   		    	 headerNavigation.reInitialize();
                 getAccountData(accountServiceURL,accountCustLabel);
              })
    		 
    		 .fail(function(xhr, textStatus, errorThrown) {
    			 if (typeof console != 'undefined' && typeof console.log  != 'undefined') {
    				 console.log( "Unable to retrieve product menu nav." );  
    			 }
    			 new InlineHeader();
    		});
    		 
        }else{
        	  getAccountData(accountServiceURL,accountCustLabel);
        }

    }
};
$jq7(function() {
    hybrisProductMenu.init();
});
*/
(function($) {

    $.widget( "anixter.megaNav", {

        options: {},

        hidePanel: function (e) {

            window.clearTimeout(this.timeoutID);

            this.navButtons.removeClass('on');
            this.panelsContainer.hide();

            this.categoryLists.removeClass('on');
            this.subListsContainer.removeClass('open');
            this.subLists.removeClass('on');

        },

        _create: function () {

            this.ie7mode = (document.all && !window.opera && window.XMLHttpRequest && !document.querySelectorAll) ? true : false;

            //main nav and big drop down panel
            this.navButtons = $('.nav-primary a').not('.nav-shopping-list a');
            this.panelsContainer = $('.nav-drop-panels');
            this.timeoutID;

            this.navButtons.on('mouseenter', {context: this}, this._buttonOver);
            this.navButtons.on('mouseleave', {context: this}, this._buttonOut);
            this.panelsContainer.on('mouseenter', {context: this}, this._panelOver);
            this.panelsContainer.on('mouseleave', {context: this}, function (e) {e.data.context.hidePanel();});

            //sub nav lists
            this.categoryLists = $('.expand-link');

            this.subListsContainer = $('.nav-drop-panels .expand');
            this.subLists = $('.nav-drop-panels .sub-list');

            this.categoryLists.on('mouseenter', {context:this}, this._listItemOver);
        },

        _buttonOver: function (e) {
            window.clearTimeout(e.data.context.timeoutID);

            //show panel container and swap panels
            e.data.context.timeoutID = window.setTimeout(function () {
                e.data.context.panelsContainer.show();
                e.data.context.panelsContainer.css('display', 'block');
                }, 250);
            e.data.context.panelsContainer.find('.panel').hide();
            $( $(this).attr('data-navigation') ).show();

            //add on class to link to retain over with panel
            e.data.context.navButtons.removeClass('on');
            $(this).addClass('on');
        },

        _buttonOut: function (e) {
            window.clearTimeout(e.data.context.timeoutID);
            //delay panel just in case mouse is over panel
            e.data.context.timeoutID = window.setTimeout(function () { e.data.context.hidePanel(); }, 100);
        },

        _panelOver: function (e) {
            window.clearTimeout(e.data.context.timeoutID);
        },

        _listItemOver: function (e) {
            //item rollover
            if (!e.data.context.ie7mode) {
                e.data.context.categoryLists.removeClass('on');
                $(this).addClass('on');
            }

            //reveal column and swap sublists
            e.data.context.subListsContainer.addClass('open');
            e.data.context.subLists.removeClass('on');
            e.data.context.subLists.filter( $(this).attr('data-navigation') ).addClass('on');
        }

    });

}($jq7));
(function($){
    $jq7.fn.extend({
        truncate: function (options) {
            var defaults = {
                text: '',
                lines: 3,
                complete: function(){}
            }

            var options = $jq7.extend(defaults, options);

            var count = this.length;

            return this.each(function () {
                var el = $jq7(this),
                    a = $jq7('a', el);
                	b = $jq7('b', a);
                    txt = a.text();
                    txtB = b.text();
                    origValue = a.html();
                    lineHeight = 0,
                    maxHeight = 0,
                    chunks = null,
                    truncTxt = '';

                //get lineHeight and fontSize, then compute maxHeight
                //check if element is taller than maxHeight, if not, no truncate
                lineHeight = parseInt( el.css('line-height') );
                if (lineHeight < 5) {
                    //correct for ie
                    lineHeight = Math.ceil( parseInt(el.css('font-size')) * 1.4 );
                }

                maxHeight = Math.ceil( lineHeight * options.lines );

                //recursive - break up string, remove last word, reassemble with ellipsis on end,
                //check height, if still too tall, go again...
                var checkHeight = function () {
                    return (el.height() <= maxHeight);
                };

                var truncateString = function () {

                	if (!chunks) {
                        chunks = txt.split(' ');
                    }

                    if (chunks.length > 1) {
                        chunks.pop();
                        truncTxt = chunks.join(' ');
                    } else {
                        chunks = null;
                    }

                    //Put the bold formatting back on
                    if(truncTxt.indexOf(txtB) > -1){
                    	truncTxt = truncTxt.replace(txtB, '<b>' + txtB + '</b>');
                    }else{
                    	truncTxt = '<b>' + truncTxt + '</b>';
                    }
                    
                    a.html(truncTxt);

                    if (!checkHeight()) {
                       truncateString();
                    } else {
                        return false;
                    }

                };


                //dispath complete callback
                count--;
                if (count == 0) {
                    options.complete();
                }

                //initial check that gets things going
                if (checkHeight()) {
                    a.removeClass('truncated');
                    return; //no need to truncate
                } else {
                	a.attr('productName', origValue);
                    a.addClass('truncated');
                    truncateString();
                }
            });
        }
    });
})(jQuery);
(function($) {

    $jq7.widget( "anixter.searchSideBar", {
        options:{},

        openAllGroups: function() {
            this.groups.addClass('open');
        },

        closeAllGroups: function() {
            this.groups.removeClass('open');
        },

        clearAllChecks: function() {
            this.groups.find('input:checked').prop('checked', false);
            this._refresh();
        },

        _create: function() {
            this.groups = this.element.find('.productGroup');
            this.filters = this.element.find('.search-categories .facet, .search-categories .productSetLink');

            //tools
            this.element.find('.open-all').click({context: this}, function(e){ e.data.context.openAllGroups(); e.preventDefault(); });
            this.element.find('.close-all').click({context: this}, function(e){ e.data.context.closeAllGroups(); e.preventDefault(); });
            this.element.find('.clear-all').click({context: this}, function(e){ e.data.context.clearAllChecks(); e.preventDefault(); });

            //header clicks
            this.groups.find('> div > a, .icon-collapse').click({context: this}, this._clickGroup);

            //checkbox filters
            this.filters.click({context: this}, this._clickFilter);

            this._refresh();

        },

        _refresh: function() {
            //check for checkmarks in the groups - show/hide clear buttons when needed.
            this.groups.each(function(i, el){
                var group = $jq7(el);

                if (group.find('input:checked').length > 0) {
                    group.find('.btn-clear').addClass('on');
                } else {
                    group.find('.btn-clear').removeClass('on');
                }

            });
        },

        _clickGroup: function(e) {
            var group = $jq7(this).parents('.productGroup');

            if (group.hasClass('open')) {
                group.removeClass('open');
            } else {
                group.addClass('open');
            }
        },

        _clickFilter: function(e) {
            var target = $jq7(e.target);
            if (target.is('a')) {
                var box = target.siblings('.facet');
                var toggle = (box.prop('checked')) ? false : true;
                box.prop('checked', toggle);

                e.preventDefault();
            }

            e.data.context._refresh();
        }
    });

}(jQuery));
$jq7(document).ready(function(){
    initEventHandlers();
});

function initEventHandlers() {

    //submit general search
    $jq7('form[name="globalSearch"] button').click(function(){
        $jq7(this).parent().submit();
    });
    //submit hybris search from page
    $jq7('form[name="globalHybrisSearch"] button').click(function(){
        document.globalHybrisSearch.submit();
    });
    //submit hybris search from page when enter key pressed in search bar
    $jq7('form[name="globalHybrisSearch"] input').keypress(function (e) {
    	   if(e.which === 13) {
    		   document.globalHybrisSearch.submit();
    	   }
    });
    //submit supplier search form on the featured supplier page
    $jq7('form[name="supplierSearch"] button').click(function(){
        $jq7(this).parent().submit();
    });

    $jq7('form[name$="Search"]').submit(function(){
        //only submit if input has >=1 characters
        //$jq7('div.searchWithin').find('input[placeholder], textarea[placeholder]').each(remove);
        var phrase = $jq7(this).find('input[name="searchTerms"]').val();
        if ($jq7.trim(phrase).length < 1) {
            return false;
        }
        //$jq7('div.searchWithin').find('input[placeholder], textarea[placeholder]').blur(add).focus(remove).each(add);
    });

  //remove non-digit characters from the quantity field (when user types)
    $jq7('#Content').on('keyup', 'input[name="quant"], input.quantity-input', function(event) {
        if (event.which === $jq7.ui.keyCode.ENTER) {
            //simulate clicking the 'Add to cart' button - this is based on the structure of the pages
            $jq7(this).parent().parent().find('a').click();
            return false;
        }
        if (event.which === $jq7.ui.keyCode.LEFT || event.which === $jq7.ui.keyCode.RIGHT
                || event.which === $jq7.ui.keyCode.DELETE || event.which == 91 || event.which === $jq7.ui.keyCode.SHIFT || event.which === $jq7.ui.keyCode.BACKSPACE) {
            //don't do anything
        } else {
            var updated = $jq7(this).val().replace(/[^\d]/g, "");
            $jq7(this).val(updated);
        }
    });


    //remove non-digit characters from the quantity field in the shopping cart add quantity overlay
    $jq7('body').on('keyup', 'div.dialog-no-quant .product-add input', function(event) {
        if (event.which === $jq7.ui.keyCode.ENTER) {
            //simulate clicking the 'Update' link
            $jq7(this).parent().find('a').click();
            return false;
        }
        if (event.which === $jq7.ui.keyCode.LEFT || event.which === $jq7.ui.keyCode.RIGHT
                || event.which === $jq7.ui.keyCode.DELETE || event.which == 91 || event.which === $jq7.ui.keyCode.SHIFT || event.which === $jq7.ui.keyCode.BACKSPACE) {
            //don't do anything
        } else {
            var updated = $jq7(this).val().replace(/[^\d]/g, "");
            $jq7(this).val(updated);
        }
    });

    //get variables needed for the type ahead call
    var span = $jq7('form[name="globalSearch"]:first span.hidden');
    var label = $jq7(span).attr('suppliersLabel');
    var typeAheadPath = $jq7(span).attr('path');
    var minTypeaheadChars=parseInt($jq7(span).attr('minTypeaheadChars'));
    //TypeAhead on the globalSearch form
    $jq7('form[name="globalSearch"] input[name="searchTerms"]').autocomplete({
        source: function(request, response) {
            if ($jq7.trim(request.term).length >= minTypeaheadChars) {
                $jq7.ajax({
                    url: typeAheadPath,
                    dataType: "json",
                    data: {
                        label: label,
                        term: request.term
                    },
                    success: function(data) {
                        response(data);
                    }
                });
            }
        },
        minLength: minTypeaheadChars,
        select: function(event, ui) {
            //if URL is defined, redirect immediately
            if (ui.item) {
                if (ui.item.url.length > 0) {
                    window.location.href = ui.item.url;
                } else {
                    //execute search
                    $jq7(this).val(ui.item.value); // Added to handle also mouseover/mouseclick event
                    $jq7(this).parent().submit();
                }
            }
        },
        open: function() {
            $jq7(this).removeClass("ui-corner-all").addClass("ui-corner-top");
        },
        close: function() {
            $jq7(this).removeClass("ui-corner-top").addClass("ui-corner-all");
        }
    }).data('autocomplete');

    //uncheck all the facet checkboxes - to prevent any checkboxes being checked when loading page from browser cache
    $jq7('#updateResultsForm ul.search-categories input[name="selectedDimensionValues"]:checked').each(function() {
        if (!$jq7(this).hasClass('selected')) {
            $jq7(this).prop('checked', false);
        }
    });

    //truncate product name in grid view
    //if you need to refresh grid - destroy tooltip first: $jq7('.truncated').tooltip('destroy');
    //then recall the entire truncate block below after you refresh results:
    $jq7('.products-grid .product-name p').truncate({
        lines: 5,
        complete: function(){ $jq7('.truncated').tooltip(); }
    });

    // when changing tabs on search results page - display and hide appropriate search facets
    $jq7('#search-results-tabs').bind('tabsselect', function(event, ui) {

        //$jq7('div.searchWithin').find('input[placeholder], textarea[placeholder]').each(remove);

            if (ui.index == 0) {
            	var url = $jq7(ui.tab).attr('data-url');
            	if((document.referrer!="") && (document.referrer.indexOf("searchTerms=")==-1)){
            		url=document.referrer;
            	}
                if( url && $jq7(ui.tab).attr('data-hybris') == 'true') {
                    location.href = url;
                    return false;
                }else{
                	
                	$jq7('#facets-content').hide();
                	$jq7('#facets-products').show();
                	toggleFeaturedProducts();
                }
            } else if (ui.index == 1) {
                if ($jq7.trim($jq7('span#contentSearchExecuted').text()) == 'false') {
                    //kick off content search
                    //set the page, results per page and search keyword
                    $jq7('#updateContentResultsForm .hidden input[name="pageNumber"]').val(1);
                    $jq7('#updateContentResultsForm .hidden input[name="resultsPerPage"]').val(20);
                    var keyword = '<input type="hidden" name="searchTerms" value="' + $jq7('.toolbar-results form input[name="searchTerms"]').attr('placeholder') + '" />';
                    $jq7('#updateContentResultsForm .hidden').append(keyword);

                    getInitialContentSearchResults();
                }
                $jq7('#facets-products').hide();
                $jq7('#facets-content').show();
                hideFeaturedProducts();
            }
            //$jq7('div.searchWithin').find('input[placeholder], textarea[placeholder]').blur(add).focus(remove).each(add);
    });

    // remove selected facet (in the breadcrumb facets)
    $jq7('#searchResultsPage div#selectedFilters').on('click', 'a.remove', function(event) {

    	var searchEvent = 'filters';
    	
        //$jq7('div.searchWithin').find('input[placeholder], textarea[placeholder]').each(remove);
        if ($jq7(this).hasClass('productSet')) {
            //if product set filter is being removed, all other filters should be removed as well
            $jq7('div#selectedFilters a.remove').each(function(){
                $jq7('#updateResultsForm').find('input[value="' + $jq7(this).attr('data-item-id') + '"]').val('');
            });
            $jq7(this).parent().remove();
        } else if($jq7(this).hasClass('attribute')) {
            $jq7('#removedFacet').text($jq7(this).attr('data-item-id'));
            $jq7('#updateResultsForm').find('input[value="' + $jq7(this).attr('data-item-id') + '"]').prop('checked', false);
        } else {
            $jq7('#removedFacet').text($jq7(this).attr('data-item-uid'));
            $jq7('#updateResultsForm div.hidden input[data-item-uid="' + $jq7(this).attr('data-item-id') + '"]').remove();
        }

        setPage1();
        updateSearchResults(searchEvent);
        return false;
    });
    // content search - remove selected facet
    $jq7('#searchResultsPage div#selectedContentFilters').on('click', 'a.remove', function(event) {

         //$jq7('div.searchWithin').find('input[placeholder], textarea[placeholder]').each(remove);
        if ($jq7(this).hasClass('refinement')) {
            $jq7('#removedContentFacet').text($jq7(this).attr('data-item-id'));
            $jq7('#updateContentResultsForm').find('input[name="selectedDimensionValues"][value="' + $jq7(this).attr('data-item-id') + '"]').prop('checked', false);
        }else{
            $jq7('#removedContentFacet').text($jq7(this).attr('data-item-uid'));
            $jq7('#updateContentResultsForm').find('input[data-item-uid="' + $jq7(this).attr('data-item-uid') + '"]').prop('checked', false);
        }

        setContentPage1();
        updateContentSearchResults();
        //$jq7('div.searchWithin').find('input[placeholder], textarea[placeholder]').blur(add).focus(remove).each(add);
        return false;
    });

    // ** select or unselect a facet
    $jq7('#searchResultsPage #refinements').on('change', 'input.facet', function(event) {
    	
    	var searchEvent = 'filters';
    	
        //$jq7('div.searchWithin').find('input[placeholder], textarea[placeholder]').each(remove);
        if ($jq7(this).prop('checked')) {
            //selecting a new facet
            $jq7('#updateResultsForm').find('input[name="selectedDimensionValues"][value="' + $jq7(this).val() + '"]').prop('checked', true);
            setPage1();
            updateSearchResults(searchEvent);
        } else {
            //unselecting a facet
            $jq7('#removedFacet').text($jq7(this).val());
            $jq7('#updateResultsForm').find('input[name="selectedDimensionValues"][value="' + $jq7(this).val() + '"]').prop('checked', false);
            setPage1();
            updateSearchResults(searchEvent);
        }
        //$jq7('div.searchWithin').find('input[placeholder], textarea[placeholder]').blur(add).focus(remove).each(add);
        return false;
    });
    // content search - select or unselect a facet
    $jq7('#searchResultsPage #contentRefinements').on('change', 'input.facet', function(event) {
        //$jq7('div.searchWithin').find('input[placeholder], textarea[placeholder]').each(remove);
    	 $jq7('div.searchWithin').find('input[placeholder], textarea[placeholder]').each(function() {
     	    var input = $jq7(this);
     	    if (input.val() == input.attr('placeholder')) {
     	      input.val('');
     	    }
     	  });
    	if ($jq7(this).prop('checked')) {
            //selecting a new facet
            setContentPage1();
            updateContentSearchResults();
        } else {
            //unselecting a facet
            $jq7('#removedContentFacet').text($jq7(this).val());
            $jq7('#updateContentResultsForm').find('input[name="selectedDimensionValues"][value="' + $jq7(this).val() + '"]').prop('checked', false);
            setContentPage1();
            updateContentSearchResults();
        }
        //$jq7('div.searchWithin').find('input[placeholder], textarea[placeholder]').blur(add).focus(remove).each(add);
        return false;
    });

    // ** select a product set
    $jq7('#searchResultsPage').on('click', '.productSetLinkFun', function(event) {

    	var searchEvent = 'filters';
        $jq7('form#updateResultsForm input[name="selectedDimensionValues"]').val($jq7(this).attr('data-product-set-id'));
        setPage1();
        updateSearchResults(searchEvent);

        return false;
    });

    // ** submit a search within search
    $jq7('#searchResultsPage').on('click', '#searchWithinBtn', function(event) {

    	var searchEvent = 'new search';
    	
    	if ($jq7(this).parent().find('input[name="searchTerms"]').val() != "") {
            setPage1();
            updateSearchResults(searchEvent);
        }
        return false;
    });
    
    // content search - submit a search within search
    $jq7('#searchResultsPage').on('click', '#searchWithinContentBtn', function(event) {

        //$jq7('div.searchWithin').find('input[placeholder], textarea[placeholder]').each(remove);
        if ($jq7(this).parent().find('input[name="searchTerms"]').val() != "") {
            setContentPage1();
            updateContentSearchResults();
        }
        //$jq7('div.searchWithin').find('input[placeholder], textarea[placeholder]').blur(add).focus(remove).each(add);
        return false;
    });

    // submit search within search with Enter
    $jq7('#searchResultsPage #refinements').on('keydown', 'div.searchWithin', function(event) {
    	
    	var searchEvent = 'filters';
    	if (event.which === $jq7.ui.keyCode.ENTER) {
            //submit search within
            //$jq7('div.searchWithin').find('input[placeholder], textarea[placeholder]').each(remove);
            setPage1();
            updateSearchResults(searchEvent);
            return false;
        }
    });
    // content search - submit search within search with Enter
    $jq7('#searchResultsPage #contentRefinements').on('keydown', 'div.searchWithin', function(event) {
        if (event.which === $jq7.ui.keyCode.ENTER) {
            //$jq7('div.searchWithin').find('input[placeholder], textarea[placeholder]').each(remove);
            //submit search within
            setContentPage1();
            updateContentSearchResults();
            return false;
        }
    });

    // ** remove selected set from within search facets
    $jq7('#searchResultsPage').on('click', 'a#removeAll', function(event) {

    	var searchEvent = 'filters';
    	
        //if product set filter is being removed, all other filters should be removed as well
        $jq7('div#selectedFilters a.remove').each(function(){
            $jq7('#updateResultsForm').find('input[value="' + $jq7(this).attr('data-item-id') + '"]').val('');
        });

        setPage1();
        updateSearchResults(searchEvent);
        return false;
    });

    // ** clear selected checkboxes from a single category
    $jq7('#searchResultsPage #refinements').on('click', 'a.clearGroup', function(event) {

    	var searchEvent = 'filters';
    	
    	$jq7(this).parents('li.productGroup').find('ul input.facet:checked').each(function() {
            //uncheck this checkbox, as well as the hidden one
            $jq7('#removedFacet').text($jq7('#removedFacet').text() + ',' + $jq7(this).val());
            $jq7(this).prop('checked', false);
            $jq7('#updateResultsForm div.hidden').find('input[value="' + $jq7(this).val() + '"]').prop('checked', false);
        });

        setPage1();
        updateSearchResults(searchEvent);
        return false;
    });
    // content search - clear selected checkboxes from a single category
    $jq7('#searchResultsPage #contentRefinements').on('click', 'a.clearGroup', function(event) {
       // $jq7('div.searchWithin').find('input[placeholder], textarea[placeholder]').each(remove);
        $jq7(this).parents('li.productGroup').find('input.facet:checked').each(function() {
            //uncheck this checkbox, as well as the hidden one
            $jq7('#removedContentFacet').text($jq7('#removedContentFacet').text() + ',' + $jq7(this).val());
            $jq7(this).prop('checked', false);
            $jq7('#updateContentResultsForm div.hidden').find('input[value="' + $jq7(this).val() + '"]').prop('checked', false);
        });

        setContentPage1();
        updateContentSearchResults();
        return false;
    });

    // ** clear all selected facets - except the product set
    $jq7('#searchResultsPage #refinements').on('click', 'a.clear-all', function(event) {

    	var searchEvent = 'filters';
    	
        //clear all selectedDimensionValues except the product set
        $jq7('#updateResultsForm').find('input[name="selectedDimensionValues"]').prop('checked', false);
        $jq7('#updateResultsForm .hidden').find('input.productSet').prop('checked', true);
        $jq7('#updateResultsForm .hidden').find('input.productSetSearch').prop('checked', true);

        setPage1();
        updateSearchResults(searchEvent);
        return false;
    });
    // content search - clear all selected facets
    $jq7('#searchResultsPage #contentRefinements').on('click', 'a.clear-all', function(event) {
        //$jq7('div.searchWithin').find('input[placeholder], textarea[placeholder]').each(remove);
        //clear all selectedDimensionValues except the product set
        $jq7('#updateContentResultsForm').find('input[name="selectedDimensionValues"]').prop('checked', false);

        setContentPage1();
        updateContentSearchResults();
        return false;
    });

    // change the list view and grid view
    $jq7('#searchResultsPage .viewType i.icon').click(function() {
        if (!$jq7(this).parent().hasClass('selected')) {
            if ($jq7(this).hasClass('icon-grid')) {
                $jq7('div#searchResults').removeClass('products-list').addClass('products-grid');

                //truncate titles and create tooltips
                $jq7('.products-grid .product-name p').truncate({
                    lines: 5,
                    complete: function(){ $jq7('.truncated').tooltip(); }
                });

            } else if ($jq7(this).hasClass('icon-list')) {
                $jq7('div#searchResults').removeClass('products-grid').addClass('products-list');

                //destory tooltips and set the product name back to non-truncated form
                $jq7('.truncated').tooltip('destroy');
                $jq7('.truncated').each(function(){
                    if($jq7(this).attr('productName')){
                        $jq7(this).html($jq7(this).attr('productName'));
                    }else{
                        $jq7(this).text($jq7(this).attr('title'));
                    }
                    $jq7(this).removeClass('truncated');
                });
            }
            $jq7('#searchResultsPage .viewType a').removeClass('selected');
            $jq7(this).parent().addClass('selected');
        }
        return false;
    });

    //change the number of results per page
    $jq7('#searchResultsPage div#productList select[name="resultsPerPage"]').change(function() {

    	var searchEvent = 'page change';
    	
        $jq7('#updateResultsForm .hidden').find('input[name="resultsPerPage"]').val($jq7(this).children(':selected').val());
        setPage1();
        updateSearchResults(searchEvent);
        return false;
    });
    //content search - change the number of results per page
    $jq7('#searchResultsPage div#contentList').on('change', 'select[name="resultsPerPage"]', function(event) {

       // $jq7('div.searchWithin').find('input[placeholder], textarea[placeholder]').each(remove);
    	 $jq7('div.searchWithin').find('input[placeholder], textarea[placeholder]').each(function() {
    	    var input = $jq7(this);
    	    if (input.val() == input.attr('placeholder')) {
    	      input.val('');
    	    }
    	  });
        $jq7('#updateContentResultsForm .hidden').find('input[name="resultsPerPage"]').val($jq7(this).children(':selected').val());
        setContentPage1();
        updateContentSearchResults();
        return false;
    });

    //change the sort field
    $jq7('#searchResultsPage div#productList select[name="sortField"]').change(function() {

    	var searchEvent = 'page change';
    	
        $jq7('#updateResultsForm .hidden').find('input[name="sortField"]').val($jq7(this).children(':selected').val());
        $jq7('#updateResultsForm .hidden').find('input[name="sortDirection"]').val($jq7(this).children(':selected').attr('data-sort-direction'));
        updateSearchResults(searchEvent);
        return false;
    });

    //change the page number
    $jq7('#searchResultsPage div#productList').on('click', 'a.pageLink', function(event) {

        //$jq7('div.searchWithin').find('input[placeholder], textarea[placeholder]').each(remove);
        $jq7('#updateResultsForm .hidden input[name="pageNumber"]').val($jq7(this).attr('data-page-number'));
        updateSearchResults();
        return false;
    });
    //content search - change the page number
    $jq7('#searchResultsPage div#contentList').on('click', 'a.pageLink', function(event) {

        //$jq7('div.searchWithin').find('input[placeholder], textarea[placeholder]').each(remove);
        $jq7('#updateContentResultsForm .hidden input[name="pageNumber"]').val($jq7(this).attr('data-page-number'));
        updateContentSearchResults();
        return false;
    });

    activateAddToCartButtons('div.col.product');

}

function activateAddToCartButtons(selector) {
    //make add to cart button
    $jq7(selector).each(function() {
        //create the object with all required parameters
        var prodDiv = $jq7(this);
        var prodProps = $jq7(prodDiv).data();
        var anixterId=$jq7(prodDiv).attr("data-anixter-id");
        prodProps["anixterId"]=anixterId;


        //Added not([name='addToCartButtonAccesory']) to exclude the accessory buttons, this was causing a conflict. -Brad
        $jq7(prodDiv).find("a.btn-small,not(a[name='addToCartButtonAccesory'])").makeUpdateCartButton("/bin/shoppingCart/add", prodProps, function() {
            var mediumImage = $jq7(this).find('a.product-thumb img').attr('src');
            var addOnProps = {
                quantity: $jq7(this).find('input[name="quant"]').val().replace(/^0+/, ''),
                minimum: parseInt($jq7(this).find('input[name="quant"]').data("min"), 10),
                increment: parseInt($jq7(this).find('input[name="quant"]').data("step"), 10),
                imageUrlMedium: mediumImage,
                imageUrlSmall: mediumImage.replace('V7.', 'V8.'),
                imageUrlLarge: mediumImage.replace('V7.', 'V6.')
            };
//            console.log("plural", prodProps, addOnProps);
            return addOnProps;
        }, prodDiv, function() {
            $jq7(prodDiv).find('input[name="quant"]').val("");
        });
    });
}

//this function is used on the product detail page
function activateAddToCartButton() {
    var prodDiv = $jq7('#productDetail');
    var prodProps = $jq7(prodDiv).data();
    prodProps["name"] = prodProps["description"];
    var anixterId=$jq7(prodDiv).attr("data-anixter-id");
    prodProps["anixterId"]=anixterId;

    //Added not([name='addToCartButtonAccesory']) to exclude the accessory buttons, this was causing a conflict. -Brad
    $jq7(prodDiv).find("a[name='addToCartButtonDetail']").makeUpdateCartButton("/bin/shoppingCart/add", prodProps, function() {
        var mediumImage = prodProps.imageUrlMedium;
        var addOnProps = {
            quantity: $jq7(this).find('input[name="quant"]').val().replace(/^0+/, ''),
            minimum: parseInt($jq7(this).find('input[name="quant"]').data("min"), 10),
            increment: parseInt($jq7(this).find('input[name="quant"]').data("step"), 10),
            imageUrlMedium: mediumImage,
            imageUrlSmall: mediumImage.replace('V7.', 'V8.'),
            imageUrlLarge: mediumImage.replace('V7.', 'V6.')
        };
//        console.log("singular", prodProps, addOnProps);
        return addOnProps;
    }, prodDiv, function() {
        $jq7(prodDiv).find('input[name="quant"]').val("");
    });
}

function setPage1() {
    $jq7('#updateResultsForm .hidden input[name="pageNumber"]').val(1);
    return false;
}

function hideFeaturedProducts() {
    $jq7('#searchResultsPage div.featuredproducts').hide();
    $jq7('ul.share-this-list li > span').hide();
}

function toggleFeaturedProducts() {
    //if there are any selected facets or keywords, hide featured products div and shareThis and email, otherwise make it visible
    //$jq7('div.searchWithin').find('input[placeholder], textarea[placeholder]').each(remove);
    if (($jq7('#updateResultsForm div.hidden input[name="selectedDimensionValues"][class!="productSet"][value!=""]').size() > 0
            && $jq7('#updateResultsForm div.hidden input[name="selectedDimensionValues"][class!="selectedSupplier"][value!=""]').size() > 0)
            || $jq7('#updateResultsForm div.hidden input[name="searchTerms"][class!="firstKeyword"]').size() > 0) {
        //hide
        hideFeaturedProducts();
    } else {
        //show
        $jq7('#searchResultsPage div.featuredproducts').show();
        $jq7('ul.share-this-list li > span').show();
    }

    //$jq7('div.searchWithin').find('input[placeholder], textarea[placeholder]').blur(add).focus(remove).each(add);
}

function updateSearchResults(searchEvent) {
    
	var count = 0;
	
	$jq7.ajax({
        url: $jq7('#searchResultsPage span#path').text(),
        data: $jq7('#updateResultsForm').serialize(),
        type: "GET",
        success: function(data, textStatus, jqXHR) {
//        	console.log(data);

        	count = data.products.length;
        	
            if (Object.prototype.toString.call(data) === "[object Object]" && count > 0) {
                var grid = $jq7("#searchResults").is(".products-grid");
                var pl = $jq7("#productList").data();
                var list = common.template.makeSearchResultsProductList(data.products, grid, pl);
                $jq7("#searchResults").replaceWith($jq7(list)); // converts HTML string into jQuery DOM object and appends it to the container element
                $jq7('#productList').show();

                var pagination = common.template.makeSearchResultsPagination(data.page, pl);
                $jq7("#productList .paginate").replaceWith(pagination);

                //Add the breadcrumbs
                var breadcrumbText = common.template.makeBreadcrumb(data.page);
                $jq7("#searchResultsPage #selectedFilters").html(breadcrumbText);

                //update the facets
                var facets = common.template.updateFacets(data.page);
                $jq7("#updateResultsForm #searchFacets").html(facets);

                //if there are any selected facets or keywords, hide featured products div, otherwise make it visible
                toggleFeaturedProducts();

                //truncate titles in grid view and re-create tooltips
                if (grid) {
                    $jq7('.products-grid .product-name p').truncate({
                        lines: 5,
                        complete: function() { $jq7('.truncated').tooltip(); }
                    });
                }

                activateAddToCartButtons('#searchResults div.col.product');

                //re-create the searchSideBar widget
                $jq7('#refinements').searchSideBar('destroy');
                $jq7('#searchResultsPage #refinements').searchSideBar();
                //initEventHandlers();

                $jq7("html, body").animate({ scrollTop: 0 });
            } else {
                if (count == 0) {
                    //check whether a removed facet did not return results, or whether it was a search within keyword
                    if ($jq7('#removedFacet').text().length > 0) {
                        //get the dimension values of the facets that the user was trying to remove and check those checkboxes back
                        var removedFacetIDs = $jq7('#removedFacet').text().split(',');
                        for (var i = 0; i < removedFacetIDs.length; i++) {
                            var removedFacet = removedFacetIDs[i];
                            if (removedFacet.length > 0) {
                                //check back the removed facet
                                $jq7('#updateResultsForm').find('input[value="' + removedFacet + '"]').prop('checked', true);
                            }
                        }
                        //clear the removedFacet span
                        $jq7('#removedFacet').text('');
                        //show the noResultsForFacetsDialog dialog
                        $jq7('#noResultsForFacetsDialog').dialog({position:'center', width:500, modal: true, resizable: false});
                    } else {
                        //just update the noResults dialog and open it
                        $jq7('#noResultsDialog div.noResultsHeader #noResultsValue').text(data.page.searchTerms[data.page.searchTerms.length-1].name);
                        $jq7('#noResultsDialog').dialog({position:'center', width:500, modal: true, resizable: false});
                        $jq7('#searchResultsPage #refinements div.search input[name="searchTerms"]').val('');
                        $jq7.ajax({
                            url: $jq7('#searchResultsPage span#path').text(),
                            data: $jq7('#updateResultsForm').serialize(),
                            type: "GET"
                        });
                    }
                } else {
                    //some error occurred - display error dialog
                    $jq7('#searchErrorDialog').dialog({position:'center', width:500, modal: true, resizable: false});
                }
            }
            
            setSearchAnalytics(searchEvent, count);
        },
        error: function(jqXHR, textStatus, errorThrown) {
            $jq7('#searchErrorDialog').dialog({position:'center', width:500, modal: true, resizable: false});
        }
    });
}

//functions handling content search

function setContentPage1() {
    $jq7('#updateContentResultsForm .hidden input[name="pageNumber"]').val(1);
    return false;
}

function updateContentSearchResults() {
//    console.log("updateContentSearchResults() AJAX Info:", $jq7('#searchResultsPage span#contentPath').text(), $jq7('#updateContentResultsForm').serialize());
    $jq7.ajax({
        url: $jq7('#searchResultsPage span#contentPath').text(),
        data: $jq7('#updateContentResultsForm').serialize(),
        type: 'GET',
        cache: false,
        dataType:"html",
        success: function(data, textStatus, jqXHR) {
//    		console.log("updateContentSearchResults() AJAX Success:", data, textStatus, jqXHR);
            //check if there are any results, if no, update the dialog and open it
            var numResults = $jq7(data).find('span#numContentResults').text();
            //$jq7('div.searchWithin').find('input[placeholder], textarea[placeholder]').blur(add).focus(remove).each(add);
          
            if (numResults == "") {
                //some error occurred - display error dialog
                $jq7('#searchErrorDialog').dialog({position:'center', width:500, modal: true, resizable: false});
            } else if (numResults > 0) {
                $jq7('#contentRefinements').searchSideBar('destroy');

                //$jq7('#searchResultsPage #selectedContentFilters').replaceWith($jq7(data).find('#selectedContentFilters'));
                $jq7('#searchResultsPage #selectedContentFilters').empty();
                $jq7('#searchResultsPage #selectedContentFilters').append($jq7(data).find('#selectedContentFilters p'));
            	
                $jq7('#searchResultsPage #contentRefinements form').replaceWith($jq7(data).find('#contentRefinements form'));

                $jq7('#searchResultsPage div#contentList div.toolbar-results p.range').replaceWith($jq7(data).find('div.toolbar-results p.range'));
                $jq7('#searchResultsPage #contentSearchResults').replaceWith($jq7(data).find('#contentSearchResults'));
                $jq7('#searchResultsPage div#contentList nav.paginate ul').replaceWith($jq7(data).find('nav.paginate ul'));

                //clear the removedFacet span
                $jq7('#removedContentFacet').text('');

                //re-create the searchSideBar widget
                $jq7('#searchResultsPage #contentRefinements').searchSideBar();
                $jq7("html, body").animate({ scrollTop: 0 });
            } else {
                $jq7('#searchResultsPage div#contentList').append($jq7(data).find('div.searched_for_text'));
                $jq7('#searchResultsPage div#contentList').append($jq7(data).find('div.didYouMean'));
                //check whether a removed facet did not return results, or whether it was a search within keyword
                if ($jq7('#removedContentFacet').text().length > 0) {
                    //get the dimension values of the facets that the user was trying to remove and check those checkboxes back
                    var removedFacetIDs = $jq7('#removedContentFacet').text().split(',');
                    for (var i = 0; i < removedFacetIDs.length; i++) {
                        var removedFacet = removedFacetIDs[i];
                        if (removedFacet.length > 0) {
                            //check back the removed facet
                            $jq7('#updateContentResultsForm').find('input[value="' + removedFacet + '"]').prop('checked', true);
                        }
                    }
                    //clear the removedFacet span
                    $jq7('#removedContentFacet').text('');
                    //show the noResultsForFacetsDialog dialog
                    $jq7('#noResultsForFacetsDialog').dialog({position:'center', width:500, modal: true, resizable: false});
                } else {
                    //just update the noResults dialog and open it
                    $jq7('#noResultsDialog div.noResultsHeader #noResultsValue').text($jq7(data).find('#updateContentResultsForm input.lastKeyword').val());
                    $jq7('#noResultsDialog').dialog({position:'center', width:500, modal: true, resizable: false});
                    $jq7('#searchResultsPage #contentRefinements div.search input[name="searchTerms"]').val('');
                    //$jq7('div.searchWithin').find('input[placeholder], textarea[placeholder]').blur(add).focus(remove).each(add);
                }
            }


           //$jq7('div.searchWithin').find('input[placeholder], textarea[placeholder]').blur(add).focus(remove).each(add);
        },
        error: function(jqXHR, textStatus, errorThrown) {
//        	console.log("updateContentSearchResults() AJAX Error:", jqXHR, textStatus, errorThrown);
            $jq7('#searchErrorDialog').dialog({position:'center', width:500, modal: true, resizable: false});
        }
    });
}

function setSearchAnalytics(searchEvent, count) {
	
	digitalData.page.event = "search";
	digitalData.search = {
			searchTerm: $jq7('#globalSearch input[name="searchTerms"]').val(),
			searchResults: count,
			searchEvent: searchEvent,
			searchFilter: "na",
			searchSort: "na",
			searchType: "standard",
			searchSuccess: count > 0 ? "successful search" : "null search",
			searchTermRecommended: "na",
			searchTermTypeAhead: "na",
			searchPagination:  $jq7('#updateResultsForm .hidden input[name="pageNumber"]').val(),
			searchRedirect: "na",
			searchLandingPage: ((window.location.pathname.replace('/content/anixter/', '')).replace('.html','')),
			searchResultList: $jq7('#productList').data('product-number-list')
	}
}

function getInitialContentSearchResults() {
//	console.log("getInitialContentSearchResults() AJAX Info:", $jq7('#searchResultsPage span#contentPath').text(), $jq7('#updateContentResultsForm').serialize());
    $jq7.ajax({
        url: $jq7('#searchResultsPage span#contentPath').text(),
        data: $jq7('#updateContentResultsForm').serialize(),
        type: 'GET',
        success: function(data, textStatus, jqXHR) {
//        	console.log("getInitialContentSearchResults() AJAX Success:", data, textStatus, jqXHR);
            //check if there are any results, if no, update the dialog and open it
            var numResults = $jq7(data).find('span#numContentResults').text();
            //$jq7('div.searchWithin').find('input[placeholder], textarea[placeholder]').blur(add).focus(remove).each(add);
            if (numResults == "") {
                //some error occurred - display error dialog
                $jq7('#searchErrorDialog').dialog({position:'center', width:500, modal: true, resizable: false});
            } else if (numResults > 0) {
                $jq7('#contentRefinements').searchSideBar('destroy');

                $jq7('#searchResultsPage #selectedContentFilters').empty();
                $jq7('#searchResultsPage #selectedContentFilters').append($jq7(data).find('#selectedContentFilters p'));

                $jq7('#searchResultsPage #contentRefinements form').replaceWith($jq7(data).find('#contentRefinements form'));

                $jq7('#searchResultsPage div#contentList').append($jq7(data).find('div.searched_for_text'));
                $jq7('#searchResultsPage div#contentList').append($jq7(data).find('div.didYouMean'));
                $jq7('#searchResultsPage div#tab-content').append($jq7(data).find('div.didYouMean'));
                $jq7('#searchResultsPage div#contentList').append($jq7(data).find('div.toolbar-results'));
                $jq7('#searchResultsPage div#contentList').append($jq7(data).find('#contentSearchResults'));
                $jq7('#searchResultsPage div#contentList').append($jq7(data).find('nav.paginate'));

                //re-create the searchSideBar widget
                $jq7('#searchResultsPage #contentRefinements').searchSideBar();
            } else {
                //search returned no results, display no results text
                $jq7('.nocontentresultstext').parent().removeClass('editModeOnly').show();
                $jq7('#searchResultsPage div#contentList').append($jq7(data).find('div.didYouMean'));
                if ($jq7(data).find('div.didYouMean').length){
                    $jq7('#didYouMeanContent').html($jq7(data).find('div.didYouMean'));
                    $jq7('#didYouMeanContent').show();
                    $jq7('#didYouMeanContentResults').hide();
                }
            }
            $jq7('span#contentSearchExecuted').text('true');

            //$jq7('div.searchWithin').find('input[placeholder], textarea[placeholder]').blur(add).focus(remove).each(add);
        },
        error: function(jqXHR, textStatus, errorThrown) {
//        	console.log("getInitialContentSearchResults() AJAX Error:", jqXHR, textStatus, errorThrown);
            $jq7('#searchErrorDialog').dialog({position:'center', width:500, modal: true, resizable: false});
        }
    });
}
var accessories = accessories || {
    handlers: {},
    validators: {}
};

jQuery("document").ready( function($) {
    if ($jq7(".accessories-display").length > 0) {
        accessories.info = $jq7(".accessories-display").data();

        $jq7(".accessories-display").hide();
        var productKey = $jq7('div.product-info').data("detailsId");
        if (!productKey) { productKey = accessories.info.productKey; }
        
        var parentMfgNum = $jq7('div.product-info').data("mfgid"); 
        if (!parentMfgNum) { parentMfgNum = accessories.info.mfgNum; }
        
        var parentMfgName = $jq7('div.product-info').data("mfgname"); 
        if (!parentMfgName) { parentMfgName = accessories.info.mfgName; }
        
        if (productKey) {
            $jq7.ajax({
                url: accessories.info.path + ".accessories.json?anixterId=" + productKey,
                type: "GET",
                success: function(data, textStatus, jqXHR) {
//                	console.log(data);

                    if (Object.prototype.toString.call(data) === "[object Array]" && data.length > 0) {
                        var pl = $jq7(".accessories-display").first().data();
                        var list = common.template.makeProductAccessoriesList(data, true, pl);
//                		console.log(list);
                        $jq7("#accessory-list").replaceWith($jq7(list)); // converts HTML string into jQuery DOM object and appends it to the container element

                        //truncate titles in grid view and re-create tooltips
                        $jq7('.products-grid .product-name p').truncate({
                            lines: 5,
                            complete: function() { $jq7('.truncated').tooltip(); }
                        });

                        var accessoryEls = $jq7("#accessory-list .product");

                        //Commented this out as it seems to just be repeating the same work done later. -Brad
                        //activateAddToCartButtons(accessoryEls);

                        accessoryEls.each(function() {
                            var accessory = $jq7(this);

                            var aData = accessory.data();

                            // This is for the Add-To-Cart functionality
                            var prodProps = {
                                anixterId         : aData.anixterId,
                                name              : aData.description,
                                description       : aData.description,
                                url               : aData.url,
                                mfgName           : aData.mfgName,
                                mfgNum            : aData.mfgNum,
                                specSheetUrl      : aData.specSheetURL,
                                quantityQualifier : aData.quantityQualifier,
                                productSetName    : aData.productSetName,
                                productSetUrl     : aData.productSetURL,
                                conversionFactor  : aData.conversionFactor,
                                uom               : aData.uom,
                                uomDesc           : aData.uomDesc,
                                baseUom           : aData.baseUom,
                                baseUomDesc       : aData.baseUomDesc,
                                sellOnline        : aData.sellOnline,
                                price             : aData.price
                            };

                            $jq7(accessory).find("a[name='addToCartButtonAccesory']").makeUpdateCartButton("/bin/shoppingCart/add", prodProps, function() {
                                var mediumImage = $jq7(this).find('a.product-thumb img').attr('src');
                                return {
                                    quantity: $jq7(this).find('input[name="quant"]').val().replace(/^0+/, ''),
                                    minimum: parseInt($jq7(this).find('input[name="quant"]').data("min"), 10),
                                    increment: parseInt($jq7(this).find('input[name="quant"]').data("step"), 10),
                                    imageUrlMedium: mediumImage,
                                    imageUrlSmall: mediumImage.replace('V7.', 'V8.'),
                                    imageUrlLarge: mediumImage.replace('V7.', 'V6.')
                                };
                            }, accessory, function() {
                                $jq7(accessory).find('input[name="quant"]').val("");
                            });
                        });

                        $jq7(".availability > [title]").tooltip();
                        $jq7(".purchase-button > .btn-question").tooltip();
                        $jq7(".accessories-display").show();
                    }
                }
            });
//            $jq7.get(accessories.info.path + ".accessories.json?anixterId=" + productKey, function(data) {
//
//                var accessoriesList = $jq7("#accessory-list");
//                accessoriesList.html("");
//
//                var productList = data;
//                if (productList && productList.length) {
//                    for (var i = 0; i < productList.length; i++) {
//                        var product = productList[i];
//
//                        var productLinkText   = product.productName;
//                        var productMfgId      = $jq7('<div/>').text(product.manufacturerNumber).html();
//                        var productAnixterId  = product.anixterNumber;
//                        var prodDetailURL     = product.prodDetailURL;
//                        var productImageSmall = product.smallImageSrc;
//                        var uomDisplay        = (product.uomCode !== 'EA') ? accessories.info.lblPerUOM + ' ' + product.uomCodeDesc : ' ';
//
//                        var sellOnline         = product.sellOnline
//                        var priceDisplay       = product.price;
//                        var priceFormatted     = product.priceFormatted;
//                        var leadtimeCode       = product.leadTimeCode;
//                        var leadTimeDesc       = product.leadTimeDesc;
//                        var lowQtyThreshold    = (product.lowQuantityThreshold) ? parseInt(product.lowQuantityThreshold) : 0;
//                        var availableInventory = (product.availableInventory) ? parseInt(product.availableInventory) : 0;
//                        var minOrderQuantity   = (product.minOrderQuantity) ? parseInt(product.minOrderQuantity) : 1;
//                        var orderIncrementQty  = (product.orderIncrementQuantity) ? parseInt(product.orderIncrementQuantity) : 1;
//
//                        var caption   = (productMfgId) ? productMfgId + ' | ' + productLinkText : productLinkText;
//                        var imgClass  = (product.imageExists) ? 'nonDefault' : '';
//                        var alternate = (product.manufacturerName) ? product.anixterNumber : caption;
//                        var mfgNumStr = (product.manufacturerNumber) ? product.manufacturerNumber : '';
//
//                        var imgDisplay = (product.thumbnailImageSrc) ? product.thumbnailImageSrc : (accessories.info.defaultImgSrc.length > 0) ? accessories.info.defaultImageSrc : '';
//
//                        var googleAncClk = "onclick=\"_gaq.push([";
//                        var googleAncEvn = "\'_trackEvent'";
//
//                        var googleAncAnxLnk = ", \'accessories - " + product.anixterNumber + "\'";
//                        var googleAncAnxCrt = ", \'added to cart - accessories - " + product.anixterNumber + "\'";
//
//                        var googleAnltcsLnk = googleAncClk + googleAncEvn + googleAncAnxLnk + ", \'click\', \'" + mfgNumStr + "\']);\"";
//                        var googleAnltcsCrt = googleAncClk + googleAncEvn + googleAncAnxCrt + ", \'click\', \'" + mfgNumStr + "\']);\"";
//
//                        var accessory = "<div class='col product' data-axe-acc-id='" + productAnixterId + "'>"
//                            + "<div class='product-image-and-info clearfix'>"
//                                + "<div class='product-image'>"
//                                    + "<a href='" + prodDetailURL + "' class='product-thumb' " + googleAnltcsLnk + "><img src='" + imgDisplay + "' width='125' height='125' alt='" + alternate + "' class='" + imgClass + "' /></a>"
//                                + "</div>"
//                                + "<div class='product-info clearfix'>"
//                                    + "<div class='product-name'>"
//                                        + "<span class='hidden prop-description'>" + product.description + "</span>"
//                                        + "<span class='hidden prop-mfgName'>" + product.manufacturerName + "</span>"
//                                        + "<p><a href='" + prodDetailURL + "' title='" + product.productName + "' " + googleAnltcsLnk + "> " + product.productName + "</a></p>"
//                                    + "</div>"
//                                    + "<div class='product-numbers'>"
//                                        + "<ul>";
//                                            if (productMfgId) { accessory += "<li class='product-attribute prop-mfgNum'><span class='attribute-title'>" + accessories.info.labelManufacturerNumber + "</span>"+ productMfgId + "</li>"; }
//                                            accessory += "<li class='product-attribute'><span class='attribute-title'>" + accessories.info.labelAnixterNumber + "</span><span class='prop-anixterId'>" + productAnixterId + "</span></li>"
//                                        + "</ul>"
//                                    + "</div>"
//                                    + "<div class='product-links'>"
//                                        + "<ul>";
//                                            if (product.specSheetURL) { accessory += "<li class='product-attribute'><span class='attribute-title'><img src='/apps/settings/wcm/designs/anixter/images/icon-pdf.gif' alt='pdf' width='16' height='16' /></span><a class='prop-specSheetUrl' href='" + product.specSheetURL + "' target='_blank' title='" + accessories.info.labelSpecSheet + "'>" + accessories.info.labelSpecSheet + "</a></li>"; }
//                                            if (product.prodSetURL) { accessory += "<li class='product-attribute'><span class='attribute-title'>" + accessories.info.labelViewAll + "</span><a href='" + product.prodSetURL + "' class='prop-productSet' title='" + product.prodSetName + "'>" + product.prodSetName +"</a></li>"; }
//                                        accessory += "</ul>"
//                                    + "</div>"
//                                + "</div>"
//                            + "</div>"
//                            + "<div class='product-buy'>"
//                                + "<div class='product-stock product-add'>"
//                                    + "<span class='hidden prop-conversionFactor'>" + product.uomBaseConversionFactor + "</span>"
//                                    + "<span class='hidden prop-uom'>" + product.uomCode + "</span>"
//                                    + "<span class='hidden prop-uomDesc'>" + product.uomCodeDesc + "</span>"
//                                    + "<span class='hidden prop-baseUOM'>" + product.baseUOM + "</span>"
//                                    + "<span class='hidden prop-baseUOMDesc'>" + product.baseUOMDesc + "</span>"
//                                    + "<span class='hidden prop-sellOnline'>" + product.sellOnline + "</span>"
//                                    + "<ul class='product-price-and-uom'>";
//                                        if ((product.price) && (product.commerceEnabled === "true")) {
//                                            accessory += "<li class='unit-price'><span class='price-label'></span> <span class='price'>" + product.priceFormatted + "</span><span class='hidden prop-price'>" + product.price + "</span></li>";
//                                            if (product.uomCode != "EA") {
//                                                accessory += "<li class='unit-size' ><span class='unit-uom'>" + uomDisplay  + "</span></li>";
//                                            }
//                                        } else {
//                                            accessory += "<li class='unit-price no-price'>" + accessories.info.pricingNotAvailableMessage + "</li>";
//                                        }
//                                    accessory += "</ul>"
//                                    + "<ul class='product-availability'>";
//                                        if (availableInventory <= 0) {
//                                            accessory += "<li class='availability zero'><span title='" + accessories.info.zeroHoverMessage + "' style='color: " + accessories.info.zeroAvailabilityColor + "; border-color: " + accessories.info.zeroAvailabilityColor + ";'>" + accessories.info.zeroAvailabilityMessage + "</span></li>";
//                                        } else if (availableInventory < lowQtyThreshold) {
//                                            accessory += "<li class='availability limited'><span title='" + accessories.info.limitedHoverMessage + "' style='color: " + accessories.info.limitedAvailabilityColor + "; border-color: " + accessories.info.limitedAvailabilityColor + ";'>" + accessories.info.limitedAvailabilityMessage + "</span></li>";
//                                        } else {
//                                            accessory += "<li class='availability ready'><span title='" + accessories.info.readyHoverMessage + "' style='color: " + accessories.info.quantityAvailableColor + "; border-color: " + accessories.info.quantityAvailableColor + ";'>" + accessories.info.quantityAvailableMessage + "</span></li>";
//                                        }
//                                        accessory += "<li class='lead-time'><span>"+ product.leadTimeDesc + "</span></li>"
//                                    + "</ul>"
//                                    + "<ul class='product-quantity'>";
//                                        if (product.commerceEnabled === "true") {
//                                            if (minOrderQuantity > 1) { accessory += "<li class='min-quantity'><span>" + accessories.info.labelMinOrderQuantity + "</span> <span class='min-quantity-value'>" + minOrderQuantity + "</span></li>"; }
//                                            if (orderIncrementQty > 1) { accessory += "<li class='quantity-increment'><span>" + accessories.info.labelOrderIncrementQuantity + "</span> <span class='quantity-increment-value'>" + orderIncrementQty + "</span></li>"; }
//                                        }
//                                    accessory += "</ul>"
//                                    + "<ul class='product-add-to-cart'>"
//                                        + "<li class='purchase-quantity'><label class='quantity-label'>" + accessories.info.labelQuantity + "</label><input type='text' name='quant' value='' maxlength='9' data-min='" + minOrderQuantity + "' data-step='" + orderIncrementQty + "' data-default='' /></li>";
//                                        if (product.uomCode !== "EA") {
//                                        	accessory += "<li class='purchase-unit prop-quantityQualifier'>" + product.baseUOMDesc + "</li>";
//                                    	} else {
//                                    		accessory += "<li class='purchase-unit prop-quantityQualifier'> </li>";
//                                		}
//                                        if ((product.price) && (product.commerceEnabled === "true") && (product.sellOnline === "true") && (availableInventory > 0)) {
//                                            accessory += "<li class='purchase-button'><a href='#' data-axe-btn='" + product.anixterNumber + "' title='" + accessories.info.labelAddToCart + "' class='add-to-cart-btn btn btn-small' " + googleAnltcsCrt + " >" + accessories.info.labelAddToCart + "</a>";
//                                        } else {
//                                            accessory += "<li class='purchase-button'><a href='#' data-axe-btn='" + product.anixterNumber + "' title='" + accessories.info.labelRequestQuote + "' class='add-to-cart-btn btn btn-small with-question' " + googleAnltcsCrt + " >" + accessories.info.labelRequestQuote + "</a>";
//                                            accessory += "<a data-axe-qtn='" + product.anixterNumber + "' class='btn btn-small btn-question' onclick='event.preventDefault(); return false;' title='" + accessories.info.qHoverMessage + "'>?</a></li>";
//                                        }
//                                    accessory += "</ul>"
//                                + "</div>"
//                            + "</div>"
//                            + "<div class='dot-div clearfix'></div>"
//                        + "</div>";
//                        accessory = $jq7(accessory);
//
//                        //This is for the Add-To-Cart functionality
//                        var prodProps = {
//                            anixterId         : productAnixterId,
//                            name              : productLinkText,
//                            description       : productLinkText,
//                            url               : prodDetailURL,
//                            mfgName           : product.manufacturerName,
//                            mfgNum            : productMfgId,
//                            specSheetUrl      : product.specSheetURL,
//                            quantityQualifier : product.uom,
//                            productSetName    : product.prodSetName,
//                            productSetUrl     : product.prodSetURL,
//                            conversionFactor  : product.uomBaseConversionFactor,
//                            uom               : product.uomCode,
//                            uomDesc           : product.baseUOMDesc,
//                            baseUOM           : product.baseUOM,
//                            baseUOMDesc       : product.baseUOMDesc,
//                            sellOnline        : product.sellOnline,
//                            price             : product.price
//                        };
//
//                        $jq7(accessory).find("a.add-to-cart-btn").makeUpdateCartButton("/bin/shoppingCart/add", prodProps, function() {
//                            var mediumImage = $jq7(this).find('a.product-thumb img').attr('src');
//                            return {
//                                quantity: $jq7(this).find('input[name="quant"]').val().replace(/^0+/, ''),
//                                minimum: parseInt($jq7(this).find('input[name="quant"]').data("min"), 10),
//                                increment: parseInt($jq7(this).find('input[name="quant"]').data("step"), 10),
//                                imageUrlMedium: mediumImage,
//                                imageUrlSmall: mediumImage.replace('V7.', 'V8.'),
//                                imageUrlLarge: mediumImage.replace('V7.', 'V6.')
//                            };
//                        }, accessory, function() {
//                            $jq7(accessory).find('input[name="quant"]').val("");
//                        });
//
//                        accessoriesList.append(accessory);
//                    }
//
//                    $jq7(".availability > [title]").tooltip();
//                    $jq7(".purchase-button > .btn-question").tooltip();
//                    $jq7(".accessories-display").show();
//                }
//            });
        }
    }
});
var featuredproducts = featuredproducts || {};

jQuery("document").ready(function($) {
//    console.log("featuredproducts document ready");
    if ($jq7("#featuredProducts").length > 0) {
//        console.log("featuredProducts component is loaded");
        featuredproducts.info = $jq7("#featuredProducts").data();
//        var path = featuredproducts.info.path + ".priceinventory.json?anixterId=";
//        var ids = JSON.parse($jq7("#featuredProductsResultSet").html());
//        console.log(ids);
//        var feat = [];
//        for (var i = 0; i < ids.length; i ++) {
//        	var url = path + ids[i];
//        	$jq7.ajax({
//        		url: url,
//        		async: true,
//        		type:"GET",
//        		success: function(data) {
//        			feat.push(data);
//        			console.log(feat);
////        			if (feat.length >== ids.length) {
////        				console.log(feat);
////        			}
//        		}
//        	});
//        }
        /*$jq7.ajax({
        	url: path,
        	async: true,
        	type: "GET",
        	success: function(data) {
//                console.log(data);
                var products = common.template.makeFeaturedProductList(data.products, true, $jq7("#featuredProducts").data());
                $jq7("#featuredProducts .products-grid").replaceWith($jq7(products));
            },
            error: function(jqXHR, textStatus, errorThrown) {
//            	console.log(jqXHR, textStatus, errorThrown);
            }
        });*/

        $jq7(".availability > [title]").tooltip();

        $jq7(".purchase-button > .btn-question").tooltip();

        var container = $jq7("#featuredProducts");

		if ($jq7(".supplier").length > 0) {
        	container.addClass("supplierproducts");
//        	featuredproducts.info.gridViewItemsPerRow = 1;
        } else if ($jq7(".product-groups").length > 0) {
        	container.addClass("productgroup");
//        	featuredproducts.info.gridViewItemsPerRow = 4;
        } else if ($jq7("#searchResults").length > 0) {
        	container.addClass("productset");
//        	featuredproducts.info.gridViewItemsPerRow = 4;
        } else {
        	// do nothing
        }

        var productEls = container.find(".product");

        productEls.each(function() {
        	var prod = $jq7(this);

        	var prodData = prod.data();

        	prod.find(".unit-price").hide();
            prod.find(".no-price").hide();

            prod.find(".zero").hide();
            prod.find(".limited").hide();
            prod.find(".ready").hide();
            prod.find(".lead-time").hide();

        	var prodPath = featuredproducts.info.path + ".priceinventory.json?anixterId=" + prodData.axeId;

            $jq7.get(prodPath, function(data) {

            	if (data.minOrderQuantity > 1) {
            		prod.find(".min-quantity-value").text(data.minOrderQuantity);
            		prod.find(".min-quantity").show();
            	}

                if (data.orderIncrementQuantity > 1) {
                	prod.find(".quantity-increment-value").text(data.orderIncrementQuantity);
                	prod.find(".quantity-increment").show();
                }

                prod.find("input[name='quant']").data("min", data.minOrderQuantity);
                prod.find("input[name='quant']").data("step", data.orderIncrementQuantity);
                prod.data("sellOnline", data.sellOnline);
                prod.data("price", data.price);

                if (data.commerceEnabled) {
                    if (data.minOrderQuantity > 1) { prod.find(".min-quantity").show(); }
                    if (data.orderIncrementQuantity > 1) { prod.find(".quantity-increment").show(); }
                }

                var btn = prod.find(".btn:not(.btn-question)");
                var qtn = prod.find(".btn-question");

                if (data.price && data.commerceEnabled) {
                    prod.data("sellOnline", data.sellOnline);
                    prod.find(".unit-price .price-label").text(featuredproducts.info.priceLabel);
                    prod.find(".unit-price .price").text(data.priceFormatted);
                    prod.data("price", data.price);

                    if(prodData("uom") != 'EA'){
                    	var uomText = featuredproducts.info.labelPerUom + " " + prodData.uomCodeDesc;
                        prod.find(".unit-size .unit-uom").text(uomText);
                    }

                    prod.find(".unit-price").show();
                    prod.find(".no-price").hide();
                    prod.find(".unit-size").show();

                    if (data.sellOnline && data.availableInventory > 0) {
                        btn.text(featuredproducts.info.labelAddToCart);
                        btn.attr('title',featuredproducts.info.labelAddToCart);
                        btn.removeClass('with-question');
                        qtn.hide();
                    } else {
                        btn.text(featuredproducts.info.labelRequestQuote);
                        btn.attr('title',featuredproducts.info.labelRequestQuote);
                        btn.addClass('with-question');
                        qtn.show();
                    }

                } else {
                    prod.find(".unit-price").hide();
                    prod.find(".no-price").show();

                    // if there is no price we want to display "Request Quote" for the button instead of "Add To Cart"
                    btn.text(featuredproducts.info.labelRequestQuote);
                    btn.attr('title', featuredproducts.info.labelRequestQuote);
                    btn.addClass('with-question');
                    qtn.show();
                }

                if (data.availableInventory <= 0) {
                    prod.find(".zero").show();
                    prod.find(".lead-time").show();
                } else if (data.availableInventory < data.lowQuantityThreshold) {
                    prod.find(".limited").show();
                    prod.find(".lead-time span").text(data.leadTimeDesc);
                    prod.find(".lead-time").show();
                } else {
                    prod.find(".ready").show();
                    prod.find(".lead-time span").text(data.leadTimeDesc);
                    prod.find(".lead-time").show();
                }

            });

        });
    }
});
$jq7("#productList").on("hover", ".availability.limited > span", function(e) {
    switch(e.type) {
        case "mouseenter":
            var popup = $jq7("#productListLimitedPopup");
            popup.removeClass("hidden");
            popup.addClass("tooltip");
            target = $jq7(e.target);
            prodBuy = target.parents(".product-buy");
            popup.width(prodBuy.width());
            popup.offset({
                left: prodBuy.offset().left,
                top: target.offset().top + target.height() + 10
            });
            break;
        case "mouseleave":
            var popup = $jq7("#productListLimitedPopup");
            popup.addClass("hidden");
            popup.removeClass("tooltip");
            break;
    }
});

$jq7("#productList").on("hover", ".purchase-button > .btn-question", function(e) {
    switch(e.type) {
        case "mouseenter":
            var popup = $jq7("#productListRFQInfoPopup");
            popup.removeClass("hidden");
            popup.addClass("tooltip");
            target = $jq7(e.target);
            prodBuy = target.parents(".product-buy");
            popup.width(prodBuy.width());
            popup.offset({
                left: prodBuy.offset().left,
                top: target.offset().top + target.height() + 10
            });
            break;
        case "mouseleave":
            var popup = $jq7("#productListRFQInfoPopup");
            popup.addClass("hidden");
            popup.removeClass("tooltip");
            break;
    }
});
$jq7(document).ready(function() {
	if ($jq7("#productList").length > 0) {
		$jq7("#productList").on("hover", ".product-availability", function() {
			$jq7(".availability > [title]").tooltip();
		});

		$jq7("#productList").on("hover", ".product-add-to-cart", function() {
			$jq7(".purchase-button > .btn-question").tooltip();
		});

		/*$jq7("input[type='number'][name='quant']").on("change", function(e) {
		    var val = parseInt($jq7(this).val(), 10);
		    var min = parseInt($jq7(this).attr("min"), 10);
		    var step = parseInt($jq7(this).attr("step"), 10);
//		    console.log(min, step, val);
		    while ((val - min) % step !== 0) {
		        val += 1;
		        $jq7(this).val(val);
		    }
		});*/
	}
});
/* script taken unaltered from searchbox.jsp -- unknown function */
/*<c:if test="${isEditMode or isDesignMode}">
    <script type="text/javascript">
        CQ.WCM.onEditableReady('${box.path}',
            function(editable) {
                //backup original method
                editable._show = editable.show;
                editable.show = function() {
                    <c:if test="${box.editable}">
                        this._show();
                    </c:if>
                }
                //hide the editbar
                <c:if test="${not box.editable}">
                    editable.hide();
                </c:if>
            }
        );
    </script>
</c:if>*/

/*<%--
<script>
// Code for showing placeholder text for IE7/8
 function add() {
    if($jq7(this).val() === ''){
      $jq7(this).val($jq7(this).attr('placeholder')).addClass('placeholder');
    }
  }

  function remove() {
    if($jq7(this).val() === $jq7(this).attr('placeholder')){
      $jq7(this).val('').removeClass('placeholder');
    }
  }

  // Create a dummy element for feature detection
  if (!('placeholder' in $jq7('<input>')[0])) {

    // Select the elements that have a placeholder attribute
    $jq7('input[placeholder], textarea[placeholder]').blur(add).focus(remove).each(add);

    // Remove the placeholder text before the form is submitted
    $jq7('form').submit(function(){
      $jq7(this).find('input[placeholder], textarea[placeholder]').each(remove);
    });
  }
</script>--%>*/
var posData = {
	  storeCode:"",
	  address1:"",
	  address2:"",
	  town:"",
	  region:"",
	  country:"",
	  postalCode:"",
	  phone1:"",
	  phone2:"",
	  website:"",
	  hourList:{},
	  dept:"",
	  email:"",
	  lat:"",
	  lng:"",
	  solutionList:{},
	  fax:"",
	  featureList:{}
};


var axeLocs = {
	 elements: {
		 posList:{}
	 },
	  init: function() {
		  $jq7('.axeLocUnit').each(function(){           
		       var idSelector="#"+ this.id;
		       var posInfo=$jq7(idSelector + " #posInfo");
		       var axeLoc=$jq7(idSelector + " #axeLoc");
		       var loadingImg=$jq7(idSelector + ' #loading');
				  if (typeof posInfo.data("hybrisposurl") !== 'undefined'){
					var posServiceURL = posInfo.data("hybrisposurl");
				  	var posCode= posInfo.attr("data-poscode");
				  	var searchType=posInfo.data("searchtype");	
				  	var siteBrand=posInfo.data("brand");
				  	axeLocs.requestPOSData(posServiceURL,searchType,posCode,siteBrand,idSelector);
				    
				  	
				    axeLoc.fadeIn(2000);
				    loadingImg.fadeOut(1500);   
				    
				   
				  }
		   });
		 
	 },
	requestPOSData:function( requestURI, searchType,  searchCode, siteBrand, idSelector) {
		getPosURL=requestURI;
		if(searchType==null||searchType==""){
			return;
		}
		if(searchType=="loc"){
			getPosURL+="/" + searchCode+ "?brand=" + siteBrand;   // +".json";
			
		}else{
			getPosURL+="/" + searchType + "?searchCode=" + searchCode + "&brand=" + siteBrand; //+"s.json"
		}
	

	 $jq7.getJSON( getPosURL,  function (data){
		
        	if(searchType=="loc"){
        		axeLocs.setUpLocationInfo(data,idSelector);
        	}else{
        		axeLocs.setUpMultLocInfo(data,idSelector);
        	}

})//end ajax to pos service
.always(function() {
         
})
 .fail(function(xhr, textStatus, errorThrown) {
        
});//fail for pos service
	},
setUpLocationInfo: function(data,idSelector){
	    var locNum=data.name;
	    $jq7(idSelector + ' #location-id').empty().append(locNum);
		var locData = $jq7('<h1>');
		if(data.address.hasOwnProperty('department')){
			if(data.address.department!=null){
				locData.append(data.address.department);
				$jq7(idSelector + ' #location-name').empty().append(locData);
			}else{
				locData.append(locNum)
			 $jq7(idSelector + ' #location-name').empty().append(locData);
			}
		}else{
			locData.append(locNum)
			 $jq7(idSelector + ' #location-name').empty().append(locData);
		}
		locData=$jq7('<span>');
		addr=axeLocs.getAddr(data);
		if(addr!=""){
			locData.append(addr);
			$jq7(idSelector + ' #location-address').empty().append(addr);
			$jq7(idSelector + ' #addressSection').removeClass("hidden");
		}
		var openingHours=data.openingHours;
		
		 if(openingHours!=null){
			 hoursList=openingHours.weekDayOpeningList;
			 if(hoursList!=null){
				 locData=$jq7('<span>');
				 hoursList.forEach(function(day) {
					 locData.append(day.weekDay + ":&nbsp;");
					 if(day.closed){
						 locData.append("Closed<br>");
					 }else{
						 var open=day.openingTime==null?"":day.openingTime.formattedHour;
						 var closeTime =day.closingTime==null?"":day.closingTime.formattedHour;
						 locData.append(open + "&nbsp;-&nbsp;"+ closeTime + "<br>");
					 }
					 
				});
				$jq7(idSelector + ' #location-hours').empty().append(locData);
			    $jq7(idSelector + ' #hoursSection').removeClass("hidden");
			 }
		 }
		
		 if(axeLocs.setButtonUrl($jq7(idSelector + ' #location-directions-url'),data)){
			 $jq7(idSelector + ' #directionsButtonSection').removeClass("hidden");
		 }
		
		 locData=axeLocs.getSolutions(data,"single");
		 if(locData.solutions!=""){
			 $jq7(idSelector + ' #location-solutions-numbers').empty().append(locData.solutions);
			 $jq7(idSelector + ' #solutionsSection').removeClass("hidden");
		 }
		 locData=$jq7('<span>');
		 var features= data.features;
		 if(features!=null){
			 for (var key in features) {
				 locData.append(features[key]+"<br>");
				}
			 $jq7(idSelector + ' #location-services').empty().append(locData);
			 $jq7(idSelector + ' #servicesSection').removeClass("hidden");
		 }
	},
	setUpMultLocInfo: function(data,idSelector){
		locTemplate=$jq7(idSelector + ' #location-component-list').clone();
		var allLocs=$jq7('<div>');
		var i = 0;
		 data.forEach(function(loc) {
			 i++;
			 if(i==1){
				 $jq7(idSelector + ' #region-header').empty().append(loc.address.region.name);
			 }
			 locData=locTemplate.clone();
			 var locNum=loc.name;
			 	locData.find('#location-id').empty().append(locNum);
			 if(loc.address.hasOwnProperty('department')){
				name=loc.address.department;
				if(loc.address.department!=null){
					locData.find('#location-name').empty().append(name);
			 	}else{
				 locData.find('#location-name').empty().append(locNum);
			 	}
		 	  }else{
				locData.find('#location-name').empty().append(locNum);
			  }
				var addr=axeLocs.getAddr(loc);
				
				locData.find('#location-address').empty().append(addr);
				solData=axeLocs.getSolutions(loc,"multiple");
				if(solData.solutions!=""){
					locData.find('#location-solutions-numbers').empty().append(solData.solutions);	
				}
				locData.find('#solutions-section-list').removeClass("hidden");
				if(solData.viewMore){
					locData.find('#solutionsViewMore').removeClass("hidden")
				}
				
				if(axeLocs.setButtonUrl(locData.find('#location-directions-url'),loc)){
					locData.find('#location-directions-url').removeClass("hidden");
				}
				
				 if(loc.address.hasOwnProperty('url') && loc.address.url!=null){
					 var locUrl=loc.address.url;
					 var canUrl = $jq7("link[rel='canonical']").attr("href");
					 var n = canUrl.indexOf('_');
					 var urlStart = canUrl.substring(0,n + 3);
					 var newUrl=urlStart+locUrl;
						locData.find('#location-detail-url').attr("href",newUrl);
						locData.find('#location-detail-url').removeClass("hidden");
				 }
				 
			allLocs.append(locData);
			
			 
		});
		$jq7(idSelector + ' #location-component-list').replaceWith(allLocs);
	},
	getAddr: function(data){
		var addr="";
		if(data.address.line1!=null){
			addr+=data.address.line1 + "<br>";
		}
		if(data.address.line2!=null){
			addr+=data.address.line2 + "<br>";
		}
		if(data.address.town!=null){
			addr+=data.address.town;
		}
		region=data.address.region;
		if(region!=null){
			state=data.address.region.anixterRegionCode;
			if(state!=null){
				addr+=",&nbsp;" + state;
			}
		}
		if(data.address.postalCode!=null){
			addr+="&nbsp;" + data.address.postalCode;
		}
		return addr;
	},
	getSolutions: function(data,pageType){
		var solData={
				solutions:"",
				viewMore:false
		}
		if(data.hasOwnProperty('departments')){
			 solutions=data.departments;
			 solDataList="";
			 i=0;
			 if(solutions!=null){
				solutions.forEach(function(dept) {
					 i++;
					 if(pageType=="single"||solutions.length<=4||i<=3){
						 solDataList+=(dept.deptName + ":&nbsp;"+ dept.phone + "<br>");
					 }
				});
				solData.solutions=solDataList;
				if(pageType=="multiple" && solutions.length>4){
					solData.viewMore=true;
				}
			 }
		}
		 return solData;
	},
	setButtonUrl:function(btn,data){
		buttonurl=btn.attr("href");
		 var lat=data.geoPoint.latitude;
		 var lng=data.geoPoint.longitude;
		 if(lat!=null && lng!=null){
			if(lat==0 && lng==0){
				return false;
			}else{
				 buttonurl=buttonurl.replace("LAT",lat);
				 buttonurl=buttonurl.replace("LONG",lng);
				 btn.attr("href", buttonurl);
				 return true;
			}
		 }else{
			 return false;
		 }
	}
	
};
	$jq7(function() {
	   axeLocs.init();
	});


var requestquote = {
    handlers: {
        changeCountry: function() {
            /*
             * Setup the State selection
             */
            common.utility.form.populateStateSelection($jq7("#rfq-state-input-field-container"), {
                selectedCountry: $jq7(this).val(),
                selectedLanguage: requestquote.info.currentLanguage,
                stateInputValue: (($jq7("#state").is("input")) ? $jq7("#state").val() : ""),
                required: true
            });
//			populateStateSelection( selectedCountry, selectedLanguage, stateInputValue );
        },
        clickReset: function(){
            /*
             * Clear error container when reset is pressed
             */
            var validator = $jq7("form#requestquote").validate();
            validator.resetForm();
        }
    },
    validators: {
        phone: function(phone_number, element) {
            phone_number = phone_number.replace(/\s+/g, "");
            var numberCount = 0;
            if (this.optional(element)) {
                return true;
            }
            if (phone_number.length > 0) {
                var numberArray = phone_number.split("");
                for (var i=0;i<numberArray.length;i++) {
                    if(!isNaN(numberArray[i])) {
                        numberCount++;
                    }
                }
            }
            return (numberCount >= 10);
        }
    }
};

/*
 * Submit the analytics.
 */
function submitQuoteAnalytics() {
	
	
	
	if(! $jq7('[name="hybrisCommerceEnabled"]').val()){
		return false;//-->UOB-322 If commerce not enabled, do not submit analytics.
	}
	
    var orderId = "";
    var timeStamp = new Date().getTime();
//        <%-- depending on if the user is logged in or not we want to format the order id differently --%>
    if (requestquote.info.sessionScopeaUsername.length > 0) {
        orderId = requestquote.info.sessionScopeaUsername + '-RFQ-' + timeStamp;
    } else {
        orderId = document.getElementById("country").value + '-RFQ-' + timeStamp;
    }
    var pauseSec = 150;

    var productsArray = [];
    var analyticsArray = [];
    var numItems=0;
    var shopCartTable = document.getElementById("shopping-cart");
    for (var i = 0, row; row = shopCartTable.rows[i]; i++) {
        var productsRow = {
            'sku':row.cells[2].innerHTML, // SKU will be using Anixter ID/SKU
            'name':row.cells[0].innerHTML, // Product Name will be using part description
            'category':row.cells[4].innerHTML, // Category will be using "product set"
            'price':'0.00', // Price placeholder
            'quantity':row.cells[3].innerHTML // Quantity will be using quantity
        };
        
        var analyticsRFQ = {
        		'parNumber':row.cells[2].innerHTML // SKU will be using Anixter ID/SKU		
        };
        
        var axeNum=row.cells[2].innerHTML;
        var mfrName=row.cells[0].innerHTML.substring(0,row.cells[0].innerHTML.indexOf(" - "));
        var desc = row.cells[0].innerHTML.substring(row.cells[0].innerHTML.indexOf(" - ")+ 3,row.cells[0].innerHTML.length);
        var quantity=row.cells[3].innerHTML;
        var label=axeNum + " | " + row.cells[1].innerHTML +  " | " + desc;
        numItems++;
      
        dataLayer.push({
        	'event':'gtm.formSubmit',
            'data-eventvar': 'RFQ',
            'data-categoryvar': 'RFQ',
            'data-actionvar':  mfrName,
            'data-labelvar': label,
            'data-valuevar':quantity
        });
        productsArray.push(productsRow);
        analyticsArray.push(analyticsRFQ);
        pauseSec+=200;
    }
   

    digitalData.linkName="rfq submit:success";
    digitalData.linkType="custom link";
    digitalData.linkCategory="rfq submit";
    digitalData.rfqDetailNumber = orderId;
    digitalData.products = analyticsArray;
    _satellite.track("RFQ Submit");
    
    dataLayer.push({
    	'event':'gtm.formSubmit',
        'data-eventvar': 'RFQ',
        'data-categoryvar': 'RFQ',
        'data-actionvar':  'RFQ',
        'data-labelvar': orderId,
        'data-valuevar': numItems
    });
    pauseSec+=200;
 
    // I'm certain there's a less brutal way to do this, but right now I'm not clear on the full scope of the problem, and thus do not wish to break anything. -Adrian
    common.utility.ga.pauseExec(pauseSec);
}

jQuery("document").ready( function($) {
    if ($jq7("#requestquote").length > 0) {
        $jq7(document).on("change.country", "#country", requestquote.handlers.changeCountry);

        $jq7(document).on("click.reset", "#btnReset", requestquote.handlers.clickReset);

        requestquote.errors = $jq7("#requestquote-form-error-container").data();

        requestquote.info = $jq7("#requestquote").data();
        
        
        /*
         * Subscribe the add-item-to-cart-dialog to shoppingcart addition events
         */
        $jq7( "#shopping-cart" ).makeShoppingCart( requestquote.info.shoppingCartPath, function( data ) {
            /*
             * Successful update function.  The "this" identifier in this context points to the container
             * which makeShoppingCart was called on
             */
            var shoppingCartTableBody = this.find( "tbody" ).first();
            shoppingCartTableBody.html( "" );

            for ( var i=0; i<data.response.items.length; i++ ) {
                var curItem = data.response.items[i];
                var curItemNameAndDescription = "";

                if ( curItem.mfgName && curItem.mfgName != "" ) {
                    curItemNameAndDescription = curItemNameAndDescription + curItem.mfgName + " - ";
                }
                curItemNameAndDescription = curItemNameAndDescription + curItem.name;
                if ( curItem.description && curItem.description != "" ) {
                    if(curItem.description != (curItem.name)){ //eDigital 871 - no duplicate description
                        curItemNameAndDescription = curItemNameAndDescription + " / " + curItem.description;
                    }
                }

                var curItemInfoCol = $jq7( "<td>"  + curItemNameAndDescription + "</td>");
                var mfgNumberCol = $jq7( "<td>" + curItem.mfgNum + "</td>" );
                var anixterNumberCol = $jq7( "<td>" + curItem.anixterId + "</td>" );
                var quantityCol = $jq7( "<td>" + curItem.quantity + "</td>" );
                var prodSetCol = $jq7( "<td>" + curItem.productSetName + "</td>" );

                var curItemDom = $jq7( "<tr/>" );
                curItemDom.append( curItemInfoCol );
                curItemDom.append( mfgNumberCol );
                curItemDom.append( anixterNumberCol );
                curItemDom.append( quantityCol );
                curItemDom.append( prodSetCol );
                curItemDom.append( "</tr>" );

                shoppingCartTableBody.append( curItemDom );
            }
        }, function( message ) {

        });

        $jq7.validator.addMethod("phone", requestquote.validators.phone, requestquote.errors.phoneNumberFormatMessage);

        requestquote.serverValidationErrorMessages = {
            Address_StateNotInCountry : requestquote.errors.addressStateNotInCountry,
            Address_PostalCodeNotInState : requestquote.errors.addressPostalCodeNotInState,
            Address_CityNotInState : requestquote.errors.addressCityNotInState,
            Address_InvalidCityPostalCombination : requestquote.errors.addressInvalidCityPostalCombination,
            Quote_AtLeastOneLine : requestquote.errors.quoteAtLeastOneLine
        };

        requestquote.validatedForm = $jq7( "form#requestquote" ).validate({
            errorContainer: "#requestquote-form-error-container",
            errorLabelContainer : "#requestquote-form-error-container-error-list",
            wrapper : "li",
            messages : {
                firstName: requestquote.errors.firstNameMissingMessage,
                lastName: requestquote.errors.lastNameMissingMessage,
                companyName: requestquote.errors.companyNameMissingMessage,
                emailAddress: {
                    required: requestquote.errors.emailAddressMissingMessage,
                    email: requestquote.errors.emailAddressEmailFormatMessage
                },
                streetAddress1: requestquote.errors.streetAddress1MissingMessage,
                city: requestquote.errors.cityMissingMessage,
                state: requestquote.errors.stateMissingMessage,
                country: requestquote.errors.countryMissingMessage,
                telephoneNumber: {
                    required: requestquote.errors.telephoneMissingMessage,
                    phone: requestquote.errors.phoneNumberFormatMessage
                },
                faxNumber: {
                    phone: requestquote.errors.faxNumberFormatMessage
                }
            },
            rules: {
                telephoneNumber: {
                    required: true,
                    phone: true
                },
                faxNumber: {
                    required: false,
                    phone: true
                },
                state: {
                    required: {
                        depends: function() {
                            return $jq7("#state").is("select");
                        }
                    }
                },
                country: {
                	required:true
                }
            },
            focusInvalid: false,
            invalidHandler : function(form, validator) {
                var errorPane = $jq7( "#requestquote-form-error-container");
                errorPane.show();
                if (!validator.numberOfInvalids()) return;

                $jq7('html, body').animate({
                    scrollTop: errorPane.offset().top - 20
                }, 1000);
                setTimeout(function() { $jq7(validator.errorList[0].element).focus(); }, 0);
            },
            submitHandler : function(form) {
                $jq7(form).ajaxSubmit( {
                    dataType : "json",
                    beforeSend : function () {
                        $jq7("form#requestquote input[type=submit]").attr("disabled", true);
                    },
                    error : function() {
                        window.location = requestquote.info.errorPagePath + ".html";
                    },
                    success : function(data) {
                    	
                    	
                        if (!data.success) {
                            //check to see if this is a validation issue
                            if (data.errorCode && data.errorCode == 400) {
                                var errorsToShow = {};

                                if (data.payload) {
                                    for (curPayloadItem in data.payload) {
                                        if (requestquote.serverValidationErrorMessages[data.payload[curPayloadItem]]) {
                                            errorsToShow[curPayloadItem] = requestquote.serverValidationErrorMessages[data.payload[curPayloadItem]];
                                        }
                                    }
                                }

                                /*
                                 * If the service call returned errors which the user can fix, we present them.
                                 * If not, we route the user to an error page.
                                 */
                                if (!$jq7.isEmptyObject(errorsToShow)) {
                                    requestquote.validatedForm.showErrors(errorsToShow);
                                    //focus on the error messages
                                    window.scroll(0,0);
                                } else {
                                    window.location = requestquote.info.errorPagePath + ".html";
                                }
                            } else {
                                //if errorCode was not sent or if it is not 400 send to the error page
                                window.location = requestquote.info.errorPagePath + ".html";
                            }
                        } else {
                            //if the form succeeds, forward onto the thank you page
                        	
                            submitQuoteAnalytics();
                            window.location = requestquote.info.axeThankYouPagePath + ".html";
                        }
                    }
                });
            }
        });

        common.utility.form.populateStateSelection($jq7("#rfq-state-input-field-container"), {
            selectedCountry: $jq7("#country").val(),
            selectedLanguage: requestquote.info.currentLanguage,
            stateInputValue: (($jq7("#state").is("input")) ? $jq7("#state").val() : ""),
            required: true
        });
//		populateStateSelection( selectedCountry, selectedLanguage, stateInputValue );

    }
});
function isNumeric(n) {
    //return !isNaN(parseInt(n)) && isFinite(n);
    return !isNaN(n) && !isNaN(parseInt(n)) && (parseFloat(n) == parseInt(n)) && isFinite(n);
}

var shoppingcart = shoppingcart || {
	/**
	 *
	 * @param success
	 * 				  <ul>
	 * 					<li>data: The data object returned by the shoppinglist request.  This object will take the form:
	 * 					  <ul>
	 * 						<li>Boolean success</li>
	 * 					    <li>Int returnCode</li>
	 * 					    <li>Object response : This is expected to be an array of shopping list items</li>
	 * 					  </ul>
	 * 				  </ul>
	 * @param failure A function which will be called in the case that the shopping cart request fails.
	 *                A failure is defined as an unsuccessful HTTP result or a returned message object
	 *                which indicates a failure has occurred.  The signature of the failure function
	 *                must be
	 *                <ul>
	 *                	<li>message: The message object indicating the reason for failure</li>
	 *                	<li>container: Optional - the jQuery element which was passed as the container to getShoppingCart will be
	 *                                 passed to the failure function.</li>
	 *                </ul>
	 *
	 */
	getShoppingCart: function( container, requestURI, success, failure ) {
	    $jq7.ajax( requestURI,{
	        accepts : "application/json",
	        dataType : "json",
	        cache : false,
	        error : function( jqXHR, textStatus, errorThrown) {
	            /*
	             * Error is triggered if the request fails completely.  This being the case
	             * we presume that the returned message is not in an acceptable format
	             */
	            return failure.apply(container, [{
	                success : false,
	                returnCode : jqXHR.status,
	                response : {
	                    message : errorThrown
	                }
	            }]);
	        },
	        success : function( data, textStatus, jqXHR ) {
	            if ( ! data || ! data.success ) {
	                return failure( data, container );
	            }

	            success.apply( container, [data] );

	        }
	    });
	},
	/**
	 *
	 * This method is used both to add an item to a Shopping Cart and to update an item within an existing cart.  The
	 * Shopping Cart is keyed by Anixter product ID and as such, if a product with the same Anixter ID as a product already
	 * in the Shopping Cart is passed to this method, assuming success the existing product in the Shopping Cart is overwritten
	 * by the new product.
	 *
	 * Removing an item from the Shopping Cart can also be accomplished by using this method, defining a quantity of "0"
	 * in the itemObj.  In this case, the only germane attributes of itemObj are anixterId and quantity.
	 *
	 * @param serviceURI The URI of the shopping cart manipulation service
	 * @param itemObj A JS object representing the item to add to the Shopping Cart.  Valid object attributes are
	 * 		  <ul>
	 * 			<li>anixterId</li>
	 *          <li>name</li>
	 *          <li>url</li>
	 *          <li>mfgName</li>
	 *          <li>mfgNum</li>
	 *          <li>specSheetUrl</li>
	 *          <li>imageUrlSmall</li>
	 *          <li>imageUrlMedium</li>
	 *          <li>imageUrlLarge</li>
	 *          <li>price</li>
	 *          <li>priceUnit</li>
	 *          <li>quantity</li>
	 *          <li>quantityQualifier</li>
	 *          <li>productSetName</li>
	 *          <li>productSetUrl</li>
	 *        </ul>
	 * @param success The function to call upon successful manipulation. This function will be passed the data object returned
	 *                as the response to the manipulation
	 * @param failure The function to call in the case where the manipulation failed. If failure was due to an HTTP error, an
	 *                object of the following form will be passed to the function:
	 *                <ul>
	 *                  <li>success : Boolean</li>
	 *                  <li>returnCode : Int - the HTTP status code</li>
	 *                  <li>respone : String - the message provided</li>
	 *                </ul>
	 *                If the failure was due to a bad update as determined by the manipulation functionality, the data object
	 *                returned from the server will have the same form as above but will most likely contain more verbose information
	 *                in the response string.
	 */
	manipulateItemInCart: function( serviceURI, itemObj, success, failure ) {
	    /*
	     * First make sure a quantity was included. If the user has not defined a quantity
	     * then we need to ask them for one
	     */
	    if (itemObj.quantity == 'remove' || (isNumeric(itemObj.quantity) && itemObj.quantity >= itemObj.minimum && itemObj.quantity % itemObj.increment === 0)) {
	        $jq7.ajax( serviceURI, {
	            data : itemObj,
	            type : "POST",
	            error : function( jqXHR, textStatus, errorThrown ) {
	                return failure({
	                    success : false,
	                    returnCode : jqXHR.status,
	                    response : {
	                        message : errorThrown
	                    }
	                });
	            },
	            success : function( data, textStatus, jqXHR ) {
	                if ( ! data || ! data.success ) {
	                    if ( failure ) {
	                        return failure( data );
	                    }
	                    return null;
	                }

	                if ( success ) {
	                    success( data );
	                }

	                var responseData = data.response || {};

	                var updateType = responseData.updateType || "update";

	                $jq7.publish( "anixter.shoppingcartupdate." + updateType, [ responseData ] );
	            }
	        });
	    } else if( shoppingcart.requestQuantityForItem ) {
	        /*
	         * Only ask for quantity if a method has been defined to handle it
	         */
	        shoppingcart.requestQuantityForItem( serviceURI, itemObj, success, failure );
	    }
	},
	/*
	 * Establish the request for quantity functionality
	 */
	requestQuantityForItem: function( serviceURI, itemObj, success, failure ) {
//		console.log("requestQuantityForItem", serviceURI, itemObj);

		var requestQuantityContainer = $jq7( "#shopping-cart-quantity-needed-dialog" );

		//clear out the input field as it might contain invalid characters
		$jq7(requestQuantityContainer).find('.product-add input').val('');

		//show only the appropriate message for the scenario
		var minDescContainer = $jq7(requestQuantityContainer).find(".description-minimum");
		var minDesc = minDescContainer.data("default-value");
		var minVar = "${minimum}"; // The token to search for and replace
		if (itemObj.quantity <= itemObj.minimum) {
			minDescContainer.text(minDesc.replace(minVar, itemObj.minimum));
			minDescContainer.show();
		}

		var incDescContainer = $jq7(requestQuantityContainer).find(".description-increment");
		var incDesc = incDescContainer.data("default-value");
		var incVar = "${increment}"; // The token to search for and replace
		if (itemObj.quantity % itemObj.increment !== 0 && itemObj.quantity !== 0) {
			incDescContainer.text(incDesc.replace(incVar, itemObj.increment));
			incDescContainer.show();
		}

		/*
		 * Populate the quantity qualifier label if one was provided
		 */
		if ( itemObj.baseUOM && itemObj.baseUOMDesc &&  itemObj.baseUOM !== "EA" ) {
			requestQuantityContainer.find( "#shopping-cart-quantity-needed-quantity-qualifier-label" ).first().html( itemObj.baseUOMDesc );
		}

		var getQuantityFunction = function() {
			var retObj = {};

			requestQuantityContainer.find( "input.quantity-input-field").each( function() {
				if ( $jq7( this ).val() != "" ) {
					retObj[ $jq7( this ).attr( "name" ) ] = $jq7( this ).val().replace(/^0+/, '');
				}
			});

			return retObj;
		}

		var curSuccessFunction = success;

		var getQuantitySuccessFunction = function( data ) {

			requestQuantityContainer.dialog( 'close' );
			minDescContainer.hide();
			incDescContainer.hide();
			if( curSuccessFunction ) {
				curSuccessFunction( data );
			}
		}
		requestQuantityContainer.find( "#shopping-cart-quantity-needed-button" ).makeUpdateCartButton(
			serviceURI,
			itemObj,
			getQuantityFunction,
			null,
			getQuantitySuccessFunction,
			failure
		);

		requestQuantityContainer.dialog({
			position: "center",
			width: 300,
			minHeight: 120,
			modal: true,
			resizable: false,
			autoOpen: true,
			draggable: false,
			resizable: false,
			zIndex: 9999
		});
	},
	requestAccessoriesForProduct: function( requestURI, productId, container, success, failure ) {
		$jq7.ajax( requestURI + "?anixterId=" + productId, {
	        accepts : "application/json",
	        dataType : "json",
	        cache : false,
	        error : function( jqXHR, textStatus, errorThrown) {
	            if ( failure ) {
	                return failure.apply (
	                    container,
	                    [{
	                    success : false,
	                    returnCode : jqXHR.status,
	                    response : { message : errorThrown }
	                    }]
	                );
	            }
	        },
	        success : function( data, textStatus, jqXHR ) {
	            if ( ! data || ! data.success ) {
	                if ( failure ) {
	                    return failure.apply (container, [{
	                        success : false,
	                        returnCode : 0,
	                        response : data
	                    }]);
	                }
	            }

	            success.apply( container, [data] );
	        }
	    });
	},
	/**
	 *
	 * @param itemObj The object containing all known item properties, presumably without a quantity
	 * @param trObj Text replacement object containing the text to present in the popup.  The following properties are
	 *              used in this message.
	 *              <ul>
	 *              	<li>closeBtnTxt : The text to show in the close button</li>
	 *              	<li>whatQuantityTxt : The primary text question shown in the overlay</li>
	 *              	<li>qtyTxt : The label for the quantity field</li>
	 *              	<li>goTxt : The text to present in the GO button</li>
	 *              </ul>
	 */
	noQuantityErrorOverlay: function( itemObj, trObj ) {
	    var closeBtnTxt = trObj.closeBtnTxt || "Close";
	    var whatQuantityTxt = trObj.whatQuantityTxt || "How much would you like to buy?";
	    var qtyTxt = trObj.qtyTxt || "Qty:";
	    var goTxt = trObj.goTxt || "GO";

	    var popupDiv = jQuery( "<div style='position:absolute; z-index:1000; top:400; left:400;' class='overlay no-quantity-overlay' />" );
	    var closeButton = jQuery( "<span style='background-color:#CCCCCC; color:#FFFFFF;' />" );
	}
};

(function( $ ) {

    var methods = {
        init : function( requestURI, success, failure ) {

            var container = this;

            methods.update.apply( container, [requestURI, success, failure] );

            $jq7.subscribe( "anixter.shoppingcartupdate.add anixter.shoppingcartupdate.update anixter.shoppingcartupdate.remove", function( e, response ) {
                methods.update.apply( container, [requestURI, success, failure] );
            });
        },
        update : function( requestURI, success, failure ) {
            shoppingcart.getShoppingCart( this, requestURI, success, failure );
        }
    };

    /**
     * Treat this as a shopping cart
     *
     * @param requestURI See shoppingcart.getShoppingCart requestURI
     * @param success See shoppingcart.getShoppingCart success
     * @param failure See shoppingcart.getShoppingCart failure
     */
    $jq7.fn.makeShoppingCart = function( requestURI, success, failure ) {
        methods.init.apply( this, [requestURI, success, failure] );
    };
})( jQuery );

(function( $ ) {
    var methods = {};

    /**
     * @param serviceURI The URI which will be called when the button is pressed
     * @param itemObj A Javascript object containing default item properties associated with this button
     * @param propertyFunction An optional function for gathering additional properties prior to submission
     * @param propertyFunctionContext The context in which the propertyFunction will be called.  The default is the object on which the plugin was called
     * @param success An optional function to be run if the request succeeds
     * @param failure An optional function to be run if the request fails
     */
    $jq7.fn.makeUpdateCartButton = function( serviceURI, itemObj, propertyFunction, propertyFunctionContext, success, failure ) {
        this.off( "click.shoppingCart" );
        this.on( "click.shoppingCart", function(event) {
        	//console.log("clicked update cart button", serviceURI, itemObj, propertyFunction, propertyFunctionContext, success, failure);
            
        	event = event || window.event //For IE
        	
        	var itemData = itemObj || {};

            if ( propertyFunction ) {
                var context = propertyFunctionContext || this;
                var additionalProperties = propertyFunction.apply( context );

                for( var curPropertyKey in additionalProperties ) {
                    itemData[ curPropertyKey ] = additionalProperties[ curPropertyKey ];
                }
            }

            shoppingcart.manipulateItemInCart( serviceURI, itemData, success, failure );

            if (event.preventDefault) { 
            	event.preventDefault(); 
            } else { 
            	event.returnValue = false; 
            }
            
        });
    };
})( jQuery );

var shoppinglist = shoppinglist || {};

$jq7(document).ready(function(){
	if ($jq7("#shoppinglist").length > 0) {
		shoppinglist.info = $jq7("#shoppinglist").data();

	    $jq7( ".shopping-list-body" ).makeShoppingCart( shoppinglist.info.shoppingCartPath,
	        /*
	         * Successful update function.  The this identifier in this context identifies the container
	         * makeShoppingCart was called on
	         */
	        function( data ) {
	            /*
	             * Clear the Shopping Cart
	             */
	            this.html( "" );

	            var shoppingCartList = $jq7( "<ul/>" );

	            for ( var i=0; i<data.response.items.length; i++ ) {
	                var curItem = data.response.items[i];

	                var curItemDom = $jq7( "<li />" );
	                curItemDom.html( curItem.name + " : " + curItem.anixterId + " : quantity " );

	                var quantityInput = $jq7( "<input type='number' name='quantity' class='shopping-cart-input' value='" + curItem.quantity + "' />" );
	                curItemDom.append( quantityInput );

	                var updateButton = $jq7( "<span>update</span>" );
	                updateButton.makeUpdateCartButton(shoppinglist.info.shoppingCartAddPath, curItem, function() {
	                    return { quantity : this.val() };
	                }, quantityInput);

	                var removeButton = $jq7( "<span>remove</span>" );
	                removeButton.makeUpdateCartButton(shoppinglist.info.shoppingCartAddPath, curItem, function() {
	                    return { quantity: "remove" };
	                });

	                curItemDom.append( updateButton );
	                curItemDom.append( removeButton );

	                shoppingCartList.append( curItemDom );
	            }
	            this.append( shoppingCartList );
	        },
	        function( message ) { }
	    );

	    $jq7( "#submit-button" ).makeUpdateCartButton(shoppinglist.info.shoppingCartAddPath, null, function() {
	        var updateObject = {};
	        $jq7( ".shopping-list-input" ).each( function() {
	    		if ( $jq7( this ).val() != "" ) {
	                updateObject[ $jq7( this ).attr( "name" ) ] = $jq7( this ).val();
	            }
	        });
	        return updateObject;
	    });
	}
});
var drawer = drawer || {};

$jq7(document).ready(function(){
    if ($jq7("#drawer").length > 0) {
        drawer.info = $jq7("#drawer").data();

        $jq7( "#" + drawer.info.name + "-shopping-cart" ).makeShoppingCart( drawer.info.shoppingCartPath, function( data ) {
            /*
             * Successful update function.  The "this" identifier in this context points to the container
             * which makeShoppingCart was called on
             */
            this.html("");
            //var container = $jq7("<ul/>");
            var containerPriced = $jq7("<ul class='drawer-items-section drawer-items-priced'></ul>");
            var containerQuote = $jq7("<ul class='drawer-items-section drawer-items-quote'></ul>")
            var subtotal = 0;
            var rfqCount = 0;
            var priceCount = 0;

            for ( var i=0; i<data.response.items.length; i++ ) {
                var curItem = data.response.items[i];
                var imgSrc = (typeof(curItem.imageUrlSmall) === "undefined") ? drawer.info.shoppingCartDrawerDefaultImage : (curItem.imageUrlSmall.indexOf("V8") >= 0) ? curItem.imageUrlSmall : drawer.info.shoppingCartDrawerDefaultImage;
                var manufacturer = (curItem.mfgName) ? $jq7.trim(curItem.mfgName) : "";
                var name = (curItem.name) ? $jq7.trim(curItem.name) : "";
                var description = (curItem.description) ? $jq7.trim(curItem.description) : "";
                var title = "";
                if (manufacturer !== "") title += manufacturer + " - ";
                if (name !== "") title += name;
                if (description !== "" && name !== description) title += " / " + description;
                var url = (curItem.url) ? $jq7.trim(curItem.url) : "#";
                var quantity = (curItem.quantity) ? parseInt($jq7.trim(curItem.quantity), 10) : "";
                var minimum = (curItem.minimum) ? parseInt($jq7.trim(curItem.minimum), 10) : 1;
                var increment = (curItem.increment) ? parseInt($jq7.trim(curItem.increment), 10) : 1;
                var baseUOMDesc = (curItem.baseUOMDesc) ? $jq7.trim(curItem.baseUOMDesc) : "";
                var id = (curItem.anixterId) ? $jq7.trim(curItem.anixterId) : "";
                    id = $jq7.trim(id.split("#").slice().pop()); // if the ID contains "Anixter #" this splits the string at that point and returns the last of the resulting array items, which should be the actual ID number, and finally removes any spaces that might have ended up at the beginning of the new id string.
                var mfg = (curItem.mfgNum) ? $jq7.trim(curItem.mfgNum) : "";
                    mfg = $jq7.trim(mfg.split("#").slice().pop());

                var curItemDom = $jq7(""
                    + "<li class='drawer-item'>"
                        + "<div class='drawer-item-col'>"
                            + "<ul class='drawer-item-imagecol'>"
                                + "<li>"
                                    + "<a href='" + url + "' class='drawer-item-image-link' title='" + name + "'>"
                                        + '<img class="drawer-item-image cart-product-thumb" onerror="this.onerror=null;if(\'' + drawer.info.shoppingCartDrawerDefaultImage + '\'.length > 0) { this.src=\'' + drawer.info.shoppingCartDrawerDefaultImage + '\';}" src="' + imgSrc + '">'
                                    + "</a>"
                                + "</li>"
                            + "</ul>"
                        + "</div>"
                        + "<div class='drawer-item-col'>"
                            + "<ul class='drawer-item-info'>"
                                + "<li class='drawer-item-title'>"
                                    + "<p>"
                                        + "<a href='" + url + "' class='drawer-item-title-link' title='" + title + "'>"
                                            + title
                                        + "</a>"
                                    + "</p>"
                                + "</li>"
                                + "<li class='drawer-item-quantity'><span class='attribute-title'>" + drawer.info.shoppingCartDrawerQtyText + " </span><span class='attribute-value'>" + quantity + " " + baseUOMDesc + "</span></li>"
                                + "<li class='drawer-item-price'></li>"
                            + "</ul>"
                        + "</div>"
                    + "</li>");

                $jq7.ajax({
                    url:        drawer.info.shoppingCartDrawerPath + ".accessories.json?anixterId=" + id,
                    type:       "GET",
                    async:      false,
                    success:    function(data) {
                        var productList = data;

                        if (productList && productList.length) {
                            var accessories = $jq7( "<li><a href='#' class='drawer-item-accessories'>" + drawer.info.shoppingCartDrawerAccessoriesText + "</a></li>" );
                            accessories.makeUpdateCartButton( "/bin/shoppingCart/add", curItem, function() {
                                return {
                                    quantity: quantity,
                                    minimum: minimum,
                                    increment: increment
                                };
                            });
                            curItemDom.find(".drawer-item-imagecol").append(accessories);
                        }
                    }
                });

                var rfqItem = true;
                $jq7.ajax({
                    url:        drawer.info.shoppingCartDrawerPath + ".partrfq.html?anixterId=" + id + "&quantity=" + curItem.quantity,
                    type:       "GET",
                    async:      false,
                    success:    function(data) {
                        if (data === 204) { rfqItem = false; }
                    }
                });

                if (drawer.info.commerceEnabled) {
                    $jq7.ajax({
                        url:        drawer.info.shoppingCartDrawerPath + ".priceinventory.json?anixterId=" + id + "&conversion=" + curItem.conversionFactor + "&qnty=" + curItem.quantity,
                        type:       "GET",
                        dataType:   "json",
                        async:      false,
                        success:    function(data) {
                            var productDetails = data;
                            var extendedPrice      = productDetails.extendedPrice;
                            var extPriceF          = productDetails.formattedExtPrice;
                            var priceF             = productDetails.priceFormatted;
                            if (!rfqItem) {
                                curItemDom.find(".drawer-item-price").html("<span class='attribute-value'>" + extPriceF + "</span>");
                                containerPriced.append(curItemDom);
                                subtotal += extendedPrice;
                                priceCount += 1;
                            } else {
                                if (extPriceF){
                                    curItemDom.find(".drawer-item-price").html("<span class='attribute-value'>" + extPriceF + "</span>");
                                } else {
                                    curItemDom.find(".drawer-item-price").html("<span class='attribute-value'>" + drawer.info.pricingNotAvailableMessage + "</span>");
                                }
                                containerQuote.append(curItemDom);
                                rfqCount += 1;
                            }
                        }
                    });
                } else {
                    curItemDom.find(".drawer-item-price").html("<span class='attribute-value'>" + drawer.info.pricingNotAvailableMessage + "</span>");
                    containerQuote.append(curItemDom);
                    rfqCount += 1;
                }
            }

            $jq7('#cart-status').html("");
            var button = $jq7("#callToActionDrawer");
            if (priceCount == 0 && rfqCount == 0) {
                button.html("");
                $jq7('ul.cart-controls').find('li').hide();
            } else {
                $jq7( "#" + drawer.info.name + "-cart-button" ).attr('href', drawer.info.shoppingCartPageUrl);
                $jq7('ul.cart-controls').find('li').show();
                var subtotalDisplay = 0.00;


                if (drawer.info.commerceEnabled === "true") {
                    button.html("<a href='" + drawer.info.shoppingCartOrderUrl + "' class='btn btn-small' title='" + drawer.info.shoppingCartDrawerOrderText + "'>" + drawer.info.shoppingCartDrawerOrderText + "</a>");
                    $jq7("#cart-status").append("<li class='drawer-priced-subtotal clearfix'><span class='attribute-title drawer-priced-subtotal-label'>" + drawer.info.shoppingCartDrawerSubtotalTabText + " </span><span class='attribute-value drawer-priced-subtotal-value'>" + subtotalDisplay + "</span></li>");
                } else {
                    button.html("<a href='" + drawer.info.shoppingCartRequestAQuoteUrl + "' class='btn btn-small' title='" + drawer.info.shoppingCartDrawerRequestAQuoteText + "'>" + drawer.info.shoppingCartDrawerRequestAQuoteText + "</a>");
                }

                $jq7("#cart-status").append("<li class='drawer-rfq-count clearfix'><span class='attribute-title drawer-rfq-count-label'>" + drawer.info.shoppingCartDrawerRfqText + " </span><span id='rfqCount' class='attribute-value drawer-rfq-count-value'>" + rfqCount + "</span></li>");
            }

            if (priceCount > 0) {
                var containerPricedLI = $jq7("<li />").append(containerPriced);
                this.append("<li class='drawer-items-header drawer-items-header-priced'><span  class='header4 drawer-header'>" + drawer.info.shoppingCartDrawerPricedItemsHeaderText + "</span></li>")
                    .append(containerPricedLI);
            }

            if (rfqCount > 0) {
                var containerQuoteLI = $jq7("<li />").append(containerQuote);
                this.append("<li class='drawer-items-header drawer-items-header-quote'><span  class='header4 drawer-header'>" + drawer.info.shoppingCartDrawerQuoteItemsHeaderText + "</span></li>")
                    .append(containerQuoteLI);
            }

            if (priceCount === 0 && rfqCount === 0) {
                this.append("<li class='drawer-items-header drawer-items-header-quote'><span  class='header4 drawer-header'>" + drawer.info.shoppingCartDrawerEmptyCartMessageText + "</span></li>");
            }
            //this.append(container);
            $jq7( "#" + drawer.info.name + "-cart-button" ).html( "<i class='icon icon-cart'></i>" + drawer.info.shoppingCartDrawerLabel + " (" + data.response.items.length + ")<span class='btn-edge'></span>" );

        }, function( message ) {

        });
    }
});
var common = common || {
    utility : {
        form: {
            populateStateSelection: function(stateInputFieldContainer, opts) { //function(selectedCountry, selectedLanguage, fieldName, stateInputVal, selectedState, placeholderVal, stateInputFieldContainer, isReq) {
                /*
                 * Defining default values:
                 */
                //opts.selectedCountry = opts.selectedCountry || "us";
                //opts.selectedLanguage = opts.selectedLanguage || "en";
                opts.fieldName = opts.fieldName || "state";
                opts.stateInputValue = opts.stateInputValue || "";
                opts.selectedState = opts.selectedState || "";
                opts.placeholderValue = opts.placeholderValue || "";
                opts.required = opts.required || false;

                /*
                 * anixter.forms.utilities.populateStateSelection(selectedCountry, selectedLanguage, fieldName, stateInputVal, selectedState, placeholderVal, stateInputFieldContainer, isReq) {
                 * purpose:
                 * Create and populate the state/province list -- if no such list exists, a plaintext input is output instead
                 * parameters:
                 * stateInputFieldContainer - a reference to the DOM element into which the generated <select> or <input> field should be placed
                 * opts - a configuration object:
                 * 	+	selectedCountry - the ISO code of the country to use when populating the state/province list
                 * 	+	selectedLanguage - the ISO code of the current language to use when populating the state/province list
                 * 	+	fieldName - the name and id for the element, for the form querystring
                 * 	+	stateInputValue - the plaintext value of the stored state/province; used only if no list exists
                 * 	+	selectedState - the ISO code of the stored state/province; used to determine which <option> should be active
                 * 	+	placeholderValue - the plaintext label which should be placed as the first element in the list, serving as a placeholder
                 * 	+	required - whether the field is required
                 */

                var countryIsDefined = (typeof(opts.selectedCountry) !== "undefined" && opts.selectedCountry !== null && opts.selectedCountry !== ""),
                    languageIsDefined = (typeof(opts.selectedLanguage) !== "undefined" && opts.selectedLanguage !== null && opts.selectedLanguage !== ""),
                    elementIsDefined = (typeof(stateInputFieldContainer) !== "undefined" && stateInputFieldContainer !== null);

                if (countryIsDefined && languageIsDefined && elementIsDefined) {
                    $jq7.ajax( "/bin/statelist" , {
                        data: {
                            country: opts.selectedCountry,
                            language: opts.selectedLanguage
                        },
                        dataType: "json",
                        type: "GET",
                        success: function(data) {
                            stateInputFieldContainer.html("");
                            // whether the field should be marked as required
                            var attrRequired = (opts.required) ? "class='required'" : "";
                            if (data.length) { // if we have a state/province list for the selected country, append it to the DOM
                                var stateInputField = $jq7("<select name='" + opts.fieldName + "' id='" + opts.fieldName + "' " + attrRequired + "/>");
                                // if there is no selected state, the placeholder option is selected
                                var attrSelected = (opts.selectedState.length == 0) ? "" : " selected='selected'";
                                stateInputField.append("<option" + attrSelected + " value=''>" + opts.placeholderValue + "</option>");
                                // otherwise, whichever state is passed in will be selected
                                for (var i = 0; i < data.length; i ++) {
                                    var curState = data[i];
                                    if (!curState.hideFromDropdown) {
                                        var attrSelected = (opts.selectedState == curState.isoCode) ? " selected='selected'" : "";
                                        var curStateOption = $jq7("<option" + attrSelected + " value='" + curState.isoCode + "'>" + curState.label + "</option>");
                                        stateInputField.append(curStateOption);
                                    }
                                }
                            } else { // otherwise, just place a text field in the DOM
                                var stateInputField = $jq7("<input type='text' name='" + opts.fieldName + "' id='" + opts.fieldName +"' value='" + opts.stateInputValue + "' SIZE='30' maxlength='30' " + attrRequired + "/>");
                            }
                            stateInputFieldContainer.append(stateInputField);
                        }
                    });
                }
            }
        },
        ga: {
            pauseExec: function(millis) {
                var date = new Date();
                var curDate = null;

			    do { curDate = new Date(); }
			    while (curDate-date < millis);
			}
		},
		data: {
			// encode(decode) html text into html entity -- https://gist.github.com/CatTail/4174511
			decodeHtmlEntities: function(str) {
				if(str){
					return str.replace(/&#(\d+);/g, function(match, dec) {
						return String.fromCharCode(dec);
					});
				}
				return '';
			},
			encodeHtmlEntities: function(str) {
				var buf = [];
				if(str){
					for (var i=str.length-1;i>=0;i--) {
						if (typeof str[i] !== "undefined"){
                         buf.splice(0,0,'&#' + str[i].charCodeAt() + ';');
                     }                    
					}
				}
				return buf.join('');
			},
			isEmpty: function(prop, obj) {
				var d = common.utility.data;
				if (typeof(obj) != "undefined" && obj != null) {
					return (!obj.hasOwnProperty(prop)							// if property does not exist -- mostly here for IE7/8
							|| typeof(obj[prop]) == "undefined"					//
							|| obj[prop] == null								// if property is defined, but has a null value
							|| obj[prop].length == 0							// if property is an array or string with no contents
							|| (typeof(prop) == "string" && d.trim(prop) == ""));	// if property is a string with no contents or only spaces
				} else {
					return (typeof(prop) == "undefined" || prop == null || prop.length == 0 || (typeof(prop) == "string" && d.trim(prop) == ""));
				}
			},
			propDefault: function(prop, defaultVal, obj) {
				if(typeof(obj) != "undefined" && obj != null) {
					return isEmpty(prop, obj) ? obj[prop] : defaultVal;
				} else {
					return isEmpty(prop, obj) ? prop : defaultVal;
				}
			},
			trim: function(str) {
				return str.replace(/^\s+|\s+$/g, '');
			}
		}
	}
};

function followUsClick(socialNetwork) {
		digitalData.linkName = 'social follow:' + socialNetwork;
		digitalData.linkType = 'exit link';
		digitalData.linkCategory = 'social follow';
		_satellite.track("click tracking");
};


function tagAddToCart(event, label, category) {
	dataLayer.push(
		{ 
			'data-eventvar': event, 
			'data-actionvar' : 'click', 
			'data-labelvar' : label, 
			'data-categoryvar': category
		});
	
	
	var fields = label.split('|');
	var part = fields[0];
	
	digitalData.products = [
	    {
            partNumber: part,
        }

    ];
	
	var count = $jq7('#rfqCount').text();
	if (count > 0) {
		_satellite.track("cart_add");	
	} else {
		_satellite.track("cart_open"); 		
	}
	
	return true;
}


function trackBanner(campaignName) {
    
    dataLayer.push({'data-eventvar':'banner','data-actionvar':'click','data-labelvar':campaignName,'data-categoryvar':'banner' });
    digitalData.linkName=campaignName;
    digitalData.linkType="custom link";
    digitalData.linkCategory="internal campaign";
    _satellite.track("click tracking")
    return true;
}

/*
// Most of these don't qualify for de-duplication, since they each only have one instance:

editBtnHandler: function() {}, // addressbook.js
deleteBtnHandler: function() {}, // addressbook.js
submitAnalyticsData: function() {}, // placeorder.js
fixRowHeights: function() {}, // shipping.js
calculateCost: function() {}, // shipping.js
resetShipToLabels: function() {}, // shipping.js

// search.js:
activateAddToCartButtons
activateAddToCartButton
setPage1
hideFeaturedProducts
toggleFeaturedProducts
updateSearchResults
setContentPage1
updateContentSearchResults
getInitialContentSearchResults

imgError // drawer.js
// NEED TO FIX UTILITYNAV.JS*/
common.template = common.template || {
    /*
     * makeSearchResultsProductList
     *
     * outputs a string representing the HTML generated from the supplied data
     *
     * parameters:
     * +	arr		-	<array> 	an array of objects representing the product list items to be displayed
     * +	grid	-	<boolean> 	if true, generates a grid view; otherwise, generates a list view
     * +	pl		-	<object>	the data-* attributes from the component container
     *
     * return value:
     * +	html	-	<string>	a string of HTML to be inserted into the DOM with .innerHTML() or $jq7.append()
     */
    makeSearchResultsProductList: function(arr, grid, pl) {
    	var encodeHtmlEntities = common.utility.data.encodeHtmlEntities;
    	var listGrid = (grid) ? "products-grid" : "products-list";
        var html = '<div id="searchResults" class="products-results ' + listGrid + '">';
        if (Object.prototype.toString.call(arr) === "[object Array]") {
            var list = JSON.parse(JSON.stringify(arr)); // clone the data in case we need the original again later
            while (list.length > 0) {
                // make row container
                html += '<div class="products-row clearfix">';
                var pageGridViewItemsPerRow = parseInt(pl.gridViewItemsPerRow, 10);
                var howManyPerRow = (list.length >= pageGridViewItemsPerRow) ? pageGridViewItemsPerRow : list.length;
                for (var i = 0; i < howManyPerRow; i ++) {
                    var obj = list.shift();

                    
                    var gaqValue = "";
                    
                    if (obj.displayProps.anixterNumber) {
                    	gaqValue = obj.displayProps.anixterNumber;
                    } 
                    
                    if (obj.displayProps.manufacturerName) {
                    	gaqValue = gaqValue + " | " + obj.displayProps.manufacturerName;
                    } else {
                    	gaqValue = gaqValue + " | " + "none";
                    }
                    
                    if (obj.displayProps.manufacturerNumber) {
                    	gaqValue = gaqValue + " | " + obj.displayProps.manufacturerNumber;
                    } else {
                    	gaqValue = gaqValue + " | " + "none";
                    }
                    
                    gaqValue = encodeHtmlEntities(gaqValue);
                    
                    var dataEventVarAdd = "'added-to-cart'";
                    var dataActionVar = "'click'";
                    var dataLabelVar = "'" + gaqValue + "'";
                    var dataCategoryVarAdd = "'non-add-searchresult'";
                    
                    var uaLinkAdd = "onclick=\"tagAddToCart(" + dataEventVarAdd + ", " + dataLabelVar + ", 'data-categoryvar' : " + dataCategoryVarAdd + ")\"" ;
                    
                    var o = {
                        caption: obj.caption,
                        commerceEnabled: obj.commerceEnabled == 'false' ? !obj.commerceEnabled : !!obj.commerceEnabled,
                        priceFormatted: obj.priceDisplay,
                        leadTimeDescription: obj.leadTimeDescription,

                        anixterNumber: obj.displayProps.anixterNumber,
                        baseUOM: obj.displayProps.baseUOM,
                        baseUOMDesc: obj.displayProps.baseUOMDesc,
                        description: obj.displayProps.description,
                        imageExists: obj.displayProps.imageExists,
                        manufacturerNumber: obj.displayProps.manufacturerNumber,
                        manufacturerName: obj.displayProps.manufacturerName,
                        prodDetailURL: obj.displayProps.prodDetailURL,
                        prodSetName: obj.displayProps.prodSetName,
                        prodSetURL: obj.displayProps.prodSetURL,
                        productName: obj.displayProps.productName,
                        productNameClean: obj.displayProps.productNameClean,
                        specSheetURL: obj.displayProps.specSheetURL,
                        thumbnailImageSrc: obj.displayProps.thumbnailImageSrc,
                        uom: (obj.displayProps.uom || ""),
                        uomBaseConversionFactor: obj.displayProps.uomBaseConversionFactor,
                        uomCode: obj.displayProps.uomCode,
                        uomCodeDesc: obj.displayProps.uomCodeDesc,

                        availableInventory: obj.priceInventory.availableInventory,
                        lowQuantityThreshold: obj.priceInventory.lowQuantityThreshold,
                        minOrderQuantity: obj.priceInventory.minOrderQuantity,
                        orderIncrementQuantity: obj.priceInventory.orderIncrementQuantity,
                        price: obj.priceInventory.price,
                        sellOnline: obj.priceInventory.sellOnline,
                        uaLinkClick: '',
                        uaLinkAdd: uaLinkAdd
                    };
                    html += common.template.makeProductListItem(o, pl);
                }
                html += "</div>"; // closing div.products-row
                html += '<div class="clearfix"></div>'; //ie7 fix for margin bottom
            }
        }
        html += "</div>"; // closing div#searchResults
        return html;
    },
    /*
     * makeFeaturedProductList
     *
     * outputs a string representing the HTML generated from the supplied data
     *
     * parameters:
     * +	arr		-	<array> 	an array of objects representing the product list items to be displayed
     * +	grid	-	<boolean> 	if true, generates a grid view; otherwise, generates a list view
     * +	pl		-	<object>	the data-* attributes from the component container
     *
     * return value:
     * +	html	-	<string>	a string of HTML to be inserted into the DOM with .innerHTML() or $jq7.append()
     * 
     ***************** NOT CURRENTLY USED *****************************
     *     
     */
    makeFeaturedProductList: function(arr, grid, pl) {
        var listGrid = (grid) ? "products-grid" : "products-list";
        var html = '<div class="' + listGrid + ' columns-six">';
        if (Object.prototype.toString.call(arr) === "[object Array]") {
            var list = JSON.parse(JSON.stringify(arr)); // clone the data in case we need the original again later
            while (list.length > 0) {
                // make row container
                html += '<div class="products-row clearfix">';
                var pageGridViewItemsPerRow = parseInt(pl.gridViewItemsPerRow, 10);
                var howManyPerRow = (list.length >= pageGridViewItemsPerRow) ? pageGridViewItemsPerRow : list.length;
                for (var i = 0; i < howManyPerRow; i ++) {
                    var obj = list.shift();
                    var o = {
                        caption: obj.caption,
                        commerceEnabled: obj.commerceEnabled == 'false' ? !obj.commerceEnabled : !!obj.commerceEnabled,
                        priceFormatted: obj.priceDisplay,
                        leadTimeDescription: obj.leadTimeDescription,

                        anixterNumber: obj.displayProps.anixterNumber,
                        baseUOM: obj.displayProps.baseUOM,
                        baseUOMDesc: obj.displayProps.baseUOMDesc,
                        description: obj.displayProps.description,
                        imageExists: obj.displayProps.imageExists,
                        manufacturerNumber: obj.displayProps.manufacturerNumber,
                        manufacturerName: obj.displayProps.manufacturerName,
                        prodDetailURL: obj.displayProps.prodDetailURL,
                        prodSetName: obj.displayProps.prodSetName,
                        prodSetURL: obj.displayProps.prodSetURL,
                        productName: obj.displayProps.productName,
                        productNameClean: obj.displayProps.productNameClean,
                        specSheetURL: obj.displayProps.specSheetURL,
                        thumbnailImageSrc: obj.displayProps.thumbnailImageSrc,
                        uom: (obj.displayProps.uom || ""),
                        uomBaseConversionFactor: obj.displayProps.uomBaseConversionFactor,
                        uomCode: obj.displayProps.uomCode,
                        uomCodeDesc: obj.displayProps.uomCodeDesc,

                        availableInventory: obj.priceInventory.availableInventory,
                        lowQuantityThreshold: obj.priceInventory.lowQuantityThreshold,
                        minOrderQuantity: obj.priceInventory.minOrderQuantity,
                        orderIncrementQuantity: obj.priceInventory.orderIncrementQuantity,
                        price: obj.priceInventory.price,
                        sellOnline: obj.priceInventory.sellOnline,
                        dataCategory: 'featured-add-productpage'
                    };
                    html += common.template.makeProductListItem(o, pl);
                }
                html += "</div>"; // closing div.products-row
            }
        }
        html += "</div>"; // closing div#searchResults
        return html;
    },
    /*
     * makeProductAccessoriesList
     *
     * outputs a string representing the HTML generated from the supplied data
     *
     * parameters:
     * +	arr		-	<array> 	an array of objects representing the product list items to be displayed
     * +	grid	-	<boolean> 	if true, generates a grid view; otherwise, generates a list view
     * +	pl		-	<object>	the data-* attributes from the component container
     *
     * return value:
     * +	html	-	<string>	a string of HTML to be inserted into the DOM with .innerHTML() or $jq7.append()
     */
    makeProductAccessoriesList: function(arr, grid, pl) {
        var listGrid = (grid) ? "products-grid" : "products-list";
        var html = '<div class="' + listGrid + ' columns-six" id="accessory-list">';
        var encodeHtmlEntities = common.utility.data.encodeHtmlEntities;
        if (Object.prototype.toString.call(arr) === "[object Array]") {
            var list = JSON.parse(JSON.stringify(arr)); // clone the data in case we need the original again later
            while (list.length > 0) {
//				console.log("while loop", list, list.length);
                // make row container
                html += '<div class="products-row clearfix">';
                var pageGridViewItemsPerRow = parseInt(pl.gridViewItemsPerRow, 10);
                var howManyPerRow = (list.length >= pageGridViewItemsPerRow) ? pageGridViewItemsPerRow : list.length;
//				console.log(html, pageGridViewItemsPerRow, howManyPerRow);
                for (var i = 0; i < howManyPerRow; i ++) {
                    var obj = list.shift();
//                    console.log(i, list, obj);
//                    console.log(pl);
                    
                    var gaqValue = "";
                    if (obj.anixterNumber) {
                    	gaqValue = obj.anixterNumber;
                    } 
                    if (obj.manufacturerName) {
                    	gaqValue = gaqValue + " | " + obj.manufacturerName;
                    } else {
                    	gaqValue = gaqValue + " | " + "none";
                    }
                    if (obj.manufacturerNumber) {
                    	gaqValue = gaqValue + " | " + obj.manufacturerNumber;
                    } else {
                    	gaqValue = gaqValue + " | " + "none";
                    }
                    gaqValue = encodeHtmlEntities(gaqValue);
  
                    var parentGaqValue = "";
                    if (pl.productKey) {
                    	parentGaqValue = pl.productKey;
                    } 
                    
                    if (pl.mfgName) {
                    	parentGaqValue = parentGaqValue + " | " + pl.mfgName;
                    } else {
                    	parentGaqValue = parentGaqValue + " | " + "none";
                    }
                    
                    if (pl.mfgNum) {
                    	parentGaqValue = parentGaqValue + " | " + pl.mfgNum;
                    } else {
                    	parentGaqValue = parentGaqValue + " | " + "none";
                    }
                  
                  
                    var dataEventVarClick = "'accessories - " + parentGaqValue + "'";
                    var dataEventVarAdd = "'added to cart - accessories - " + parentGaqValue + "'";
                    var dataActionVar = "'click'";
                    var dataLabelVar = "'" + gaqValue + "'";
                    var dataCategoryVarClick = "'accessory-click-productpage'";
                    var dataCategoryVarAdd = "'accessory-add-productpage'";
                    
                    var uaLinkClickAccessory = "onclick=\"tagAddToCart(" + dataEventVarClick + ", " + dataLabelVar + ", " + dataCategoryVarClick + ")\"" ;
                    var uaLinkAddAccessory = "onclick=\"tagAddToCart(" + dataEventVarAdd + ", " + dataLabelVar + ",  " + dataCategoryVarAdd + ")\"" ;
                    
                    var o = {
                        anixterNumber: obj.anixterNumber,
                        availableInventory: obj.availableInventory,
                        baseUOM: obj.baseUOM,
                        baseUOMDesc: obj.baseUOMDesc,
                        caption: ((obj.manufacturerNumber) ? obj.manufacturerNumber + ' | ' + obj.productName : obj.productName),
                        commerceEnabled: obj.commerceEnabled == 'false' ? !obj.commerceEnabled : !!obj.commerceEnabled,
                        description: obj.description,
                        imageExists: obj.imageExists,
                        leadTimeDescription: obj.leadTimeDesc,
                        lowQuantityThreshold: obj.lowQuantityThreshold,
                        manufacturerName: obj.manufacturerName,
                        manufacturerNumber: obj.manufacturerNumber,
                        minOrderQuantity: obj.minOrderQuantity,
                        orderIncrementQuantity: obj.orderIncrementQuantity,
                        price: obj.price,
                        priceFormatted: obj.priceFormatted,
                        prodDetailURL: obj.prodDetailURL,
                        prodSetName: obj.prodSetName,
                        prodSetURL: obj.prodSetURL,
                        productName: obj.productName,
                        productNameClean: obj.productNameClean,
                        sellOnline: obj.sellOnline,
                        specSheetURL: obj.specSheetURL,
                        thumbnailImageSrc: obj.thumbnailImageSrc,
                        uom: (obj.uom || ""),
                        uomBaseConversionFactor: obj.uomBaseConversionFactor,
                        uomCode: obj.uomCode,
                        uomCodeDesc: obj.uomCodeDesc,
                        uaLinkClick: uaLinkClickAccessory,
                        uaLinkAdd: uaLinkAddAccessory
                    };
//					console.log("o", o, "pl", pl);
                    html += common.template.makeProductListItem(o, pl);
                }
                html += "</div>"; // closing div.products-row
            }
        }
        html += "</div>"; // closing div#searchResults
        return html;
    },
    /*
     * makeProductListItem
     *
     * outputs a string representing the HTML generated from the supplied data
     * designed to be called from makeSearchResultsProductList() or makeFeaturedProductList()
     *
     * parameters:
     * +	obj		-	<object> 	an object representing the product list item to be displayed
     * +	pl		-	<object>	the data-* attributes from the component container
     *
     * return value:
     * +	html	-	<string>	a string of HTML to be concatenated to the container element HTML string and any previous or following list elements' HTML
     */
    makeProductListItem: function(o, pl) {
        var isEmpty = common.utility.data.isEmpty;
        var propDefault = common.utility.data.propDefault;
        var encodeHtmlEntities = common.utility.data.encodeHtmlEntities;

        /* BEGIN PRODUCT TEMPLATE HTML */
        /*
         * HTML attributes are in double-quotes "
         * strings containing other quotes are in single-quotes '
         * beginning and ending tags which are inside conditional statements are marked with comments for consistency of indentation and visual alignment
         */
        
        var product = ""
            + '<div class="col product" '
                + 'data-anixter-id="' + o.anixterNumber + '" '
                + 'data-product-name="' + encodeHtmlEntities(o.productName) + '" '
                + 'data-product-name-clean="' + encodeHtmlEntities(o.productNameClean) + '" '
                + 'data-name="' + encodeHtmlEntities(o.description) + '" '
                + 'data-description="' + encodeHtmlEntities(o.description) + '" '
                + 'data-url="' + o.prodDetailURL + '" '
                + 'data-mfg-name="' + o.manufacturerName + '" '
                + 'data-mfg-num="' + o.manufacturerNumber + '" '
                + 'data-spec-sheet-url="' + o.specSheetURL + '" '
                + 'data-quantity-qualifier="' + o.uom + '" '
                + 'data-product-set-name="' + encodeHtmlEntities(o.prodSetName) + '" '
                + 'data-product-set-url="' + o.prodSetURL + '" '
                + 'data-conversion-factor="' + o.uomBaseConversionFactor + '" '
                + 'data-uom="' + o.uomCode + '" '
                + 'data-uom-desc="' + o.uomCodeDesc + '" '
                + 'data-base-uom="' + o.baseUOM + '" '
                + 'data-base-uom-desc="' + o.baseUOMDesc + '" '
                + 'data-sell-online="' + o.sellOnline + '" '
                + 'data-price="' + o.price + '" '
                + 'data-img-url-medium="' + o.thumbnailImageSrc + '" '
                + 'data-minimum="' + o.minOrderQuantity + '" '
                + 'data-increment="' + o.orderIncrementQuantity + '" '
                + ">"
                + '<div class="product-image-and-info clearfix">'
                    + '<div class="product-image">'
                        + '<a href="' + o.prodDetailURL + '"' + o.uaLinkClick + '" class="product-thumb">';
                            var imageClass = !isEmpty("thumbnailImageSrc", o) ? "nonDefault" : "";
                            /*<img />*/ product += '<img onerror="this.onerror=null;if (\'' + pl.defaultImageSrc + '\'.length > 0) { this.src=\'' + pl.defaultImageSrc + '\'; }" src="' + o.thumbnailImageSrc + '" width="125" height="125" alt="' + encodeHtmlEntities(o.caption) + '" class="' + imageClass + '" />'
                        + "</a>"
                    + "</div>"
                    + '<div class="product-info">'
                        + '<div class="product-name">'
                            + "<p>"
                                + '<a href="' + o.prodDetailURL + '"' + o.uaLinkClick + 'title="' + encodeHtmlEntities(o.productNameClean) + '">' + o.productName + "</a>"
                            + "</p>"
                        + "</div>"
                        + '<div class="product-numbers">'
                            + "<ul>";
                                /*<li>*/ if (!isEmpty("manufacturerNumber", o)) { product += '<li class="product-attribute">'
                                    + '<span class="attribute-title">' + pl.labelManufacturerNumber + "</span>"
                                    + '<span>' + encodeHtmlEntities(o.manufacturerNumber) + "</span>"
                                + "</li>"; }
                                /*<li>*/ product += '<li class="product-attribute">'
                                    + '<span class="attribute-title">' + pl.labelAnixterNumber + "</span>"
                                    + '<span>' + o.anixterNumber + "</span>"
                                + "</li>"
                            + "</ul>"
                        + "</div>"
                        + '<div class="product-links">'
                            + "<ul>";
                                /*<li>*/ if (!isEmpty("specSheetURL", o)) { product += '<li class="product-attribute">'
                                    + '<span class="attribute-title">'
                                        + '<img src="' + pl.specSheetIconSrc + '" alt="pdf" width="16" height="16" />'
                                    + '</span>'
                                    + '<a target="_blank" href="' + o.specSheetURL + '" title="' + encodeHtmlEntities(pl.labelSpecSheet) + '">' + pl.labelSpecSheet + '</a>'
                                + "</li>"; }
                                /*<li>*/ if (!isEmpty("prodSetURL", o) && pl.displayProductSetLink) { product += '<li class="product-attribute">'
                                    + '<span class="attribute-title">' + pl.labelViewAll + "</span>"
                                    + '<a href="' + o.prodSetURL + '" title="' + encodeHtmlEntities(o.prodSetName) + '">' + o.prodSetName + '</a>'
                                + "</li>"; }
                            /*</ul>*/ product += "</ul>"
                        + "</div>"
                    + "</div>"
                + "</div>"
                + '<div class="product-buy">'
                    + '<div class="product-stock product-add">'          
                        + '<ul class="product-add-to-cart">'
                            + '<li class="purchase-quantity">'
                                + '<label class="quantity-label">' + pl.labelQuantity + "</label>"
                                + '<input type="text" name="quant" data-min="' + o.minOrderQuantity + '" data-step="' + o.orderIncrementQuantity + '" value="" maxlength="9" data-default="" />'
                            + "</li>"
                            + '<li class="purchase-unit">' + o.uom + "</li>";
                            /*<li>*/ if (!(o.commerceEnabled && o.availableInventory > 0 && !isEmpty("price", o) && o.sellOnline)) { product += '<li class="purchase-button">'
                                + '<a name="addToCartButtonAccesory" href="#" title="' + pl.labelRfq + '" class="btn btn-small with-question"' + o.uaLinkAdd + '>' + pl.labelRfq + "</a>"
                                + '<a class="btn btn-small btn-question" onclick="return false;" title="' + pl.qHoverMessage + '">?</a>'
                            + "</li>";
                            /*<li>*/ } else { product += '<li class="purchase-button">'
                                + '<a name="addToCartButtonAccesory" href="#" title="' + pl.labelAddToCart + '" class="btn btn-small" ' + o.uaLinkAdd + '>' + pl.labelAddToCart + "</a>"
                            + "</li>"; }
                        /*</ul>*/ product += "</ul>"
                    + "</div>"
                + "</div>"
                + '<div class="dot-div clearfix"></div>'
            + "</div>";
        return product;
    },
    /*
     * makePagination
     *
     * outputs a string representing the HTML generated from the supplied data
     * designed for use on the Product Set and Search Results pages
     *
     * parameters:
     * +	page	-	<object> 	an object representing the state of the current page of search results and its relation to neighbouring pages
     * +	pl		-	<object>	the data-* attributes from the component container
     *
     * return value:
     * +	html	-	<string>	a string of HTML to be inserted into the DOM with .innerHTML() or $jq7.append()
     */
    makeSearchResultsPagination: function(page, pl) {
        var showingText = pl.showingText
            .replace("${first}", (page.itemsOnPreviousPages + 1))
            .replace("${last}", (page.itemsOnPreviousPages + page.actualPageSize))
            .replace("${total}", (page.resultCount));
        $jq7("#productList .toolbar-results .range").text(showingText);

        var pagination = ""
            + '<nav class="paginate notranslate">'
                + "<ul>"
                    + '<li class="page-prev">';
                        /*<i></i>*/ if (page.prevLinkDisabled) { pagination += '<i class="icon icon-prev"></i>';
                        /*<a><i></i></a>*/ } else { pagination += '<a class="pageLink" href="#" data-page-number="' + (page.currentPage - 1) + '"><i class="icon icon-prev"></i></a>'; }
                    /*</li>*/ pagination += "</li>";
                    /*<li>*/ for (var i = page.paginationStart; i <= page.paginationEnd; i ++) { pagination += "<li>";
                        /*[textNode]*/ if (i == page.currentPage) { pagination += page.currentPage;
                        /*<a></a>*/ } else { pagination += '<a href="#" class="pageLink" data-page-number="' + i + '">' + i + '</a>'; }
                    /*</li>*/ pagination += "</li>"; }
                    /*<li>*/ pagination += '<li class="page-fwd">';
                        /*<i></i>*/ if (page.nextLinkDisabled) { pagination += '<i class="icon icon-fwd"></i>';
                        /*<a><i></i></a>*/ } else { pagination += '<a class="pageLink" href="#" data-page-number="' + (page.currentPage + 1) + '"><i class="icon icon-fwd"></i></a>'; }
                    /*</li>*/ pagination += "</li>"
                + "</ul>"
            + "</nav>";
        return pagination;
    },

    /*
     * makeBreadcrumb
     *
     * outputs a string representing the HTML generated from the supplied data
     * designed for use on the Product Set and Search Results pages
     *
     * parameters:
     * +	page	-	<object> 	an object representing the state of the current page of search results and its relation to neighbouring pages
     *
     * return value:
     * +	html	-	<string>	a string of HTML to be inserted into the DOM with .innerHTML() or $jq7.append()
     */
    makeBreadcrumb: function(page) {
        var breadCrumbText = '';
        if(page.selectedProductSet){
            if(page.selectedProductSet.id){
                if($jq7('#updateResultsForm div.hidden input.productSetSearch').length > 0
                        || $jq7('#updateResultsForm div.hidden input.supplierId').length > 0){
                    breadCrumbText += '<li>' + page.selectedProductSet.name + '<a class="remove productSet" href="#" data-item-id="' + page.selectedProductSet.id + '"> (X) </a> </li>';
                }
            }
        }

        if(page.selectedRefinementGroups){
            for(var i=0; i<page.selectedRefinementGroups.length; i++){
                for(var j=0; j<page.selectedRefinementGroups[i].valueRefinements.length; j++){
                    if($jq7('#updateResultsForm div.hidden input.supplierId').val() != page.selectedRefinementGroups[i].valueRefinements[j].id){
                        if(breadCrumbText.length > 0){
                            breadCrumbText += ', ';
                        }
                        breadCrumbText += '<li>' + page.selectedRefinementGroups[i].valueRefinements[j].name + '<a class="remove attribute" href="#" data-item-id="' + page.selectedRefinementGroups[i].valueRefinements[j].id + '"> (X) </a> </li>';
                    }
                }
            }
        }

        if(breadCrumbText.length > 0){
            breadCrumbText = '<ul> Filters: ' + breadCrumbText + '</ul>';
        }

        var searchWithinText = '';
        if(page.searchTerms && page.searchTerms.length > 0){
            for(var i=0; i<page.searchTerms.length; i++){
                if($jq7('#updateResultsForm div.hidden input.firstKeyword').val() != page.searchTerms[i].name){
                    if(searchWithinText.length > 0){
                        searchWithinText += ', ';
                    }
                    searchWithinText += '<li>"' + page.searchTerms[i].name + '"<a class="remove searchWithin" href="#" data-item-id="' + page.searchTerms[i].uniqueid + '"> (X) </a> </li>';
                }
            }
        }

        if(searchWithinText.length > 0){
            breadCrumbText += '<ul> Search Within: ' + searchWithinText + '</ul>';
        }

        return breadCrumbText;
    },

    /*
     * updateFacets
     *
     * outputs a string representing the HTML generated from the supplied data
     * designed for use on the part search
     *
     * parameters:
     * +	page	-	<object> 	an object representing the state of the current page of search results and its relation to neighbouring pages
     *
     * return value:
     * +	html	-	<string>	a string of HTML to be inserted into the DOM with .innerHTML() or $jq7.append()
     */
    updateFacets: function(page) {
        $jq7('#updateResultsForm div.hidden input[name="selectedDimensionValues"][class!="productSetSearch"]').remove();
        $jq7('#updateResultsForm div.hidden input.lastKeyword').remove();
        $jq7('#updateResultsForm ul.product-set').empty();
        $jq7('#updateResultsForm ul.mass-facet-mod a.clear-all').parent().remove();
        $jq7('#updateResultsForm ul.mass-facet-mod').show();
        $jq7('footer.search-tools').show();
        $jq7('#updateResultsForm div.search input[name="searchTerms"]').val('');

        var facetText = '';
        if(page.selectedProductSet){
            if(page.selectedProductSet.name){
                if($jq7('#updateResultsForm div.hidden input.productSetSearch').length > 0
                        || $jq7('#updateResultsForm div.hidden input.supplierId').length > 0){
                    $jq7('#updateResultsForm ul.product-set').html('<li><h2>' + page.selectedProductSet.name + '</h2> <a id="removeAll" class="btn-clear" title="Clear" href="#" data-item-id="' + page.selectedProductSet.id + '">Clear</a> </li>');
                }

                if ((page.productGroups && page.productGroups.length > 0) || (page.refinements && page.refinements.length > 0) ) {
                    $jq7('#updateResultsForm ul.mass-facet-mod').append('<li> <a class="clear-all" title="Clear All" href="#">Clear All</a> </li>');
                } else {
                    $jq7('#updateResultsForm ul.mass-facet-mod').hide();
                    $jq7('footer.search-tools').hide();
                }

                $jq7('#updateResultsForm div.hidden').append('<input class="productSet" type="checkbox" checked="" name="selectedDimensionValues" value="' + page.selectedProductSet.id + '" >');
            }
        }

        if(page.selectedRefinementGroups){
            for(var i=0; i<page.selectedRefinementGroups.length; i++){
                for(var j=0; j<page.selectedRefinementGroups[i].valueRefinements.length; j++){
                    if (page.selectedRefinementGroups[i].valueRefinements[j].id == $jq7('#updateResultsForm div.hidden .supplierId').val()) {
                        $jq7('#updateResultsForm div.hidden').append('<input type="checkbox" checked="" class="selectedSupplier" name="selectedDimensionValues" value="' + page.selectedRefinementGroups[i].valueRefinements[j].id + '" >');
                    }else {
                        $jq7('#updateResultsForm div.hidden').append('<input type="checkbox" checked="" name="selectedDimensionValues" value="' + page.selectedRefinementGroups[i].valueRefinements[j].id + '" >');
                    }
                }
            }
        }

        if(page.searchTerms && page.searchTerms.length > 0){
            for(var i=0; i<page.searchTerms.length; i++){
                if($jq7('#updateResultsForm div.hidden input.firstKeyword').val() != page.searchTerms[i].name){
                    $jq7('#updateResultsForm div.hidden').append('<input class="lastKeyword" type="checkbox" checked="" name="searchTerms" data-item-uid="' + page.searchTerms[i].uniqueid + '" value="' + page.searchTerms[i].name + '">');
                }
            }
        }

        if(page.productGroups && page.productGroups.length > 0){
            facetText += '<ul class="search-categories">';
            for(var i=0; i<page.productGroups.length; i++){
                var refinement = page.productGroups[i];
                if(i < 2){
                    facetText +=   '<li class="productGroup open">';
                }else{
                    facetText +=   '<li class="productGroup">';
                }
                facetText +=     '<div>';
                facetText +=       '<i class="icon icon-collapse"> </i>';
                facetText +=       '<a onclick="return false;" title="' + refinement.name + '" href="#">' + refinement.name + '</a>';
                facetText +=       '<div class="btn-clear"><a class="clearGroup" href="#" title="clear">clear</a></div>';
                facetText +=       '<div class="group-point"></div>';
                facetText +=     '</div>';
                facetText +=     '<ul>';
                for(var j=0; j<refinement.productSets.length; j++){
                    var valueRefinement = refinement.productSets[j];
                    facetText +=     '<li>';
                    facetText +=       '<a class="productSetLinkFun" href="#" data-product-set-id="' + valueRefinement.id + '">' + valueRefinement.name +  '</a>';
                    facetText +=     '</li>';
                }
                facetText +=     '</ul>';
                facetText +=   '</li>';
            }
            facetText += '</ul>';

        }else if(page.refinements){
            facetText += '<ul class="search-categories">';
            for(var i=0; i<page.refinements.length; i++){
                var refinement = page.refinements[i];
                if(i < 2){
                    facetText +=   '<li class="productGroup open">';
                }else{
                    facetText +=   '<li class="productGroup">';
                }
                facetText +=     '<div>';
                facetText +=       '<i class="icon icon-collapse"> </i>';
                facetText +=       '<a onclick="return false;" title="' + refinement.name + '" href="#">' + refinement.name + '</a>';
                facetText +=       '<div class="btn-clear"><a class="clearGroup" href="#" title="clear">clear</a></div>';
                facetText +=       '<div class="group-point"></div>';
                facetText +=     '</div>';
                facetText +=     '<ul>';
                for(var j=0; j<refinement.valueRefinements.length; j++){
                    var valueRefinement = refinement.valueRefinements[j];
                    facetText +=     '<li>';
                    if(valueRefinement.selected){
                        facetText +=       '<input id="facet' + i + j + '" class="facet" type="checkbox" value="' + valueRefinement.id + '" name="selectedDimensionValues" checked="true" >';
                        facetText +=       '<label class="productSetLink" for="facet' + i + j + '">' + valueRefinement.name + '</label>';
                    }else{
                        facetText +=       '<input id="facet' + i + j + '" class="facet" type="checkbox" value="' + valueRefinement.id + '" name="selectedDimensionValues" >';
                        facetText +=       '<label class="productSetLink active" for="facet' + i + j + '">' + valueRefinement.name + ' (' + valueRefinement.count + ')</label>';
                    }
                    facetText +=     '</li>';
                }
                facetText +=     '</ul>';
                facetText +=   '</li>';
            }
            facetText += '</ul>';
        }

        return facetText;
    }
};
(function($) {

    $jq7.widget( "anixter.cartDropDown", {

        options: {},

        _create: function() {
            this.tab = this.element.find('.shopping-list-tab a');

            this.element.on('mouseenter', {context: this}, this._over);
            this.element.on('mouseleave', {context: this}, this._out);
        },

        _over: function(e) {
            e.data.context.element.addClass('open');
        },

        _out: function(e) {
            e.data.context.element.removeClass('open');
        }

    });

})(jQuery);
$jq7(function() {

    //Global MegaNav & Cart Drop Down
    $jq7('#Masthead').megaNav().find('.nav-shopping-list').cartDropDown();

    //Facet Search Sidebar
    var searchBar = $jq7('#refinements').searchSideBar();

    //listen for search within submit
    searchBar.bind('ssb-searchin', function(e){
        //console.log('Search Within Results');
    });

    //listen for facet or set changes (add or remove)
    searchBar.bind('ssb-update', function(e){
        //console.log('Update Search');
    });

    //remove a facet example
    searchBar.searchSideBar('removeFacet', '4294967286');

});  
var updateObjects = Array();
var errorQuantityFields = Array();
var shoppingcartcomponent = shoppingcartcomponent || {
    handlers: {
        clickShippingDetails: function(e) {
            e.preventDefault();
            $jq7("#shopping-cart-details-dialog").find(".cart-message-shipping").show();
            $jq7("#shopping-cart-details-dialog").find(".cart-message-sales").hide();
            $jq7("#shopping-cart-details-dialog").dialog({
                position: "center",
                modal: true,
                resizable: false,
                autoOpen: true,
                draggable: false,
                zIndex: 9999,
                title: shoppingcartcomponent.info.shippingDetailsTitle
            });
        },
        clickSalesTaxDetails: function(e) {
            e.preventDefault();
            $jq7("#shopping-cart-details-dialog").find(".cart-message-shipping").hide();
            $jq7("#shopping-cart-details-dialog").find(".cart-message-sales").show();
            $jq7("#shopping-cart-details-dialog").dialog({
                position: "center",
                modal: true,
                resizable: false,
                autoOpen: true,
                draggable: false,
                zIndex: 9999,
                title: shoppingcartcomponent.info.salesTaxDetailsTitle
            });
        },
        clickRemoveCartItem: function(event) {
            $jq7('#shopping-cart-remove-item-dialog').dialog({
                position:'center',
                width:300,
                modal: true,
                resizable: false,
                zIndex: 9999
            });
            var curItem = {
                anixterId: $jq7(this).attr('data-axe-delete-id'),
                quantity: 'remove'
            };
            $jq7('#shopping-cart-remove-item-dialog a.removeItemBtn').makeUpdateCartButton(shoppingcartcomponent.info.shoppingCartAddPath, curItem, function() {
                return { quantity: 'remove' };
            }, null, function() {
                //on success, close the dialog
                $jq7('#shopping-cart-remove-item-dialog').dialog('close');
                window.location.reload();
            }, function(e) {
//		    	console.log("Error removing item:", e.success, e.returnCode, e.response, e.response.message);
            });
            return false;
        },
        clickRemoveCartItemCancel: function(event) {
            //close the dialog
            $jq7('#shopping-cart-remove-item-dialog').dialog('close');
            return false;
        },
        clickRFQJump: function(e) {
            e.preventDefault();
            var selector = $jq7(this).attr("href").replace("#", ".");
            var top = $jq7(selector + ":visible").offset().top - 20;
            $jq7(window).scrollTop($jq7(window).scrollTop() + top);
        },
        keyupQuantity: function(e) {

            var currVal = $jq7(this).attr("data-item-curr-val");
            var newVal = $jq7(this).val();

            if (newVal !== currVal) {
                $jq7(this).parents(".col-quantity").find(".item-update-function").show();
            } else {
                $jq7(this).parents(".col-quantity").find(".item-update-function").hide();
            }
        },
        clickUpdateAll: function(event) {
        	errorQuantityFields = Array();
        	
            $jq7( "#shopping-cart .shopping-cart-item" ).each(updateCartItems);

            $jq7( "#shopping-cart-price .shopping-cart-item" ).each(updateCartItems);

            if (errorQuantityFields.length == 0) {
                $jq7.ajax(shoppingcartcomponent.info.bulkQuantityUpdatePath, {
                    data :  { 'items' : JSON.stringify( updateObjects ) },
                    type : "POST",
                    traditional : true,
                    success : function( data ) {
                        $jq7.publish( "anixter.shoppingcartupdate.update", [ data ] );
                        //open the dialog with confirmation message
                        $jq7('#shopping-cart-updated-dialog').dialog({
                            position:'center',
                            width:300,
                            modal: true,
                            resizable: false,
                            zIndex: 9999,
                            close: function() { window.location.reload(); }
                        });
                    }
                });
            } else {
                //open the dialog with error message
                $jq7('#shopping-cart-update-error-dialog').dialog({
                    position:'center',
                    width:300,
                    modal: true,
                    resizable: false,
                    zIndex: 9999
                });
            }

            //we want to return false so when the "Update All" link is clicked the page will not scroll to the top
            return false;
        }
    }
};

//List of HTML entities for escaping.
var htmlEscapes = {
  '&': '&amp;',
  '<': '&lt;',
  '>': '&gt;',
  '"': '&quot;',
  "'": '&#x27;',
  '/': '&#x2F;'
};

// Regex containing the keys listed immediately above.
var htmlEscaper = /[&<>"'\/]/g;

// Escape a string for HTML interpolation.
function escapeXml(text) {
    return ('' + text).replace(htmlEscaper, function(match) {
        return htmlEscapes[match];
    });
}

function activateUpdateButtons(selector) {
    var container = $jq7(selector);
    var properties = container.data();

    $jq7(container).find("a.cart-item-update-button").makeUpdateCartButton(shoppingcartcomponent.info.shoppingCartAddPath, properties, function() {
        return {
            quantity  : container.find("input[name='quantity']").val().replace(/^0+/, ''),
            minimum   : parseInt(container.find(".cart-item-minimum").text()),
            increment : parseInt(container.find(".cart-item-increment").text())
        };
    }, container, function(e) {
//    	console.log("Error updating quantity:", e.success, e.returnCode, e.response, e.response.message);
    });
}

function isNotDivisible(value, increment) {
    return (value % increment !== 0 && value !== 0) ;
}

function updateCartItems() {

    var curAnixterId = $jq7(this).find(".cart-item-anixterId").text();//$jq7(this).find( ".anixter-id-column" ).first().html();
    var min = ($jq7(this).find(".cart-item-minimum").text()) ? parseInt($jq7.trim($jq7(this).find(".cart-item-minimum").text()), 10) : 1;
    var inc = ($jq7(this).find(".cart-item-increment").text()) ? parseInt($jq7.trim($jq7(this).find(".cart-item-increment").text()), 10) : 1;
    var curQuantityField = $jq7(this).find( ".quantity-input" ).first();
    var curQuantity = curQuantityField.val().replace(/^0+/, '');

    if (!isNumeric(curQuantity) || curQuantity < min || isNotDivisible(curQuantity, inc)) {
        curQuantityField.addClass( "error" );
        errorQuantityFields.push(curQuantityField);
    } else {
        curQuantityField.removeClass( "error" );
    }

    updateObjects.push({
        anixterId : curAnixterId,
        quantity : curQuantity
    });
}

jQuery("document").ready( function($) {
    if ($jq7("#cartStatus").length > 0) {
        /*$jq7(".shopping-cart")*/$jq7(document).on("click.shippingDetails", ".cart-shipping-details", shoppingcartcomponent.handlers.clickShippingDetails);

        /*$jq7(".shopping-cart")*/$jq7(document).on("click.salesTaxDetails", ".cart-sales-tax-details", shoppingcartcomponent.handlers.clickSalesTaxDetails);

        // Setup the remove item dialog
        $jq7(document).on('click.removeCartItem', 'a.removeShoppingCartItemBtn', shoppingcartcomponent.handlers.clickRemoveCartItem);

        // When user cancels REMOVE action
        $jq7(document).on('click.removeCartItemCancel', '#shopping-cart-remove-item-dialog a.cancelBtn', shoppingcartcomponent.handlers.clickRemoveCartItemCancel);

        $jq7(document).on("click.rfqJump", ".rfq-jump-link", shoppingcartcomponent.handlers.clickRFQJump);

        // EDIG-1823 : (Defect 1753) BA wants to only show the update button if there is an actual change to the value
        $jq7(document).on("keyup.quantity", ".quantity-input", shoppingcartcomponent.handlers.keyupQuantity);

        // Setup the Update All button
        $jq7(document).on('click.updateAll', '#shopping-cart-update-all-button', shoppingcartcomponent.handlers.clickUpdateAll);

        shoppingcartcomponent.info = $jq7("#cartStatus").data();

        $jq7(".btn-question").tooltip();

        /* START - Code to populate the Cart Status presentation */
        var buyOnly = shoppingcartcomponent.info.cartStatusBuyOnlyText,
            RFQOnly = shoppingcartcomponent.info.cartStatusRFQOnlyText,
            buyAndRFQ = shoppingcartcomponent.info.cartStatusBuyAndRFQText,
            buyVar = "${buy}",
            rfqVar = "${rfq}",
            linkBegin = "${linkBegin}",
            linkEnd = "${linkEnd}";

        if (shoppingcartcomponent.info.hasSellableItems === "true" && shoppingcartcomponent.info.hasRfqItems === "true") {
            $jq7("#cartStatus").html(buyAndRFQ
                .replace(buyVar, parseInt(shoppingcartcomponent.info.sellableCount, 10))
                .replace(rfqVar, parseInt(shoppingcartcomponent.info.rfqCount, 10))
                .replace(linkBegin, "<a href='#rfq-container' class='rfq-jump-link'>")
                .replace(linkEnd, "</a>"));
        } else if (shoppingcartcomponent.info.hasSellableItems === "true") {
            $jq7("#cartStatus").html(buyOnly
                .replace(buyVar, parseInt(shoppingcartcomponent.info.sellableCount, 10)));
        } else if (shoppingcartcomponent.info.hasRfqItems === "true") {
            $jq7("#cartStatus").text(RFQOnly
                .replace(rfqVar, parseInt(shoppingcartcomponent.info.rfqCount, 10))
                .replace(linkBegin, "<a href='#rfq-container' class='rfq-jump-link'>")
                .replace(linkEnd, "</a>"));
        } else {
            $jq7("#cartStatus").html("");
        }
        /* END - Code to populate the Cart Status presentation */

        /* Activate the Update links of items under Sellable Table */
        $jq7( "#shopping-cart-price .shopping-cart-item" ).each( function() {
            activateUpdateButtons(this);
        });

        /* Activate the Update links of items under RFQ Table */
        $jq7( "#shopping-cart .shopping-cart-item" ).each( function() {
            activateUpdateButtons(this);
        });
    }
});

//add tooltip again on window load to ensure all elements have been created at this point
$jq7(window).load(function() {
    if ($jq7("#cartStatus").length > 0) {
        $jq7(".btn-question").tooltip();
    }
});
var email = email || {};
$jq7(document).ready(function(){

  /*
   * Setup validation
   */

    email.info = $jq7("#email-shoppingcart-container").data();

  /*
   * Setup the overlay buttons
   */
   $jq7( "#email-shopping-cart-close-button, #email-shopping-cart-cancel-button" ).click( function() {

     $jq7( "#email-shoppingcart-container" ).hide();

   });

  $jq7( "#email-shopping-cart-error-ok-button" ).click( function() {
    $jq7( ".email-shopping-cart-page" ).hide();
    $jq7( "#email-shopping-cart-email-page" ).show();
  });

  $jq7( "#email-shopping-cart-send-button" ).click( function() {

    var valid = true;

    $jq7( "#email-shopping-cart-email-page .required" ).each( function() {
      if ( $jq7( this ).val() == "" ) {
        valid = false;
        $jq7( this ).addClass( "invalid" );
      }
    });

    if ( valid ) {
      var emailData = {};

      emailData.sender = $jq7.trim( $jq7( "#email-shoppingcart-container #email-shopping-cart-sender" ).val() );
      emailData.recipientList = $jq7.trim( $jq7( "#email-shoppingcart-container #email-shopping-cart-recipient-list" ).val() );
      var emailPath = email.info.path + ".email.mail";


      $jq7.ajax(
          emailPath,
          {
            type: "GET",
            data: emailData,
            success: function() {
              $jq7( ".email-shopping-cart-page" ).hide();
              $jq7( "#email-shopping-cart-sent-page" ).show();
            },
            error: function(req, textStatus, errorThrown) {
              //alert ( textStatus + " :: " + errorThrown );
              //$jq7( "#email-shopping-cart-error-page .error-message" ).html( errorThrown );
              $jq7( ".email-shopping-cart-page" ).hide();
              $jq7( "#email-shopping-cart-error-page" ).show();
            }
          }
        );
    }

  });

  /*
   * Setup all the e-mail buttons on the page
   */
  $jq7( ".email-shopping-cart-link" ).click( function() {

    //grab the sharing container
    var sharingContainer = $jq7("#email-shoppingcart-container");
    sharingContainer.detach();

    sharingContainer.find( ".email-shopping-cart-page" ).hide();
    sharingContainer.find( "#email-shopping-cart-email-page" ).show();

    $jq7( "body" ).append( sharingContainer );

    var iconPosition = $jq7( this ).offset();

    var windowWidth = $jq7( window ).width();

    sharingContainer.css( "top", iconPosition.top + 25 );

    if ((iconPosition.left + 353) > (windowWidth - 15)) {
      sharingContainer.css( "left", iconPosition.left - 353 + 40);
    } else {
      sharingContainer.css( "left", iconPosition.left );
    }

    sharingContainer.show();

    return false;
  });
});
var shoppingcartoverlay = shoppingcartoverlay || {};

jQuery("document").ready( function($) {
    if ($jq7("#overlay-dialog").length > 0) {
        shoppingcartoverlay.info = $jq7("#overlay-dialog").data();

        /*
         * Subscribe the add-item-to-cart-dialog to shoppingcart addition events
         */
        $jq7.subscribe( "anixter.shoppingcartupdate.add anixter.shoppingcartupdate.update anixter.shoppingcartupdate.show", function( e, response ) {
            if (!response.itemAdded) {
                if (response.maxCartSizeReached) {
                    //item was not added because max number of items in the cart has been reached
                    $jq7('#shopping-cart-max-size-reached-dialog').dialog({
                        position:'center',
                        width:300,
                        modal: true,
                        resizable: false,
                        zIndex: 9999
                    });
                }
            } else if ( response.product ) {
                var itemAddedDialog = $jq7("#overlay-dialog");
                var product = response.product;

                if ( response.updateType && response.updateType == "update" ) {
                    itemAddedDialog.find("#overlay-item-added-text").text( shoppingcartoverlay.info.shoppingCartAddToCartOverlayItemUpdatedText );
                } else if ( response.updateType && response.updateType == "show" ) {
                    itemAddedDialog.find("#overlay-item-added-text").text( shoppingcartoverlay.info.shoppingCartAddToCartOverlayItemUpdatedText );
                } else {
                    var cartOrQuote = (response.cartOrQuote == "cart") ? shoppingcartoverlay.info.itemAddedtoCartText : shoppingcartoverlay.info.itemAddedtoQuoteText;
                    itemAddedDialog.find("#overlay-item-added-text").text( shoppingcartoverlay.info.shoppingCartAddToCartOverlayItemAddedText + " " + cartOrQuote );
                }
                itemAddedDialog.find(".overlay-count").html(response.cartTotal);

                itemAddedDialog.find(".overlay-image-link").attr('href', product.url);
                itemAddedDialog.find(".overlay-desc-link").attr('href', product.url);
                itemAddedDialog.find(".overlay-image").attr('src', product.imageUrlMedium);
                itemAddedDialog.find(".overlay-image").attr('onError', "if ('" + product.imageUrlMedium + "'.length > 0) { this.src='" + shoppingcartoverlay.info.defaultImage + "'; }");
                itemAddedDialog.find(".overlay-manufacturer").html(product.mfgName);
                itemAddedDialog.find(".overlay-anixterId").html(product.anixterId);

                if (product.mfgNum) {
                    itemAddedDialog.find(".overlay-mfgNum").html(product.mfgNum);
                } else {
                    itemAddedDialog.find(".product-mfg-num").hide();
                }


                if ( product.name ) {
                    var nameAndDescription = "";
                    if ( product.mfgName ) {
                        nameAndDescription = nameAndDescription + product.mfgName + " - ";
                    }
                    nameAndDescription = nameAndDescription + product.name;
                    if ( product.description ) {
                        if ( product.description != product.name ){
                            nameAndDescription = nameAndDescription + " / " + product.description;
                        }
                    }
                    itemAddedDialog.find(".overlay-product-label").html(nameAndDescription);
                } else if ( product.description ) {
                    itemAddedDialog.find(".overlay-product-label").html(product.description);
                }
                itemAddedDialog.find(".overlay-quantity").html(product.quantity);

                if( product.quantityQualifier && product.quantityQualifier != "" ) {
                    itemAddedDialog.find(".overlay-quantity-qualifier" ).html(product.baseUOMDesc);
                } else {
                    itemAddedDialog.find(".overlay-quantity-qualifier" ).html("");
                }

                if ((product.price) && (product.price > 0) && shoppingcartoverlay.info.commerceEnabled === "true") {
                    var price = 0.00;
                    var extPrice = 0.00;
                    var uomDesc = (product.uom != "EA") ? shoppingcartoverlay.info.lblPerUOM + " " + product.uomDesc : " ";
                    itemAddedDialog.find(".overlay-item-price").html("<p class='overlay-item-price-value'>" + price + "</p><p class='overlay-item-price-uom'>" + uomDesc + "</p>");
                    itemAddedDialog.find(".overlay-extended-price").html(extPrice);
                } else {
                    itemAddedDialog.find(".overlay-item-price").html(shoppingcartoverlay.info.shoppingCartAddToCartOverlayRequestAQuoteText);
                    itemAddedDialog.find(".overlay-extended-price").html("");
                }

                /*
                 * Open the dialog
                 */
                itemAddedDialog.dialog({
                    position: "center",
                    width: 960,
                    minHeight: 450,
                    modal: true,
                    resizable: false,
                    autoOpen: true,
                    draggable: false,
                    resizable: false,
                    zIndex: 9999,
                    position: { my: "top", at: "top"},
                    title: "<img src='/apps/settings/wcm/designs/anixter/images/Anixter_60_Logo_112x27.png' alt='Anixter'>",
                    close : function() { if (response.updateType === "update") { window.parent.location.reload(); }}
                });

                $jq7(itemAddedDialog.dialog("widget")[0].firstChild).attr("id", "overlay-dialog-titlebar");

                /*
                 * Setup the close button
                 */
                var dialogCloseButton = itemAddedDialog.find( "#overlay-continue-shopping-button" );
                dialogCloseButton.off("click.dialogClose").on("click.dialogClose", function(event) {
                	event = event || window.event //For IE
                	itemAddedDialog.dialog( "close" );
                    if (event.preventDefault) { 
                    	event.preventDefault(); 
                    } else { 
                    	event.returnValue = false; 
                    }
                    return false;
                });

                var encodeHtmlEntities = common.utility.data.encodeHtmlEntities;              
                var parentGaq = "";
                
                if (product.anixterId) {
                	parentGaq = product.anixterId;
                } 
                
                if (product.mfgName) {
                	parentGaq = parentGaq + " | " + product.mfgName;
                } else {
                	parentGaq = parentGaq + " | " + "none";
                }
                
                if (product.mfgNum) {
                	parentGaq = parentGaq + " | " + product.mfgNum;
                } else {
                	parentGaq = parentGaq + " | " + "none";
                }
                
                parentGaq = encodeHtmlEntities(parentGaq);
                
                /*
                 * Populate the accessories
                 */
                shoppingcart.requestAccessoriesForProduct(shoppingcartoverlay.info.shoppingCartOverlayPath + ".accessories.json", product.anixterId, $jq7("#overlay-accessory-list" ), function( data ) {
                	var encodeHtmlEntities = common.utility.data.encodeHtmlEntities;
                	var productList = data;
                    var accessoriesListContainer = $jq7("<ul/>");
                    this.html("");
                    
                    if (productList && productList.length) {
                        $jq7("#accessoryHeader").html(shoppingcartoverlay.info.shoppingCartAddToCartOverlayAccessoriesText);
                        for (var i = 0; i < productList.length; i++) {
                            var curProduct = productList[i];

                            var curAccessoryContainer = $jq7("<li class='accessory-item' />");

                            var productLinkText   = curProduct.productName;
                            var productMfgId      = curProduct.manufacturerNumber;
                            var productMfgName    = curProduct.manufacturerName;
                            var productAnixterId  = curProduct.anixterNumber;
                            var prodDetailURL     = curProduct.prodDetailURL;
                            var productImageSmall = curProduct.smallImageSrc;

                            var sellOnline         = curProduct.sellOnline
                            var price              = curProduct.price;
                            var priceFormatted     = curProduct.priceFormatted;
                            var lowQtyThreshold    = curProduct.lowQuantityThreshold;
                            var availableInventory = curProduct.availableInventory;
                            var leadtimeCode       = curProduct.leadTimeCode;
                            var leadTimeDesc       = curProduct.leadTimeDesc;

                            var isCommerceEnabled  = ((curProduct.commerceEnabled) && (curProduct.commerceEnabled === "true")) ? true : false;
                            var uom                = (curProduct.uomCode != "EA") ? shoppingcartoverlay.info.lblPerUom + " " + curProduct.uomCodeDesc : " ";

                            var gaqValue = "";
                            
                            if (productAnixterId) {
                            	gaqValue = productAnixterId;
                            } 
                            
                            if (productMfgName) {
                            	gaqValue = gaqValue + " | " + productMfgName;
                            } else {
                            	gaqValue = gaqValue + " | " + "none";
                            }
                            
                            if (productMfgId) {
                            	gaqValue = gaqValue + " | " + productMfgId;
                            } else {
                            	gaqValue = gaqValue + " | " + "none";
                            }
                            
                            gaqValue = encodeHtmlEntities(gaqValue);
                            
                            var accessoryDetails = ""
                                + "<div class='accessory-image'>"
                                    + "<a href='" + prodDetailURL + "' onclick=\"tagAddToCart('accessories popup - " + parentGaq + "', " + gaqValue + "', 'accessory-click-popup');\">"
                                    + '<img width="75" height="75" onerror="this.onerror=null;if(\'' + productImageSmall + '\'.length > 0) { this.src=\'' + shoppingcartoverlay.info.defaultImage + '\';}" src="' + productImageSmall + '">'
                                    + "</a>"
                                + "</div>"
                                + "<div class='accessory-info'>"
                                    + "<p class='accessory-description'>"
                                        + "<a href='" + prodDetailURL + "' onclick=\"tagAddToCart('accessories popup - " + parentGaq + "', " + gaqValue + "', 'accessory-click-popup');\">" + productLinkText + "</a>"
                                    + "</p>"
                                    + "<ul class='accessory-numbers'>"
                                        + "<li class='accessory-attribute'><span class='attribute-title'>" + shoppingcartoverlay.info.shoppingCartAddToCartOverlayManufacturerText + "</span><span class='attribute-value prop-mfgId'>" + productMfgId + "</span></li>"
                                        + "<li class='accessory-attribute'><span class='attribute-title'>" + shoppingcartoverlay.info.shoppingCartAddToCartOverlayAnixterText + "</span><span class='attribute-value prop-anixterId'>" + productAnixterId + "</span></li>"
                                    + "</ul>"
                                    + "<ul class='accessory-price'>"
                                        + "<li class='unit-price'>";
                                            accessoryDetails += (isCommerceEnabled && price) ? shoppingcartoverlay.info.priceLabel + ": " + priceFormatted + " " + uom : shoppingcartoverlay.info.pricingNotAvailableMessage;
                                        accessoryDetails += "</li>";
                                    accessoryDetails += "</ul>"
                                + "</div>";
                            curAccessoryContainer.append(accessoryDetails);
                            accessoriesListContainer.append( curAccessoryContainer );
                        }
                    }
                    this.html( accessoriesListContainer );
                });
            }
        });
    }
});
$CQ(document).ready(function() {

	bindAnixterLink();

    
   
    
    anixterPageAnalytics();

});

function bindAnixterLink(){
	$CQ("a").bind("click", function(){
        var url = this.href.toUpperCase();
        if (url.indexOf("ANIXTER.JOBS") > -1 || url.indexOf("INVESTORS.ANIXTER.COM") > -1) {
            //return false;
        } else if (url.indexOf(".PDF") > -1 || url.indexOf(".DOC") > -1 || url.indexOf(".XLS") > -1 || url.indexOf(".ZIP") > -1 || url.indexOf(".PPT") > -1) {
            var urlparts = url.split('/');
            var file_name = urlparts[urlparts.length - 1];
            var folder_name = urlparts[urlparts.length - 2];
            if (folder_name == '$FILE') {
                folder_name = 'Lotus Notes Document Library';
            } else if (folder_name =='OBJECTS.EANIXTER.COM') {
                folder_name = 'Spec Sheet';
            } else {
                folder_name = unescape(folder_name);
            }
            dataLayer.push({'data-eventvar': folder_name, 'data-actionvar' : 'download', 'data-labelvar' : file_name, 'data-categoryvar': 'document-download' });
            
            digitalData.linkName = file_name;
            digitalData.linkType = "download link";
            digitalData.linkCategory = 'download';
            _satellite.track("click tracking");
            
            return true;
        } else {
            return true;
        }
    });
}




function anixterPageAnalytics() {
	var banners = setBanners();
	
	var path = window.location.pathname;
	
	var locale = $("#localeAnalytics").data("locale");
	var language = (typeof locale != 'undefined') ? locale.toLowerCase() : 'na';
	if (path.indexOf("404") >= 0){
		var pageName= '404';
	} else{
		var pageName = $("#breadcrumbAnalytics").data("pagename");
		if (typeof pageName == 'undefined') {
			pageName = 'na';
		} else {
			pageName = pageName.replace('home/', '');
		}		
	}
	var categoryList = pageName.split('/');
	var vendor = 'na';
	var pageType = categoryList[0];

	if (pageType == '') {
		pageType = "Home";
	} else if (pageType == 'manufacturers' && categoryList[1] != undefined){
		vendor = categoryList[1];
	}	
	
	var categoryRow = {};
	var categoryGroup = "";
	for (var i=0; i < 6; i++ ) {

		if (typeof categoryList[i] == 'undefined'){
			var catName = 'na';
			var categoryGroup = 'na';	
	    } else if(categoryList[i].indexOf('anixter #') >= 0 ){
	    	var catName = 'na';
			var categoryGroup = 'na';
	    } else {
	    	catName = categoryList[i];
	    	if (categoryGroup == "") {
		    	categoryGroup = categoryList[i];
	    	} else {
		    	categoryGroup = categoryGroup + ':' + categoryList[i];		    		
	    	}
	    }
		
        if (i == 0) {
        	categoryRow["primaryCategory"] = catName;
	    } else {
        	var subCategoryLabel = 'subCategory' + (i);
	    	categoryRow[subCategoryLabel] = catName;
			var categoryGroupLabel = 'categorygroup' + (i);
			categoryRow[categoryGroupLabel] = (categoryGroup.replace(/\s/g,'')).toLowerCase();
	    }
	}
	
	var pageNameFinal = language + '/' + (pageName.replace(/\s/g,'-')).toLowerCase();
	 
	digitalData.page = {
			pageInfo:
			{
				pageName: pageNameFinal,
				pageType: pageType
			 },
			 category: categoryRow, 
			 attributes:
	         {
				  server: window.location.hostname,
	              language: language,
	              vendorName: vendor,
	              banners: banners != "" ? banners: 'na'
	         }
   };
	

	if (path.indexOf('search-results') != -1) {
		var searchTerm = $('#globalSearch input[name="searchTerms"]').val();
		if (typeof searchTerm==='undefined') {
			 searchTerm = $('#globalHybrisSearch input[name="text"]').val();
		} else {
			searchTerm = 'na';
		}
		var searchRedirect = 'na';
		setAnixterSearchAnalytics(undefined, searchTerm, searchRedirect);
	}
	
	else if (window.location.search.indexOf('?term=') != -1) {
		var searchSuccess = 'successful search';
		var searchTerm = (window.location.search.replace('?term=','')).replace(/%20/g, " ");
		var searchRedirect = 'redirect';
		setSearchAnalytics(searchSuccess, searchTerm, searchRedirect);
	}
	
	else if (path.indexOf('product-detail') != -1) {
		setPDP();
	}
}




function setAnixterSearchAnalytics(searchSuccess, searchTerm, searchRedirect) {
	count = $('#numResults').text();
	if (count == ''){
		count = 'na';
	}
	
	if (typeof searchSuccess==='undefined') {
		searchSuccess = count > 0 ? "successful search" : "null Search";
	}
	var searchPagination= $('#updateResultsForm .hidden input[name="pageNumber"]').val();
	
	if (typeof searchPagination==='undefined'){
		searchPagination = 'na';
	}
	var searchResultList =  $('#productList').data('product-number-list');
	if (typeof searchResultList==='undefined'){
		searchResultList = 'na';
	}
	
	digitalData.page.event = "search";
	digitalData.search = {
			searchTerm: searchTerm,
			searchResults: count,
			searchEvent: 'new search',
			searchFilter: "na",
			searchSort: "na",
			searchType: "standard",
			searchSuccess: searchSuccess,
			searchTermRecommended: "na",
			searchTermTypeAhead: "na",
			searchPagination: searchPagination,
			searchRedirect: searchRedirect,
			searchLandingPage: ((window.location.pathname.replace('/content/anixter/', '')).replace('.html','')),
			searchResultList: searchResultList
	};	
	 _satellite.track("searchInfo_load ");	
} 

function setPDP() {
	digitalData.page.event = 'pdp';
	digitalData.products = [{
	   partNumber: $("#productDetail").data("anixter-id")
	}];
}





var productdetail = productdetail || {
	handlers: {
		updatePriceInventoryInfo: function() {
		    $jq7('.buy-container .zero').hide();
		    $jq7('.buy-container .limited').hide();
		    $jq7('.buy-container .ready').hide();
		    $jq7('.buy-container .lead-time').hide();
		    $jq7('.product-price .product-price-unavailable').hide();
		    $jq7('.product-price .product-price-available').hide();
		    $jq7(" .unit-with-price")

		    var itemId = productdetail.info.detailsId; //$jq7('.product-info').attr('data-details-id');
		    if (!itemId) {
//		        itemId = $jq7('.product-info').data('details-id');
//		        if (!itemId) {
		            itemId = productdetail.info.anixterId;
//		        }
		    }

		    var prodPath = productdetail.info.path + itemId;

		    var prodContainer = $jq7(".product-container");

		    $jq7.get(prodPath, function(data) {

		        prodContainer.data("sellOnline", data.sellOnline);
		        prodContainer.data("price", data.price);

		        if (data.commerceEnabled) {

		            $jq7('.buy-container .min-qty').text(data.minOrderQuantity);
		            $jq7('.buy-container input[name="quant"]').attr('data-min', data.minOrderQuantity);
		            if (data.minOrderQuantity > 1) {
		                $jq7('.buy-container .min-quantity').show();
		            }

		            $jq7('.buy-container .incr-qty').text(data.orderIncrementQuantity);
		            $jq7('.buy-container input[name="quant"]').attr('data-step', data.orderIncrementQuantity);
		            if (data.orderIncrementQuantity > 1) {
		               $jq7('.buy-container .quantity-increment').show();
		            }
		        }

		        if (data.price && data.commerceEnabled) {

		            $jq7('.product-price .unit-price').text(data.priceFormatted);
		            $jq7('.product-price .product-price-unavailable').hide();
		            $jq7('.product-price .product-price-available').show();
		        } else {
		            $jq7('.product-price .product-price-unavailable').show();
		        }

		        if ((data.commerceEnabled) && (data.sellOnline) && (data.availableInventory > 0)) {
		            $jq7('.buy-container .sellable').show();
		            $jq7('.buy-container .rfq').hide();
		        } else {
		            $jq7('.buy-container .sellable').hide();
		            $jq7('.buy-container .rfq').show();
		        }

		        var uomCode = productdetail.info.uom;
		        if (uomCode == "EA") {
		            $jq7('.product-buy .unit-size').hide();
		        }

		        if (data.availableInventory <= 0){
		            $jq7('.buy-container .zero').show();
		            $jq7('.buy-container .lead-time').show();
		        } else if (data.availableInventory < data.lowQuantityThreshold){
		            $jq7('.buy-container .limited').show();
		            $jq7('.buy-container .lead-time').text(data.leadTimeDesc);
		            $jq7('.buy-container .lead-time').show();
		        } else {
		            $jq7('.buy-container .ready').show();
		            $jq7('.buy-container .lead-time').text(data.leadTimeDesc);
		            $jq7('.buy-container .lead-time').show();
		        }

		        activateAddToCartButton();
		    });
		}
	}
};

$jq7(document).ready(function() {
	if ($jq7("#productDetail").length > 0) {
		productdetail.info = $jq7("#productDetail").data();

		$jq7('#product-detail-tabs').tabs();

	    //product detail image carousel
	    var createArrowButtons = function (imgName) {
	        var img = $jq7(document.createElement('img')).attr({src: '/etc.clientlibs/settings/wcm/designs/anixter/images/resources/img/' + imgName, width: '22', height: '59', alt: ''});
	        var link = $jq7(document.createElement('a')).attr('src', '#').addClass('btn-prevNext');
	        return link.append(img);
	    }

	    //only initialize carousel if there are any thumbnail images
	    var numThumbnails = $jq7('.thumb-slider .rs-carousel-item').size();
	    if (numThumbnails > 0) {
	        $jq7('.thumb-slider').carousel({
	            itemsPerPage: 4,
	            pagination: false,
	            disabled: ( $jq7('.thumb-slider .rs-carousel-item').length < 4 ),
	            insertPrevAction: function () { return createArrowButtons('btnProductCarouselLeft.png').prependTo(this); },
	            insertNextAction: function () { return createArrowButtons('btnProductCarouselRight.png').appendTo(this); }
	        });
	        //if there is less than 5 images, hide the prev and next buttons
	        if (numThumbnails <= 4) {
	            $jq7('.thumb-slider .btn-prevNext').attr('style', 'visibility: hidden;');
	        }
	    }

	    //click thumbnail image in the carousel
	    $jq7('.detail-image-viewer li a').click(function() {
	        var src = $jq7(this).children().attr('src');
	        $jq7('.detail-image-viewer .detail-image img').attr('src', src.replace('V8.', 'V6.'));
	        $jq7('.detail-image-viewer .detail-image img').attr('caption', $jq7(this).children().attr('caption'));
	        return false;
	    });
	    $jq7('.detail-image-viewer li a:first').click();

	    //update the price and setup call to add to cart
	    productdetail.handlers.updatePriceInventoryInfo();

	    $jq7(".availability > [title]").tooltip();

	    $jq7(".purchase-button > .btn-question").tooltip();

	    /*$jq7("div.buy-container div.product-stock li.limited").tooltip();*/
	}
});
$jq7(function() {
    $jq7('#refinements').searchSideBar();
});
$jq7(function() {
    $jq7('#search-results-tabs').tabs();

    $jq7('#refinements').searchSideBar();
    $jq7('#contentRefinements').searchSideBar();

    //replace the marker with the search keyword
    var keyword = $jq7('.toolbar-results form input[name="searchTerms"]').attr('placeholder');
    if (!keyword) { keyword = $jq7('.toolbar-results form input[name="searchTerms"]').val(); }
    $jq7('div.noResults span.keyword').text(keyword);

    //check if content results tab should be selected
    if ($jq7('span#isHybrisEnabled').text() == 'true') {
    	 $jq7('#search-results-tabs').tabs('select', 1);
    	 $jq7('#didYouMeanWithResults').hide();
         $jq7('#productList').hide();
    }else{
    	if ($jq7('span#displayContentResults').text() == 'true') {
    		if($jq7('span#displayDidYouMean').text() == 'false'){
    			$jq7('#search-results-tabs').tabs('select', 1);
    		} else {
    			$jq7('#search-results-tabs').tabs('select', 0);
    		}
    		//this means that product search did not return any results, make the no results div visible
    		$jq7('.noproductresultstext').parent().removeClass('editModeOnly').show();
    		$jq7('#didYouMeanWithResults').hide();
    		$jq7('#productList').hide();
    	}

    	if ($jq7('span#displayDidYouMean').text() == 'true' && $jq7('span#noResults').text() == 'true') {
    		$jq7('.noproductresultstext').parent().removeClass('editModeOnly').show();
    		$jq7('#productList').hide();
    		$jq7('#didYouMeanWithResults').hide();
    	}
    }

});

function swapHybris(vale){
    //replace the marker with the search keyword
    var keyword = $jq7('.toolbar-results form input[name="text"]').val();
    try{
        $jq7('.toolbar-results form input[name="text"]').val(vale);
    }catch(err){}
};

function swap(vale){
    //replace the marker with the search keyword
    var keyword = $jq7('.toolbar-results form input[name="searchTerms"]').val();
    try{
        $jq7('.toolbar-results form input[name="searchTerms"]').val(vale);
    }catch(err){}
};

function submitform() {
    document.getElementById("globalSearch").submit();
};

function submitHybrisForm() {
    document.getElementById("globalHybrisSearch").submit();
};
$jq7(function() {
    $jq7('.products-grid .product-name p').truncate({
        lines: 5,
        complete: function(){ $jq7('.truncated').tooltip(); }
    });
});
$jq7(function() {
    $jq7('#refinements').searchSideBar();
});
