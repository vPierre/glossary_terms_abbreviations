

function tagAddToCart(productCode, mfgName, mfgNum, pageType) {
	if (typeof dataLayer !== 'undefined') {
		if (mfgNum == "") {
			mfgNum = "none";
		}
		if (mfgName == "") {
			mfgName = "none";
		}

		var eventvar = "added-to-cart";
		var category = "";
		if (pageType == "PRODUCT") {
			category = "non-add-productpage";
		} else if (pageType == "CATEGORY") {
			category = "non-add-category";
		} else if (pageType == "PRODUCTSEARCH") {
			category = "non-add-searchresult";
		} else {
			return true;
		}

		var labelvar = productCode + " | " + mfgName + " | " + mfgNum;
		dataLayer.push({
			'data-eventvar': eventvar,
			'data-actionvar': 'click',
			'data-labelvar': labelvar,
			'data-categoryvar': category
		});
		return true;
	}
}

function tagBundleAddToCart(bundleId, labelvar) {

	var eventvar = "added to cart - bundle - " + bundleId;
	var category = "multi-add-bundle";

	dataLayer.push({
		'data-eventvar' : eventvar,
		'data-actionvar' : 'click',
		'data-labelvar' : labelvar,
		'data-categoryvar' : category
	});
	return true;
}

function tagMultiSearchAddToCart() {
	if (typeof dataLayer !== 'undefined') {
		var eventvar = "added to cart - multiple search";
		var category = "multi-add-searchresult";
		var labelvar = "";

		$.each($('.form-add-to-cart'), function (index, val) {
			if ($(val).find('input[name="qty"]').val() > 0) {
				if ($(val).find('input[name="productMfgPost"]').val() == "") {
					labelvar = labelvar + $(val).find('input[name="productCodePost"]').val() + ' | ';
				} else {
					labelvar = labelvar + $(val).find('input[name="productMfgPost"]').val() + ' | ';
				}
			}
		});

		dataLayer.push({
			'data-eventvar': eventvar,
			'data-actionvar': 'click',
			'data-labelvar': labelvar,
			'data-categoryvar': category
		});
		return true;
	}
}



function tagAccessories(productCode, mfgName, mfgNum, curProdcode,  curProdMfgName, curProdMfgNum, pageType) {

	if (typeof dataLayer !== 'undefined') {
		if (mfgNum == "") {
			mfgNum = "none";
		}
		if (mfgName == "") {
			mfgName = "none";
		}
		if (curProdMfgNum == "") {
			curProdMfgNum = "none";
		}
		if (curProdMfgName == "") {
			curProdMfgName = "none";
		}

		var eventvar = "";
		var category = "";
		if (pageType == "PRODUCT") {
			eventvar = "accessories";
			category = "accessory-click-productpage";
		} else if (pageType == "CART") {
			eventvar = "accessories shopping cart";
			category = "accessory-click-shoppingcart";
		} else if (pageType == "ADDTOCARTPOPUP") {
			eventvar = "accessories popup";
			category = "accessory-click-popup";
		} else {
			return true;
		}

		var labelvar = productCode + " | " + mfgName + " | " + mfgNum;
		var ref = curProdcode + " | " + curProdMfgName + " | " + curProdMfgNum;
		if (pageType == "CART") {
			var data_eventvar = eventvar;
		} else {
			var data_eventvar = eventvar + " - " + ref;
		}
		dataLayer.push({
			'data-eventvar': data_eventvar,
			'data-actionvar': 'click',
			'data-labelvar': labelvar,
			'data-categoryvar': category
		});
		return true;
	}
}

function tagAccessoriesAddToCart(productCode, mfgName, mfgNum, curProdcode, curProdMfgName, curProdMfgNum, pageType) {

	if (typeof dataLayer !== 'undefined') {
		if (mfgNum == "") {
			mfgNum = "none";
		}
		if (mfgName == "") {
			mfgName = "none";
		}
		if (curProdMfgNum == "") {
			curProdMfgNum = "none";
		}
		if (curProdMfgName == "") {
			curProdMfgName = "none";
		}

		var eventvar = "";
		var category = "";
		if (pageType == "PRODUCT") {
			eventvar = "added to cart - accessories";
			category = "accessory-add-productpage";
		} else if (pageType == "CART") {
			eventvar = "added to cart - accessories shopping cart";
			category = "accessory-add-shoppingcart";
		} else if (pageType == "ADDTOCARTPOPUP") {
			eventvar = "added to cart - accessories popup";
			category = "accessory-add-popup";
		} else {
			return true;
		}

		var labelvar = productCode + " | " + mfgName + " | " + mfgNum;
		var ref = curProdcode + " | " + curProdMfgName + " | " + curProdMfgNum;
		if (pageType == "CART") {
			var data_eventvar = eventvar;
		} else {
			var data_eventvar = eventvar + " - " + ref;
		}
		dataLayer.push({
			'data-eventvar': data_eventvar,
			'data-actionvar': 'click',
			'data-labelvar': labelvar,
			'data-categoryvar': category
		});
		return true;
	}
}

function tagRequestQuote() {

	if (typeof dataLayer !== 'undefined') {
		Helper.showLoadingGraphic();

		var eventvar = "quote";
		var category = "quote";
		var labelvar = "RFQ";

		dataLayer.push({
			'data-eventvar': eventvar,
			'data-actionvar': 'submit',
			'data-labelvar': labelvar,
			'data-categoryvar': category
		});
		return true;
	}
}

function tagLogin(userId, userType) {

	if (typeof dataLayer !== 'undefined') {
		var eventvar = "login";
		var category = "login";
		var labelvar = userId;

		dataLayer.push({
			'data-eventvar': eventvar,
			'data-actionvar': 'click',
			'data-labelvar': labelvar,
			'data-categoryvar': category,
			'data-loginvar': 'Logged In',
			'data-customerType': userType,
			'event': 'login-click'
		});

		return true;
	}
}

function tagSoftLogin(userId, userType) {

	if (typeof dataLayer !== 'undefined') {
		var eventvar = "login";
		var category = "login";
		var labelvar = userId;

		dataLayer.push({
			'data-eventvar': eventvar,
			'data-actionvar': 'site-entry',
			'data-labelvar': labelvar,
			'data-categoryvar': category,
			'data-loginvar': 'Logged In',
			'data-customerType': userType,
			'event': 'login-click'
		});

		return true;
	}
}

function tagRegister(userId) {

	if (typeof dataLayer !== 'undefined') {
		var eventvar = 'register';
		var category = 'register';
		var labelvar = userId;

		dataLayer.push({
			'data-eventvar': eventvar,
			'data-actionvar': 'click',
			'data-labelvar': labelvar,
			'data-categoryvar': category,
			'data-loginvar': 'Logged In'
		});

		return true;
	}
}

function tagCheckout(userId, action) {

	if (typeof dataLayer !== 'undefined') {
		var eventvar = 'register';
		var category = 'register';
		var labelvar = userId;
		var actionvar = action;

		dataLayer.push({
			'event': 'register-click',
			'data-eventvar': eventvar,
			'data-actionvar': actionvar,
			'data-labelvar': labelvar,
			'data-categoryvar': category,
			'data-loginvar': 'Logged In',
			'data-customerType': 'B2C'
		});

		return true;
	}
}

function tagRegister(userId, customerType) {

	if (typeof dataLayer !== 'undefined') {
		var eventvar = "register";
		var category = "register";
		var labelvar = userId;
		var actionvar = customerType;

		if (actionvar == 'B2B') {
			dataLayer.push({
				'event': 'register-click',
				'data-eventvar': eventvar,
				'data-actionvar': actionvar,
				'data-labelvar': labelvar,
				'data-categoryvar': category,
				'data-customerType': actionvar
			});
		} else {
			dataLayer.push({
				'event': 'register-click',
				'data-eventvar': eventvar,
				'data-actionvar': actionvar,
				'data-labelvar': labelvar,
				'data-categoryvar': category,
				'data-loginvar': 'Logged In',
				'data-customerType': actionvar
			});
		}

		return true;
	}
}

function tagRegisterInvitation(userId) {
	if (typeof dataLayer !== 'undefined') {
		var eventvar = 'register';
		var category = 'register';
		var labelvar = userId;

		dataLayer.push({
			'event': 'register-click',
			'data-eventvar': eventvar,
			'data-actionvar': 'invitation',
			'data-labelvar': labelvar,
			'data-categoryvar': category,
			'data-loginvar': 'Logged In',
			'data-customerType': 'B2C'
		});

		return true;
	}
}

function tagAddToList(partNumber) {
	if (typeof dataLayer !== 'undefined') {
		dataLayer.push({
			'event': 'list-click',
			'data-eventvar': 'list',
			'data-actionvar': 'add',
			'data-labelvar': partNumber,
			'data-categoryvar': 'list'
		});
		return true;
	}
}

function tagCreateNewList(privacyLevel) {
	if (typeof dataLayer !== 'undefined') {
		dataLayer.push({
			'event': 'list-click',
			'data-eventvar': 'list',
			'data-actionvar': 'create',
			'data-labelvar': privacyLevel,
			'data-categoryvar': 'list'
		});
		return true;
	}
}

function tagAddToCartReelCut(productCode, mfgName, mfgNum) {
	if (typeof dataLayer !== 'undefined') {
		var labelvar = productCode + " | " + mfgName + " | " + mfgNum;

		dataLayer.push({
			'event': 'reel-cut',
			'data-eventvar': 'added-to-cart-reel-cut',
			'data-actionvar': 'click',
			'data-labelvar': labelvar,
			'data-categoryvar': 'reel-cut-add'
		});
		return true;
	}
}

