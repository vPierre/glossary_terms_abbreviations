// JavaScript Get Weather Data from XML
var xmlWeatherForecast = null;
var sWeatherXMLFile = "https://secure1.info.gov.hk/infofeed/en/weather.data.xml";
var sWeatherImagePath = "/images/weather/";
var sError = "[ERROR]";
var isIE6 = 0;
function loadWeather() {
    /****Added for data demo in testing domain****/
var url = window.location.href;
    if(url.indexOf("demo.com") > -1||url.indexOf("localhost") > -1) {
       if(url.indexOf("/tc/")){
           sWeatherXMLFile = "/data/demo.tc.weather.data.xml";
       } else if(url.indexOf("/sc/")){
           sWeatherXMLFile = "/data/demo.sc.weather.data.xml";
       } else {
           sWeatherXMLFile = "/data/demo.en.weather.data.xml";
       }
       sWeatherImagePath = "https://www.ogcio.gov.hk/module/weather/images/weather_png/";
    }
    /****END Added for data demo in testing domain****/
    var randomnumber = Math.floor(Math.random() * 100000);
    if (window.XDomainRequest) {
        xdr = new window.XDomainRequest();
        xdr.open("GET", sWeatherXMLFile + "?" + randomnumber);
        var params = "" + randomnumber;
        xdr.send(params);
        xdr.onload = function () {
            xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
            xmlDoc.async = false;
            xmlDoc.loadXML(xdr.responseText);
            xmlWeatherForecast = xmlDoc;
        };
    } else {
        if (window.XMLHttpRequest) {
            xhttp = new window.XMLHttpRequest();
            try {
                var params = "" + randomnumber;
                xhttp.open("GET", sWeatherXMLFile + "?" + randomnumber, false);
                xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                xhttp.send(params);

            } catch (e) {
                //e.message
            }
        } else {
            xhttp = new ActiveXObject("Microsoft.XMLHTTP");
            var sWeatherXMLFile2 = sWeatherXMLFile + "?" + randomnumber;
            xhttp.open("GET", sWeatherXMLFile2, false);
            isIE6 = 1;
            xhttp.send("");
        }
        try {
            xmlWeatherForecast = xhttp.responseXML;
        } catch (e) {
        }
    }


}
;
function foo() {
    var internalVal = 5;
    function timoutFunc() {
        reloadWeather();
    }
    this.setVal = function (val) {
        internalVal = val;
    }
    this.causeTimeout = function () {
        setTimeout(timoutFunc, 60000);
    }
}
function reloadWeather() {
    console.log("reloadWeather");
    getWeatherData("WD", "EN");
}
function returnCurrentENMonth() {
    var d = new Date();
    var month = new Array(12);
    month[0] = "January";
    month[1] = "February";
    month[2] = "March";
    month[3] = "April";
    month[4] = "May";
    month[5] = "June";
    month[6] = "July";
    month[7] = "August";
    month[8] = "September";
    month[9] = "October";
    month[10] = "November";
    month[11] = "December";
    return month[d.getMonth()];
}
function returnCurrentTCMonth() {
    var d = new Date();
    var month = new Array(12);
    month[0] = "1";
    month[1] = "2";
    month[2] = "3";
    month[3] = "4";
    month[4] = "5";
    month[5] = "6";
    month[6] = "7";
    month[7] = "8";
    month[8] = "9";
    month[9] = "10";
    month[10] = "11";
    month[11] = "12";
    return month[d.getMonth()];
}
function getWeatherIconLocalFileName(weatherFileName) {
    switch (weatherFileName) {
        //weather icon sunny 
        case "pic50.png":
            return "weathericon-07.png";
        case "pic51.png":
            return "weathericon-08.png";
        case "pic52.png":
            return "weathericon-09.png";
        case "pic53.png":
            return "weathericon-10.png";
        case "pic54.png":
            return "weathericon-11.png";
            //weather icon  cloudy or rain
        case "pic60.png":
            return "weathericon-12.png";
        case "pic61.png":
            return "weathericon-13.png";
        case "pic62.png":
            return "weathericon-14.png";
        case "pic63.png":
            return "weathericon-15.png";
        case "pic64.png":
            return "weathericon-16.png";
        case "pic65.png":
            return "weathericon-17.png";
            //weather icon star and cloudy
        case "pic70.png":
            return "weathericon-18.png";
        case "pic71.png":
            return "weathericon-19.png";
        case "pic72.png":
            return "weathericon-20.png";
        case "pic73.png":
            return "weathericon-21.png";
        case "pic74.png":
            return "weathericon-22.png";
        case "pic75.png":
            return "weathericon-23.png";
        case "pic76.png":
            return "weathericon-24.png";
        case "pic77.png":
            return "weathericon-25.png";
            //weather icon star and cloudy
        case "pic80.png":
            return "weathericon-26.png";
        case "pic81.png":
            return "weathericon-27.png";
        case "pic82.png":
            return "weathericon-28.png";
        case "pic83.png":
            return "weathericon-29.png";
        case "pic84.png":
            return "weathericon-30.png";
        case "pic85.png":
            return "weathericon-31.png";
            //weather icon temperature
        case "pic90.png":
            return "weathericon-32.png";
        case "pic91.png":
            return "weathericon-33.png";
        case "pic92.png":
            return "weathericon-34.png";
        case "pic93.png":
            return "weathericon-35.png";
            //weather warning
        case "raina.png":
            return "weathericon-36.png";
        case "rainb.png":
            return "weathericon-37.png";
        case "rainr.png":
            return "weathericon-38.png";
        case "sms.png":
            return "weathericon-39.png";
        case "tc1.png":
            return "weathericon-40.png";
        case "tc3.png":
            return "weathericon-41.png";
        case "tc8ne.png":
            return "weathericon-42.png";
        case "tc8nw.png":
            return "weathericon-43.png";
        case "tc8se.png":
            return "weathericon-44.png";
        case "tc8sw.png":
            return "weathericon-45.png";
        case "tc9.png":
            return "weathericon-46.png";
        case "tc10.png":
            return "weathericon-47.png";
        case "ts.png":
            return "weathericon-48.png";
        case "tsunami-warn.png":
            return "weathericon-49.png";
        case "vhot.png":
            return "weathericon-50.png";
        case "cold.png":
            return "weathericon-01.png";
        case "firer.png":
            return "weathericon-02.png";
        case "firey.png":
            return "weathericon-03.png";
        case "frost.png":
            return "weathericon-04.png";
        case "ntfl.png":
            return "weathericon-05.png";
        case "landslip.png":
            return "weathericon-06.png";
        default:
            return "weathericon-07.png";
    }
}
function getWeatherIconCaption(warning, sLang) {
    warning = warning.substring(0, warning.length - 4);
    if (sLang == 'EN') {
        return getWeatherIconCaptionEN(warning);
    } else if (sLang == 'TC') {
        return getWeatherIconCaptionTC(warning);
    } else if (sLang == 'SC') {
        return getWeatherIconCaptionSC(warning);
    }
}
function getWeatherIconCaptionEN(warning) {
    if (warning == "pic50")
        return "Sunny";
    if (warning == "pic51")
        return "Sunny Periods";
    if (warning == "pic52")
        return "Sunny Intervals";
    if (warning == "pic53")
        return "Sunny Periods with A Few Showers";
    if (warning == "pic54")
        return "Sunny Intervals with Showers";
    if (warning == "pic60")
        return "Cloudy";
    if (warning == "pic61")
        return "Overcast";
    if (warning == "pic62")
        return "Light Rain";
    if (warning == "pic63")
        return "Rain";
    if (warning == "pic64")
        return "Heavy Rain";
    if (warning == "pic65")
        return "Thunderstorms";
    if (warning == "pic70")
        return "Fine";
    if (warning == "pic71")
        return "Fine";
    if (warning == "pic72")
        return "Fine";
    if (warning == "pic73")
        return "Fine";
    if (warning == "pic74")
        return "Fine";
    if (warning == "pic75")
        return "Fine";
    if (warning == "pic76")
        return "Mainly Cloudy";
    if (warning == "pic77")
        return "Mainly Fine";
    if (warning == "pic80")
        return "Windy";
    if (warning == "pic81")
        return "Dry";
    if (warning == "pic82")
        return "Humid";
    if (warning == "pic83")
        return "Fog";
    if (warning == "pic84")
        return "Mist";
    if (warning == "pic85")
        return "Haze";
    if (warning == "pic90")
        return "Hot";
    if (warning == "pic91")
        return "Warm";
    if (warning == "pic92")
        return "Cool";
    if (warning == "pic93")
        return "Cold";
    if (warning == "cold")
        return "Cold Weather Warning";
    if (warning == "firer")
        return "Red Fire Danger Warning";
    if (warning == "firey")
        return "Yellow Fire Danger Warning";
    if (warning == "frost")
        return "Frost Warning";
    if (warning == "landslip")
        return "Landslip Warning";
    if (warning == "ntfl")
        return "Special Announcement on flooding in The Northern New Territories";
    if (warning == "raina")
        return "Amber Rainstorm Warning Signal";
    if (warning == "rainb")
        return "Black Rainstorm Warning Signal";
    if (warning == "rainr")
        return "Red Rainstorm Warning Signal";
    if (warning == "sms")
        return "Strong Monsoon Signal";
    if (warning == "tc1")
        return "Standby Signal No.1";
    if (warning == "tc3")
        return "Strong Wind Signal No.3";
    if (warning == "tc8ne")
        return "No.8 Northeast Gale or Storm Signal";
    if (warning == "tc8nw")
        return "No.8 Northwest Gale or Storm Signal";
    if (warning == "tc8se")
        return "No.8 Southeast Gale or Storm Signal";
    if (warning == "tc8sw")
        return "No.8 Southwest Gale or Storm Signal";
    if (warning == "tc9")
        return "Increasing Gale or Storm Signal No.9";
    if (warning == "tc10")
        return "Hurricane Signal No.10";
    if (warning == "ts")
        return "Thunderstorm Warning";
    if (warning == "tsunami-warn")
        return "Tsunami Warning";
    if (warning == "vhot")
        return "Very Hot Weather Warning";
    return "";
}
function getWeatherIconCaptionSC(warning) {
    if (warning == "pic50")
        return "阳光充沛";
    if (warning == "pic51")
        return "间有阳光";
    if (warning == "pic52")
        return "短暂阳光";
    if (warning == "pic53")
        return "间有阳光几阵骤雨";
    if (warning == "pic54")
        return "短暂阳光有骤雨";
    if (warning == "pic60")
        return "多云";
    if (warning == "pic61")
        return "密云";
    if (warning == "pic62")
        return "微雨";
    if (warning == "pic63")
        return "雨";
    if (warning == "pic64")
        return "大雨";
    if (warning == "pic65")
        return "雷暴";
    if (warning == "pic70")
        return "天色良好";
    if (warning == "pic71")
        return "天色良好";
    if (warning == "pic72")
        return "天色良好";
    if (warning == "pic73")
        return "天色良好";
    if (warning == "pic74")
        return "天色良好";
    if (warning == "pic75")
        return "天色良好";
    if (warning == "pic76")
        return "大致多云";
    if (warning == "pic77")
        return "天色大致良好";
    if (warning == "pic80")
        return "大风";
    if (warning == "pic81")
        return "干燥";
    if (warning == "pic82")
        return "潮湿";
    if (warning == "pic83")
        return "雾";
    if (warning == "pic84")
        return "薄雾";
    if (warning == "pic85")
        return "烟霞";
    if (warning == "pic90")
        return "热";
    if (warning == "pic91")
        return "暖";
    if (warning == "pic92")
        return "凉";
    if (warning == "pic93")
        return "冷";
    if (warning == "cold")
        return "寒冷天气警告";
    if (warning == "firer")
        return "红色火灾危险警告";
    if (warning == "firey")
        return "黄色火灾危险警告";
    if (warning == "frost")
        return "霜冻警告";
    if (warning == "landslip")
        return "山泥倾泻警告";
    if (warning == "ntfl")
        return "新界北部水浸特别报告";
    if (warning == "raina")
        return "黄色暴雨警告信号";
    if (warning == "rainb")
        return "黑色暴雨警告信号";
    if (warning == "rainr")
        return "红色暴雨警告信号";
    if (warning == "sms")
        return " 强烈季候风信号";
    if (warning == "tc1")
        return " 一号戒备信号";
    if (warning == "tc3")
        return " 三号戒备信号";
    if (warning == "tc8ne")
        return " 八号东北烈风或暴风信号";
    if (warning == "tc8nw")
        return "八号西北烈风或暴风信号";
    if (warning == "tc8se")
        return "八号东南烈风或暴风信号";
    if (warning == "tc8sw")
        return "八号西南烈风或暴风信号";
    if (warning == "tc9")
        return " 九号烈风或暴风增强信号";
    if (warning == "tc10")
        return "十号飓风信号";
    if (warning == "ts")
        return "雷暴警告";
    if (warning == "tsunami-warn")
        return "海啸警告";
    if (warning == "vhot")
        return "酷热天气警告";
    return "";
}
function getWeatherIconCaptionTC(warning) {
    if (warning == "pic50")
        return "陽光充沛";
    if (warning == "pic51")
        return "間有陽光";
    if (warning == "pic52")
        return "短暫陽光";
    if (warning == "pic53")
        return "間有陽光幾陣驟雨";
    if (warning == "pic54")
        return "短暫陽光有驟雨";
    if (warning == "pic60")
        return "多雲";
    if (warning == "pic61")
        return "密雲";
    if (warning == "pic62")
        return "微雨";
    if (warning == "pic63")
        return "雨";
    if (warning == "pic64")
        return "大雨";
    if (warning == "pic65")
        return "雷暴";
    if (warning == "pic70")
        return "天色良好";
    if (warning == "pic71")
        return "天色良好";
    if (warning == "pic72")
        return "天色良好";
    if (warning == "pic73")
        return "天色良好";
    if (warning == "pic74")
        return "天色良好";
    if (warning == "pic75")
        return "天色良好";
    if (warning == "pic76")
        return "大致多雲";
    if (warning == "pic77")
        return "天色大致良好";
    if (warning == "pic80")
        return "大風";
    if (warning == "pic81")
        return "乾燥";
    if (warning == "pic82")
        return "潮濕";
    if (warning == "pic83")
        return "霧";
    if (warning == "pic84")
        return "薄霧";
    if (warning == "pic85")
        return "煙霞";
    if (warning == "pic90")
        return "熱";
    if (warning == "pic91")
        return "暖";
    if (warning == "pic92")
        return "涼";
    if (warning == "pic93")
        return "冷";
    if (warning == "cold")
        return "寒冷天氣警告";
    if (warning == "firer")
        return "紅色火災危險警告";
    if (warning == "firey")
        return "黃色火災危險警告";
    if (warning == "frost")
        return "霜凍警告";
    if (warning == "landslip")
        return "山泥傾瀉警告";
    if (warning == "ntfl")
        return "新界北部水浸特別報告";
    if (warning == "raina")
        return "黃色暴雨警告信號";
    if (warning == "rainb")
        return "黑色暴雨警告信號";
    if (warning == "rainr")
        return "紅色暴雨警告信號";
    if (warning == "sms")
        return " 強烈季候風信號";
    if (warning == "tc1")
        return " 一號戒備信號";
    if (warning == "tc3")
        return " 三號戒備信號";
    if (warning == "tc8ne")
        return " 八號東北烈風或暴風信號";
    if (warning == "tc8nw")
        return "八號西北烈風或暴風信號";
    if (warning == "tc8se")
        return "八號東南烈風或暴風信號";
    if (warning == "tc8sw")
        return "八號西南烈風或暴風信號";
    if (warning == "tc9")
        return " 九號烈風或暴風增強信號";
    if (warning == "tc10")
        return "十號颶風信號";
    if (warning == "ts")
        return "雷暴警告";
    if (warning == "tsunami-warn")
        return "海嘯警告";
    if (warning == "vhot")
        return "酷熱天氣警告";
    return "";
}
function getFileNameFromImageURL(imageUrl) {
    var i = imageUrl.lastIndexOf("/");
    if (i == -1) {
        return null;
    }
    return imageUrl.substring(i + 1);
}
function retryLater(func, arrayOfArgs, trial) {
    var trialCount = trial - 1;
    setTimeout(function () {
        if (!func.apply(undefined, arrayOfArgs) && trialCount > 0) {
            retryLater(func, arrayOfArgs, trialCount - 1);
        }
    }, 500);
}
function getWeatherData(sType, sLang, retry) {
    if (xmlWeatherForecast == null) {
        if (typeof retry === 'undefined' || retry) {
            retryLater(getWeatherData, [sType, sLang, false], 10);
        }
        return false;
    }
    var warningArray = new Array();
    var iCountWarning = 0;
    var sHTMLTemp = "";
    var sHTMLImage = "";
    var sHTMLDate = "";
    var sHTMLHumi = "";
    var sLastUpdateTime = "";
    var sEN = "";
    var sTC = "";
    var sTemp = "";
    var sHumi = "";
    var sURL = "";
    // Part: Node - <LastUpdateTime>
    var oLastUpdateTime = xmlWeatherForecast.getElementsByTagName("LastUpdateTime");
    if (oLastUpdateTime != null) {
        if (oLastUpdateTime.length > 0) {
            sLastUpdateTime = oLastUpdateTime[0].childNodes[0].nodeValue;

        }
    }
    //show weather info if data was updated in last 2 hour.
    var showWeather = true;
    /*
     if(sLastUpdateTime!=""){
     var curruntTime = new Date().getTime();
     var updateTime = stringToDate(sLastUpdateTime).getTime();
     showWeather = (curruntTime - updateTime)<7200000;
     }*/

    if (showWeather) {
        // Part: Node - <LocalWeatherForecast>
        var oCurrentWeather = xmlWeatherForecast.getElementsByTagName("CurrentWeather");
        if (oCurrentWeather != null && oCurrentWeather.length > 0) {
            var oTemperatureInformation = oCurrentWeather[0].getElementsByTagName("TemperatureInformation");
            if (oTemperatureInformation != null && oTemperatureInformation.length > 0) {
                var curTemp = oTemperatureInformation[0].getElementsByTagName("Measure");
                if (curTemp != null && curTemp.length > 0) {
                    sTemp = curTemp[0].childNodes[0].nodeValue;
                }
            }
        }
        // Part: Node - <RelativeHumidityInformation>
        if (oCurrentWeather != null && oCurrentWeather.length > 0) {
            oRelativeHumidityInformation = oCurrentWeather[0].getElementsByTagName("RelativeHumidityInformation");
            if (oRelativeHumidityInformation != null) {
                if (oRelativeHumidityInformation.length > 0) {
                    // Part: Node - <Measure>
                    var oMeasure = oRelativeHumidityInformation[0].getElementsByTagName("Measure");
                    if (oMeasure != null) {
                        if (oMeasure.length > 0) {
                            sHumi = oMeasure[0].childNodes[0].nodeValue;
                        }
                    }
                }
            }
        }
        var oLocalWeatherForecast = xmlWeatherForecast.getElementsByTagName("LocalWeatherForecast");
        if (oLocalWeatherForecast != null) {
            if (oLocalWeatherForecast.length > 0) {
                // Part: Node - <WeatherForecast>
                var oWeatherForecast = oLocalWeatherForecast[0].getElementsByTagName("WeatherForecast");
                if (oWeatherForecast != null) {
                    if (oWeatherForecast.length > 0) {
                        // Part: Node - <WeatherDescription>
                        oWeatherDescription = oWeatherForecast[0].getElementsByTagName("WeatherDescription");
                        if (oWeatherDescription != null) {
                            if (oWeatherDescription.length > 0) {
                                // Part: Node - <EN>
                                var oEN = oWeatherDescription[0].getElementsByTagName("EN");
                                if (oEN != null) {
                                    if (oEN.length > 0) {
                                        sEN = oEN[0].childNodes[0].nodeValue;
                                    }
                                }
                                // Part: Node - <TC>
                                var oTC = oWeatherDescription[0].getElementsByTagName("TC");
                                if (oTC != null) {
                                    if (oTC.length > 0) {
                                        sTC = oTC[0].childNodes[0].nodeValue;
                                    }
                                }
                            }
                        }
                        // Part: Node - <TemperatureInformation>
                        oTemperatureInformation = oWeatherForecast[0].getElementsByTagName("TemperatureInformation");
                        if (oTemperatureInformation != null) {
                            if (oTemperatureInformation.length > 0) {
                                // Part: Node - <Measure>
                                var oMeasure = oTemperatureInformation[0].getElementsByTagName("Measure");
                                if (oMeasure != null) {
                                    if (oMeasure.length > 0) {
                                        //sTemp = oMeasure[0].childNodes[0].nodeValue;
                                    }
                                }
                            }
                        }
                        // Part: Node - <WeatherIcon>
                        oWeatherIcon = oWeatherForecast[0].getElementsByTagName("WeatherIcon");
                        if (oWeatherIcon != null) {
                            if (oWeatherIcon.length > 0) {
                                // Part: Node - <EN>
                                var oURL = oWeatherIcon[0].getElementsByTagName("URL");
                                if (oURL != null) {
                                    if (oURL.length > 0) {
                                        sURL = oURL[0].childNodes[0].nodeValue;
                                        sURL = getFileNameFromImageURL(sURL);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        // Part: Node - <WeatherWarningSummary>
        var oWeatherWarningSummary = xmlWeatherForecast.getElementsByTagName("WeatherWarningSummary");
        if (oWeatherWarningSummary != null) {
            if (oWeatherWarningSummary.length > 0) {
                // Part: Node - <WeatherWarning>
                var oWeatherWarning = oWeatherWarningSummary[0].getElementsByTagName("WeatherWarning");
                if (oWeatherWarning != null) {
                    for (var i = 0; i < oWeatherWarning.length; i++) {
                        // Part: Node - <WeatherIcon>
                        oWeatherIcon = oWeatherWarning[i].getElementsByTagName("WeatherIcon");
                        if (oWeatherIcon != null) {
                            if (oWeatherIcon.length > 0) {
                                // Part: Node - <URL>
                                var oURL = oWeatherIcon[0].getElementsByTagName("URL");
                                if (oURL != null) {
                                    if (oURL.length > 0) {
                                        warningArray[iCountWarning] = getFileNameFromImageURL(oURL[0].childNodes[0].nodeValue);
                                        iCountWarning++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        // part: replace the HTML code (Start)
        if (!isNullEmptyError(sTemp)) {
            //sHTMLTemp = sTemp+"&deg;C<br>";
            sHTMLTemp = sTemp + "&deg;C";
        }
        if (!isNullEmptyError(sHumi)) {
            sHTMLHumi = sHumi + "%";
        }
        var weatherCaption = "";
        if (!isNullEmptyError(sURL)) {
            weatherCaption = getWeatherIconCaption(sURL, sLang);
            //sHTMLImage = "<img src=\""+sWeatherImagePath+""+sURL+"s.gif\" alt=\"" + weatherCaption + "\" title=\"" + weatherCaption + "\" />";
            // sHTMLImage = "<img src=\"" + sWeatherImagePath + "" +  + " alt=\"" + weatherCaption + "\" title=\"" + weatherCaption + "\" />";
            sHTMLImage = "<img src=\"" + sWeatherImagePath + getWeatherIconLocalFileName(sURL) + "\"" + " alt=\"" + weatherCaption + "\" title=\"" + weatherCaption + "\" >";
        }
        if (warningArray != null) {
            for (var i = 0; i < warningArray.length; i++) {
                if (!isNullEmptyError(warningArray[i])) {
                    weatherCaption = getWeatherIconCaption(warningArray[i], sLang);
                    //sHTMLImage += "<img src=\""+sWeatherImagePath+""+warningArray[i]+"s.gif\" alt=\"" + weatherCaption + "\" title=\"" + weatherCaption + "\" >";
                    sHTMLImage += "<img src=\"" + sWeatherImagePath + "" + getWeatherIconLocalFileName(warningArray[i]) + "\" alt=\"" + weatherCaption + "\" title=\"" + weatherCaption + "\">";
                }
            }
        }
        //if (!isNullEmptyError(sLastUpdateTime)){
        //	if (!isNullEmptyError(sLastUpdateTime.substring(8,10))
        //		&& !isNullEmptyError(sLastUpdateTime.substring(5,7))
        //		&& !isNullEmptyError(sLastUpdateTime.substring(0,4))){
        //var sHTMLDateD = sLastUpdateTime.substring(8,10)
        //var sHTMLDateM = sLastUpdateTime.substring(5,7);
        //var sHTMLDateY = sLastUpdateTime.substring(0,4);
        var sHTMLDateTime = '';
        if (sLastUpdateTime != "") {
            var tmpTime = sLastUpdateTime.substring(11, 16);
            var hour = parseInt(tmpTime.substring(0, 2));
            var minSt = tmpTime.substring(3, 5);
            if (hour >= 12) {
                if (hour == 12) {
                    sHTMLDateTime = "12:" + minSt + " pm";
                } else if (hour == 24) {
                    sHTMLDateTime = "00:" + minSt + " am";
                } else if ((hour - 12).toString().length > 1) {
                    sHTMLDateTime = (hour - 12).toString() + ":" + minSt + " pm";
                } else {
                    sHTMLDateTime = "0" + (hour - 12).toString() + ":" + minSt + " pm";
                }
            } else {
                sHTMLDateTime = tmpTime + " am";
            }
            // sHTMLDateTime = sLastUpdateTime.substring(11, 16)
        }
        //alert(sHTMLDateTime)
        //alert(sHTMLDateD + "/" + sHTMLDateM + "/" + sHTMLDateY)
        var clientDate = new Date();
        //alert(clientDate.toUTCString())
        var sHTMLDateD = clientDate.getDate();
        var sHTMLDateM = clientDate.getMonth();
        var sHTMLDateY = clientDate.getFullYear();
        if (sLang == "EN") {
			sHTMLDateTime = "[ Last Update " + sHTMLDateTime + " ]";
            sHTMLDateM = returnCurrentENMonth();
            sHTMLDate = sHTMLDateM + " " + sHTMLDateD + ", " + sHTMLDateY;
            sHTMLHumi = "Humidity " + sHTMLHumi;
        } else if (sLang == "TC") {
			sHTMLDateTime = "[ 最後更新 " + sHTMLDateTime + " ]";
            sHTMLDateM = returnCurrentTCMonth();
            sHTMLDate = sHTMLDateY + "年" + sHTMLDateM + "月" + sHTMLDateD + "日";
            sHTMLHumi = "濕度 " + sHTMLHumi;
        } else if (sLang == "SC") {
			sHTMLDateTime = "[ 最後更新 " + sHTMLDateTime + " ]";
            sHTMLDateM = returnCurrentTCMonth();
            sHTMLDate = sHTMLDateY + "年" + sHTMLDateM + "月" + sHTMLDateD + "日";
            sHTMLHumi = "湿度 " + sHTMLHumi;
        }
        if (sType == "WT") {
            var oTemp = document.getElementById("tempTop");
            if (oTemp != null) {
                var newdiv = document.createElement("span");
                newdiv.innerHTML = sHTMLTemp;
                oTemp.removeChild(oTemp.firstChild);
                oTemp.appendChild(newdiv);
            }
        } else if (sType == "DT") {
            var oDateTime = document.getElementById("lastUpdateTime");
            if (oDateTime != null) {
                var newdiv = document.createElement("span");
                newdiv.innerHTML = sHTMLDateTime;
                oDateTime.removeChild(oDateTime.firstChild);
                oDateTime.appendChild(newdiv);
            }
        } else if (sType == "WI") {
            var oImage = document.getElementById("weatherIconTop");
            if (oImage != null) {
                var newdiv = document.createElement("span");
                newdiv.innerHTML = sHTMLImage;
                oImage.removeChild(oImage.firstChild);
                oImage.appendChild(newdiv);
            }
        } else if (sType == "WD") {
            var oDate = document.getElementById("today");
            if (oDate != null) {
                var newdiv = document.createElement("span");
                newdiv.innerHTML = sHTMLDate;
                oDate.removeChild(oDate.firstChild);
                oDate.appendChild(newdiv);
            }
        } else if (sType == "WH") {
            var oHumidity = document.getElementById("CurrentHumidity");
            if (oHumidity != null) {
                var newdiv = document.createElement("span");
                newdiv.innerHTML = sHTMLHumi;
                oHumidity.removeChild(oHumidity.firstChild);
                oHumidity.appendChild(newdiv);
            }
        }
    }
    // part: replace the HTML code (End)
    return true;
}

function getWeatherForecastData(sType, sLang) {
    var sHTMLWeather1 = "";
    var sHTMLWeather2 = "";
    var sHTMLWeather3 = "";
    // Part: Node - <SeveralDaysWeatherForecast>
    var oSeveralDaysWeatherForecast = xmlWeatherForecast.getElementsByTagName("SeveralDaysWeatherForecast");
    if (oSeveralDaysWeatherForecast != null) {
        if (oSeveralDaysWeatherForecast.length > 0) {
            // Part: Node - <WeatherForecast>
            var oWeatherForecast = oSeveralDaysWeatherForecast[0].getElementsByTagName("WeatherForecast");
            if (oWeatherForecast != null) {
                for (var j = 0; j < oWeatherForecast.length; j++) {
                    var sHTMLWeather = "";
                    var sStartTime = "";
                    var sStartTimeWeekday = "";
                    var sEN = "";
                    var sTC = "";
                    var sFromTemp = "";
                    var sEndTemp = "";
                    var sFromHumi = "";
                    var sEndHumi = "";
                    var sURL = "";
                    // Part: Node - <StartTime>
                    oStartTime = oWeatherForecast[j].getElementsByTagName("StartTime");
                    if (oStartTime != null) {
                        if (oStartTime.length > 0) {
                            sStartTime = oStartTime[0].childNodes[0].nodeValue;
                            //alert("StartTime["+j+"]: "+sStartTime);
                        }
                    }
                    // Part: Node - <StartTimeWeekday>
                    oStartTimeWeekday = oWeatherForecast[j].getElementsByTagName("StartTimeWeekday");
                    if (oStartTimeWeekday != null) {
                        if (oStartTimeWeekday.length > 0) {
                            sStartTimeWeekday = oStartTimeWeekday[0].childNodes[0].nodeValue;
                            //alert("StartTimeWeekday["+j+"]: "+sStartTimeWeekday);
                        }
                    }
                    // Part: Node - <WeatherDescription>
                    oWeatherDescription = oWeatherForecast[j].getElementsByTagName("WeatherDescription");
                    if (oWeatherDescription != null) {
                        if (oWeatherDescription.length > 0) {
                            // Part: Node - <EN>
                            var oEN = oWeatherDescription[0].getElementsByTagName("EN");
                            if (oEN != null) {
                                if (oEN.length > 0) {
                                    sEN = oEN[0].childNodes[0].nodeValue;
                                    //alert("EN["+j+"]: "+sEN);
                                }
                            }
                            // Part: Node - <TC>
                            var oTC = oWeatherDescription[0].getElementsByTagName("TC");
                            if (oTC != null) {
                                if (oTC.length > 0) {
                                    sTC = oTC[0].childNodes[0].nodeValue;
                                    //alert("TC["+j+"]: "+sTC);
                                }
                            }
                        }
                    }
                    // Part: Node - <TemperatureInformation>
                    oTemperatureInformation = oWeatherForecast[j].getElementsByTagName("TemperatureInformation");
                    if (oTemperatureInformation != null) {
                        if (oTemperatureInformation.length > 0) {
                            // Part: Node - <From>
                            var oFrom = oTemperatureInformation[0].getElementsByTagName("From");
                            if (oFrom != null) {
                                if (oFrom.length > 0) {
                                    sFromTemp = oFrom[0].childNodes[0].nodeValue;
                                    //alert("From["+j+"]: "+sFromTemp);
                                }
                            }
                            // Part: Node - <End>
                            var oEnd = oTemperatureInformation[0].getElementsByTagName("End");
                            if (oEnd != null) {
                                if (oEnd.length > 0) {
                                    sEndTemp = oEnd[0].childNodes[0].nodeValue;
                                    //alert("End["+j+"]: "+sEndTemp);
                                }
                            }
                        }
                    }
                    // Part: Node - <RelativeHumidityInformation>
                    oRelativeHumidityInformation = oWeatherForecast[j].getElementsByTagName("RelativeHumidityInformation");
                    if (oRelativeHumidityInformation != null) {
                        if (oRelativeHumidityInformation.length > 0) {
                            // Part: Node - <From>
                            var oFrom = oRelativeHumidityInformation[0].getElementsByTagName("From");
                            if (oFrom != null) {
                                if (oFrom.length > 0) {
                                    sFromHumi = oFrom[0].childNodes[0].nodeValue;
                                    //alert("From["+j+"]: "+sFromHumi);
                                }
                            }
                            // Part: Node - <End>
                            var oEnd = oRelativeHumidityInformation[0].getElementsByTagName("End");
                            if (oEnd != null) {
                                if (oEnd.length > 0) {
                                    sEndHumi = oEnd[0].childNodes[0].nodeValue;
                                    //alert("End["+j+"]: "+sEndHumi);
                                }
                            }
                        }
                    }
                    // Part: Node - <WeatherIcon>
                    oWeatherIcon = oWeatherForecast[j].getElementsByTagName("WeatherIcon");
                    if (oWeatherIcon != null) {
                        if (oWeatherIcon.length > 0) {
                            // Part: Node - <EN>
                            var oURL = oWeatherIcon[0].getElementsByTagName("URL");
                            if (oURL != null) {
                                if (oURL.length > 0) {
                                    sURL = oURL[0].childNodes[0].nodeValue;
                                    //alert("URL["+j+"]: "+sURL);
                                }
                            }
                        }
                    }
                    // part: replace the HTML code (Start)
                    if (!isNullEmptyError(sURL)) {
                        sHTMLWeather = "<img src=\"" + sWeatherImagePath + "" + sURL + ".gif\" alt=\"\" ><br>";
                    }
                    if (!isNullEmptyError(sStartTime)) {
                        if (!isNullEmptyError(sStartTime.substring(8, 10))
                                && !isNullEmptyError(sStartTime.substring(5, 7))
                                && !isNullEmptyError(sStartTime.substring(0, 4))) {
                            sHTMLWeather += sStartTime.substring(8, 10) + "." + sStartTime.substring(5, 7) + "." + sStartTime.substring(0, 4);
                        }
                    }
                    if (sLang == "EN") {
                        if (!isNullEmptyError(sEN)) {
                            sHTMLWeather += "<p>" + sEN + "</p>";
                        }
                    } else if (sLang == "TC") {
                        if (!isNullEmptyError(sTC)) {
                            sHTMLWeather += "<p>" + sTC + "</p>";
                        }
                    }
                    if (!isNullEmptyError(sFromTemp) && !isNullEmptyError(sEndTemp)) {
                        sHTMLWeather += sFromTemp + "-" + sEndTemp + "&deg;C<br>";
                    }
                    if (!isNullEmptyError(sFromHumi) && !isNullEmptyError(sEndHumi)) {
                        sHTMLWeather += sFromHumi + "-" + sEndHumi + "%";
                    }
                    //alert(sHTMLWeather);

                    if (j == 0 && sType == "F1") {
                        var oObj = document.getElementById("weather_1");
                        if (oObj != null) {
                            //document.write(sHTMLWeather);
                            if (isIE6 == 1) {
                                var newdiv = document.createElement("div");
                                newdiv.innerHTML = sHTMLWeather;
                                oObj.removeChild(oObj.firstChild);
                                oObj.appendChild(newdiv);
                            } else
                                oObj.innerHTML = sHTMLWeather;
                        }
                    } else if (j == 1 && sType == "F2") {
                        var oObj = document.getElementById("weather_2");
                        if (oObj != null) {
                            //document.write(sHTMLWeather);
                            if (isIE6 == 1) {
                                var newdiv = document.createElement("div");
                                newdiv.innerHTML = sHTMLWeather;
                                oObj.removeChild(oObj.firstChild);
                                oObj.appendChild(newdiv);
                            } else
                                oObj.innerHTML = sHTMLWeather;
                        }
                    } else if (j == 2 && sType == "F3") {
                        var oObj = document.getElementById("weather_3");
                        if (oObj != null) {
                            //document.write(sHTMLWeather);
                            if (isIE6 == 1) {
                                var newdiv = document.createElement("div");
                                newdiv.innerHTML = sHTMLWeather;
                                oObj.removeChild(oObj.firstChild);
                                oObj.appendChild(newdiv);
                            } else
                                oObj.innerHTML = sHTMLWeather;
                        }
                    }
                    // part: replace the HTML code (End)
                }
            }
        }
    }
    if (sType == "F3") {
        var obj = new foo();
        obj.causeTimeout();
        obj.setVal(9);
    }
}
function isNullEmptyError(sInput) {
    var bResult = false;
    if (sInput != null) {
        if (sInput == "") {
            bResult = true;
        } else if (sInput == sError) {
            bResult = true;
        }
    } else {
        bResult = true;
    }
    return bResult;
}
function stringToDate(DateStr) {
    var myDate = new Date(DateStr);
    if (isNaN(myDate)) {
        DateStr = DateStr.replace(/:/g, "-");
        DateStr = DateStr.replace(".", "-");
        DateStr = DateStr.replace("T", "-");

        var arys = DateStr.split('+')[0].split('-');
        myDate = new Date(arys[0], arys[1] - 1, arys[2], arys[3], arys[4], arys[5]);
    }
    ;
    return myDate;
}