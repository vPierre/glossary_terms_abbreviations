/* =================================================================================================
 common.js
 > store general scripts which would be used on multiple templates
 
 * @TODO Simplify
 ================================================================================================= */

// ========================
// Common Variables
// ========================
var windowWidth = $(window).width();
var windowHeight = $(window).height();
var windowJsWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
var windowJsHeight = windowHeight;
var windowJsOldWidth, windowJsOldHeight;   //Used to detect unexpected trigger on window resize
var mobileDevice = false;
var lang = $('html').attr('lang');
var isChrome, isFirefox, isSafari, isEdge, isIE, isMSTouchDevice, isIOSDevice;
var scrollTop = 0;
var $body = $('body');
var aniElemPos = []; //Store position for scrolling effects

// Layout or content related variables
var cmErrMsgSamples = '';   //Stores Field Error Msgs
var formErrMsgSamples = '';    //Stores Form Error Msgs
var resizeTimer;
var scrollTimer;
var page_lang = "en";
var browserZoom = 1;		

// ========================
// Page-specific variables
// ========================


// ========================

$(document).ready(function () {
    $body = $('body');
    lang = $('html').attr('lang');
    init();
    resize(true);

//=============
						
           
			jQuery.browser = {};
			(function () {
				jQuery.browser.msie = false;
				jQuery.browser.version = 0;
				if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
					jQuery.browser.msie = true;
					jQuery.browser.version = RegExp.$1;
				}
			})();
	
			$( window ).resize(function() {
				if($(window).width() >769 ){
					        $(".popover").css("display", "none");
							$("a.maintainHover").removeClass("maintainHover");
							$(".off-canvas-wrap").removeClass("move-right");
							$(".off-canvas-wrap").removeClass("move-left");
				}					
			});	
			

				//console.log(".event-search__search-bar doc ready");
				if( $('.event-search__search-bar').length > 0 ){
					//console.log(".event-search__search-bar");
					if(page_lang == 'en'){
						goTodayEN();
					}else if(page_lang == 'tc'){
						goTodayTC();
					}else if(page_lang == 'sc'){
						goTodaySC();
					}
				}			

//=============	
	
});

$(window).load(function () {
    resize(true);
    $body.addClass('page-ready');
    initHeaderContact();
    setTimeout(function () {
        $body.addClass('page-ani-end');
    }, 3000);
    handleHash();
});

$(window).on('resize', _.debounce(function () {
    resize();
}, 150));


$(window).scroll(_.throttle(function () {
    scroll();
}, 300));

function scrollToElemTop(elem) {
    $('html, body').animate({
        //'scrollTop': elem.offset().top - $('header').outerHeight() - 40
        'scrollTop': elem.offset().top
    }, 1000);
}



function init() {
    scrollTop = $(document).scrollTop();
    windowWidth = $(window).width();
    windowHeight = $(window).height();
    windowJsWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    windowJsHeight = windowHeight;
    /**high priority**/

    /**Browser or Devices Related**/
    checkEnvironment();

    /***Common***/
    initLangBtn();
    initMenu();
    initMobMenu();
    initBreadcrumbs();
    initAccordion();
    initTabs();
	initOGCIOTabs();
	initAnchor();
    //initFields();
    //initFormErrMsg();
	langGetter();
	
	if ($(".getData_en").length >0){
		initDate_article_list();
		getDataEN(thisYear);		
		$('#jumpMenu').change(function(){
				getDataEN($('#jumpMenu').val());
		});
	}
	if ($(".getData_tc").length >0){
		initDate_article_list();
		getDataTC(thisYear);		
		$('#jumpMenu').change(function(){
				getDataTC($('#jumpMenu').val());
		});
	}		
	if ($(".getData_sc").length >0){
		initDate_article_list();
		getDataSC(thisYear);		
		$('#jumpMenu').change(function(){
				getDataSC($('#jumpMenu').val());
		});
	}
	
	if ($("#features_top_banner").length >0){
		//console.log("features");
		initDate_features_list();
		//console.log("end");
		if (page_lang=="en"){
			getDataEN(thisYear);
		} else if (page_lang=="tc") {
			getDataTC(thisYear);
		} else if (page_lang=="sc") {
			getDataSC(thisYear);
		} else {
			getDataEN(thisYear);
		}										
		$('#jumpMenu').change(function(){
			if (page_lang=="en"){
				getDataEN($('#jumpMenu').val());
			} else if (page_lang=="tc") {
				getDataTC($('#jumpMenu').val());
			} else if (page_lang=="sc") {
				getDataSC($('#jumpMenu').val());
			} else {
				getDataEN($('#jumpMenu').val());
			}
		});
	}
	
    initCustomSelect();
    initBackToTop();
    initShareBtns();
    initPrintBtn();
    initBackBtn();
    initAniElem();
	langGetter();
    initMainSearch();

    /***Page or sections specific***/
    initEventCalendar();
    initMainBannerSlider();
    initMainBannerNewsSlider();
    initStaticBanner();
    initItemsSlider();
    initThumbSlider();
    initVideoSlider();
    initGallerySlider();
    initImageSlider();
    initSecShortcut();
    initOrgChart();
    initDatepickerItem();
    initVideos();
    initBreakingNews();
    initPopup();
    initSecArticleHead();
    initScrollReader();
    initInfoTable();
	initKeyTabClick();
    initSelectKeybroad();
	initKeyCheckBoxText();
	initMultiLineFF();
    //*OGCIO Integration
    initialCookie();
    initRevisionDate();
	initShowWCAG();
    initTopBanner();
    showKeyBoard();
    if ($("#whats_new").length > 0) {
        initDate_what_news();
        if (page_lang == "en") {
            getDataEN(thisYear);
        } else if (page_lang == "tc") {
            getDataTC(thisYear);
        } else if (page_lang == "sc") {
            getDataSC(thisYear);
        } else {
            getDataEN(thisYear);
        }
    }
    if ($("#yearSelector").length > 0) {
        initYearSelector()
    }
    if ($(".royalSlider").length > 0) {
        initPhotoGallery()
    }
    if ($(".galleryItem").length > 0) {
        initStudentCornerGallery()
    }

    var src = $('#our-work--icon-1').data('src') + 'img-our-work-lightblub.svg';

    /***Actions and triggers***/
    handleMainBanner();
    $(window).scroll();
	initBWVer();  

}
function resize(initMode) {
    windowWidth = $(window).width();
    windowHeight = $(window).height();
    windowJsWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    windowJsHeight = windowHeight;
    browserZoom=window.devicePixelRatio;
    if(browserZoom<1){
      $body.addClass('is-zoomout');
    } else {
      $body.removeClass('is-zoomout');
    }																		   
    if (!initMode) {
        if (resizeTimer) {
            clearTimeout(resizeTimer);
        }
        $body.addClass('resizing');
        resizeTimer = setTimeout(function () {
            $body.removeClass('resizing');
        }, 50);
    }
    handleMain();
    handleMobMenu();

    //handleHighlightBoxes();
    handleEventCalendar();
    handleEventSearch();
    handleTabs();
	handleOGCIOTabs();			   
    handleGallerySlider();
    handleInfoTable();
    setAniElemPos();

    if (windowJsOldWidth != windowWidth) {
        windowJsOldWidth = windowWidth;
        //These will be run only for WIDTH Resize:
    }
    if (!$body.hasClass('is-scrolling')) {
        handleMainBanner();
    }
}
function scroll() {
    scrollTop = $(document).scrollTop();
    handleAniElem();
    handleSecShortcut();
    handleFixedMenu();
    if (scrollTimer) {
        clearTimeout(resizeTimer);
    }
    $body.addClass('is-scrolling');
    scrollTimer = setTimeout(function () {
        $body.removeClass('is-scrolling');
    }, 250);
}
// ========================
// Init functions
// ========================
var isTouchingMenu = false; //isMSTouchDevice
function initMenu() {
    var $header = $('header');
    $header.addClass('header--ready');

    var $menuLinkItems = $('.header-nav__link-holder, .header-nav__tool--search');
    var $menuLink = $('.header-nav__link');
    var $otherElem = $('*').not('.header-nav__sub-holder, .header-nav__link-group, .header-nav__link, .header-nav__sub-link, .header-nav__tool-trigger, .main-search__input, .main-search__btn, .header-contact__map-holder *');
    $menuLinkItems.each(function (key) {
        var $thisItem = $(this);
        var $thisLink = $thisItem.find('.header-nav__link, .header-nav__tool-trigger');
        var $thisSubHolder = $thisItem.next('.header-nav__sub-holder');
        var $thisSubLinks = $thisSubHolder.find('.header-nav__sub-link, .main-search__input, .main-search__btn');
        $thisLink.unbind().on('touchstart.menuLink click.menuLink', function (e) {
            e.preventDefault();
        })
		
		
        $thisLink.keyup(function (e) {
            var keycode = event.keyCode || event.which;
            if (keycode == '13') {
                window.location.href = $thisLink.attr('href');
            }
        });

        $thisLink.hammer().bind("tap click.menuLink", _.debounce(function (e) {
            e.preventDefault();
            if ($thisItem.hasClass('is-open') || $thisItem.hasClass('is-hover') || (!isIOSDevice&&$thisItem.is(':hover'))) {
                window.location.href = $thisLink.attr('href');
            } else {
                $thisItem.addClass('is-open');
                clearSearchForm();
                closeAllSubMenu($thisItem);
                //resizeHeaderContact();
            }
        }, 300));
	
        $thisLink.focus(function () {
            $thisItem.addClass('is-focus');
            clearSearchForm();
            closeAllSubMenu($thisItem);
            //resizeHeaderContact();
        });
        $thisSubLinks.focus(function () {
            closeAllSubMenu($thisItem);
            $thisItem.addClass('is-open');
        });
		
        $thisLink.hover(function () {
            //if(isIE||isEdge){
            closeAllSubMenu($thisItem);
            $menuLink.blur();
           // resizeHeaderContact();
        });
        $thisSubHolder.hover(function () {
            $thisItem.addClass('is-hover');
        }, function () {
            $thisItem.removeClass('is-hover');		
            $('.header-nav__tool--search').removeClass('is-open');
            clearSearchForm();
        });

		if(navigator.userAgent.indexOf("Trident")>0 || navigator.userAgent.indexOf("Edge") > -1){
			$("#map").mouseover(function(){		
				$(".header-nav__sub-holder").css("display", "block");
			});
			$("#map").mouseout(function(){
				$(".header-nav__sub-holder").css("display", "");
			});
		}

        //************Handle Sub Menu**************//

        var $thisGroups = $thisSubHolder.find('.header-nav__link-group');
        var $menuSubLink = $thisSubHolder.find('.header-nav__sub-link');
        $menuSubLink.on('touchstart.headerSub click.headerSub', function (e) {
            var $thisLink = $(this);
            if ($thisLink.hasClass('has-sub')) {
                e.preventDefault();
                var $thisTarget = $($thisLink.data('sub-target'));
                $thisGroups.hide().removeClass('is-active');
                $thisTarget.fadeIn(300, function () {
                    $thisTarget.addClass('is-active');
                    $thisTarget.find('.header-nav__group-back').focus();
                });
            }
        });
        //************END Handle SubMenu**************//

    });

    //************Handle SubMenu Back Btn**************//
    var $linkGrp = $('.header-nav__link-group');
    $linkGrp.each(function () {
        var $thisGrp = $(this)
        var $subMenuBackBtn = $thisGrp.find('.header-nav__group-back');
        $subMenuBackBtn.on('touchstart.headerSub click.headerSub', function (e) {
            e.preventDefault();
            //var $thisLink = $(this);
            var $motherId = $thisGrp.attr('id');
            var $motherSubLink = $('[data-sub-target="#' + $motherId + '"]');
            var $motherSubHolder = $motherSubLink.parent();
            $thisGrp.hide().removeClass('is-active');
            $motherSubHolder.fadeIn(300, function () {
                $motherSubHolder.addClass('is-active');
            });
        });
    });

    //************END Handle SubMenu Back Btn**************//


    $otherElem.focus(function (e) {
        clearSearchForm();
        var $strongException = $(".header-nav__group-back");
        if (!$strongException.is(e.target) && $strongException.has(e.target).length === 0) {
            //$menuLinkItems.removeClass('is-open');
        }
    });
    $otherElem.click(function (e) {
        clearSearchForm();
        var $strongException = $(".header-nav__sub-link, .header-nav__link-holder, .header-nav__link-group");
        if (!$strongException.is(e.target) && $strongException.has(e.target).length === 0) {
            $menuLinkItems.removeClass('is-open is-focus');
        }
    });
    var currentSec = $body.data('section');
    if (currentSec == 'about-us') {
        $('.header-nav__link[href*="/about_us/"]').addClass('is-active');
    } else if (currentSec == 'our-work') {
        $('.header-nav__link[href*="/our_work/"]').addClass('is-active');
    } else if (currentSec == 'news') {
        $('.header-nav__link[href*="/news/"]').addClass('is-active');
    } else if (currentSec == 'service-desk') {
        $('.header-nav__link[href*="/service_desk/"]').addClass('is-active');
    }


}
function closeAllSubMenu($exception) {
    if (!$exception) {
        var $menuLinkItems = $('.header-nav__link-holder, .header-nav__tool--search');
    } else {
        var $menuLinkItems = $('.header-nav__link-holder, .header-nav__tool--search').not($exception);
    }
    $menuLinkItems.removeClass('is-open');
    $menuLinkItems.removeClass('is-hover');
    $menuLinkItems.removeClass('is-focus');
    closeAllShareBtn();
}
function initLangBtn() {
    //Desktop only. For mobile, please refer to initMobMenu()
    var $langBtn = $('.header-nav__tool--lang');
    var $langBtnTrigger = $langBtn.find('.header-nav__tool-trigger');
    $langBtnTrigger.click(function (e) {
        e.preventDefault();
        if (!$langBtn.hasClass('is-active')) {
            $langBtn.addClass('is-active');
        } else {
            $langBtn.removeClass('is-active');
        }
    });
    $(document).on('touchstart.langbtn click.langbtn', function (e) {
        if (!$langBtn.is(e.target) && $langBtn.has(e.target).length === 0) {
            $langBtn.removeClass('is-active');
        }
    });
    var $langSwitchers = $('[data-langbtn-target]');
    var winLocation = window.location;
    var loc = winLocation + "";
    var currentLangPath = getTxtByLang(['/en/', '/tc/', '/sc/']);
    $langSwitchers.each(function (key) {
        var $this = $(this);
        var thisTarget = $this.data('langbtn-target');
        var targetLang = '/' + thisTarget + '/';
        var newHref = loc.replace(currentLangPath, targetLang);
        $this.attr("href", newHref);
        $this.click(function (e) {
            e.preventDefault();
            var url = $this.attr("href");
            if (checkURLexist(url)) {
			if(window.location.href != url){
				clearTimeout(menuOpenTimer);
                closeAllMenu();
                window.top.location.href = url;
            } else {
				location.reload();
            }
            } else {
                var msg = getTxtByLang(['English Version Only', '只有中文版', '只有中文版']);
                alert(msg);
            }
        });
    });
}
var menuOpenTimer;
function initMobMenu() {
    var $btnMenu = $('.btn-menu, .btn-lang, .btn-search-menu');
    if ($btnMenu.length < 1) {
        return false;
    }
    $btnMenu.click(function (e) {
        e.preventDefault();
        var $this = $(this);
        var $mobMenu = $('.mob-nav');
        var $mobSubnav = $('.mob-subnav');
        var thisTarget = $this.attr('href');
        var $thisTarget = $(thisTarget);
        if ($this.hasClass('btn-menu--close')) {
            closeAllMenu();
        } else {
            if (!$this.hasClass('is-active')) {
                clearTimeout(menuOpenTimer);
                $mobMenu.removeClass('is-active').hide();
                $btnMenu.removeClass('is-active');
                $this.addClass('is-active');
                $thisTarget.addClass('is-active').show();
                $thisTarget.find('.btn-menu--close').focus();
                menuOpenTimer = setTimeout(function () {
                    //handleMobMenu();
                    $body.addClass('mob-menu-open');
                    setData($body, 'menu-open', thisTarget);
                }, 50);
            } else {
                $body.removeClass('mob-menu-open');
                unsetData($body, 'menu-open');
                $thisTarget.removeClass('is-active').show();
                $thisTarget.find('.btn-menu--close').focus();
                menuOpenTimer = setTimeout(function () {
                    $mobMenu.removeClass('is-active').hide();
                    $btnMenu.removeClass('is-active');
                    $mobSubnav.removeClass('is-active').hide();
                }, 300);
            }
        }
    });

    var $moblink = $('.mob-nav__link.has-sub, .mob-subnav__sub-link.has-sub');
    $moblink.click(function (e) {
        e.preventDefault();
        var $this = $(this);
        var $tar = $($this.attr('href'));
        $tar.show();
        menuOpenTimer = setTimeout(function () {
            $tar.addClass('is-active');
        }, 50);
    });
    var $mobSubnav = $('.mob-subnav');
    $mobSubnav.each(function () {
        var $this = $(this);
        var $backBtn = $this.find('.mob-subnav__backbtn');
        var menuLv = $this.data('submenu-level');
        $backBtn.click(function (e) {
            e.preventDefault();
            var $thisLvMenu = $('[data-submenu-level="' + menuLv + '"]');
            $thisLvMenu.removeClass('is-active');
            menuOpenTimer = setTimeout(function () {
                $thisLvMenu.hide();
            }, 300);
        });
    });

    $('.mob-subnav__sub-link').click(function(){
        clearTimeout(menuOpenTimer);
        closeAllMenu();
    })
}
function closeAllMenu(mode) {
    var $btnMenu = $('.btn-menu, .btn-lang');
    var $mobMenu = $('.mob-nav');
    var $mobSubnav = $('.mob-subnav');
    if (mode == 'static') {
        $body.removeClass('mob-menu-open');
        unsetData($body, 'menu-open');
        $btnMenu.removeClass('is-active');
        $mobSubnav.removeClass('is-active').hide();
        $mobMenu.removeClass('is-active').hide();
    } else {
        $body.removeClass('mob-menu-open');
        unsetData($body, 'menu-open');
        $btnMenu.removeClass('is-active');
        $mobSubnav.removeClass('is-active').hide();
        menuOpenTimer = setTimeout(function () {
            $mobMenu.removeClass('is-active').hide();
        }, 300);
    }

}
function initBreadcrumbs() {
    var $breadcrumbs = $('.breadcrumbs');
    var $breadcrumbsItems = $breadcrumbs.find('.breadcrumbs__item');
    var $otherElem = $('*').not('.breadcrumbs__link, .breadcrumbs__sub-link');

    var isTouchingBreadcrumb = false;
    $breadcrumbsItems.each(function (index, key) {
        var $thisItem = $(this);
        var $thisItemLink = $thisItem.find('.breadcrumbs__link');
        var $thisItemSubLink = $thisItem.find('.breadcrumbs__sub-link');
        var $thisItemSubLinkHasSub = $thisItem.find('.breadcrumbs__sub-link.has-sub');
        var $thisSubHolders = $thisItem.find('.breadcrumbs__sub-items-holder');
        var $thisFirstLvSubHolder = $thisItem.find('.breadcrumbs__sub-items-holder[data-submenu-lv="1"]')
        var $thisBackBtn = $thisItem.find('.breadcrumbs__sub-back');
        var isHome = $thisItem.find('.breadcrumbs__home').length;
		if(isHome){

          $thisItem.unbind().on('touchstart.breadcrumb click.breadcrumb', function (e) {
              e.preventDefault();
              window.location.href = $thisItemLink.attr('href');
          });
        } else {
          $thisItemLink.unbind().on('touchstart.breadcrumb click.breadcrumb', function (e) {

              e.preventDefault();
          })
          $thisItemLink.keyup(function (e) {
              var keycode = event.keyCode || event.which;
              if (keycode == '13') {
                  window.location.href = $thisItemLink.attr('href');
              }
          });
          $thisItemLink.hammer().bind("tap click.breadcrumb", _.debounce(function (e) {
              e.preventDefault();
              if ($thisItem.hasClass('is-open') || $thisItem.hasClass('is-hover') || (!isIOSDevice&&$thisItem.is(':hover'))) {

                  window.location.href = $thisItemLink.attr('href');
              } else {
                  $breadcrumbsItems.not($thisItem).removeClass('is-open');
                  $thisItem.addClass('is-open');
              }
          }, 300));

          $thisItemLink.focus(function () {
              $thisItem.addClass('is-focus');

              $breadcrumbsItems.not($thisItemLink).removeClass('is-open');
          });

          $thisItemSubLink.focus(function () {
              if (!isTouchingBreadcrumb) {
                  $breadcrumbsItems.removeClass('is-open');
                  $thisItem.addClass('is-open');
              }
          });

          $thisItemSubLinkHasSub.click(function (e) {
              e.preventDefault();
              var $this = $(this);
              var thisTarget = $this.attr('href');
              var $thisTarget = $(thisTarget);
              $thisSubHolders.removeClass('is-active');
              $thisTarget.addClass('is-active');
          });

          $thisBackBtn.click(function (e) {
              e.preventDefault();
              $thisSubHolders.removeClass('is-active');
              $thisFirstLvSubHolder.addClass('is-active');
          });
        }
        //** handle current link**//
        if (!isHome && !$thisItem.hasClass('has-sub') && index == $breadcrumbsItems.length -1 ) {
            $thisItem.addClass('is-current');
            //$thisItemLink.removeAttr('href'); // 20171110 TIR10020
        }
    });
	
    $otherElem.focus(function (e) {
        if (!$breadcrumbsItems.is(e.target) && $breadcrumbsItems.has(e.target).length === 0) {
            $breadcrumbsItems.removeClass('is-open');
        }
    });
    $(document).on('touchstart.langbtn click.langbtn', function (e) {
        if (!$breadcrumbsItems.is(e.target) && $breadcrumbsItems.has(e.target).length === 0) {

            $breadcrumbsItems.removeClass('is-open is-focus');

        }
    });
	
	var breadcrumbName = $('div .breadcrumbs__holder').children().last().children().first().text();
	$('div .breadcrumbs__holder').children().last().children().first().replaceWith('<span class="breadcrumbs__link">' + breadcrumbName + '</span>');
}
function initMainBannerBg() {
    var $mainBannerBg = $('.main-banner__slide-bg');
    if (!$mainBannerBg.length) {
        return false;
    }
    var $mainBannerBgBody = $mainBannerBg.find('span');
    $mainBannerBgBody.wrap("<div class='main-banner__bg-wrapper'></div>").wrap("<div class='main-banner__bg-wrapper'></div>");
}
function initMainBannerSlider() {
    var $slides = $('.main-banner__slide');
    if (!$slides.length) {
        return false;
    }
    var $mainBanner = $('.main-banner');
    var $mainBannerSlider = $mainBanner.find('.main-banner__slider')[0];
    var $btnScrollDown = $mainBanner.find('.main-banner__scroll-down');
    var $btnPause = $mainBanner.find('.swiper-button-pause');
    var swiperMainBanner = new Swiper($mainBannerSlider, {
        pagination: '.main-banner__pagination',
        slidesPerView: 1,
        nextButton: '.main-banner__slider-next',
        prevButton: '.main-banner__slider-prev',
        slidesPerGroup: 1,
        loop: true,
        effect: 'fade',
        observeParents: true,
        autoplay: 5000,
        //autoplay: false,
        paginationClickable: true,
        autoplayDisableOnInteraction: true,
        simulateTouch: true,
        a11y: true,
        paginationBulletRender: function (swiper, index, className) {
            var txtSlide = getTxtByLang(['To slide no. ' + (index + 1), '去第' + (index + 1) + '張', '去第' + (index + 1) + '张'])
            return '<a href="#" class="' + className + '" title="' + txtSlide + '">' + '</a>';
        },
        onInit: function (swiper) {
            if(windowJsWidth > 991 && !navigator.userAgent.match(/iPad/i)){
                dzsprx_init('.main-banner__slide-bg', {direction: "normal", mode_scroll: "fromtop"});
            }
            initSwiperPause($mainBanner, swiper);
        },
        onResize: function () {
        },
        onTransitionEnd: function (swiper) {
            updateKeyTxtColor();
        }
    });
    $(window).on('mouseup', function () {
        updateKeyTxtColor();
    });
    var $nextSec = $('.main-banner').next();
    $btnScrollDown.click(function (e) {
        e.preventDefault();
        scrollToElemTop($nextSec);
    });

    //Handle pause area (will pause the autoplay on mouseover this area)
    var $pauseArea = $mainBanner.find('.main-banner__title, .main-banner__title-tag, .main-banner__txt .btn');
    $pauseArea.hover(function () {
        if (!$mainBanner.hasClass('swiper-paused')) {
            $btnPause.click();
            if (mainBannerNewsSwiper) {
                mainBannerNewsSwiper.stopAutoplay();
            }
        }
    }, function () {
        if ($mainBanner.hasClass('swiper-paused')) {
            $btnPause.click();
            if (mainBannerNewsSwiper) {
                mainBannerNewsSwiper.startAutoplay();
            }
        }
    });
    
    //Handle focus pause (apply only for desktop device to prevent accident pause on mobile devices)
    if(!mobileDevice){
      $mainBanner.find('*').focus(function () {
          if (!$mainBanner.hasClass('swiper-paused')) {
              $btnPause.click();
              if (mainBannerNewsSwiper) {
                  mainBannerNewsSwiper.stopAutoplay();
              }
          }
      });
    }
    
    //init main banner animation (delay for menu to fade in)
    setTimeout(function () {
        $body.addClass('menu--ready');
    }, 1500);
	  initSlideTabindex();
}

function initSwiperPause($holder, swiper) {
    var $pauseBtn = $holder.find('.swiper-button-pause');
    $pauseBtn.unbind().click(function () {
        var $thisPauseBtn = $(this);
        if (!$holder.hasClass('swiper-paused')) {
            $holder.addClass('swiper-paused');
            swiper.stopAutoplay();
            var txtPause = getTxtByLang(['Play', '播放', '播放']);
            var txtPauseAccess = '<span class="access">' + txtPause + '</span>';
            $thisPauseBtn.attr('title', txtPause);
            $thisPauseBtn.html(txtPauseAccess);
        } else {
            $holder.removeClass('swiper-paused');
            swiper.startAutoplay();
            var txtPause = getTxtByLang(['Pause', '暫停', '暂停']);
            var txtPauseAccess = '<span class="access">' + txtPause + '</span>';
            $thisPauseBtn.attr('title', txtPause);
            $thisPauseBtn.html(txtPauseAccess);
        }
    });
}
var mainBannerNewsSwiper;
function initMainBannerNewsSlider() {
    var $slides = $('.main-banner__news-item');
    if ($slides.length < 1) {
        return false;
    }
    var $newsSlider = $('.main-banner__news-slider');
    var swiper = new Swiper('.main-banner__news-slider', {
        loop: true,
        speed: 500,
        slidesPerView: 'auto',
        slidesPerGroup: 1,
        autoplay: 5000,
        autoHeight: true,
        paginationClickable: true,
        autoplayDisableOnInteraction: true,
        simulateTouch: true,
        a11y: true,
        observer: true,
        observeParents: true,
        paginationBulletRender: function (swiper, index, className) {
            var txtSlide = getTxtByLang(['To slide no. ' + (index + 1), '去第' + (index + 1) + '張', '去第' + (index + 1) + '张'])
            return '<span class="' + className + '" title="' + txtSlide + '">' + '</span>';
        },
        onInit: function (swiper) {
            mainBannerNewsSwiper = swiper;
            $newsSlider.addClass('is-ready');
        },
        onResize: function () {
        },
        onTransitionEnd: function (swiper) {
        }
    });
}

function initStaticBanner() {
    var $staticBanner = $('.static-banner');
    if ($staticBanner.length < 1) {
        return false;
    }
    var bannerTxtColor = $staticBanner.find('.static-banner__data-txt-color a');
    if (bannerTxtColor.length) {
        var color = bannerTxtColor.html();
        bannerTxtColor.remove();
        $staticBanner.attr('data-txt-color', color).data('txt-color', color);
    }
    updateKeyTxtColor();
}
function initAccordion() {
    var $accordion = $('.accordion__item');
    if ($accordion.length < 1) {
        return false;
    }
    $accordion.each(function (key) {
        var $thisItem = $(this);
        var $thisToggle = $thisItem.find('.accordion__toggle');
        var $thisContent = $thisItem.find('.accordion__content');
        $thisToggle.click(function (e) {
            e.preventDefault();
            if (!$thisItem.hasClass('is-active')) {
                $thisItem.addClass('is-active');
                $thisContent.hide().slideDown(300, function () {
                    handleInfoTable();
                });
            } else {
                $thisItem.removeClass('is-active');
                $thisContent.show().slideUp(300, function () {
                });
            }
        });
    });
}
function initOGCIOTabs() {
    var $tabs = $('.ogcio__tabs');
    if (!$tabs.length) {
        return false;
    }
    $tabs.each(function () {
        var $thisTabs = $(this);
        var $thisContents = $thisTabs.find('.ogcio__tabs__content');
        var $tabTrigger = $thisTabs.find('.ogcio__tabs__btn');
        var $tabMob = $thisTabs.find('.ogcio__tabs__btns-mob');
        var $tabMobSelect = $tabMob.find('select');
        var $tabMobTrigger = $thisTabs.find('.ogcio__tabs__btns-mob a span');
        $tabTrigger.click(function (e) {
            e.preventDefault();
            var thisTarId = $(this).attr('href');
            var thisTarget = $(thisTarId);
            if (thisTarget.length) {
                $tabMobSelect.val(thisTarId);
                $tabMobSelect.trigger('change');
            }
        });

        $tabMobSelect.change(function () {
            var $this = $(this);
            var thisTarId = $this.val();
            var thisTarget = $(thisTarId);
            var $thisTabTrigger = $thisTabs.find('.ogcio__tabs__btn[href="' + thisTarId + '"]');
            $tabTrigger.removeClass('is-active');
            $thisContents.hide().removeClass('is-active');
            $thisTabTrigger.addClass('is-active');
            thisTarget.addClass('is-active').show();

        });
    });
}						  

function initTabs() {
    var $tabs = $('.tabs');
    if (!$tabs.length) {
        return false;
    }
    $tabs.each(function () {
        var $thisTabs = $(this);
        var $thisContents = $thisTabs.find('.tabs__content');
        var $tabTrigger = $thisTabs.find('.tabs__btn');
        var $tabMob = $thisTabs.find('.tabs__btns-mob');
        var $tabMobSelect = $tabMob.find('select');
        var $tabMobTrigger = $thisTabs.find('.tabs__btns-mob a span');
        $tabTrigger.click(function (e) {
            e.preventDefault();
            var thisTarId = $(this).attr('href');
            var thisTarget = $(thisTarId);
            if (thisTarget.length) {
                $tabMobSelect.val(thisTarId);
                $tabMobSelect.trigger('change');
            }
        });

        $tabMobSelect.change(function () {
            var $this = $(this);
            var thisTarId = $this.val();
            var thisTarget = $(thisTarId);
            var $thisTabTrigger = $thisTabs.find('.tabs__btn[href="' + thisTarId + '"]');
            $tabTrigger.removeClass('is-active');
            $thisContents.hide().removeClass('is-active');
            $thisTabTrigger.addClass('is-active');
            thisTarget.addClass('is-active').show();
                $.cookie('home-tab', thisTarId);
            setTimeout(function(){
              if(itemsSliders){
                var slideId = thisTarget.find('.items-slider').attr('id');
                var arr = slideId.split('--');
                var slider = itemsSliders[arr[1]];
              	slider.slideTo(0, 0);
              }
            }, 10);
        });
    });
    if($('#sec-news-centre').length){
      if ($.cookie('home-tab')) {
        var thisTarId = $.cookie('home-tab');
        var $thisTabTrigger = $tabs.find('.tabs__btn[href="' + thisTarId + '"]');
        $thisTabTrigger.click();
      } else {
          $('.tabs__btn.is-active').click();
      }
    }
}
/*
function initFields() {
    var $fields = $('.field');
    if (!$fields.length) {
        return false;
    }
    var checkFilled = function (input, field) {
        var thisVal = input.val();
        if (thisVal != '') {
            field.addClass('field--filled');
        } else {
            field.removeClass('field--filled');
        }
    };
    $fields.each(function () {
        var $thisField = $(this);
        var $thisInput = $thisField.find('input');
        var thisLabelTxt = $thisInput.data('custom-label');
        if (thisLabelTxt) {
            var thisLabel = '<span class="field__custom-label">' + thisLabelTxt + '</span>';
        }
        $thisField.append(thisLabel);
        $thisInput.keyup(function () {
            checkFilled($thisInput, $thisField);
        });
    });
}

function initFormErrMsg() {
    var $cmErrMsgElem = $('.common-err-msg-holder');
    var $formErrMsgElem = $('.form-err-msg-holder');
    if (!$cmErrMsgElem.length && !$formErrMsgElem.length)
        return false;
    cmErrMsgSamples = $cmErrMsgElem.clone();
    formErrMsgSamples = $formErrMsgElem.clone();
    $cmErrMsgElem.remove();
    $formErrMsgElem.remove();

    $('.form-err-msg--target').replaceWith(formErrMsgSamples);
}
*/
function initCustomSelect() {
    var $customSelect = $('.custom-select');
    if (!$customSelect.length) {
        return false;
    }
    $customSelect.each(function () {
        var $selectHolder = $(this);
        var $selectFieldHolder = $selectHolder.parent();
        var $selectListMob = $selectHolder.find('select');
        var isSecArticleHead = $selectHolder.hasClass('sec-article__head-option');
        var initListForDesktop = function () {
            var listForDesktop = '<ul class="custom-select__list">';
            var triggerContent = '';
            var labelTxt = $selectHolder.data('label');
            $selectListMob.find('option').each(function (key) {
                var thisOption = $(this);
                var thisLabel = $(this).html();
                var thisVal = $(this).val();
                var defaultClass = '';
                if (key == 0 || thisOption.hasClass('option-default')) {
                    triggerContent = thisLabel;
                }
                if (thisOption.hasClass('option-default')) {
                    defaultClass = ' class="option-default"';
                    if (!labelTxt) {
                        labelTxt = thisLabel;
                    }
                }
                var thisDtOption = '<li data-val="' + thisVal + '"' + defaultClass + '><span tabindex="0">' + thisLabel + '</span></li>';
                listForDesktop += thisDtOption;
            });
            listForDesktop += '</ul>';
            listForDesktop = '<a href="#" class="custom-select__trigger"><span>' + triggerContent + '</span></a>' + listForDesktop;

            $(listForDesktop).insertBefore($selectListMob);

            if (labelTxt) {
                var label = '<span class="field__custom-label">' + labelTxt + '</span>';
                $(label).insertBefore($selectHolder);
            }
        };
        initListForDesktop();

        var $selectTrigger = $selectHolder.find('.custom-select__trigger');
        var $selectListHolder = $selectHolder.find('.custom-select__list');
        var $selectList = $selectHolder.find('.custom-select__list>li');
        //var $defaultOption = $selectList.eq(0);


        var selectOpen = function () {
            selectAllClose();
            $selectTrigger.addClass('animating');
            $selectListHolder.hide().slideDown(300, function () {
                $selectHolder.addClass('open');
                $selectTrigger.removeClass('animating');
                $(document).click(function (event) {
                    if (!$(event.target).closest('.custom-select').length && !$(event.target).is('.custom-select')) {
                        selectClose();
                    }
                });
				var isEventSearchItem = $selectFieldHolder.hasClass('event-search__control-item');
				var isCustomMaxHeightItem = $selectHolder.hasClass('custom-max-height');
                if (isSecArticleHead || isEventSearchItem || isCustomMaxHeightItem) {
                    var listHeight = $selectList.addUp($.fn.outerHeight);
                    listHeight = $selectList.eq(0).outerHeight() + $selectList.eq(1).outerHeight() + $selectList.eq(2).outerHeight();
                    $selectListHolder.css('max-height', listHeight);
                    $selectListHolder.height(listHeight).customScrollbar({
                        skin: "default-skin",
                        hScroll: false,
                        updateOnWindowResize: true
                    }).customScrollbar("resize", true);
					if(isCustomMaxHeightItem){
						$selectHolder.find('.overview').css("position", "relative");
					}
                }
            });
        };
        var selectClose = function () {
            $selectTrigger.addClass('animating');
            $selectHolder.removeClass('open');
            $selectListHolder.show().slideUp(300, function () {
                $selectTrigger.removeClass('animating');
                $(document).unbind();
            });
        };
        var selectAllClose = function () {
            $('.custom-select.open ul').show().slideUp(300, function () {
                $('.custom-select.open .animating').removeClass('animating');
                $('.custom-select.open').removeClass('open');
                $(document).unbind();
            });
        };
        var syncSelectList = function () {
            var thisVal = $selectListMob.val();
            $selectList.removeClass('selected');
            var targetLi = $selectListHolder.find('li[data-val="' + thisVal + '"]')
            targetLi.addClass('selected');
            var selectedTxt = targetLi.find('span').html()
            $selectListHolder.hide();
            $selectTrigger.find('span').html(selectedTxt);
            if (thisVal == '') {
                $selectHolder.addClass('custom-select--on-default');
                $selectFieldHolder.removeClass('field--filled');
            } else {
                $selectHolder.removeClass('custom-select--on-default');
                $selectFieldHolder.addClass('field--filled');
            }
        };
        $selectTrigger.click(function (event) {
            event.preventDefault();
            if ($selectTrigger.hasClass('animating'))
                return false;
            if (!$selectHolder.hasClass('open')) {
                selectOpen();
            } else {
                selectClose();
            }
        });
        $selectList.click(function (event) {
            event.preventDefault();
            if (!$(this).hasClass('selected')) {
                $selectList.removeClass('selected');
                $(this).addClass('selected');
                var thisVal = $(this).data('val');
                $selectListMob.val(thisVal);
                $selectListMob.trigger('change');
            }
            selectClose();
        });
		$selectList.keypress(function (e) { //handle keyboard "enter"
         var key = e.which;
         if(key == 13) {
            $(this).click();
            return false;  
          }
        });   
        $selectListMob.change(function () {
            syncSelectList();
        });
        syncSelectList();
    });
}
function initSelectKeybroad(){
    var $customSelect = $('.custom-select');
    $customSelect.find('span').on('keydown',function(e){
        var keyCode = e.keyCode;
        if( keyCode == 13 || keyCode == 32){
            var $text = $(this).text();
            $(this).parents('ul:first').prev('a').find('span').text($text);
            $(this).parents('ul:first').slideToggle();
            $(this).parents('.custom-select:first').removeClass('open');
        }
    })
}

function initBackToTop() {
    var $backToTop = $('.footer__backtotop');
    if ($backToTop.length < 1) {
        return false;
    }
    $backToTop.click(function (e) {
        e.preventDefault();
        document.getElementsByClassName("header-logo")[0].focus();
        $('html, body').animate({
            'scrollTop': 0
        }, 1000, function () {
        });
    });
}

function initAniElem() {
    var $aniElem = $('.ani-elem, .our-work, .news-centre, .event-calendar, .sec-highlights');
    if (!$aniElem.length) {
        return false;
    }
    $aniElem.each(function (key) {
        var $thisElem = $(this);
        var thisObj = {
            elem: $thisElem
        };
        aniElemPos.push(thisObj);
    });
    setAniElemPos();
}

var itemsSliders = {};
function initItemsSlider() {
    var $slider = $('.items-slider');
    if ($slider.length < 1) {
        return false;
    }
    $slider.each(function (key) {
        var $this = $(this);
        var thisId = 'items-slider--' + key;
        $this.attr('id', thisId);
        var $thisSlider = $this.find('.items-slider__slider');
        var itemNum = $thisSlider.find('.items-slider__item').length;
        setData($this, 'item-no', itemNum);
        if (itemNum > 1) {
            var btnPrev = '#' + thisId + ' .items-slider__slider-prev';
            var btnNext = '#' + thisId + ' .items-slider__slider-next';
            itemsSliders[key] = new Swiper($thisSlider[0], {
                pagination: '.items-slider__pagination',
                spaceBetween: 50,
                slidesPerView: 'auto',
                nextButton: btnNext,
                prevButton: btnPrev,
                slidesPerGroup: 1,
                loop: false,
                //effect: 'fade',
                observeParents: true,
                initialSlide:0,
                //autoHeight: true,
                observer: true,
                autoplay: false,
                paginationClickable: true,
                autoplayDisableOnInteraction: false,
                simulateTouch: true,
                paginationBulletRender: function (swiper, index, className) {
                    var txtSlide = getTxtByLang(['To slide no. ' + (index + 1), '去第' + (index + 1) + '張', '去第' + (index + 1) + '张'])
                    return '<span class="' + className + '" title="' + txtSlide + '">' + '</span>';
                },
                onInit: function () {
                    //initSwiperPause();
					initSlideTabindex();
                },
                breakpoints: {
                },
            });
        }
    });
	initSlideTabindex();

}

var thumbSliders = {};
function initThumbSlider() {
    var $slider = $('.thumb-slider');
    if ($slider.length < 1) {
        return false;
    }
    $slider.each(function (key) {
        var $this = $(this);
        var thisId = 'thumb-slider--' + key;
        $this.attr('id', thisId);
        var $thisSlider = $this.find('.thumb-slider__slider');
        var btnPrev = '#' + thisId + ' .thumb-slider__slider-prev';
        var btnNext = '#' + thisId + ' .thumb-slider__slider-next';
        thumbSliders[key] = new Swiper($thisSlider[0], {
            pagination: '.thumb-slider__pagination',
            spaceBetween: 20,
            slidesPerView: 6,
            nextButton: btnNext,
            prevButton: btnPrev,
            slidesPerGroup: 6,
            loop: true,
            //effect: 'fade',
            //autoHeight: true,
            observer: true,
            autoplay: 8000,
            paginationClickable: true,
            autoplayDisableOnInteraction: false,
            simulateTouch: true,
            paginationBulletRender: function (swiper, index, className) {
                var txtSlide = getTxtByLang(['To slide no. ' + (index + 1), '去第' + (index + 1) + '張', '去第' + (index + 1) + '张'])
                return '<span class="' + className + '" title="' + txtSlide + '">' + '</span>';
            },
            onInit: function () {
                //initSwiperPause();
				initSlideTabindex();
            },
            breakpoints: {
                991: {
                    slidesPerView: 4,
                    slidesPerGroup: 4,
                },
                767: {
                    slidesPerView: 3,
                    slidesPerGroup: 3,
                },
                640: {
                    slidesPerView: 2,
                    slidesPerGroup: 2,
                },
                380: {
                    slidesPerView: 1,
                    slidesPerGroup: 1,
                }
            },
        });
    });
	initSlideTabindex();
}

var videoSliders = {};
function initVideoSlider() {
    var $slider = $('.video-sec__slider');
    if ($slider.length < 1) {
        return false;
    }
    $slider.each(function (key) {
        var $this = $(this);
        var thisId = 'video-sec__slider--' + key;
        $this.attr('id', thisId);
        var $thisSlider = $this.find('.video-sec__slider-inner');
        var btnPrev = '#' + thisId + ' .video-sec__slider-prev';
        var btnNext = '#' + thisId + ' .video-sec__slider-next';
        videoSliders[key] = new Swiper($thisSlider[0], {
            pagination: '.video-sec__pagination',
            spaceBetween: 20,
            nextButton: btnNext,
            prevButton: btnPrev,
            slidesPerView: 3,
            slidesPerGroup: 3,
            loop: true,
            //effect: 'fade',
            autoHeight: true,
            autoplay: false,
            paginationClickable: true,
            autoplayDisableOnInteraction: false,
            simulateTouch: true,
            paginationBulletRender: function (swiper, index, className) {
                var txtSlide = getTxtByLang(['To slide no. ' + (index + 1), '去第' + (index + 1) + '張', '去第' + (index + 1) + '张'])
                return '<span class="' + className + '" title="' + txtSlide + '">' + '</span>';
            },
            onSlideNextEnd: function () {
                initVideos();
            },
            onSlidePrevEnd: function () {
                initVideos();
            },
            onTransitionEnd: function () {
                initVideos();
            },
            onTouchStart: function () {
                initVideos();
            },
            onInit: function () {
                //initSwiperPause();
				initSlideTabindex();
            },
            breakpoints: {
                767: {
                    slidesPerView: 2,
                    slidesPerGroup: 2,
                },
                480: {
                    slidesPerView: 1,
                    slidesPerGroup: 1,
                }
            },
        });
    });
	initSlideTabindex();
}

var gallerySliders = {};
function initGallerySlider() {
    var $slider = $('.gallery-slider');
    if ($slider.length < 1) {
        return false;
    }
    $slider.find('.gallery-slider__info-holder').wrapInner("<div class='gallery-slider__info-holder-inner'></div>");
    $slider.each(function (key) {
        var $this = $(this);
        var thisId = 'gallery-slider--' + key;
        $this.attr('id', thisId);
        var $thisSlider = $this.find('.gallery-slider__slider');
        var btnPrev = '#' + thisId + ' .gallery-slider__slider-prev';
        var btnNext = '#' + thisId + ' .gallery-slider__slider-next';
        if ($thisSlider.find('.gallery-slider__item').length < 2) {
            $this.find('.gallery-slider__control').hide();
        }
        $thisSlider.find('.gallery-slider__img-holder').each(function () {
            var $this = $(this);
            var $thisImg = $this.find('img');
            var image = new Image();
            image.src = $thisImg.attr("src");
            if (image.naturalWidth > image.naturalHeight) {
                $this.addClass('is-landscape');
            } else {

            }
        });
        gallerySliders[key] = new Swiper($thisSlider[0], {
            pagination: '.gallery-slider__pagination',
            spaceBetween: 20,
            slidesPerView: 1,
            slidesPerGroup: 1,
            nextButton: btnNext,
            prevButton: btnPrev,
            loop: true,
            effect: 'fade',
			fade: { crossFade: true },
            autoHeight: true,
            observer: true,
            autoplay: false,
            paginationClickable: true,
            autoplayDisableOnInteraction: false,
            simulateTouch: true,
            touchMoveStopPropagation: true,
            threshold: 90,
            observeParents: true,
            paginationBulletRender: function (swiper, index, className) {
                var txtSlide = getTxtByLang(['To slide no. ' + (index + 1), '去第' + (index + 1) + '張', '去第' + (index + 1) + '张'])
                return '<span class="' + className + '" title="' + txtSlide + '">' + '</span>';
            },
            onTransitionEnd: function () {
                handleGallerySlider()
                $(window).resize();
            },
            onInit: function () {
                //initSwiperPause();
				initSlideTabindex();
				$('.gallery-slider__info-txt').on('mousedown touchstart pointerdown', function (e){
                    e.stopPropagation();
                });
            },
            breakpoints: {
            },
        });
    });
	initSlideTabindex();
}
function initSlideTabindex(){
    var $sliderDuplicate = $('.swiper-slide-duplicate');
    $sliderDuplicate.attr({'tabindex':'-1','aria-hidden':'true'});
    $sliderDuplicate.find('div').attr({'tabindex':'-1','aria-hidden':'true'});
    $sliderDuplicate.find('a').attr({'tabindex':'-1','aria-hidden':'true'});
}							 

var imageSliders = {};
function initImageSlider() {
    var $slider = $('.image-slider');
    if ($slider.length < 1) {
        return false;
    }
    $('.image-slider__img-holder').customScrollbar({
        skin: "default-skin",
        hScroll: false,
        updateOnWindowResize: true
    }).customScrollbar("resize", true);
    $slider.each(function (key) {
        var $this = $(this);
        var thisId = 'image-slider--' + key;
        $this.attr('id', thisId);
        var $thisSlider = $this.find('.image-slider__slider');
        var btnPrev = '#' + thisId + ' .image-slider__slider-prev';
        var btnNext = '#' + thisId + ' .image-slider__slider-next';
        imageSliders[key] = new Swiper($thisSlider[0], {
            pagination: '.image-slider__pagination',
            spaceBetween: 20,
            slidesPerView: 1,
            nextButton: btnNext,
            prevButton: btnPrev,
            slidesPerGroup: 1,
            loop: true,
            effect: 'fade',
            autoHeight: true,
            observer: true,
            autoplay: false,
            paginationClickable: true,
            autoplayDisableOnInteraction: false,
            simulateTouch: true,
            observeParents: true,
            paginationBulletRender: function (swiper, index, className) {
                var txtSlide = getTxtByLang(['To slide no. ' + (index + 1), '去第' + (index + 1) + '張', '去第' + (index + 1) + '张'])
                return '<span class="' + className + '" title="' + txtSlide + '">' + '</span>';
            },
            onSlideChangeEnd: function () {
                //initSwiperPause();
                $this.find('.image-slider__img-holder').customScrollbar({
                    skin: "default-skin",
                    hScroll: false,
                    updateOnWindowResize: true
                }).customScrollbar("resize", true);
            },
            breakpoints: {
            },
        });
		if(isIE){
			// var $control = $this.find('.image-slider__control');
			// var $controlClone = $control.clone(true);
			
			// $control.remove();
			// var $holder = $this.find('.image-slider__slider .swiper-wrapper');
			// $holder.prepend($controlClone);

            var $control = $this.find('.image-slider__item');
            var $controlClone = $control.clone(true);
            $control.remove();
            var $holder = $this.find('.image-slider__slider .swiper-wrapper');
            $holder.append($controlClone);
		}
    });
	initSlideTabindex();
}

function initDateNth(d) {
  if(d>3 && d<21) return 'th'; // thanks kennebec
  switch (d % 10) {
        case 1:  return "st";
        case 2:  return "nd";
        case 3:  return "rd";
        default: return "th";
    }
}
var events = [];
function initEventCalendar() {
    var $eventCalendar = $('.event-calendar');
    if ($eventCalendar.length < 1) {
        return false;
    }
    var dateToday = new Date();

    var $holder = $('.event-calendar__list');
    $.each(canvasEvents, function (key, value) {
        var name = getTxtByLang([value.name_en, value.name_tc, value.name_sc]);
        var venue = getTxtByLang([value.venue_en, value.venue_tc, value.venue_sc]);
        var href = value.href;
        var start_date = value.start_date;
        var end_date = value.end_date;
        var EnMonth=["January","February","March","April","May","June","July","August","September","October","November","December"];
        var start_day = value.start_day;
		var start_month = value.start_month;
        var end_day = value.end_day;
		var end_month = value.end_month;
        if (start_date == end_date) {
            var showDate = start_date;
            var showDays = start_day;
            var start_dateType = start_date.substring(start_date.indexOf("-")+1,start_date.lastIndexOf("-"));
            for (var i = 0; i <= EnMonth.length;i++) {
                if (start_dateType == i.toString()) {
                    start_dateType = EnMonth[i-1];
                }
            }
            var showDaysType = initDateNth(showDays) + ' of ' + start_dateType;
            var showDateType = getTxtByLang([showDaysType, '日', '日']);
            var showDays = start_day + "/" + start_month; /*+ '<span class="event-calendar__dateth">' + showDateType + '</span>'*/
        } else {
            var showDate = start_date + '&nbsp;-&nbsp;' + end_date;
            var start_dateType = start_date.substring(start_date.indexOf("-")+1,start_date.lastIndexOf("-"));
            var end_dateType = end_date.substring(end_date.indexOf("-")+1,end_date.lastIndexOf("-"));
            for (var i = 0; i <= EnMonth.length;i++) {
                if (start_dateType == i.toString()) {
                    start_dateType = EnMonth[i-1];
                }
                if (end_dateType == i.toString()) {
                    end_dateType = EnMonth[i-1];
                }
            }
            var showDaysStartType = initDateNth(start_day) + ' of ' + start_dateType;
            var showDaysEndType = initDateNth(end_day) + ' of ' + end_dateType;
            var showStartDateType = getTxtByLang([showDaysStartType, '日', '日']);
            var showEndDateType = getTxtByLang([showDaysEndType, '日', '日']);
            var showDays = start_day + "/" + start_month + /*'<span class="event-calendar__dateth">'+showStartDateType+'</span>*/'&nbsp;-&nbsp;' + end_day + "/" + end_month /*+ '<span class="event-calendar__dateth">'+showEndDateType+'</span>'*/;
        }
        /*var start_time = value.start_time;
        var end_time = value.end_time;*/
		var showYear = value.start_date.substring(0,4);
        var $thisItem = $('<a href="' + href + '" class="event-calendar__list-item" data-start-date="' + start_date + '" data-end-date="' + end_date + '">' +
                '<div class="event-calendar__list-day"><div class="event-calendar__dateyeardata">' + showYear + '</div><div class="event-calendar__datedata">' + showDays + '</div></div>' +
                '<div class="event-calendar__list-txt">' +
                '<h3 class="event-calendar__list-title">' + name + '</h3>' +
                '<p class="event-calendar__list-detail">' +
                venue + '<br>' + showDate + /*'&nbsp;&nbsp;|&nbsp;&nbsp;' + start_time + '-' + end_time +*/
                '</p>' +
                '</div>' +
                '</a>');

        if ($holder.hasClass('scrollable')) {
            $holder.find('.overview').append($thisItem);
        } else {
            $holder.append($thisItem);
        }
    });

    $eventCalendar.each(function (key) {
        var $this = $(this);
        var $thisCalendarHolder = $this.find('.event-calendar__calendar-holder');
        var $thisCalendar = $this.find('.event-calendar__calendar');
        var $thisCalendarList = $this.find('.event-calendar__list')

        //var targetH = $thisCalendarHolder.outerHeight();

        var $thisEvenItems = $this.find('.event-calendar__list-item');
        $thisEvenItems.each(function (key) {
            var $thisItem = $(this);
            var thisStartDate = [0, 0, 0], thisEndtDate = [0, 0, 0];
            if ($thisItem.data('start-date')) {
                thisStartDate = $thisItem.data('start-date').split("-");
            }
            if ($thisItem.data('end-date')) {
                thisEndtDate = $thisItem.data('end-date').split("-");
            }
            var thisInfo = {
                target: $thisItem,
                startDate: new Date(thisStartDate[0], thisStartDate[1] * 1 - 1, thisStartDate[2], 0, 0, 0),
                endDate: new Date(thisEndtDate[0], thisEndtDate[1] * 1 - 1, thisEndtDate[2], 0, 0, 0)
            }
            events.push(thisInfo);
        });
        $thisCalendar.datepicker({
            showOtherMonths: false,
            selectOtherMonths: true,
            changeYear: true,
            changeMonth: true,
            dateFormat: "yy-mm-dd",
            dayNamesMin: getDaysName('long'),
            monthNames: getMonthName(),
            monthNamesShort: getMonthName(),
            minDate: '-2Y',
            maxDate: '+2Y',
            prevText: getTxtByLang(['Previous Month', '上一月', '上一月']),
            nextText: getTxtByLang(['Next Month', '下一月', '下一月']),
            onSelect: function (date) {
                var thisDate = date.split("-");
                thisDate = new Date(thisDate[0], thisDate[1] * 1 - 1, thisDate[2], 0, 0, 0);
                var targets = null;
                targets = [];
                var i;
                for (i = 0; i < events.length; ++i) {
                    // do something with `substr[i]`
                    var thisItem = events[i];
                    if (thisDate >= thisItem.startDate && thisDate <= thisItem.endDate) {
                        targets.push(thisItem.target);
                    }
                }
                if (targets.length > 0) {
                    var i;
                    $thisEvenItems.removeClass('is-active');
                    for (i = 0; i < targets.length; ++i) {
                        targets[i].addClass('is-active');
                    }
                    if (isMSTouchDevice) {
                        $thisCalendarList.animate({scrollTop: targets[0].position().top}, 2000);
                    } else {
                        $thisCalendarList.customScrollbar("scrollTo", targets[0]);
                    }
                }
				addTodayBtn();
            },
            beforeShowDay: function (date) {
				
				var neededClass = "";
				
				var tmptoday = new Date();
				tmptoday.setHours(0,0,0,0);
				
				var isToday = false;
				if(tmptoday.getTime() === date.getTime()){
					//console.log("chosen one");
					isToday = true;
				}
				
                var i;
                var needHighlight = false;
                for (i = 0; i < events.length; ++i) {
                    // do something with `substr[i]`
                    var thisItem = events[i];
                    if (date >= thisItem.startDate && date <= thisItem.endDate) {
                        needHighlight = true;
                    }
                }
				
                if (needHighlight) {
                    //return [true, 'ui-state-custom-highlight', ''];
					neededClass += 'ui-state-custom-highlight';
                } else {
                    //return [true, '', ''];
                }
				
				if(isToday){
					neededClass += ' real-today';
				}
				return [true, neededClass, ''];
				
            },
			onChangeMonthYear: function(year, month, datePicker){
				var newDate = $(".event-calendar__calendar").datepicker( "getDate" );
				newDate.setMonth(month-1);
				newDate.setFullYear(year);
				//console.log("new Date: "+newDate);
				addTodayBtn();
			}
		});
		
		
		var txtToday = getTxtByLang(['Today', '今日', '今日']);
        var htmlBtn = '<span style="cursor: pointer;" class="event-calendar__btn-today" onclick="action_today()" onkeypress="action_today()"><a href="javascript:void(0);">'+txtToday+'</a></span>';
        var $titleHolder = $thisCalendar.find('.ui-datepicker-title');
        $titleHolder.append($(htmlBtn));
		
        //initCustomDatepick();

    });
}

function addTodayBtn(){
	//console.log("===== add Today btn========");
	//console.log("-->"+$(".event-calendar__calendar").datepicker( "getDate" ));
	setTimeout(function() {

		var txtToday = getTxtByLang(['Today', '今日', '今日']);
		var htmlBtn = '<span style="cursor: pointer;" class="event-calendar__btn-today" onclick="action_today()" onkeypress="action_today()"><a href="javascript:void(0);">'+txtToday+'</a></span>';
		var $titleHolder = $('.event-calendar__calendar-holder .ui-datepicker-title');
		if($('.event-calendar__calendar-holder .event-calendar__btn-today').length < 1)
			$titleHolder.append($(htmlBtn));
	});	
}

function action_today(){
	//console.log("======== btn pressed ==============="+$('.ui-datepicker-year').val() + " | " + $('.ui-datepicker-month').val());
	var tmptoday = new Date();
	tmptoday.setHours(0,0,0,0);
	
	if( !($('.ui-datepicker-year').val() == tmptoday.getFullYear() && $('.ui-datepicker-month').val() == tmptoday.getMonth()) ){
		$('.ui-datepicker-year').val(tmptoday.getFullYear()).trigger('change');
		$('.ui-datepicker-month').val(tmptoday.getMonth()).trigger('change');
	}
	
}

function initCustomDatepick() {

    //datepick handler
    var customOption = '';
	//$('.ui-datepicker-year').on('click','myYearClick');
    $this.find('.ui-datepicker-year').find('option').each(function () {
        var $thisYearOption = $(this);
        var $thisYearVal = $thisYearOption.val();
        $this.find('.ui-datepicker-year').val($thisYearVal).trigger('change');
        $this.find('.ui-datepicker-month').find('option').each(function () {
            var $thisMonthOption = $(this);
            var $thisMonthTxt = $thisMonthOption.html();
            var $thisMonthVal = $thisMonthOption.val();
            var thisOption = '<option value="' + $thisYearVal + '-' + $thisMonthVal + '">' + $thisMonthTxt + ' ' + $thisYearVal + '</option>'
            customOption += thisOption;
        });
    });
	
   // $('.ui-datepicker-year').val(2018).trigger('change');
    var $titleHolder = $this.find('.ui-datepicker-title');
    $titleHolder.append('<div class="custom-datepick"><select class="custom-datepick__options">' + customOption + '</select></div>');
    var $customDatepick = $('.custom-datepick');
    $customDatepick.each(function () {
        var $selectHolder = $(this);
        var $selectFieldHolder = $selectHolder.parent();
        var $selectListMob = $selectHolder.find('select');
        var initListForDesktop = function () {
            var listForDesktop = '<div class="custom-datepick__list"><div class="custom-datepick__list-inner"><ul>';
            var triggerContent = '';
            $selectListMob.find('option').each(function (key) {
                var $thisOption = $(this);
                var thisLabel = $thisOption.html();
                var thisVal = $thisOption.val();
                var thisDtOption = '<li data-val="' + thisVal + '"><span>' + thisLabel + '</span></li>';
                listForDesktop += thisDtOption;
            });
            listForDesktop += '</ul></div></div>';
            listForDesktop = '<a href="#" class="custom-datepick__trigger"><span>' + triggerContent + '</span></a>' + listForDesktop;

            $(listForDesktop).insertBefore($selectListMob);
        };
        initListForDesktop();

        var $selectTrigger = $selectHolder.find('.custom-datepick__trigger');
        var $selectListHolder = $selectHolder.find('.custom-datepick__list');
        var $selectListInner = $selectHolder.find('.custom-datepick__list-inner');
        var $selectList = $selectHolder.find('.custom-datepick__list ul>li');

        var selectOpen = function () {
            selectAllClose();
            $selectTrigger.addClass('animating');
            $selectListHolder.hide().slideDown(300, function () {
                $selectHolder.addClass('open');
                $selectTrigger.removeClass('animating');
                $(document).click(function (event) {
                    if (!$(event.target).closest('.custom-datepick').length && !$(event.target).is('.custom-datepick')) {
                        selectClose();
                    }
                });
                $selectListInner.customScrollbar({
                    skin: "default-skin",
                    hScroll: false,
                    updateOnWindowResize: true
                }).customScrollbar("resize", true);
            });
        };
        var selectClose = function () {
            $selectTrigger.addClass('animating');
            $selectListHolder.show().slideUp(300, function () {
                $selectHolder.removeClass('open');
                $selectTrigger.removeClass('animating');
                $(document).unbind();
            });
        };
        var selectAllClose = function () {
            $('.custom-datepick.open ul').show().slideUp(300, function () {
                $('.custom-datepick.open .animating').removeClass('animating');
                $('.custom-datepick.open').removeClass('open');
                $(document).unbind();
            });
        };
        var syncSelectList = function () {
            var thisVal = $selectListMob.val();
            $selectList.removeClass('selected');
            var targetLi = $selectListHolder.find('li[data-val="' + thisVal + '"]')
            targetLi.addClass('selected');
            var selectedTxt = targetLi.find('span').html();
            $selectTrigger.find('span').html(selectedTxt);
        };
        $selectTrigger.click(function (event) {
            event.preventDefault();
            if ($selectTrigger.hasClass('animating'))
                return false;
            if (!$selectHolder.hasClass('open')) {
                selectOpen();
            } else {
                selectClose();
            }
        });
        $selectList.click(function (event) {
            event.preventDefault();
            if (!$(this).hasClass('selected')) {
                $selectList.removeClass('selected');
                $(this).addClass('selected');
                var thisVal = $(this).data('val');
                $selectListMob.val(thisVal).trigger('change');
            }
            selectClose();
        });
        $selectListInner.customScrollbar({
            skin: "default-skin",
            hScroll: false,
            updateOnWindowResize: true
        }).customScrollbar("resize", true);
        $selectListMob.change(function () {
            syncSelectList();
        });
        syncSelectList();
    });
}

function initSecShortcut() {
    var $secShortcut = $('.sec-shortcut');
    if ($secShortcut.length < 1) {
        return false;
    }
    var $secShortcutLinks = $secShortcut.find('.sec-shortcut__link');
    $secShortcutLinks.click(function (e) {
        e.preventDefault();
        var thisTarId = $(this).attr('href');
        var thisTarget = $(thisTarId);
        scrollToElemTop(thisTarget);
    });
}
function initOrgChart() {
    var $orgItem = $('.org-chart__info-tel, .org-chart__info-email');
    $orgItem.each(function () {
        var $thisItem = $(this);
        var $thisTrigger = $thisItem.find('a');
        $thisTrigger.click(function (e) {
            e.preventDefault();
            $orgItem.removeClass('is-active');
            $thisItem.addClass('is-active');
        });
    });
    $(document).on('touchstart.orgChartInfo click.orgChartInfo', function (e) {
        if (!$orgItem.is(e.target) && $orgItem.has(e.target).length === 0) {
            $orgItem.removeClass('is-active');
        }
    });

    //Added 20180226 for collapsible table
    var $collapsibles = $('.org-chart__collapsible');
    var collapsiblesLen = $collapsibles.length;
    var $collapsiblesToggle = $('.org-chart__toggle');
    var $collapsibleToggle = $('.org-chart__collapsible>.org-chart__item');
    $collapsibleToggle.attr('tabindex', 0);
    
    $collapsibleToggle.on('click.collapsible', function (e) {
        var $this = $(this);
        var $thisItem = $this.parent('.org-chart__collapsible');
        $thisItem.toggleClass('is-active');
        checkCollapsibles();
    });
    $collapsibleToggle.keydown(function (e) {
      if (e.keyCode == 13) {
        var $this = $(this);
        var $thisItem = $this.parent('.org-chart__collapsible');
        $thisItem.toggleClass('is-active');
        checkCollapsibles();
      }
    });
    
    $collapsiblesToggle.click(function (e) {
          e.preventDefault();
          if($collapsiblesToggle.hasClass('is-active')){
            $collapsibles.removeClass('is-active');
            scrollToElemTop($('.breadcrumbs'));
          } else {
            $collapsibles.addClass('is-active');
          }
          checkCollapsibles();
    });
    $('.org-chart__collapsible>.org-chart__item a').click(function (e) {
        e.stopPropagation(); // prevent click propagation
    });
    
    function checkCollapsibles(){
      var $activeCollapsibles = $('.org-chart__collapsible.is-active');
      var activeCollapsiblesLen = $activeCollapsibles.length;
      if(activeCollapsiblesLen >= collapsiblesLen){
        $collapsiblesToggle.addClass('is-active');
      } else {
        $collapsiblesToggle.removeClass('is-active');
      }
    }
    
    
}
function initDatepickerItem() {
    var $datepickerItems = $('.datepicker-item');
    $datepickerItems.each(function () {
        var $thisItem = $(this);
        var $thisInput = $thisItem.find('input');
		$thisInput.datepicker({
			dateFormat: "dd/mm/yy",
			dayNamesMin: getDaysName('long'),
            monthNames: getMonthName(),
            monthNamesShort: getMonthName()
		});
    });
}
function initGalleryItem() {
    var $galleryItem = $('.galleryItem');
    if ($galleryItem.length < 1) {
        return false;
    }
    var groups = {};
    $galleryItem.each(function (key) {
        var $this = $(this);
        var dataGroup = $this.data('group');
        if (!dataGroup) {
            setData($this, 'group', 'no-group-' + key);
            dataGroup = 'no-group-' + key;
        } else {
            dataGroup = parseInt(dataGroup, 10);
        }
        var id = dataGroup;
        if (!groups[id]) {
            groups[id] = [];
        }
        groups[id].push(this);
    });
    $.each(groups, function () {
        $(this).magnificPopup({
            type: 'image',
            image: {
                titleSrc: 'title'
            },
            closeOnContentClick: true,
            closeBtnInside: false,
            gallery: {enabled: true}
        });
    });
}
function initShareBtns() {
    var $headerShare = $('.header-nav__tool--share');
    var $headerShareTrigger = $headerShare.find('.header-nav__tool--share-trigger');
    $headerShareTrigger.click(function (event) {
        event.preventDefault();
        closeAllSubMenu();
        $headerShare.addClass('is-active');
    });
    $(document).on('touchstart.headerShare click.headerShare', function (e) {
        if (!$headerShare.is(e.target) && $headerShare.has(e.target).length === 0) {
            $headerShare.removeClass('is-active');
        }
    });
    var $holder = $('.share-btns');
    var $btn = $holder.find('.share-btns__btn').not('.share-btns__btn--rss');
    var $windowCount = 0;
	//init QR
    new QRCode(document.getElementById("wc-qrcode"), url);
    $btn.click(function (event) {
        event.preventDefault();
        var $thisBtn = $(this);
        var pageShareTitle = encodeURIComponent(document.getElementsByTagName("title")[0].innerHTML);
        var pageShareContent = window.location.href;
        var href = "";
        var type = 'default';
        var pageUrl = encodeURIComponent(window.location.href);
        //var pageUrl = encodeURIComponent('https://www.ogcio.gov.hk' + window.location.pathname);
        if ($thisBtn.hasClass('share-btns__btn--fb')) {
            href = "https://www.facebook.com/sharer/sharer.php?u=" + pageUrl;
        } else if ($thisBtn.hasClass('share-btns__btn--email')) {
            href = "mailto:?subject=" + pageShareTitle + "&body=" +pageShareTitle+'%0A'+ pageUrl;
        } else if ($thisBtn.hasClass('share-btns__btn--wa')) {
            if(mobileDevice){
                href = "whatsapp://send?text=" + pageShareTitle + ": " + pageUrl;
            }else{
				href = "https://web.whatsapp.com/send?text=" + pageShareTitle + ": " + pageUrl;
            }
        } else if ($thisBtn.hasClass('share-btns__btn--tw')) {
            href = "http://twitter.com/share?text=" + pageShareTitle + ": " + "&url=" + pageUrl;
        } else if ($thisBtn.hasClass('share-btns__btn--wb')) {
            href = "http://service.weibo.com/share/share.php?url=" + pageUrl + "&title=" + pageShareTitle;
        } else if ($thisBtn.hasClass('share-btns__btn--wc')) {
            href = window.location.href;
            type = 'wechat';
        }
		
        PopupCenter(href, "shareWindow_" + $windowCount, 600, 500, type);
        $windowCount++;
    });
}
function closeAllShareBtn() {
    var $headerShare = $('.header-nav__tool--share');
    $headerShare.removeClass('is-active');
}
function initPrintBtn() {
    $('.header-nav__tool--print').click(function (event) {
        event.preventDefault();
        window.print();
    });
}
function initMainSearch() {
    //Top Search
    var $topSearch = $('.top-search');
    var $topSearchInput = $topSearch.find('input');
    var $topSearchSubmit = $topSearch.find('.main-search__btn');
    $topSearchSubmit.click(function (event) {
        event.preventDefault();
        createSearchForm($topSearchInput);
    });
    $topSearchInput.keypress(function (e) {
        if (e.which == 13) {
            createSearchForm($topSearchInput);
        }
    });

    //Fixed Menu Search
    var $headerSearch = $('.header-nav__tool--search');
    var $headerSearchBtn = $headerSearch.find('.header-nav__tool-trigger');
    var $searchHolder = $headerSearch.next('.header-nav__sub-holder');
    var $searchInput = $searchHolder.find('input');
    var $searchSubmit = $searchHolder.find('.main-search__btn');
    $headerSearchBtn.unbind().click(function (event) {
        event.preventDefault();
        $searchInput.focus();
        return false;
    });
    $searchSubmit.click(function (event) {
        event.preventDefault();
        createSearchForm($searchInput);
    });
    $searchInput.keypress(function (e) {
        if (e.which == 13) {
            createSearchForm($searchInput);
        }
    });

    //Mobile
    var $mobSearch = $('.mob-nav__holder .main-search');
    var $mobSearchInput = $mobSearch.find('input');
    var $mobSearchSubmit = $mobSearch.find('.main-search__btn');
    $mobSearchSubmit.click(function (event) {
        event.preventDefault();
        createSearchForm($mobSearchInput);
    });
    $mobSearchInput.keypress(function (e) {
        if (e.which == 13) {
            createSearchForm($mobSearchInput);
        }
    });
}
function clearSearchForm() {
    //$('.main-search__input').val('');
    $('.main-search__input').not('.keep_data').val('');
}
function createSearchForm($searchInput) {
    var ui_lang, msgEmpty, actionPath;
    switch (lang) {
        case 'en':
            ui_lang = 'en';
            actionPath = '/en/search/result.html';
            msgEmpty = 'Please enter keyword.';
            break;
        case 'zh-hk':
            ui_lang = 'zh-hk';
            actionPath = '/tc/search/result.html';
            msgEmpty = '請輸入關鍵字。';
            break;
        case 'zh-cn':
            ui_lang = 'zh-cn';
            actionPath = '/sc/search/result.html';
            msgEmpty = '请输入关键字。';
            break;
        default:
    }
    var searchQuery = $searchInput.val();
    if (searchQuery == '') {
        alert(msgEmpty);
        return false;
    }
    var search_form = document.createElement('form');
    search_form.name = 'query';
    search_form.id = 'searchBox';
    search_form.method = 'get';
    search_form.action = actionPath;
    var searchParam = document.createElement('input');
    searchParam.type = 'hidden';
    searchParam.name = 'ui_lang';
    searchParam.value = ui_lang;
    search_form.appendChild(searchParam);
    searchParam = document.createElement('input');
    searchParam.type = 'hidden';
    searchParam.name = 'ui_charset';
    searchParam.value = 'utf-8';
    search_form.appendChild(searchParam);
    searchParam = document.createElement('input');
    searchParam.type = 'hidden';
    searchParam.name = 'oe';
    searchParam.value = 'utf-8';
    search_form.appendChild(searchParam);
    searchParam = document.createElement('input');
    searchParam.type = 'hidden';
    searchParam.name = 'ie';
    searchParam.value = 'utf-8';
    search_form.appendChild(searchParam);
    searchParam = document.createElement('input');
    searchParam.type = 'hidden';
    searchParam.name = 'gp0';
    searchParam.value = 'ogcio_home';
    search_form.appendChild(searchParam);
    searchParam = document.createElement('input');
    searchParam.type = 'hidden';
    searchParam.name = 'gp1';
    searchParam.value = 'ogcio_home';
    search_form.appendChild(searchParam);
    searchParam = document.createElement('input');
    searchParam.type = 'hidden';
    searchParam.name = 'site';
    searchParam.value = 'ogcio_home';
    search_form.appendChild(searchParam);
    searchParam = document.createElement('input');
    searchParam.type = 'text';
    searchParam.name = 'query';
    searchParam.value = searchQuery;
    search_form.appendChild(searchParam);
    document.body.appendChild(search_form);
    search_form.submit();
}



function PopupCenter(url, title, w, h, type) {
    if (type == 'wechat') {
        var html = $('.wc-qrcode').html();
		var wechatTitle = getTxtByLang(['WeChat Share', '微信分享', '微信分享']);
        var newWindow = window.open(url, '_blank', 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left); //　
        newWindow.document.write('<!DOCTYPE html><html><head><title>' + wechatTitle + '</title></head><body></body></html>');
        newWindow.document.write(html);
    } else {
        // Fixes dual-screen position                         Most browsers      Firefox
        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
        var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

        var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
        var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

        var left = ((width / 2) - (w / 2)) + dualScreenLeft;
        var top = ((height / 2) - (h / 2)) + dualScreenTop;
        var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

        // Puts focus on the newWindow
        if (window.focus) {
            newWindow.focus();
        }
    }
}
var playerInstance = [];
function initVideos() {
    var $videoItems = $('.video-thumb');
    if ($videoItems.length < 1) {
        return false;
    }
    var loadingTxt = getTxtByLang(['Loading the player...', '影片讀取中...', '影片读取中...']);
    $videoItems.each(function (key) {
        var $this = $(this);
        var thisSrc = $this.attr('href');
        $this.removeAttr('href');
        var thisTitle = $this.attr('title');
        if (!thisTitle || thisTitle == '') {
            thisTitle = getTxtByLang(['Video', '影片', '影片']);
        }
        var thisPreviewImg = $this.find('img').attr('src');
		var thisCaptionSrc = $this.find('track').attr('src');
			
        if (!$this.hasClass('video-thumb--initialized')) {
				setData($this, 'player-id', key);
				var $video = $('<div class="video-thumb__video-body">' + '<div id="container-' + key + '">' + loadingTxt + '</div>' + '</div>');
																																								
							  

				$this.append($video);

				playerInstance[key] = jwplayer("container-" + key);
				playerInstance[key].setup({
									file: thisSrc, image: thisPreviewImg, title: thisTitle, autostart: false,
									tracks :  [{ file: thisCaptionSrc, label: "English",kind: "captions","default": true }]
									});				
				console.log('Source ' + key + thisCaptionSrc);				
				$this.addClass('video-thumb--initialized');			
		}
    });
    $videoItems.click(function (e) {
        e.preventDefault();
        var $this = $(this);
        $this.addClass('video-thumb--click').removeClass('video-thumb--hover');
    });
    $videoItems.unbind('mouseover.player').on('mouseover.player', function () {
        var $this = $(this);
        if ($this.hasClass('video-thumb--initialized') && !$this.hasClass('video-thumb--hover') && !$this.hasClass('video-thumb--click')) {
            $this.addClass('video-thumb--played video-thumb--hover');
            var playId = $this.data('player-id');
            pasueAllVideos();
            playerInstance[playId].play(true);
            $this.find('.jwplayer').focus()
        }
    });
    $videoItems.unbind('mouseout.player').on('mouseout.player', function () {
        var $this = $(this);
        if ($this.hasClass('video-thumb--initialized') && $this.hasClass('video-thumb--hover') && !$this.hasClass('video-thumb--click')) {
            var playId = $this.data('player-id');
            playerInstance[playId].pause(true);
            $this.removeClass('video-thumb--hover');
        }
    });
}
function pasueAllVideos() {
    for (i = 0; i < playerInstance.length; i++) {
        playerInstance[i].pause(true);
    }
}
function initBackBtn() {
    var $btnBack = $('.btn-back');
    if ($btnBack.length < 1) {
        return false;
    }
    $btnBack.click(function (event) {
        event.preventDefault();
        window.history.back(-1);
    });
}
var headerContactMap = {
    'center': [22.280174, 114.172575],
    'zoom': 17,
    'map': null
};
function initHeaderContact() {
    if (typeof google === 'object' && typeof google.maps === 'object') {        // check if Google Api is loaded

        var center = new google.maps.LatLng(headerContactMap.center[0], headerContactMap.center[1])
        var mapOptions = {
            center: center,
            zoom: headerContactMap.zoom,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapTypeControl: true,
            panControl: true,
            zoomControl: true,
            streetViewControl: false,
            scrollwheel: false,
            draggable: true
        };
        headerContactMap.map = new google.maps.Map(document.getElementById("header-contact-map"), mapOptions);
        var marker = new google.maps.Marker({//Central
            position: center,
            map: headerContactMap.map
        });


    }
}
function resizeHeaderContact() {
    if (headerContactMap.map) {
        var center = new google.maps.LatLng(headerContactMap.center[0], headerContactMap.center[1])
        var zoom = 17;
        if (windowJsWidth <= 991) {
            zoom = 13;
        }
        if (windowJsWidth <= 768) {
            zoom = 13;
        }
        google.maps.event.trigger(headerContactMap.map, 'resize');
        headerContactMap.map.setZoom(headerContactMap.zoom);
        headerContactMap.map.panTo(center);
    }
}
function initBreakingNews() {
    var $breakingNews = $('.breaking-news');
    if ($breakingNews.length < 1) {
        return false;
    }
    var $closeBtn = $breakingNews.find('.breaking-news__close');
    $closeBtn.click(function (event) {
        event.preventDefault();
        event.stopPropagation();
        if ($.cookie('breakingNewsClosed') != 'true') {
            $.cookie('breakingNewsClosed', 'true', {expires: 1});
        }
        closeBreakingNews();
    });
    $closeBtn.keypress(function (e) {
        if (e.which == 13) {
            if ($.cookie('breakingNewsClosed') != 'true') {
                $.cookie('breakingNewsClosed', 'true', {expires: 1});
            }
            closeBreakingNews();
        }
    });
    var $openBtn = $('.header-nav__tool--news');
    $openBtn.removeClass('is-hidden').on('touchstart.openNews click.openNews', function (event) {
        event.preventDefault();
        if (!$openBtn.hasClass('is-active')) {
            openBreakingNews();
        } else {
            closeBreakingNews();
        }
        closeAllMenu();
    });
    var $breakingNewsSlider = $breakingNews.find('.breaking-news__slider');
    var swiper = new Swiper($breakingNewsSlider[0], {
        slidesPerView: 1,
        slidesPerGroup: 1,
        loop: true,
        speed: 500,
        //effect: 'fade',
        observeParents: true,
        autoplay: 5000,
        //autoHeight: true,
        observer: true,
        observeParents: true,
        paginationClickable: true,
        simulateTouch: true,
        a11y: true,
        paginationBulletRender: function (swiper, index, className) {
            var txtSlide = getTxtByLang(['To slide no. ' + (index + 1), '去第' + (index + 1) + '張', '去第' + (index + 1) + '张'])
            return '<span class="' + className + '" title="' + txtSlide + '">' + '</span>';
        },
        onInit: function (swiper) {
        },
        onResize: function () {
        },
        onTransitionEnd: function (swiper) {
        }
    });


    if ($.cookie('breakingNewsClosed') != 'true') {
        openBreakingNews();
    }
}
function openBreakingNews() {
    var $breakingNews = $('.breaking-news');
    if ($breakingNews.length < 1) {
        return false;
    }
    var $openBtn = $('.header-nav__tool--news');
    $breakingNews.slideDown(300, function () {
        $breakingNews.addClass('is-active');
        $openBtn.addClass('is-active');
        handleMain();
    });
}
function closeBreakingNews() {
    var $breakingNews = $('.breaking-news');
    if ($breakingNews.length < 1) {
        return false;
    }
    var $openBtn = $('.header-nav__tool--news')
    $breakingNews.slideUp(300, function () {
        $breakingNews.removeClass('is-active');
        $openBtn.removeClass('is-active');
        handleMain();
    });
}
function initPopup() {
    var $btnPopup = $('.btn-popup');
    $btnPopup.magnificPopup({
        type: 'inline',
        midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
    });
}
function initSecArticleHead() {
    var $articleHead = $('.sec-article__head-option');
    //
}
function initScrollReader() {
    var $scrollReader = $('.scroll-reader');
    if ($scrollReader.length < 1) {
        return false;
    }
    $scrollReader.customScrollbar({
        skin: "default-skin",
        hScroll: false,
        updateOnWindowResize: true
    }).customScrollbar("resize", true);
}

function initInfoTable() {
    var $infoTable = $('.info-table');
    if (!$infoTable.length) {
        return false;
    }
    $infoTable.wrapInner("<div class='info-table__inner'></div>").addClass('at-left');
    $infoTable.each(function () {
        var $thisTable = $(this);
        var $thisInner = $thisTable.find('.info-table__inner');
        $thisInner.on('scroll', function () {
            if ($(this).scrollLeft() === 0) {
                $thisTable.addClass('at-left');
            } else {
                $thisTable.removeClass('at-left');
            }
            if ($(this).scrollLeft() + $(this).innerWidth() >= $(this)[0].scrollWidth) {
                $thisTable.addClass('at-right');
            } else {
                $thisTable.removeClass('at-right');
            }
        });
        $thisTable.addClass('info-table--initialized');
    });

    handleInfoTable();
}
function initBWVer(){
  var isBWVer = $body.hasClass('bw-ver');
  if(!isBWVer){
    isBWVer = (getParameterByName('bw')=='true');
    if(isBWVer){
      $body.addClass('bw-ver');
      initBWVerForIE();
    }
  }
}

function initBWVerForIE(){
  if(isIE && ! isEdge){
    $body.addClass('bw-ver--ie');
    
    // Images that's not handled well by .gray() below
    var $imgElem = $('.header-logo img, .header-nav__sub-link > .header-nav__link-icon, .values__background img');
    var i = 0, lenImgElem = $imgElem.length;
    if(lenImgElem>0){
      for (i = 0; i < lenImgElem; i++) {
        var $thisElem = $imgElem.eq(i);
        var imgSrc = $thisElem.attr('src');
        $thisElem.attr('src', getBWSrc(imgSrc));
      }
    }
  
    // Background image
    var $bgElem = $('.static-banner__bg, .main-banner__slide-bg .divimage, .sec-highlights__item-img .sec-highlights__item-img-body');
    var i = 0, lenBgElem = $bgElem.length;
    if(lenBgElem>0){
      for (i = 0; i < lenBgElem; i++) {
        var $thisElem = $bgElem.eq(i);
        var bgSrc = $thisElem.attr('style');
        $thisElem.attr('style', getBWSrc(bgSrc));
      }
    }
    // Library that will convert Image Tag
    $('img').not($imgElem).gray();
  }
}

function getBWSrc(filename){
   var re = new RegExp("(.+)\\.(gif|png|jpg|svg)", "g");
   return filename.replace(re, "$1_bw.$2");
}
function initAnchor(){
    //search <a> that have both "id" and "name" attributes then add a class anchor to it
    $('a[id][name]').addClass('anchor');

    var $anchorLink = $('a.btn--backtotop');
    var $root = $('html, body');
    $anchorLink.click(function(){
        $root.animate({
            scrollTop: $( $.attr(this, 'href') ).offset().top
        }, 500);
        return false;
    });
}
function initKeyTabClick(){
    var $headerlang = $('.header-nav__text-control').find('a');
    $headerlang.focus(function(){
        $('.header-nav__link-holder.is-focus').removeClass('is-focus');
    })
}
function initKeyCheckBoxText(){
    var $customRadioLabel = $(".custom-radio__label");
    $customRadioLabel.each(function(){
        radioTxt = $(this).text();
        $(this).html('<span>'+radioTxt+'</span>');
    })
}
function initMultiLineFF(){

        $('.main-banner__news-content p').dotdotdot();

}

// ========================
// Layout Handlers
// ========================
function handleItemsSlider() {
    var $slider = $('.items-slider:visible');
    if ($slider.length < 1) {
        return false;
    }
}
function setAniElemPos() {
    var $aniElem = $('.ani-elem, .our-work, .news-centre, .event-calendar, .sec-highlights');
    if (!$aniElem.length) {
        return false;
    }
    $aniElem.each(function (key) {
        var $thisElem = $(this);
        //aniElemPos[key]['elem'] = $thisElem;
        aniElemPos[key]['topOfElem'] = $thisElem.offset().top;
        aniElemPos[key]['bottomOfElem'] = $thisElem.offset().top + $thisElem.outerHeight();
    });
}
function handleAniElem() {
    var totalLength = aniElemPos.length;
    if (aniElemPos.length < 1) {
        return false;
    }
    for (var key = 0; key < totalLength; key++) {
        var $thisElem = aniElemPos[key]['elem'];
        var topOfElem = aniElemPos[key]['topOfElem'];
        var bottomOfElem = aniElemPos[key]['bottomOfElem'];
        var bottomOfScreen = scrollTop + windowJsHeight;
        var topOfScreen = scrollTop;

        if ((bottomOfScreen > topOfElem) && (topOfScreen < bottomOfElem)) {
            $thisElem.addClass('in-view');
        } else {
            $thisElem.removeClass('in-view');
        }
    }
}
function handleSecShortcut() {
    var $secShortcut = $('.sec-shortcut');
    if (!$secShortcut.length) {
        return false;
    }
    var $secShortcutLink = $secShortcut.find('.sec-shortcut__link');
    var topOfScreen = scrollTop;
    var offset = windowHeight / 2;
    function activiateSec() {
        if (!$secShortcut.hasClass('is-active')) {
            $secShortcut.addClass('is-active');
        }
    }
    $secShortcutLink.removeClass('is-active');
    var $activeSec = false;
    if (topOfScreen >= $('footer').offset().top - offset + 200) {
        if ($secShortcut.hasClass('is-active')) {
            $secShortcut.removeClass('is-active');
        }
    } else if (topOfScreen >= $('.thumb-slider').offset().top - offset) {
        activiateSec();
        $activeSec = $('.sec-shortcut__link[href="#sec-highlights"]');
    } else if (topOfScreen >= $('#sec-event-calendar').offset().top - offset) {
        activiateSec();
        $activeSec = $('.sec-shortcut__link[href="#sec-event-calendar"]');
    } else if (topOfScreen >= $('#sec-news-centre').offset().top - offset) {
        activiateSec();
        $activeSec = $('.sec-shortcut__link[href="#sec-news-centre"]');
    } else if (topOfScreen >= $('#sec-our-work').offset().top - offset) {
        activiateSec();
        $activeSec = $('.sec-shortcut__link[href="#sec-our-work"]');
    } else {
        if ($secShortcut.hasClass('is-active')) {
            $secShortcut.removeClass('is-active');
        }
    }
    if ($activeSec) {
        $activeSec.addClass('is-active');
    }
}
function handleMain() {
    /* Sync height for Img box */
    var $main = $('main');
    if (!$main.length) {
        return false;
    }
    var $footer = $('footer');
    var $header = $('header');
    var $weather = $('.weather-info__weather');
    var $topSearch = $('.top-search');
    //var $generalIndex = $('.general-index');
    var topBarOffset = 0;
    /*** handle outdated browser case***/
    var $browserUpdate = $('.browser-update.is-active');
    var browserUpdateH = 0;
    if ($browserUpdate.length) {
        browserUpdateH = $browserUpdate.outerHeight();
        $header.addClass('has-browser-update');
    } else {
        $header.removeClass('has-browser-update');
    }
    topBarOffset += browserUpdateH;
    /*** END handle outdated browser case***/
    /*** handle breaking news case ***/
    var $breakingNews = $('.breaking-news.is-active');
    var breakingNewsH = 0;
    if ($breakingNews.length) {
        breakingNewsH = $breakingNews.outerHeight();
        $header.addClass('has-breaking-news');
    } else {
        $header.removeClass('has-breaking-news');
    }
    topBarOffset += breakingNewsH;
    if ($browserUpdate.length) {
        $breakingNews.css({
            'margin-top': browserUpdateH + 'px'
        });
    }
    /*** END handle breaking news case ***/

    $header.css({
        'padding-top': topBarOffset + 'px'
    });
    if ($weather.length) {
        $weather.css({
            'margin-top': topBarOffset + 'px'
        });
    }
    if ($topSearch.length) {
        $topSearch.css({
            'margin-top': topBarOffset + 'px'
        });
    }
    var footerHeight = $footer.outerHeight();
    var headerHeight = $header.outerHeight();
    var minHeight = windowHeight - footerHeight;
    $main.css({
        'min-height': minHeight + 'px'
                //'padding-top': headerHeight + 'px',
    });
    if ($breakingNews.length) {
        if (!$breakingNews.hasClass('is-ready')) {
            $breakingNews.addClass('is-ready');
        }
    }
    /*
     if ($generalIndex.length) {
     $generalIndex.css({
     'padding-top': headerHeight + topBarOffset + 'px'
     });
     }
     */
}
function handleMainBanner() {
    /* Sync height for Img box */
    var $mainBanner = $('.main-banner .main-banner__slider');
    if ($mainBanner.length < 1) {
        return false;
    }
    $('.main-banner').css({
        'height': windowHeight + 'px'
    });
}
function handleMobMenu() {
    if (windowJsWidth < 992) {
    } else {
        if ($body.hasClass('mob-menu-open')) {
            closeAllMenu('static');
        }
    }

}
/*
function handleHighlightBoxes() {
    var highlightBoxes = $('.highlight-boxes');
    if (!highlightBoxes.length) {
        return false;
    }
    highlightBoxes.each(function () {
        var $this = $(this);
        //var $Item = $this.find('.highlight-boxes__item-inner');
        var $ItemInner = $this.find('.highlight-boxes__item-inner');
        $ItemInner.height('');
        var maxHeight = getMaxHeight($ItemInner);
        $ItemInner.height(maxHeight);
    });
}*/
function handleEventCalendar() {
    var $thisCalendarList = $('.event-calendar__list');
    if (!$thisCalendarList.length) {
        return false;
    }
    var $item = $thisCalendarList.find('.event-calendar__list-item');
    if ($item.length > 2) {
        var targetH = $item.eq(0).outerHeight() + $item.eq(1).outerHeight() + $item.eq(2).outerHeight();
    } else {
        var targetH = 290;
    }
    if (isMSTouchDevice) {
        $thisCalendarList.height(targetH).css('overflow-y', 'auto');
    } else {
        $thisCalendarList.height(targetH).customScrollbar({
            skin: "default-skin",
            hScroll: false,
            updateOnWindowResize: true
        }).customScrollbar("resize", true);
    }
}
function handleEventSearch() {
    var $items = $('.event-search__result-holder');
    if (!$items.length) {
        return false;
    }
    $items.each(function () {
        var $this = $(this);
        var $ItemInner = $this.find('.event-search__item-inner');
        $ItemInner.height('');
        var maxHeight = getMaxHeight($ItemInner);
        $ItemInner.height(maxHeight);
    });
}
function handleFixedMenu() {
    if (scrollTop > 50) {
        $body.addClass('header--small');
    } else {
        $body.removeClass('header--small');
        $('.header-nav__tool--search').removeClass('is-open');
    }
}

function handleMainBannerBg() {
    var $mainBanner = $('.main-banner');
    if (!$mainBanner.length) {
        return false;
    }
    var $mainBannerBg = $('.main-banner__slide-bg');
    var $mainBannerBginner = $mainBannerBg.find('.main-banner__bg-wrapper');
    var thisTop = $mainBanner.offset().top;
    var thisHeight = $mainBanner.outerHeight();
    var thisBottom = thisTop + thisHeight;
    var scrollStart = thisTop;
    var scrollEnd = thisBottom;
    if (scrollTop >= scrollStart && scrollTop < scrollEnd) {
        var scrollDistance = scrollEnd - scrollStart;
        var scrollProgress = (scrollTop - scrollStart) / scrollDistance;
        if (scrollProgress > 1) {
            scrollProgress = 1;
        } else if (scrollProgress < 0) {
            scrollProgress = 0;
        } else {
            scrollProgress = scrollProgress * 2;
        }
        scrollProgress = 0 - scrollProgress;
        cssTranslate3d($mainBannerBg, '0,' + scrollProgress * 1 + '%,0');
        cssTranslate3d($mainBannerBginner, '0,' + scrollProgress * 1 + '%,0');
    }
}
function handleStaticBannerBg() {
    var $mainBanner = $('.static-banner');
    if (!$mainBanner.length) {
        return false;
    }
    var $mainBannerBg = $('.main-banner__slide-bg');
    var $mainBannerBginner = $mainBannerBg.find('.static-banner__bg-wrapper');
    var thisTop = $mainBanner.offset().top;
    var thisHeight = $mainBanner.outerHeight();
    var thisBottom = thisTop + thisHeight;
    var scrollStart = thisTop;
    var scrollEnd = thisBottom;
    if (scrollTop >= scrollStart && scrollTop < scrollEnd) {
        var scrollDistance = scrollEnd - scrollStart;
        var scrollProgress = (scrollTop - scrollStart) / scrollDistance;
        if (scrollProgress > 1) {
            scrollProgress = 1;
        } else if (scrollProgress < 0) {
            scrollProgress = 0;
        } else {
            scrollProgress = scrollProgress * 2;
        }
        cssTranslate3d($mainBannerBg, '0,' + scrollProgress * 1 + '%,0');
        cssTranslate3d($mainBannerBginner, '0,' + scrollProgress * 1 + '%,0');
    }
}
function handleTabs() {
    var $tabs = $('.tabs');
    if (!$tabs.length) {
        return false;
    }
    var $tabsHolder = $tabs.find('.tabs__content-holder');
    $tabsHolder.each(function () {
        var $this = $(this);
        var $thisContent = $this.find('.tabs__content');
        $thisContent.show();
        var maxHeight = getMaxHeight($thisContent);
        $thisContent.not('.is-active').hide();
        $this.height(maxHeight);
    });
}
function handleOGCIOTabs() {
    var $tabs = $('.ogcio__tabs');
    if (!$tabs.length) {
        return false;
    }
    var $tabsHolder = $tabs.find('.ogcio__tabs__content-holder');
    $tabsHolder.each(function () {
        var $this = $(this);
        var $thisContent = $this.find('.ogcio__tabs__content');
        $thisContent.show();
		//var maxHeight = getMaxHeight($thisContent);
        $thisContent.not('.is-active').hide();
		//$thisContent.height(maxHeight);
		//console.log(maxHeight);
    });
}							
function handleGallerySlider() {
    var $slider = $('.gallery-slider');
    if ($slider.length < 1) {
        return false;
    }
    /*
     var $sliderImg = $slider.find('.swiper-slide-active img');
     var targetH = $sliderImg.height()*0.5;
     var $sliderTxt = $slider.find('.swiper-slide-active .gallery-slider__info-txt');
     
     $sliderTxt.height('');
     if($sliderTxt.outerHeight()>targetH){
     $sliderTxt.height(targetH).customScrollbar({
     skin: "default-skin",
     hScroll: false,
     updateOnWindowResize: true
     }).customScrollbar("resize", true);
     }
     */
    $slider.each(function () {
        var $this = $(this);
        var $thisInfoHolder = $this.find('.swiper-slide-active .gallery-slider__info-holder');
        var $thisControl = $this.find('.gallery-slider__control');
        var holderHeight = $thisInfoHolder.outerHeight() / 2;
        if (windowJsWidth < 768) {
            $thisControl.css({'bottom': holderHeight + 'px'});
        } else {
            $thisControl.css({'bottom': ''});
        }
    });

}
function handleInfoTable() {
    var $infoTable = $('.info-table');
    if (!$infoTable.length) {
        return false;
    }
    //console.log('handleInfoTable()');
    $infoTable.each(function () {
        var $thisTable = $(this);
        var $thisInner = $thisTable.find('.info-table__inner');
        if ($thisInner.length) {
            if ($thisInner.get(0).scrollWidth <= $thisInner.get(0).clientWidth) {
                $thisTable.addClass('no-scroll');
            } else {
                $thisTable.removeClass('no-scroll');
            }
        }
    });
}
// Action Handlers
// ========================
function handleHash() {
    var hashTag = window.location.hash;
    if (hashTag && hashTag != '#') {
        if ($(hashTag + ':visible').length) {
            $('html, body').animate({
                'scrollTop': $(hashTag).offset().top - $('header').outerHeight()
            }, 1000);
        }
    }
}

function updateKeyTxtColor() {
    var $currentSlide = $('.main-banner__slide.swiper-slide-active, .static-banner__slide.swiper-slide-active');
    var $staticBanner = $('.static-banner');
    var txtColor = false;
    var newsBg = false;
    if ($currentSlide.length) {
        txtColor = $currentSlide.data('txt-color');
        newsBg = $currentSlide.data('news-bg');
    } else if ($staticBanner.length) {
        txtColor = $staticBanner.data('txt-color');
        newsBg = $staticBanner.data('news-bg');
    }
    if (txtColor) {
        setData($body, 'key-txt-color', txtColor);
    } else {
        unsetData($body, 'key-txt-color');
    }
    if (newsBg) {
        setData($body, 'key-news-bg', newsBg);
    } else {
        unsetData($body, 'key-news-bg');
    }
}

function cssTranslate3d(elem, xyz) {
    elem.css({
        '-webkit-transform': 'translate3d(' + xyz + ')',
        '-ms-transform': 'translate3d(' + xyz + ')',
        'transform': 'translate3d(' + xyz + ')'
    });
}
// ========================
// Helper, Validation Functions
// ========================
/*
function is_touch_device() {
    return 'ontouchstart' in window        // works on most browsers 
            || navigator.maxTouchPoints;       // works on IE10/11 and Surface
};
*/
function getDaysName(length) {
    var daysName, daysNameShort;
    if (lang == 'zh-hk' || lang == 'zh-cn') {
        daysName = [ "日", "一", "二", "三", "四", "五", "六"];
        daysNameShort = daysName;
    } else {
        daysName = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
        daysNameShort = ["S", "M", "T", "W", "T", "F", "S"];
    }
    if (length == 'long') {
        return daysName;
    } else {
        return daysNameShort;
    }
}

function getMonthName() {
    var monthName;
    if (lang == 'zh-hk' || lang == 'zh-cn') {
        monthName = ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"];
    } else {
        monthName = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    }
    return monthName;
}
function getTxtByLang(arrayTxt) {
    var currentLang = 0;
    switch (lang) {
        case 'zh-hk':
            currentLang = 1;
            break;
        case 'zh-cn':
            currentLang = 2;
            break;
        default:
            currentLang = 0;
    }
    return (arrayTxt[currentLang]);
}

function getMaxHeight(elem, mode) {
    var maxHeight = Math.max.apply(null, elem.map(function () {
        if (mode == 'raw') {
            return $(this).height();
        } else if (mode == 'inner') {
            return $(this).innerHeight();
        } else {
            return $(this).outerHeight();
        }
    }).get());
    return maxHeight;
}
/*
function getElemBottomPos(elem) {
    var bgTopPos = elem.offset().top;
    var bgHeight = elem.outerHeight();
    return (bgTopPos + bgHeight);
}*/
function setData(elem, data, val) {
    elem.attr('data-' + data, val).data(data, val);
}
function unsetData(elem, data) {
    elem.data(data, null).removeAttr('data-' + data);
}
//validate functions
/*
function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function validatePassword(password) {
    //var re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[A-Za-z\d$@$!%*?&]{8,16}$/;
    //return re.test(password);

    return (password.length >= 6) && (password.length <= 12);
}

function validateTel(tel, numLimit) {
    var intRegex = /^\d+$/;
    if (!numLimit) {
        numLimit = 15;
    }
    return (tel.length >= 8 && tel.length <= numLimit && intRegex.test(tel));
}

function validateNum(num) {
    var intRegex = /^\d+$/;
    return (intRegex.test(num));
}

function validateAddress(address) {
    return (!address.replace(/ /g, '').match(/^\d+$/));
}

function validateName(name) {
    return (!name.match(/\d+/g) && !haveSpecialChar(name));
}

function validateChineseName(name) {
    var num = /^\d+$/;
    var english = /^[A-Za-z0-9]*$/;
    return (!num.test(name) && !english.test(name));
}

function haveSpecialChar(string) {
    var specialChars = "<>@!#$%^&*()_+[]{}?:;|'\"\\,./~`-=";
    for (i = 0; i < specialChars.length; i++) {
        if (string.indexOf(specialChars[i]) > -1) {
            return true;
        }
    }
    return false;
}

function IsHKID(str) {
    var strValidChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

    // basic check length
    if (str.length < 8)
        return false;

    // handling bracket
    if (str.charAt(str.length - 3) == '(' && str.charAt(str.length - 1) == ')')
        str = str.substring(0, str.length - 3) + str.charAt(str.length - 2);

    // convert to upper case
    str = str.toUpperCase();

    // regular expression to check pattern and split
    var hkidPat = /^([A-Z]{1,2})([0-9]{6})([A0-9])$/;
    var matchArray = str.match(hkidPat);

    // not match, return false
    if (matchArray == null)
        return false;

    // the character part, numeric part and check digit part
    var charPart = matchArray[1];
    var numPart = matchArray[2];
    var checkDigit = matchArray[3];

    // calculate the checksum for character part
    var checkSum = 0;
    if (charPart.length == 2) {
        checkSum += 9 * (10 + strValidChars.indexOf(charPart.charAt(0)));
        checkSum += 8 * (10 + strValidChars.indexOf(charPart.charAt(1)));
    } else {
        checkSum += 9 * 36;
        checkSum += 8 * (10 + strValidChars.indexOf(charPart));
    }

    // calculate the checksum for numeric part
    for (var i = 0, j = 7; i < numPart.length; i++, j--)
        checkSum += j * numPart.charAt(i);

    // verify the check digit
    var remaining = checkSum % 11;
    var verify = remaining == 0 ? 0 : 11 - remaining;

    return verify == checkDigit || (verify == 10 && checkDigit == 'A');
}
//method for hiding options
$.fn.toggleOption = function (show) {
    if (show) {
        $(this).css('display', '');
        if ($(this).parent('span.toggleOption').length) {
            $(this).unwrap();
        }
        $(this).removeClass('disabled').prop('disabled', false).removeAttr('disabled');
    } else {
        $(this).css('display', 'none');
        if ($(this).parent('span.toggleOption').length == 0) {

            $(this).wrap('<span class="toggleOption" style="display: none;" />');
        }
        $(this).addClass('disabled').prop('disabled', true).attr('disabled', '');
    }
};

function disableCustomOption(holder, val) {
    var optionDt = holder.find('ul>li[data-val="' + val + '"]');
    var optionMb = holder.find('select>option[value="' + val + '"]');
    var optionSelect = holder.find('select');

    if (val == '#all#') {
        optionDt = holder.find('ul>li');
        optionMb = holder.find('select>option');
    }
    if (optionSelect.val() == val) {
        holder.find('ul>li[data-val=""]').click();
        optionSelect.val('');
    }
    optionDt.hide();
    optionMb.toggleOption(false);
}

function enableCustomOption(holder, val) {
    var optionDt = holder.find('ul>li[data-val="' + val + '"]');
    var optionMb = holder.find('select option[value="' + val + '"]');
    optionDt.show();
    optionMb.toggleOption(true);
}
*/


function checkEnvironment() {
    if ($body.hasClass('chrome') && !$body.hasClass('edge')) {
        isChrome = true;
    } else if ($body.hasClass('firefox')) {
        isFirefox = true;
    } else if ($body.hasClass('safari')) {
        isSafari = true;
    } else if ($body.hasClass('edge')) {
        isEdge = true;
    } else if ($body.hasClass('trident')) {
        isIE = true;
    }
    isIOSDevice = $body.hasClass('ios') && $body.hasClass('mobile');
    if ((('ontouchstart' in window) || (navigator.MaxTouchPoints > 0) || (navigator.msMaxTouchPoints > 0)) && (!isSafari)) {
        $body.addClass('isMSTouchDevice');
        isMSTouchDevice = true;
    }
    mobileDevice = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
}
//get sum
(function ($) {
    $.fn.addUp = function (getter) {
        return Array.prototype.reduce.call(this, function (a, b) {
            return a + getter.call($(b));
        }, 0);
    }
})(jQuery);

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
// ========================
// Forms Functions
// ========================
/* 
function checkFormValid(form, mode) {

    var isMiniForm = form.hasClass('form-mini');
    var errMsg = form.find('.err-msg').not('.err-msg-holder .err-msg');

    //Clone error message for appending
    var errMsgEmpty = cmErrMsgSamples.find('.err-msg--empty').clone();
    var errMsgTel = cmErrMsgSamples.find('.err-msg--tel').clone();
    var errMsgName = cmErrMsgSamples.find('.err-msg--name').clone();
    var errMsgEmail = cmErrMsgSamples.find('.err-msg--email').clone();
    var errMsgPw = cmErrMsgSamples.find('.err-msg--pw').clone();
    var errMsgPwConfirm = cmErrMsgSamples.find('.err-msg--pw-confirm').clone();
    var errMsgEmailConfirm = cmErrMsgSamples.find('.err-msg--email-confirm').clone();
    var errMsgTnc = cmErrMsgSamples.find('.err-msg--tnc').clone();


    var holderOnErr = form.find('.field--err'); //Find all the holder that's currently in error

    //Cache all the fields that need checking first
    var formCheckEmpty = form.find('.form-check--empty');
    var formCheckEmptyRadio = form.find('.form-check--empty-radio');
    var formCheckTel = form.find('.form-check--tel');
    var formCheckName = form.find('.form-check--name');
    var formCheckEmail = form.find('.form-check--email');
    var formCheckEmailConfirm = form.find('.form-check--email-confirm');
    var formCheckPw = form.find('.form-check--pw');
    var formCheckPwConfirm = form.find('.form-check--pw-confirm');
    var formCheckTnc = form.find('.form-check--tnc');

    if (isMiniForm) { //For mini form (e.g. login form), err-msg will display on top instead of below the field
    } else {
        errMsg.remove();
    }

    holderOnErr.removeClass('field--err'); //reset all "on-error fields"
    var formValidFlag = true; //flag to decide whether the form is valid (default = valid)


    //*** Checking starts ***//*
    formCheckEmpty.each(function () {
        var thisInput = $(this);
        var thisHolder = thisInput.parent();
        var thisCustomErrMsg = thisInput.data('err-msg');
        if (thisInput.hasClass('custom-select')) { //handle for custom select
            thisHolder = $(this);
            thisInput = thisHolder.find('select');
        } else if (thisInput.hasClass('custom-checkbox')) { //handle for custom checkbox
            thisHolder = $(this);
            thisInput = thisHolder.find('input');
        }
        var fieldAvailable = (!thisHolder.hasClass('disabled') && !thisInput.hasClass('disabled')) && thisInput.prop('disabled') == false;
        if (fieldAvailable && $.trim(thisInput.val()) == '') {
            if (thisCustomErrMsg) {
                var thisErr = cmErrMsgSamples.find('.' + thisCustomErrMsg).clone();
            } else {
                var thisErr = errMsgEmpty.clone();
            }
            thisHolder.prepend(thisErr);
            thisErr.fadeIn(300);
            thisHolder.addClass('field--err');
            formValidFlag = false;
        }
    });
    formCheckEmptyRadio.each(function () {
        var thisHolder = $(this);
        var thisInput = thisHolder.find('.custom-radio');
        if (!thisHolder.find('.custom-radio input:checked').length) {
            var thisErr = errMsgEmpty.clone();
            thisHolder.prepend(thisErr);
            thisErr.fadeIn(300);
            thisHolder.addClass('field--err');
            formValidFlag = false;
        }
    });

    formCheckName.each(function () {
        var thisInput = $(this);
        var thisHolder = thisInput.parent();
        if (!validateName(thisInput.val()) && !thisHolder.hasClass('field--err') && !thisHolder.hasClass('disabled') && !thisInput.hasClass('disabled')) {
            var thisErr = errMsgName.clone();
            thisHolder.prepend(thisErr);
            thisErr.fadeIn(300);
            thisHolder.addClass('field--err');
            formValidFlag = false;
        }
    });
    formCheckEmail.each(function () {
        var thisInput = $(this);
        var thisHolder = thisInput.parent();
        if (!validateEmail(thisInput.val()) && (!$.trim(thisInput.val()) == '') && !thisHolder.hasClass('field--err') && !thisHolder.hasClass('disabled') && !thisInput.hasClass('disabled')) {
            var thisErr = errMsgEmail.clone();
            thisHolder.prepend(thisErr);
            thisErr.fadeIn(300);
            thisHolder.addClass('field--err');
            formValidFlag = false;
        }
    });
    formCheckEmailConfirm.each(function () {
        var thisInput = $(this);
        var thisHolder = thisInput.parent();
        var confirmTarget = thisInput.data('confirm-target');
        if (!(thisInput.val() == $(confirmTarget).val()) && !thisHolder.hasClass('field--err') && !thisHolder.hasClass('disabled') && !thisInput.hasClass('disabled')) {
            var thisErr = errMsgEmailConfirm.clone();
            thisHolder.prepend(thisErr);
            thisErr.fadeIn(300);
            thisHolder.addClass('field--err');
            formValidFlag = false;
        }
    });

    formCheckTel.each(function () {
        var thisInput = $(this);
        var thisHolder = thisInput.parent();
        var thisNumLimit = thisInput.attr('maxlength');
        if (!validateTel(thisInput.val(), thisNumLimit) && !thisHolder.hasClass('field--err') && !thisHolder.hasClass('disabled') && !thisInput.hasClass('disabled')) {
            var thisErr = errMsgTel.clone();
            thisHolder.prepend(thisErr);
            thisErr.fadeIn(300);
            thisHolder.addClass('field--err');
            formValidFlag = false;
        }
    });
    formCheckPw.each(function () {
        var thisInput = $(this);
        var thisHolder = thisInput.parent();
        if (!validatePassword(thisInput.val()) && !thisHolder.hasClass('field--err') && !thisHolder.hasClass('disabled') && !thisInput.hasClass('disabled')) {
            var thisErr = errMsgPw.clone();
            thisHolder.prepend(thisErr);
            thisErr.fadeIn(300);
            thisHolder.addClass('field--err');
            formValidFlag = false;
        }
    });

    formCheckPwConfirm.each(function () {
        var thisInput = $(this);
        var thisHolder = thisInput.parent();
        var confirmTarget = thisInput.data('confirm-target');
        if (!(thisInput.val() == $(confirmTarget).val()) && !thisHolder.hasClass('field--err') && !thisHolder.hasClass('disabled') && !thisInput.hasClass('disabled')) {
            var thisErr = errMsgPwConfirm.clone();
            thisHolder.prepend(thisErr);
            thisErr.fadeIn(300);
            thisHolder.addClass('field--err');
            formValidFlag = false;
        }
    });

    formCheckTnc.each(function () {
        var thisInput = $(this);
        var thisHolder = thisInput.parent().parent();
        if (!(thisInput.find('input[type="checkbox"]:checked').length) && !thisHolder.hasClass('field--err') && !thisHolder.hasClass('disabled') && !thisInput.hasClass('disabled')) {
            var thisErr = errMsgTnc.clone();
            thisHolder.prepend(thisErr);
            thisErr.fadeIn(300);
            thisHolder.addClass('field--err');
            formValidFlag = false;
        }
    });

    return formValidFlag;
}*/

// ========================
// OGCIO Existing Functions
// ========================
function initialCookie() {
    var url = window.location.toString();
    var str = "";
    var arr = url.split("//");
    var arr1 = arr[1].split("/");
    var arr2 = arr1[1].split("/");
    var yoururl = arr1[0];
    var changeLang = arr2[0];
    if (changeLang != "") {
        if (window.name == '')
            window.name = 'isdWin';
        var cdomain = (location.domain) ? location.domain : null;
        set_cookie('language', changeLang, 2110, 12, 12, "/", cdomain);
    }
}
// getCookie
function get_cookie(cookie_name) {
    if (cookie_name == "font-size") {
        if (document.cookie.indexOf(cookie_name) < 0) {
            return null;
        } else {
            var startStr = document.cookie.indexOf(cookie_name) + cookie_name.length + 1;
            var endStr = document.cookie.indexOf(";", startStr);
            if (endStr == -1)
                endStr = document.cookie.length;
            return decodeURI(document.cookie.substring(startStr, endStr));
        }
    } else {
        var results = document.cookie.match('(^|;)?' + cookie_name + '=([^;]*)(;|$)');
        if (results) {
            return (unescape(results[2]));
        } else {
            return null;
        }
    }
}
;
// setCookie
function set_cookie(name, value, exp_y, exp_m, exp_d, path, domain, secure) {
    var cookie_string = name + "=" + escape(value);
    if (exp_y) {
        var expires = new Date(exp_y, exp_m, exp_d);
        cookie_string += "; expires=" + expires.toGMTString();

    }
    if (path)
        cookie_string += "; path=" + escape(path);
    if (domain)
        cookie_string += "; domain=" + escape(domain);
    if (secure)
        cookie_string += "; secure";
    document.cookie = cookie_string;
}
;
// href load external links
function externallinks() {
    if (!document.getElementsByTagName)
        return;
    var anchors = document.getElementsByTagName("a");
    for (var i = 0; i < anchors.length; i++) {
        var anchor = anchors[i];
        if (anchor.getAttribute("href") &&
                anchor.getAttribute("rel") == "external")
            anchor.target = "_blank";
    }
}
;
window.onload = externallinks;
//Date Formatting
function formatDate(oldDate) {
    var temp = "";
    if (oldDate != null && oldDate != "") {
        temp = oldDate.split("/");
    }
    var dateStr = "";
    if (temp.length = 3) {
        dateStr = temp[1] + "/" + temp[0] + "/" + temp[2];
    }
    return new Date(dateStr);
}

// ShowWCAG
function initShowWCAG() {
	if($("#showWCAG").val() != undefined && $("#showWCAG").val() == 'N') {
		$("#wcagLogo").hide();
	} else {
		$("#wcagLogo").show();
	}
}

// RevisionDate
function initRevisionDate() {
    var url = window.top.location.href;
    if (url.indexOf("/tc/") != -1) {
        var dateArticle = formatDate($("#lastRevisionDate").val());
        var dateHighlight = formatDate($("#highlightRevisionDate").text());
        var dateFooter = formatDate($("#revisionDate").text());
        var rDate = "";
        var monthWord = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
        var monthWordCh = new Array("1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12");
        var dateStr = "";
        if ($("#highlightRevisionDate").val() !== undefined) {
            if (dateArticle >= dateHighlight && dateArticle >= dateFooter) {
                rDate = dateArticle;
                $('#dateRevision').show();
            } else if (dateFooter > dateHighlight && dateFooter > dateArticle) {
                rDate = dateFooter;
                $('#dateReview').show();
            } else if (dateFooter > dateHighlight) {
                rDate = dateFooter;
                $('#dateReview').show();
            } else if (dateArticle > dateHighlight) {
                rDate = dateArticle;
                $('#dateRevision').show();
            } else {
                rDate = dateHighlight;
                $('#dateRevision').show();
            }
        } else {
            if (dateArticle > dateFooter) {
                rDate = dateArticle;
                $('#dateRevision').show();
            } else {
                rDate = dateFooter;
                $('#dateReview').show();
            }
        }
        if (dateFooter == "Invalid Date" && dateArticle == "Invalid Date" && dateHighlight == "Invalid Date") {
            rDate = new Date();
        }
        dateStr = rDate.getFullYear() + "年" + monthWordCh[rDate.getMonth()] + "月" + rDate.getDate() + "日";
        $("#revisionDate").text(dateStr);
    } else if (url.indexOf("/sc/") != -1) {
        var dateArticle = formatDate($("#lastRevisionDate").val());
        var dateHighlight = formatDate($("#highlightRevisionDate").text());
        var dateFooter = formatDate($("#revisionDate").text());
        var rDate = "";
        var monthWord = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
        var monthWordCh = new Array("1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12");
        var dateStr = "";
        if ($("#highlightRevisionDate").val() !== undefined) {
            if (dateArticle >= dateHighlight && dateArticle >= dateFooter) {
                rDate = dateArticle;
                $('#dateRevision').show();
            } else if (dateFooter > dateHighlight && dateFooter > dateArticle) {
                rDate = dateFooter;
                $('#dateReview').show();
            } else if (dateFooter > dateHighlight) {
                rDate = dateFooter;
                $('#dateReview').show();
            } else if (dateArticle > dateHighlight) {
                rDate = dateArticle;
                $('#dateRevision').show();
            } else {
                rDate = dateHighlight;
                $('#dateRevision').show();
            }
        } else {
            if (dateArticle > dateFooter) {
                rDate = dateArticle;
                $('#dateRevision').show();
            } else {
                rDate = dateFooter;
                $('#dateReview').show();
            }
        }
        if (dateFooter == "Invalid Date" && dateArticle == "Invalid Date" && dateHighlight == "Invalid Date") {
            rDate = new Date();
        }
        dateStr = rDate.getFullYear() + "年" + monthWordCh[rDate.getMonth()] + "月" + rDate.getDate() + "日";
        $("#revisionDate").text(dateStr);
    } else {
        var dateArticle = formatDate($("#lastRevisionDate").val());
        var dateHighlight = formatDate($("#highlightRevisionDate").text());
        var dateFooter = formatDate($("#revisionDate").text());
        var rDate = "";
        var monthWord = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
        var monthWordCh = new Array("1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12");
        var dateStr = "";
        if ($("#highlightRevisionDate").val() !== undefined) {
            if (dateArticle >= dateHighlight && dateArticle >= dateFooter) {
                rDate = dateArticle;
                $('#dateRevision').show();
            } else if (dateFooter > dateHighlight && dateFooter > dateArticle) {
                rDate = dateFooter;
                $('#dateReview').show();
            } else if (dateFooter > dateHighlight) {
                rDate = dateFooter;
                $('#dateReview').show();
            } else if (dateArticle > dateHighlight) {
                rDate = dateArticle;
                $('#dateRevision').show();
            } else {
                rDate = dateHighlight;
                $('#dateRevision').show();
            }
        } else {
            if (dateArticle > dateFooter) {
                rDate = dateArticle;
                $('#dateRevision').show();
            } else {
                rDate = dateFooter;
                $('#dateReview').show();
            }
        }
        if (dateFooter == "Invalid Date" && dateArticle == "Invalid Date" && dateHighlight == "Invalid Date") {
            rDate = new Date();
        }
        dateStr = rDate.getDate() + " " + monthWord[rDate.getMonth()] + " " + rDate.getFullYear();
        $("#revisionDate").text(dateStr);
    }
}
// YearSelector
function initYearSelector() {
    var date = new Date();
    var year = date.getFullYear();
    var startYear = $("#startYear").val();
    if (val > 0) {
        $("#jumpMenu").attr("value", val);
    }
    for (var i = year; i >= startYear; i--) {
        $("#jumpMenu").append("<option value='" + i + "'>" + i + "</option>");
    }
    var url = window.top.location.href;
    var index = url.indexOf('./index.html');
    var val = url.substring(index - 4, index);
    $('#jumpMenu').change(function () {
        var v = $('#jumpMenu').val();
        if (url.substring(index - 5, index - 4) == "/") {
            url = './../' + url.substring(0, index - 4) + v + '/index.html';

        } else {
            url = './../' + url.substring(0, index + 1) + v + '/index.html';
        }
        if (v == year) {
            if (url.substring(index - 5, index - 4) == "/") {
                url = './../' + url.substring(0, index - 4) + v + '/index.html';
            } else {
                url = './../' + url.substring(0, index - 4) + v + '/index.html';
            }
        }
        if (checkURLexist(url)) {
            //window.top.location.href = url;
        } else {
            if (url.indexOf('/en/') >= 0) {
                alert('Sorry, page not found');
            }
            if (url.indexOf('/sc/') >= 0) {
                alert('抱歉, 您访问的页面不存在！');
            }
            if (url.indexOf('/tc/') >= 0) {
                alert('抱歉，您訪問的頁面不存在！');
            }
        }
    }
    );
}
// CheckURL Exists
function checkURLexist(targetFile) {
    targetFile = targetFile.replace('#top', '');
    if (window.XMLHttpRequest) {
        xhttp = new XMLHttpRequest();
        xhttp.open("POST", targetFile, false);
        xhttp.send(null);
        if (xhttp.status == 200) { /* the request has been returned */
            return true;
		} else if (xhttp.status == 403){
			return true;	
        } else {
            return false
        }
    } else {
        xhttp = new ActiveXObject("Microsoft.XMLHTTP");
        xhttp.open("GET", targetFile, false);
        xhttp.send(null);
        if (xhttp.status == 200) { /* the request has been returned */
            return true;
		} else if (xhttp.status == 403){
			return true;	
        } else {
            return false
        }
    }
    return true;
};
// toggle ajax error
var ajaxErr = true;
$(function () {
  var xhr = null;

  if (window.XMLHttpRequest) {
      xhr = window.XMLHttpRequest;
  }
  else if (window.ActiveXObject('Microsoft.XMLHTTP')) {
      xhr = window.ActiveXObject('Microsoft.XMLHTTP');
  }

  var send = xhr.prototype.send;
  xhr.prototype.send = function (data) {
      try {
          if(ajaxErr){
            send.call(this, data);
          }
      }
      catch (e) {
      }
  };
});
// Forms Validation 
String.prototype.trim = function () {
    return this.replace(/^\s+|\s+$/g, "");
};
String.prototype.ltrim = function () {
    return this.replace(/^\s+/, "");
};
String.prototype.rtrim = function () {
    return this.replace(/\s+$/, "");
};

function onKey_corrAdd(textarea) {
    if (textarea.value.length > 240) {
        textarea.value = textarea.value.substr(0, 240);
        return false;
    }
    return true;
}
;

function form_input_is_numeric(aStr) {
    var returnVal;
    var totalPattern;
    totalPattern = /^[0-9 ]+$/i;
    var result = totalPattern.exec(aStr);
    if (result == null && aStr != "") {
        returnVal = false;
    } else {
        returnVal = true;
    }
    return returnVal;
}
;

function checkIfValidJumbleOfEmails(aStr) {
    var returnVal;
    var totalPattern;
    totalPattern = /^((([^\s\100]+)\100([a-z0-9]([-a-z0-9]*[a-z0-9])?\.)+((a[cdefgilmnoqrstuwxz]|aero|arpa|asia)|(b[abdefghijmnorstvwyz]|biz)|(c[acdfghiklmnorsuvxyz]|cat|com|coop)|d[ejkmoz]|(e[ceghrstu]|edu)|f[ijkmor]|(g[abdefghilmnpqrstuwy]|gov)|h[kmnrtu]|(i[delmnoqrst]|info|int)|(j[emop]|jobs)|k[eghimnprwyz]|l[abcikrstuvy]|(m[acdghklmnopqrstuvwxyz]|mil|mobi|museum)|(n[acefgilopruz]|name|net)|(om|org)|(p[aefghklmnrstwy]|pro|post)|qa|r[eouw]|s[abcdeghijklmnortvyz]|(t[cdfghjklmnoprtvwz]|travel|tel)|u[agkmsyz]|v[aceginu]|w[fs]|y[etu]|z[amw]))|((-*\w+\'*)(\s*)(-*\w*\'*)(\s\w+)*(\s*)\074([^\s\100]+)\100([a-z0-9]([-a-z0-9]*[a-z0-9])?\.)+((a[cdefgilmnoqrstuwxz]|aero|arpa|asia)|(b[abdefghijmnorstvwyz]|biz)|(c[acdfghiklmnorsuvxyz]|cat|com|coop)|d[ejkmoz]|(e[ceghrstu]|edu)|f[ijkmor]|(g[abdefghilmnpqrstuwy]|gov)|h[kmnrtu]|(i[delmnoqrst]|info|int)|(j[emop]|jobs)|k[eghimnprwyz]|l[abcikrstuvy]|(m[acdghklmnopqrstuvwxyz]|mil|mobi|museum)|(n[acefgilopruz]|name|net)|(om|org)|(p[aefghklmnrstwy]|pro|post)|qa|r[eouw]|s[abcdeghijklmnortvyz]|(t[cdfghjklmnoprtvwz]|travel|tel)|u[agkmsyz]|v[aceginu]|w[fs]|y[etu]|z[amw])\076))(\s*)(([\054\073]|[\054\073]\s|\s)(\s*)((([^\s\100]+)\100([a-z0-9]([-a-z0-9]*[a-z0-9])?\.)+((a[cdefgilmnoqrstuwxz]|aero|arpa|asia)|(b[abdefghijmnorstvwyz]|biz)|(c[acdfghiklmnorsuvxyz]|cat|com|coop)|d[ejkmoz]|(e[ceghrstu]|edu)|f[ijkmor]|(g[abdefghilmnpqrstuwy]|gov)|h[kmnrtu]|(i[delmnoqrst]|info|int)|(j[emop]|jobs)|k[eghimnprwyz]|l[abcikrstuvy]|(m[acdghklmnopqrstuvwxyz]|mil|mobi|museum)|(n[acefgilopruz]|name|net)|(om|org)|(p[aefghklmnrstwy]|pro|post)|qa|r[eouw]|s[abcdeghijklmnortvyz]|(t[cdfghjklmnoprtvwz]|travel|tel)|u[agkmsyz]|v[aceginu]|w[fs]|y[etu]|z[amw]))|((-*\w+\'*)(\s*)(-*\w*\'*)(\s\w+)*(\s*)\074([^\s\100]+)\100([a-z0-9]([-a-z0-9]*[a-z0-9])?\.)+((a[cdefgilmnoqrstuwxz]|aero|arpa|asia)|(b[abdefghijmnorstvwyz]|biz)|(c[acdfghiklmnorsuvxyz]|cat|com|coop)|d[ejkmoz]|(e[ceghrstu]|edu)|f[ijkmor]|(g[abdefghilmnpqrstuwy]|gov)|h[kmnrtu]|(i[delmnoqrst]|info|int)|(j[emop]|jobs)|k[eghimnprwyz]|l[abcikrstuvy]|(m[acdghklmnopqrstuvwxyz]|mil|mobi|museum)|(n[acefgilopruz]|name|net)|(om|org)|(p[aefghklmnrstwy]|pro|post)|qa|r[eouw]|s[abcdeghijklmnortvyz]|(t[cdfghjklmnoprtvwz]|travel|tel)|u[agkmsyz]|v[aceginu]|w[fs]|y[etu]|z[amw])\076))(\s*))*$/i;
    var result = totalPattern.exec(aStr);
    if (result == null && aStr != '') {
        returnVal = false;
    } else {
        returnVal = true;
    }
    return returnVal;
}
;

function checkIfValidJumbleOfEmailsAndroid(aStr) {
    var returnVal;
    var totalPattern;
    totalPattern = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
    var result = totalPattern.exec(aStr);
    if (result == null && aStr != '') {
        returnVal = false;
    } else {
        returnVal = true;
    }
    return returnVal;
}
;

function isChinese(str) {
    var re1 = new RegExp("[\u4E00-\uFA29]+");
    var re2 = new RegExp("[\uE7C7-\uE7F3]+");
    if ((re1.test(str) && (!re2.test(str)))) {
        return true;
    }
    return false;
}
;
// InitPhotoGallery
function initPhotoGallery() {
    $('.royalSlider').royalSlider({
        controlNavigation: 'none',
        autoScaleSlider: true,
        autoScaleSliderWidth: 575,
        autoScaleSliderHeight: 315,
        loop: true,
        imageScaleMode: 'fit',
        sliderDrag: false,
        navigateByClick: false,
        numImagesToPreload: 3,
        arrowsNav: true,
        arrowsNavAutoHide: true,
        arrowsNavHideOnTouch: true,
        keyboardNavEnabled: false,
        fadeinLoadedSlide: true,
        globalCaption: true,
        globalCaptionInside: true
    });
    var items = [];
    var slider = $('.royalSlider').data('royalSlider');
    $.each(slider.slides, function (index, value) {
        items.push({
            src: value.image,
            captionText: value.image.titleSrc
        });
    })
    $(".rsGCaption").appendTo(".captionContainer");

    var htmlContent = (slider.currSlideId + 1) + " / " + slider.numSlides;
    $(".slideNumber").text(htmlContent);

    slider.ev.on('rsAfterSlideChange', function (event) {
        htmlContent = (slider.currSlideId + 1) + " / " + slider.numSlides;
        $(".slideNumber").text(htmlContent);
    });
    slider.ev.on("rsSlideClick", function (n) {
        $.magnificPopup.open({
            preloader: true,
            items: items,
            type: 'image',
            removalDelay: 300,
            mainClass: 'mfp-fade',
            gallery: {
                enabled: true
            },
            image: {
                titleSrc: function (item) {
                    var captionText = $(".royalSlider").data("royalSlider").currSlide.caption.html();
                    return  '<p>' + captionText + '</p>';
                }
            },
            autoFocusLast: true
        }, slider.currSlideId);
    });
    $.magnificPopup.instance.next = function () {
        slider.next();
        $.magnificPopup.proto.next.call(this);
    }
    $.magnificPopup.instance.prev = function () {
        slider.prev();
        $.magnificPopup.proto.prev.call(this);
    }
    $('#slider-next').click(function () {
        slider.next();
    });
    $('#slider-prev').click(function () {
        slider.prev();
    });
}
// initStudentCornerGallery
function initStudentCornerGallery() {
    var groups = {};
    $('.galleryItem').each(function () {
        var id = parseInt($(this).attr('data-group'), 10);
        if (!groups[id]) {
            groups[id] = [];
        }
        groups[id].push(this);
    });
    $.each(groups, function () {
        $(this).magnificPopup({
            type: 'image',
            image: {
                titleSrc: 'title'
            },
            closeOnContentClick: true,
            closeBtnInside: false,
            gallery: {enabled: true}
        })
    });
}
// initTopBanner
function initTopBanner() {

    var link = window.top.location.href;

    if ($('[data-section="about-us"]').length > 0) {
        $('.static-banner__bg').css('background-image', 'url(/images/topbanner/about_us_topbanner.jpg)');
        if (link.indexOf('/en/') >= 0) {
            $('.static-banner__title').html("About Us");
        } else if (link.indexOf('/tc/') >= 0) {
            $('.static-banner__title').html("關於我們");
        } else {
            $('.static-banner__title').html("关于我们");
        }
    } else if ($('[data-section="our-work"]').length > 0) {
        $('.static-banner__bg').css('background-image', 'url(/images/topbanner/our_work_topbanner.jpg)');
        if (link.indexOf('/en/') >= 0) {
            $('.static-banner__title').html("Our Work");
        } else if (link.indexOf('/tc/') >= 0) {
            $('.static-banner__title').html("我們的工作");
        } else {
            $('.static-banner__title').html("我们的工作");
        }
    } else if ($('[data-section="news"]').length > 0) {
        $('.static-banner__bg').css('background-image', 'url(/images/topbanner/news_topbanner.jpg)');
        if (link.indexOf('/en/') >= 0) {
            $('.static-banner__title').html("What's New");
        } else if (link.indexOf('/tc/') >= 0) {
            $('.static-banner__title').html("最新消息");
        } else {
            $('.static-banner__title').html("最新消息");
        }
    } else if ($('[data-section="service-desk"]').length > 0) {
        $('.static-banner__bg').css('background-image', 'url(/images/topbanner/service_desk_topbanner.jpg)');
        if (link.indexOf('/en/') >= 0) {
            $('.static-banner__title').html("Service Desk");
        } else if (link.indexOf('/tc/') >= 0) {
            $('.static-banner__title').html("服務台");
        } else {
            $('.static-banner__title').html("服务台");
        }
    } else if ($('[data-section="contact-us"]').length > 0) {
        $('.static-banner__bg').css('background-image', 'url(/images/topbanner/contact_us_topbanner.jpg)');
        if (link.indexOf('/en/') >= 0) {
            $('.static-banner__title').html("Contact Us");
        } else if (link.indexOf('/tc/') >= 0) {
            $('.static-banner__title').html("聯絡我們");
        } else {
            $('.static-banner__title').html("联络我们");
        }
    } else if ($('[data-section="strategies-government-and-it-initiatives"]').length > 0) {
        $('.static-banner__bg').css('background-image', 'url(/images/topbanner/strategies_government_and_it_initiatives_topbanner.jpg)');
        if (link.indexOf('/en/') >= 0) {
            $('.static-banner__title').html("Strategies & Government IT Initiatives");
        } else if (link.indexOf('/tc/') >= 0) {
            $('.static-banner__title').html("策略與政府資訊科技措施");
        } else {
            $('.static-banner__title').html("策略与政府资讯科技措施");
        }
    } else if ($('[data-section="legal-framework-and-domain-name-administration"]').length > 0) {
        $('.static-banner__bg').css('background-image', 'url(/images/topbanner/legal_framework_and_domain_name_administration_topbanner.jpg)');
        if (link.indexOf('/en/') >= 0) {
            $('.static-banner__title').html("Legal Framework & Domain Name Administration");
        } else if (link.indexOf('/tc/') >= 0) {
            $('.static-banner__title').html("法律架構及互聯網域名管理");
        } else {
            $('.static-banner__title').html("法律架构及互联网域名管理");
        }
    } else if ($('[data-section="community-initiatives-and-it-services"]').length > 0) {
        $('.static-banner__bg').css('background-image', 'url(/images/topbanner/community_initiatives_and_it_services_topbanner.jpg)');
        if (link.indexOf('/en/') >= 0) {
            $('.static-banner__title').html("Community Initiatives & IT Services");
        } else if (link.indexOf('/tc/') >= 0) {
            $('.static-banner__title').html("社區措施和資訊科技服務");
        } else {
            $('.static-banner__title').html("社区措施和资讯科技服务");
        }
    } else if ($('[data-section="business-and-indusstry-development"]').length > 0) {
        $('.static-banner__bg').css('background-image', 'url(/images/topbanner/business_and_indusstry_development_topbanner.jpg)');
        if (link.indexOf('/en/') >= 0) {
            $('.static-banner__title').html("Business & Industry Development");
        } else if (link.indexOf('/tc/') >= 0) {
            $('.static-banner__title').html("業務及產業發展");
        } else {
            $('.static-banner__title').html("业务及产业发展");
        }
    } else if ($('[data-section="it-infrastructure-and-standards"]').length > 0) {
        $('.static-banner__bg').css('background-image', 'url(/images/topbanner/it_infrastructure_and_standards_topbanner.jpg)');
        if (link.indexOf('/en/') >= 0) {
            $('.static-banner__title').html("IT Infrastructure & Standards");
        } else if (link.indexOf('/tc/') >= 0) {
            $('.static-banner__title').html("資訊科技基建及標準");
        } else {
            $('.static-banner__title').html("资讯科技基建及标准");
        }
    } else if ($('[data-section="information-security"]').length > 0) {
        $('.static-banner__bg').css('background-image', 'url(/images/topbanner/information_security_topbanner.jpg)');
        if (link.indexOf('/en/') >= 0) {
            $('.static-banner__title').html("Information & Cyber Security");
        } else if (link.indexOf('/tc/') >= 0) {
            $('.static-banner__title').html("資訊及網絡安全");
        } else {
            $('.static-banner__title').html("资讯及网络安全");
        }
    } else {
        $('.static-banner__bg').css('background-image', 'url(/images/topbanner/about_us_topbanner.jpg)');
        if (link.indexOf('/en/') >= 0) {
            $('.static-banner__title').html("About Us");
        } else if (link.indexOf('/tc/') >= 0) {
            $('.static-banner__title').html("關於我們");
        } else {
            $('.static-banner__title').html("关于我们");
        }
    }
}

function showKeyBoard() {
    $('.keyboardShortCutOpen').click(function () {
        $('.keyboardShortCutOpen').css('display', 'none');
        $('.keyboardShortCutClose').css('display', 'inline-block');
        $('.videoTable').css('display', 'inline-table');
    });
    $('.keyboardShortCutClose').click(function () {
        $('.keyboardShortCutOpen').css('display', 'inline-block');
        $('.keyboardShortCutClose').css('display', 'none');
        $('.videoTable').css('display', 'none');
    });
}
var today = new Date();
var thisYear = today.getFullYear();
String.prototype.trim = function () {
    return this.replace("/^\s+|\s+$/g", "");
}
function initDate_article_list() {
    var today = new Date();
    var thisYear = today.getFullYear();
    for (var i = thisYear; i > thisYear - 8; i--) {
        var yStr = "<option name='year' id='year' value='" + i + "'>";
				
		if(page_lang == "en"){
			yStr += "Year " + i;
		}else if(page_lang == "tc"){
			yStr += i + "年";
		}else if(page_lang == "sc"){
			yStr += i + "年";
		}

		yStr += "<\/option>"
        $('#jumpMenu').append(yStr);
    }
}
function initDate_features_list() {
    var today = new Date();
    var thisYear = today.getFullYear();
    for (var i = thisYear; i > thisYear - 5; i--) {
        var yStr = "<option name='year' id='year' value='" + i + "'>";
				
		if(page_lang == "en"){
			yStr += "Year " + i;
		}else if(page_lang == "tc"){
			yStr += i + "年";
		}else if(page_lang == "sc"){
			yStr += i + "年";
		}

		yStr += "<\/option>";
		
        $('#jumpMenu').append(yStr);
    }
}
function initDate_what_news() {
    var today = new Date();
    var thisYear = today.getFullYear();
    for (var i = thisYear; i > thisYear; i--) {
        var yStr = "<option name='year' id='year' value='" + i + "'>";
				
		if(page_lang == "en"){
			yStr += "Year " + i;
		}else if(page_lang == "tc"){
			yStr += i + "年";
		}else if(page_lang == "sc"){
			yStr += i + "年";
		}

		yStr += "<\/option>";
        $('#jumpMenu').append(yStr);
    }
}
function getDataEN(thisYear) {
    var posting = $.post("getData.php?sid=" + Math.random(), {year: thisYear});
    posting.done(function (data) {
		$("#eventList").show();
		$("#div_event").html(data);
		if(data.indexOf("id='cell-nodata'") > -1){
			$(".pagerTable").hide();
		}else{
			$(".pagerTable").show();
			pageNav_EN();
		}
    });
}
;
function getDataTC(thisYear) {
    var posting = $.post("getData.php?sid=" + Math.random(), {year: thisYear});
    posting.done(function (data) {
        $("#eventList").show();
		$("#div_event").html(data);
		if(data.indexOf("id='cell-nodata'") > -1){
			$(".pagerTable").hide();
		}else{
			$(".pagerTable").show();
			pageNav_TC();
		}
    });
}
;
function getDataSC(thisYear) {
    var posting = $.post("getData.php?sid=" + Math.random(), {year: thisYear});
    posting.done(function (data) {
		$("#eventList").show();
		$("#div_event").html(data);
		if(data.indexOf("id='cell-nodata'") > -1){
			$(".pagerTable").hide();
		}else{
			$(".pagerTable").show();
			pageNav_SC();
		}
    });
}
;
function pageNav_EN() {
	$("#totalNum").html("Total No. of Records : " + ($("#div_event tr").size()-1)).show();
}
function pageNav_TC() {
    $("#totalNum").html("總數 : " +($("#div_event tr").size()-1)).show();
}
function pageNav_SC() {
    $("#totalNum").html("总数 : " +($("#div_event tr").size()-1)).show();
}
function langGetter() {
    if (window.location.href.indexOf('/en/') >= 0) {
        page_lang = "en";
    }
    if (window.location.href.indexOf('/tc/') >= 0) {
        page_lang = "tc";
    }
    if (window.location.href.indexOf('/sc/') >= 0) {
        page_lang = "sc";
    }
}
//Save & print
function onSave(formName){
	      formName.action='save.php';
		  var options = { 
						success: showResponse,  // post-submit callback 
						type: 'post'
				}; 
		  $("#saveBtn").ajaxSubmit(options);
}
function showResponse(responseText, statusText, xhr, $form) { 
	window.open('savepage.html', 'popup', 'width=1024,height=768,scrollbars=yes,resizable=yes');
}

//event search
function initDate() { 
                var today=new Date();
                var thisYear = today.getFullYear();         
                for(var i=1; i<=12;i++){
                    var mStr = "<option value='"+i+"'>"+i+"<\/option>"
                    $('#s_month_to').append(mStr);
                    $('#s_month_from').append(mStr);
                }		
                for(var i=thisYear-1; i<=thisYear+3;i++){
                    var yStr = "<option value='"+i+"'>"+i+"<\/option>"
                    $('#s_year_to').append(yStr);
                    $('#s_year_from').append(yStr);
                }	
}		

function prePage(in_prepageno){
	var itemperpage = 6;
	var start_item = in_prepageno * itemperpage;
	
	var sDate =  $('#searched_s_date').val();
	var eDate =  $('#searched_e_date').val();
	var keywords = $('#searched_keyword').val();
	var keywordType = $('#searched_keywordtype').val();
	var eventType = $('#searched_eventType').val();
	var searchType = $('#searched_searchtype').val();

	//console.log($("#s_event").val() + "|\n" + sDate + "|\n" + eDate + "|\n" + "" + "|\n" + searchType + "|\n" + keywordType + "|\n" + eventType);
	
	
	
	ajaxEventEN($("#s_event").val(),sDate,eDate,"",searchType,keywordType,eventType, start_item);
	
	
}

function nextPage(in_nextpageno){
	var itemperpage = 6;
	var start_item = in_nextpageno * itemperpage;
	//console.log(start_item);
	
	var sDate =  $('#searched_s_date').val();
	var eDate =  $('#searched_e_date').val();
	var keywords = $('#searched_keyword').val();
	var keywordType = $('#searched_keywordtype').val();
	var eventType = $('#searched_eventType').val();
	var searchType = $('#searched_searchtype').val();
	//console.log("===>"+searchType)
	//console.log($("#s_event").val() + "|\n" + sDate + "|\n" + eDate + "|\n" + "" + "|\n" + searchType + "|\n" + keywordType + "|\n" + eventType);
	
	
	
	ajaxEventEN($("#s_event").val(),sDate,eDate,"",searchType,keywordType,eventType, start_item);
	
	
}

function ajaxEventEN(keyStr,startDateStr,endDateStr, theDate, searchType, keywordType, eventType, s_item){
	var itemperpage = 6;
	//console.log("ajaxEventEN");
	//console.log("--->"+searchType);
	$.ajax({
		url: "/"+ page_lang +"/our_work/business/industry_support/event_calendar/getEvent.php?sid="+Math.random(),
		type: "POST",
		data:  { isMobile:"Y", key: keyStr, startDate: startDateStr, endDate: endDateStr, theDate:theDate, searchType: searchType, keywordType:keywordType, eventType:eventType, start_item:s_item, itemperpage : itemperpage},
		//beforeSend: function(){$("#overlay").show();},
		success: function(data){
			//console.log(data);
			//$("#div_event").html(data);
			//setInterval(function() {$("#overlay").hide(); },500);
			$("#eventList").html(data);
			
			$('html,body').animate({scrollTop: $('.event-search__result').offset().top }, 'slow');
		},
		error: function() 
		{} 	        
   });
}

function getEventEN(keyStr,startDateStr,endDateStr, theDate, searchType, keywordType, eventType){
	var itemperpage = 6;
    $.post("/en/our_work/business/industry_support/event_calendar/getEvent.php?sid="+Math.random(), { isMobile:"Y", key: keyStr, startDate: startDateStr, endDate: endDateStr, theDate:theDate, searchType: searchType, keywordType:keywordType, eventType:eventType, start_item:0, itemperpage : itemperpage},
                function (data, textStatus){
                  
                        $("#noData").hide();	
                        $("#eventList").show();
                      
						$("#eventList").html(data);
						
                });
 }			
  
function goTodayEN(){				
				/*
				var today = new Date();			

				var lastDayOfMonth = new Date(today.getFullYear(), today.getMonth()+1, 0);
				$('#s_date').val( (today.getMonth()+1) + "/"+today.getDate()+"/"+today.getFullYear());
				$('#e_date').val( (today.getMonth()+1) + "/"+lastDayOfMonth.getDate()+"/"+today.getFullYear());*/
				
				
				var s_date = new Date();
				s_date = new Date(s_date.getFullYear(), s_date.getMonth()-3, 1);
				var e_date = new Date();
				e_date = new Date(e_date.getFullYear(), e_date.getMonth()+4, 0);
				
				$('#s_date').val( s_date.getDate() + "/"+(s_date.getMonth()+1)+"/"+s_date.getFullYear());
				$('#e_date').val( e_date.getDate() + "/"+(e_date.getMonth()+1)+"/"+e_date.getFullYear());
				
				searchEN();
				
				
}		
function searchEN(){
				
                //var sMon = $('#s_month_from').val();
                //var sYear = $('#s_year_from').val();
                //var eMon = $('#s_month_to').val();
                //var eYear = $('#s_year_to').val();
				
				
                var keywords = $('#s_event').val();
				var keywordType = $('#keywordType').val();
				var eventType = $('#eventType').val();
				var tmp_sdate = $('#s_date').val();
				var tmp_edate = $('#e_date').val();
				
				//console.log(sMon + "|\n" + sYear+ "|\n" + eMon+ "|\n" +eYear+ "|\n" +keywords+ "|\n" +keywordType+ "|\n" +eventType);
				
                var errMsg="";
                /*
				if((keywords==="Enter your search word(s)" || keywords.trim().length===0) 
					&& (tmp_sdate==="false" || tmp_edate==="false")){
                    errMsg+="Please enter event keyword or select date.<br>";				
                }*/
				if(( !keywords || keywords.trim().length===0) 
					&& (tmp_sdate == ""|| tmp_edate == "")){
                    errMsg+="Please enter event keyword or select date.<br>";				
                }
				
				
				//console.log("sdate"+tmp_sdate);
				//console.log("edate"+tmp_edate);
				if( tmp_sdate != '' || tmp_edate != '' ){
					if(tmp_sdate == ""){
						//console.log("\n\nNO sdate\n\n");
						errMsg+="Please select event start date.<br>";
					}
					
					if(tmp_edate == ""){
						//console.log("\n\nNO edate\n\n");
						errMsg+="Please select event end date.<br>";
					}
					
					var ts = new Date(tmp_sdate);
					var te = new Date(tmp_edate);
					//console.log("check date-->" + ts + "|" + te);
					if( ts > te ){
						errMsg+="Event start date must be earlier than event end date.<br>";
					}
				}
				
				/*
				if( tmp_sdate!=="false" || tmp_edate!=="false"){
					
					
					if(tmp_sdate==="false"){
						console.log("\n\nNO sdate\n\n");
						errMsg+="Please select event start date.<br>";
					}
					
					if(tmp_edate==="false"){
						console.log("\n\nNO edate\n\n");
						errMsg+="Please select event end date.<br>";
					}
					
					var ts = new Date(tmp_sdate);
					var te = new Date(tmp_edate);
					console.log("check date-->" + ts + "|" + te);
					if( ts > te ){
						errMsg+="Event start date must be earlier than event end date.<br>";
					}
										
					//if(Number(sMon)>Number(eMon) && sYear>=eYear){
						//errMsg+="Event start month must be earlier than event end month at the same year.<br>";
				//	}
					
				}*/
                if(errMsg!==""){
					$("#errMsg").html(errMsg).show();
					//alert(errMsg);
                    return false;
                }else{
					$("#errMsg").hide();
				}
				
				
				var sDate = tmp_sdate.split("/")[2] + "-" + tmp_sdate.split("/")[1] + "-" + tmp_sdate.split("/")[0];
				var eDate = tmp_edate.split("/")[2] + "-" + tmp_edate.split("/")[1] + "-" + tmp_edate.split("/")[0];
				
                //var sDate = sYear+"-"+sMon+"-01";
				//var eDate = eYear+"-"+eMon+"-31";

				//console.log(tmp_sdate + " | " + tmp_sdate);
				
				var searchType="";
				//if(!(keywords==="Enter your search word(s)" || keywords.trim().length===0)){
				if(!( typeof keywords == 'undefined' || keywords.trim().length===0)){
					searchType="k";
				}
				//if(sMon!=="false"){
				if(!( typeof tmp_sdate == 'undefined' || tmp_sdate.trim().length===0)){
					searchType+="d";
				}
				
				//if( typeof keywordType == 'undefined' )
				//	keywordType = "name";
				//console.log($("#s_event").val() + "|\n" + sDate + "|\n" + eDate + "|\n" + "" + "|\n" + searchType + "|\n" + keywordType + "|\n" + eventType);
                
				//ajaxEventEN($("#s_event").val(),sDate,eDate,"",searchType,keywordType,eventType, 0);
				getEventEN($("#s_event").val(),sDate,eDate,"",searchType,keywordType,eventType);		
}


function listEventEN(){
//console.log("hihihihiiih");	
				var sMon = Number($('#s_month_from').val());
                var sYear = Number($('#s_year_from').val());
                var eMon = Number($('#s_month_to').val());
                var eYear = Number($('#s_year_to').val());
				var sDate;
				var eDate;			
				if(sYear>0){
					sDate = sYear+"-"+sMon+"-01";
					eDate = eYear+"-"+eMon+"-31";				
				}else{
					var today = new Date();
					sDate = today.getFullYear()+"-"+(today.getMonth()+1)+"-01";
					eDate = today.getFullYear()+"-"+(today.getMonth()+1)+"-31";					
				}
                getEventEN($("#s_event").val(),sDate,eDate,"","d","name","false");
}


function getEventTC(keyStr,startDateStr,endDateStr, theDate, searchType, keywordType, eventType){
                //console.log("get event tc");
				var itemperpage = 6;
				$.post("/tc/our_work/business/industry_support/event_calendar/getEvent.php?sid="+Math.random(), { isMobile:"Y", key: keyStr, startDate: startDateStr, endDate: endDateStr, theDate:theDate, 
				
				searchType: searchType, keywordType:keywordType, eventType:eventType, start_item:0, itemperpage : itemperpage},
                function (data, textStatus){
						//console.log("--->"+data);
                        $("#noData").hide();	
                        $("#eventList").show();
						$("#eventList").html(data);		
                });
 }			
function goTodayTC(){				
				//console.log("goTodayTC");
				/*
				var today = new Date();
				var lastDayOfMonth = new Date(today.getFullYear(), today.getMonth()+1, 0);
				$('#s_date').val( (today.getMonth()+1) + "/"+today.getDate()+"/"+today.getFullYear());
				$('#e_date').val( (today.getMonth()+1) + "/"+lastDayOfMonth.getDate()+"/"+today.getFullYear());		
*/				
				var s_date = new Date();
				s_date = new Date(s_date.getFullYear(), s_date.getMonth()-3, 1);
				var e_date = new Date();
				e_date = new Date(e_date.getFullYear(), e_date.getMonth()+4, 0);
				
				$('#s_date').val( s_date.getDate() + "/"+(s_date.getMonth()+1)+"/"+s_date.getFullYear());
				$('#e_date').val( e_date.getDate() + "/"+(e_date.getMonth()+1)+"/"+e_date.getFullYear());
				searchTC();
}		
function searchTC(){
               
				//console.log("search TC");
				var keywords = $('#s_event').val();
				var keywordType = $('#keywordType').val();
				var eventType = $('#eventType').val();
				var tmp_sdate = $('#s_date').val();
				var tmp_edate = $('#e_date').val();

                var errMsg="";
                if(( !keywords || keywords.trim().length===0) 
					&& (tmp_sdate == ""|| tmp_edate == "")){
                    errMsg+="請輸入活動關鍵字或選擇日期。<br>";				
                }
				
				
				//console.log("sdate"+tmp_sdate);
				//console.log("edate"+tmp_edate);
				if( tmp_sdate != '' || tmp_edate != '' ){
					if(tmp_sdate == ""){
						//console.log("\n\nNO sdate\n\n");
						errMsg+="請選擇活動開始日期。<br>";
					}
					
					if(tmp_edate == ""){
						//console.log("\n\nNO edate\n\n");
						errMsg+="請選擇活動結束日期。<br>";
					}
					
					var ts = new Date(tmp_sdate);
					var te = new Date(tmp_edate);
					//console.log("check date-->" + ts + "|" + te);
					if( ts > te ){
						errMsg+="活動開始日期必須早過活動結束日期。<br>";
					}
				}
				
                if(errMsg!==""){
					$("#errMsg").html(errMsg).show();
					//alert(errMsg);
                    return false;
                }else{
					$("#errMsg").hide();
				}
				
				
				var sDate = tmp_sdate.split("/")[2] + "-" + tmp_sdate.split("/")[1] + "-" + tmp_sdate.split("/")[0];
				var eDate = tmp_edate.split("/")[2] + "-" + tmp_edate.split("/")[1] + "-" + tmp_edate.split("/")[0];

				
				var searchType="";
				if(!( typeof keywords == 'undefined' || keywords.trim().length===0)){
					searchType="k";
				}
				if(!( typeof tmp_sdate == 'undefined' || tmp_sdate.trim().length===0)){
					searchType+="d";
				}
				
				//console.log($("#s_event").val() + "|\n" + sDate + "|\n" + eDate + "|\n" + "" + "|\n" + searchType + "|\n" + keywordType + "|\n" + eventType);
				
				
                getEventTC($("#s_event").val(),sDate,eDate,"",searchType,keywordType,eventType);		
}

function listEventTC(){			
				var sMon = Number($('#s_month_from').val());
                var sYear = Number($('#s_year_from').val());
                var eMon = Number($('#s_month_to').val());
                var eYear = Number($('#s_year_to').val());
				var sDate;
				var eDate;			
				if(sYear>0){
					sDate = sYear+"-"+sMon+"-01";
					eDate = eYear+"-"+eMon+"-31";				
				}else{
					var today = new Date();
					sDate = today.getFullYear()+"-"+(today.getMonth()+1)+"-01";
					eDate = today.getFullYear()+"-"+(today.getMonth()+1)+"-31";					
				}
                getEventTC($("#s_event").val(),sDate,eDate,"","d","name","false");
}

function getEventSC(keyStr,startDateStr,endDateStr, theDate, searchType, keywordType, eventType){
                //console.log("get event tc");
				var itemperpage = 6;
				$.post("/sc/our_work/business/industry_support/event_calendar/getEvent.php?sid="+Math.random(), { isMobile:"Y", key: keyStr, startDate: startDateStr, endDate: endDateStr, theDate:theDate, 
				
				searchType: searchType, keywordType:keywordType, eventType:eventType, start_item:0, itemperpage : itemperpage},
                function (data, textStatus){
						//console.log("--->"+data);
                        $("#noData").hide();	
                        $("#eventList").show();
						$("#eventList").html(data);		
                });
            }			   
function goTodaySC(){				
/*
				var today = new Date();		
				var lastDayOfMonth = new Date(today.getFullYear(), today.getMonth()+1, 0);
				$('#s_date').val( (today.getMonth()+1) + "/"+today.getDate()+"/"+today.getFullYear());
				$('#e_date').val( (today.getMonth()+1) + "/"+lastDayOfMonth.getDate()+"/"+today.getFullYear());	
				*/
				var s_date = new Date();
				s_date = new Date(s_date.getFullYear(), s_date.getMonth()-3, 1);
				var e_date = new Date();
				e_date = new Date(e_date.getFullYear(), e_date.getMonth()+4, 0);
				
				$('#s_date').val( s_date.getDate() + "/"+(s_date.getMonth()+1)+"/"+s_date.getFullYear());
				$('#e_date').val( e_date.getDate() + "/"+(e_date.getMonth()+1)+"/"+e_date.getFullYear());
				searchSC();
			}		
function searchSC(){

				//console.log("search SC");
				var keywords = $('#s_event').val();
				var keywordType = $('#keywordType').val();
				var eventType = $('#eventType').val();
				var tmp_sdate = $('#s_date').val();
				var tmp_edate = $('#e_date').val();

                var errMsg="";
                if(( !keywords || keywords.trim().length===0) 
					&& (tmp_sdate == ""|| tmp_edate == "")){
                    errMsg+="请输入活动关键字或选择日期。<br>";				
                }
				
				
				//console.log("sdate"+tmp_sdate);
				//console.log("edate"+tmp_edate);
				if( tmp_sdate != '' || tmp_edate != '' ){
					if(tmp_sdate == ""){
						//console.log("\n\nNO sdate\n\n");
						errMsg+="请选择活动开始日期。<br>";
					}
					
					if(tmp_edate == ""){
						//console.log("\n\nNO edate\n\n");
						errMsg+="请选择活动结束日期。<br>";
					}
					
					var ts = new Date(tmp_sdate);
					var te = new Date(tmp_edate);
					//console.log("check date-->" + ts + "|" + te);
					if( ts > te ){
						errMsg+="活动开始日期必须早过活动结束日期。<br>";
					}
				}
				
                if(errMsg!==""){
					$("#errMsg").html(errMsg).show();
					//alert(errMsg);
                    return false;
                }else{
					$("#errMsg").hide();
				}
				
				
				var sDate = tmp_sdate.split("/")[2] + "-" + tmp_sdate.split("/")[1] + "-" + tmp_sdate.split("/")[0];
				var eDate = tmp_edate.split("/")[2] + "-" + tmp_edate.split("/")[1] + "-" + tmp_edate.split("/")[0];

				
				var searchType="";
				if(!( typeof keywords == 'undefined' || keywords.trim().length===0)){
					searchType="k";
				}
				if(!( typeof tmp_sdate == 'undefined' || tmp_sdate.trim().length===0)){
					searchType+="d";
				}
				
				//console.log($("#s_event").val() + "|\n" + sDate + "|\n" + eDate + "|\n" + "" + "|\n" + searchType + "|\n" + keywordType + "|\n" + eventType);
				
                getEventSC($("#s_event").val(),sDate,eDate,"",searchType,keywordType,eventType);		
}		
function listEventSC(){			
				var sMon = Number($('#s_month_from').val());
                var sYear = Number($('#s_year_from').val());
                var eMon = Number($('#s_month_to').val());
                var eYear = Number($('#s_year_to').val());
				var sDate;
				var eDate;			
				if(sYear>0){
					sDate = sYear+"-"+sMon+"-01";
					eDate = eYear+"-"+eMon+"-31";				
				}else{
					var today = new Date();
					sDate = today.getFullYear()+"-"+(today.getMonth()+1)+"-01";
					eDate = today.getFullYear()+"-"+(today.getMonth()+1)+"-31";					
				}
	             getEventSC($("#s_event").val(),sDate,eDate,"","d","name","false");
}		
