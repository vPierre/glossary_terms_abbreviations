/*
	Script : DRI Main JS
	Author : Innovative Consultants LLC
*/

// DRI Base URL
var driiBaseURL = 'https://drii.org/';

function getUrl(method){
	return driiBaseURL+method;
}

/* WebLog */
function weblog(logtype,key){
	if(logtype == ''){
		return false;
	}
	$.ajax({
	  url: getUrl('videologajax'),
	  type: "post",
	  data: {'logtype':logtype,key : key,'act':'webstaticlog', '_token': $('input[name=_token]').val()},
	  success: function(data){
			//						
	   }
	});
}
