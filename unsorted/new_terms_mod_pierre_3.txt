●      BD: bridge domain

●      EPG: endpoint group

●      EP: endpoint residing in an ACI fabric

●      L3Out: Layer 3 Out or external routed network

●      L3Out EPG: subnet-based EPG in L3Out

●      VRF: Virtual Routing and Forwarding

●      Border leaf: ACI leaf where L3Out is deployed

●      EX leaf: 2nd generation Cisco Nexus 9300 series switch ending with -EX, such as Nexus 93180YC-EX

●      FX leaf: 2nd generation Cisco Nexus 9300 series switch ending with -FX, such as Nexus 93180YC-FX