**100Base-FX**			IEEE-Standard für 100 Mbit/s-Übertragung über Lichtwellenleiter (Fast Ethernet)
**100Base-T2**			IEEE-Standard für 10 Mbit/s-Übertragung über 2-paarige verdrillte Kupferdoppeladern (Fast Ethernet)
**100Base-T4**			IEEE-Standard für 100 Mbit/s-Übertragung über 4-paarige verdrillte Kupferdoppeladern (Fast Ethernet)
**100Base-TX**			IEEE-Standard für 100 Mbit/s-Übertragung über verdrillte Kupferdoppeladern (Fast Ethernet)
**10Base-F**			IEEE-Standard für 10 Mbit/s-Übertragung über Lichtwellenleiter (Ethernet)
**10Base-T**			IEEE-Standard für 10 Mbit/s-Übertragung über verdrillte Kupferdoppeladern (Ethernet)
**10GE**			10-Gbit/s-Ethernet
**10GFC**			10-Gbit/s-Fiber-Channel
**10GbE**			10-Gbit/s-Ethernet
**2,5G**			Second and Half Generation --> Weiterentwicklung der Mobilfunksysteme der 2. Generation (GPRS, EDGE)
**2B1Q**			Two Binary One Quaternary --> Quaternärer (4-stufiger) Leitungscode (z.B. für ADSL u. EURO-ISDN)
**2DES**			Double Data Encryption Standard (IT-Sicherheit)
**2Dr**				Zweidrahtleitung (Anschlußnetze)
**2G**				Second Generation --> Mobilfunksysteme der 2. Generation (GSM)
**3964R**			Asynchrones Datenübertragungsprotokoll für Punkt-zu-Punkt-Verbindungen (Siemens)
**3DES**			Triple Data Encryption Standard 
**3DES**			Triple Data Encryption Standard --> Symmetrisches Verschlüsselungsverfahren (IT-Sicherheit)
**3G**				Third Generation --> Mobilfunksysteme der 3. Generation (UMTS)
**3GPP**			Third Generation Partnership Project (IMT 2000)
**3GPP2**			Third-Generation Partnership Project 2 (CDMA 2000)
**3PTY**			Three Party Service --> Dreierkonferenz (ISDN)
**3R**				Regeneration, Re-shaping, Reamplification, Re-timing (Optische Netze, SDH, Sonet)
**4B3T**			Four Binary Three Ternary --> Ternärer (3-stufiger) Leitungscode (z.B. für ISDN-Basisanschluß)
**4B5B**			Four Binary Five Binary --> Binärer (2-stufiger) Leitungscode (z.B. für ATM)
**4G**				Fourth Generation --> 4. Generation von Mobilfunksystemen
**5G**				Fifth Generation --> 5. Generation von Mobilfunksystemen
**8B10B**			Eight Binary Ten Binary --> Binärer (2-stufiger) Leitungscode (z.B. für Fiber Channel und Gigabit Ethernet)
**A**				Availability
**A-Netz**			Erstes analoges (handvermitteltes) Mobilfunknetz in Deutschland
**A/D**				Analog/Digital
**A2B**				Administration-to-Business --> Elektronische Interaktion zwischen Behörden und Unternehmen
**A2C**				Administration-to-Consumer --> Elektronische Interaktion zwischen Behörden und Kunden
**AA**				Accounting Authorities
**AAA**				Authentication, Authorisation and Accounting --> Zugangs- und Abrechnungssystem
**AAAC**			Authentication, Authorisation, Accounting and Charging
**AAE**				Allgemeine Anschalteerlaubnis
**AAL**				ATM Adaptation Layer --> ATM-Anpassungsschicht (ATM)
**AAL-n (n = 0 ...5)**		ATM Adaptation Layer Type n
**AAM**				Automated Availability Manager (VMware)
**AAN**				All Area Network --> Netztechnik für LAN, MAN, WAN und GAN
**AAP**				Alternative Access Provider
**AAR**				Automatic Alternate Routing
**AARP**			AppleTalk Address Resolution Protocol --> Übertragungsprotokoll von Apple
**AB**				Access Burst --> Zugriffsburst (GSM, Mobilfunknetze)
**AB**				Anrufbeantworter
**ABAC**			attribute-based access control
**ABC-F**			Protokoll zur Vernetzung von Nebenstellenanlagen (Alcatel)
**ABE**				Allgemeine Benutzungserlaubnis für Endeinrichtungen
**ABHC**			Average Busy Hour Calls
**ABIOS**			Advanced Basic Input/Output System
**ABM**				Asynchronous Balanced Mode (HDLC)
**ABR**				Area Border Router (Internet)
**ABR**				Available Bit Rate (ATM)
**AC**				Alternating Current --> Wechselstrom
**AC**				Attack Complexity
**AC**				Authentication Code
**AC-1**			Atlantic Crossing No. 1 --> 14.000-km langer Unterwasserkabelring zwischen Europa und Nordamerika
**AC-3**			Audio Codec No. 3 --> Verfahren zur hochqualitativen Tonaufzeichnung (Dolby)
**ACA**				Adaptive Channel Allocation
**ACA**				Australian Communications Authority (Gremien, Organisationen)
**ACAP**			Advanced Common Application Platform (Gremien, Organisationen)
**ACARS**			Aircraft Communication Addressing and Reporting System
**ACC**				ATM Cross Connect --> ATM-Netzknoten (ATM)
**ACC**				Analogue Control Channel
**ACC**				Area Communications Controller (Modacom)
**ACCH**			Associated Control Channel
**ACDG**			Application Configuration and Developer Guide
**ACE**				Automatic Calibration and Equalization
**ACE**				Auxiliary Control Element
**ACELP**			Algebraic Code-Excited Linear Prediction
**ACF**				Access Control Field (IEEE 802, Lokale Netze)
**ACF**				Advanced Communication Function (IBM/SNA)
**ACG**				Automatic Code Gapping
**ACI**				Adjacent Channel Interference --> Nachbarkanalstörung (GSM)
**ACIA**			Asynchronous Communication Interface Adapter
**ACIR**			Adjacent Channel Interference Ratio --> Nachbarkanalstörabstand (GSM)
**ACIT**			Adaptive Code Sub-Band Excited Transform
**ACK**				Acknowledgement --> Positive Bestätigung
**ACL**				Access Control List
**ACL**				Access Control List (IT-Sicherheit)
**ACL**				Asynchronous Connectionless --> Übertragungsart
**ACLR**			Adjacent Channel Leakage Power Ratio (GSM)
**ACM**				Address Complete Message (ATM)
**ACM**				Association for Computing Machinery (US, New York) (Gremien. Organisationen)
**ACMS**			Application Control Management System
**ACO**				Alarm Cut-Off
**ACOnet**			Wissenschaftsnetz in Österreich
**ACP**				Adjacent Channel Power --> Störleistung im Nachbarkanal (GSM)
**ACPI**			Advanced Configuration and Power Interface
**ACPR**			Adjacent Channel Power Ratio (GSM)
**ACR**				Advanced Call Routing
**ACR**				Allowed Cell Rate (ATM)
**ACR**				Attenuation-to-Crosstalk Ratio --> Logarithmisches Verhältnis von Kabeldämpfung zu Nebensprechen
**ACS**				Adjacent Channel Selectivity (GSM)
**ACS**				Advanced Cellular System --> Analoges zellulares Mobilfunknetz in Schweden (Comvik)
**ACS**				Advanced Computer System (IBM)
**ACSE**			Access Control and Signalling Equipment
**ACSE**			Association Control Service Element (ISO/OSI)
**ACSNET**			Academic Computing Services Network
**ACSSB**			Amplitude Companded Single Side Band --> Modulationsverfahren
**ACTS**			Advanced Communications Technologies and Services --> EU-Forschungsförderprogramm
**ACTS**			Automatic Coin Telephone Service
**AD**				Access Device
**AD**				Adjunct
**AD**	                    	Active Directory
**ADA**				Average Delay till Abort
**ADB**				Apple Desktop Bus (Apple)
**ADC**				American Digital Cellular --> Nordamerikanisches zellulares Mobilfunksystem (D-AMPS)
**ADC**				Analog-to-Digital Converter --> Analog-Digital-Wandler
**ADCCP**			Advanced Data Communications Control Procedure --> Bitorientiertes Übertragungsprotokoll der ANSI (ähnlich HDLC)
**ADCT**			Adaptive Discrete Cosine Transform --> Kompressionsverfahren für Videodaten
**ADH**				Average Delay till Handling
**ADI**				Autodesk Device Interface
**ADLC**			Asynchronous Data Link Control
**ADM**				Adaptive Deltamodulation --> Modulationsverfahren
**ADM**				Add-Drop Multiplexer (SDH)
**ADM**				Application Discover Manager (VMware)
**ADMD**			Admininstration Management Domain (ITU-T X.400)
**ADN**				Abbreviated Dialing Number
**ADN**				Address Complete, No Charge
**ADO**				Anschlussdose (ISDN)
**ADP**				Automatic Data Processing --> Automatische Datenverarbetiung
**ADPCM**			Adaptive Delta Pulse Code Modulation --> Verfahren zu Sprachcodierung (ITU-T G.721)
**ADPM**			Adaptive Delta Pulse Modulation --> Modulationsverfahren
**ADPS**			Automatic Data Processing System --> Automatisches Datenverarbeitungssystem
**ADR**				Adaptive Dynamic Routing
**ADR**				Astra Digital Radio
**ADR**				Automatic Data Retrieval --> Automatische Datenwiedergewinnung
**ADRD**			Automatic Data Rate Detection --> Automatische Erkennung der Datenrate
**ADRIA 1**			LWL-Unterwasserkabel zwischen Kroatien, Albanien und Griechenland
**ADS**				Active Directory Service --> Verzeichnisdienste
**ADSI**			Active Directory Service Interface
**ADSI**			Analogue Display Services Interface --> Telekommunikationsprotokoll (Bellcore)
**ADSL**			Asymmetric Digital Subscriber Line --> Digitaler Teilnehmeranschluß mit unsymmetrischer Bitrate (xDSL)
**ADSP**			AppleTalk Data Stream Protocol (Apple)
**ADSU**			ATM Data Service Unit (ATM)
**ADTF**			Allowed Cell Rate Drecease Time Factor (ATM)
**ADU**				Analog-Digital-Umsetzer
**ADU**				Automatic Dialing Unit --> Automatische Wähleinrichtung
**ADV**				Automatic Data Processing --> Automatische Datenverarbeitung
**ADVA**			Automatische Datenverarbeitungsanlage
**ADX**				Automatic Data Exchange --> Automatische Datenvermittlung
**AE**				Anschlußeinheit
**AE**				Application Enabler
**AE**				Application Entity --> Anwendungsinstanz (ISO/OSI)
**AE**				Ausschalteinheit
**AEAD**			Authenticated Encryption with Associated Data
**AEI**				Additional Equipment Interface
**AEP**				AppleTalk Echo Protocol (Apple)
**AERM**			Alignment Error Rate Monitor (Lokale Netze)
**AES**				Advanced Encryption Standard --> Symmetrisches Verschlüsselungsverfahren (IT-Sicherheit)
**AES**				Advanced Encryption System
**AES**				Advanced Environment Specification
**AES**				Aircraft Earth Station --> Bordseitige Einrichtung für Datalink via Satellit
**AES**				Audio Engineering Society (Gremien, Organisationen)
**AES-CBC**			Advanced Encryption Standard-Cipher Block Chaining 
**AES-CTR**			Advanced Encryption Standard-Counter Mode
**AET**				Application Entity Title (ISO/OSI)
**AF**				Address Field --> Adressfeld (Protokollelement)
**AF**				Assured Forwarding (QoS)
**AF**				Audio Frequency --> Tonfrequenz (Multimedia)
**AFB**				Automatic Fallback (Modem)
**AFC**				Application Foundation Class
**AFC**				Automatic Frequency Control --> Automatische Frequenznachsteuerung (ATM)
**AFD**				Avalanche Fotodiode
**AFF**				Advanced Function Feature --> Erweiterte Funktionseinrichtung
**AFI**				Address Family Identifier (Protokolle)
**AFI**				Authority and Format Identifier --> NSAP-Protokollelement (ISO/OSI)
**AFIPS**			American Federation of Information Processing Societies --> Dachverband Datenverarbeitung USA (Gremien, Organisationen)
**AFN**				Address Complete, No Charge, Subscriber Free (Telekommunikation)
**AFOS**			Active Fiber Optic Segment (Lokale Netze)
**AFP**				Advanced Function Printing (Hersteller-NW-Architukturen)
**AFP**				AppleTalk Filing Protocol --> Client/Server-Protokoll für Macintosh-Rechner (Apple)
**AFRICA 1**			35.000 km-langer LWL-Unterwasserkabelring um Afrika
**AFS**				Andrew File System (Netzmanagement)
**AFSK**			Audio Frequency Shift Keying --> Übertragung von Digitalsignalen mit Tonfrequenzen (Übertragungstechnik)
**AFT**				Adapter Fault Tolerance (Lokale Netze)
**AFT**				Anwenderforum Telekommunikation e.V. (BRD, Overath) (Gremien, Organisationen)
**AFTN**			Aeronautical Fixed Telecommunication Network (ICAO)
**AFTP**			Anonymous FTP --> Anonymer FTP-Dienst (Intenet)
**AFX**				Address Complete, Subsciber Free, Coin Box (Telekommunikation)
**AFeN**			Analoges Fernsprechnetz (Telekommunikation)
**AFuG**			Amateurfunkgesetz
**AFuV**			Verordnung zum Gesetz über den Amateurfunk
**AG**				Access Grant
**AG**				Animated Gifs --> Animierte Bilder im Gif-Format
**AGC**				Adaptive Gain Control --> Adaptive Verstärkungsregelung (Mobilfunknetze)
**AGC**				Automatic Gain Control --> Automatische Verstärkungsregelung
**AGCH**			Access Grant Channel --> Zuweisungskanal (GSM, Mobilfunknetze)
**AGF**				All Glass Fiber (Verkabelung)
**AGOF**			Arbeitsgemeinschaft Online-Forschung (Gremien, Organisationen)
**AGPF**			Anchored Geodesic Packet Forwarding (Mobile Kommunikation)
**AGW**				Access Gateway
**AH**				Application Protocol Header --> Steuerinformation von Anwendungsprotokoll
**AH**				Authentication Header --> Protokollelement in IPSec
**AHFG**			ATM Attached Host Functional Group (ATM)
**AHT**				ATM Header Translator (ATM)
**AI**				Air Interface --> Luftschnittstelle (Mobilfunknetze)
**AI**				Alarm Indication
**AI**				Artificial Intelligence --> Künstliche Intelligenz
**AI**				Authentification Information (IT-Sicherheit)
**AIA**				Application Integration Architecture (Offene NW-Konzepte)
**AIC**				Advanced Intra Coding (Codierung)
**AIDC**			Automatic Identification and Data Capture
**AIM**				ATM Inverse Multiplexer (ATM)
**AIM**				Analogue Intensity Modulation --> Modulationsverfahren
**AIM**				Automatic Identification Manufactures
**AIML**			Analoge Internationale Mietleitung (Datenkommunikations-Dienste)
**AIMUX**			ATM Inverse Multiplexing (ATM)
**AIN**				Advanced Intelligent Network (Mobilfunknetze)
**AINI**			ATM Inter-Network Interface (ATM)
**AIO**				Asynchronous Input/Output Interface (Schnittstelle)
**AIP**				Adaptive Internet Protocol (Internet)
**AIP**				Application Infrastructure Provider
**AIR**				Additive Increase Rate (ATM)
**AIRA**			American Internet Registrars Association (Gremien, Organisationen)
**AIRF**			Additive Increase Rate Factor (ATM)
**AIS**				Alarm Indication Signal --> Alarmanzeigesignal (ATM)
**AISP**			Association of Information System Professionals (Gremien, Organisationen)
**AIT**				Application Information Table
**AK**				Arbeitskreis
**AKA**				Also Known As --> Abgekürzte Redewendung (Chat)
**AKA**				Authentification and Key Agreement (IT-Sicherheit)
**AKPL**			Akustikkoppler (Modem)
**AKZ**				Anlagenkennziffer
**AL**				Access Link --> Anschlussstrecke (ATM)
**AL**				Amtsleitung (Telekommunikation)
**ALB**				Adaptive Load Balancing --> Adaptiver Lastenausgleich (Lokale Netze)
**ALC**				Analogue Leased Circuit --> Analoge Festverbindung (Weitverkehrsnetze)
**ALC**				Automatic Level Control (GSM, Mobilfunknetze)
**ALE**				Automatic Link Establishment
**ALEC**			Alternative Local Exchange Carrier (Weitverkehrsnetze)
**ALI**				Application Layer Interface (ISO/OSI)
**ALM**				AppWare Loadable Module
**ALOHA**			Verfahren zur Regelung des Kanalzugriffs in LANs (Lokale Netze)
**ALPA**			Arbitrated Loop Physical Address (Lokale Netze)
**ALS**				Adjacent Link Station (Internetworking)
**ALS**				Alternate Line Service (Mobilfunknetze)
**ALS**				Application Layer Structure (OSI-Referenzmodell)
**ALT**				ADSL Line Termination (xDSL)
**ALT**				Automatic Link Transfer
**ALU**				Arithmetic Logic Unit --> Rechenwerk (Datenverarbeitung)
**ALUA**			Asymmetrical logical unit access, a storage array feature. Duncan Epping explains it well. (VMware)
**AM**				Access Manager (PACS, Mobilfunknetze)
**AM**				Access Module --> Zugriffsmodul
**AM**				Accounting Management --> Abrechnungsmanagement (Netzmanagement)
**AM**				Active Monitor (Token Ring)
**AM**				Amplitudenmodulation (Modulation)
**AM-PSK**			Amplitude Modulation - Phase Shift Keying (Modulationsverfahren)
**AMA**				Automatic Message Accounting
**AMBE**			Advanced Multi-Band Excitation --> Verfahren zur Sprachkompression
**AMCP**			Aeronautical Mobile Communications Panel (ICAO) (Gremien, Organisationen)
**AMD**				Published Addemdum to a Published Standard
**AMDS**			AM-Radio-Daten-System (Mobilfunknetze)
**AME**				Asynchronous Modem Eliminator
**AMHS**			Aeronautical Message Handling Service (ICAO)
**AMI**				Alternate Mark Inversion --> Pseudoternärer Leitungscode (Codierung)
**AMICS**			Advanced Multimedia and Image Communication System
**AMIS-A**			Audio Message Interchange Specification, Analogue (Mobilfunkdienste)
**AMP**				Amplifier --> Verstärker (Netzkomponente)
**AMPS**			Advanced Mobile Phone System (Mobilfunknetze)
**AMPS**			Advanced Mobile Phone System --> Analoges zellulares Mobilfunksystem in Nordamerika im 850-MHz-Band
**AMPS**			Automatic Message Processing System (Mobilsfunknetze)
**AMR**				Adaptive Multirate --> Verfahren zur Sprachkompression im Mobilfunk (GSM)
**AMS**				Application Management System
**AMS**				Audio Visual Multimedia Services (ATM)
**AMS-IX**			Amsterdam Internet Exchange
**AMSAT**			Amateur (Radio) Satellite (Gremien, Organisation)
**AMSS**			Aeronatical Mobile Satellite Service (Satelliten-Kommunikation)
**AMT**				Asia Mobile Telecommunications
**AMTS**			Advanced Mobile Telephone System (Mobilfunknetze)
**AMUX**			Arithmetic Logic Unit Multiplexer
**AN**				Access Network --> Zugangsnetz (Anschlußnetze)
**AN**				Access Node --> Zugangsknoten (Netzkomponente)
**AN**				Anmeldedienst
**ANC**				Answer Signal, Charge
**ANDF**			Architecture Neutral Distribution Format
**ANDMS**			Advanced Network Design and Management (Netzmanagement)
**ANF**				Additional Network Feature
**ANF**				Automatic Number Forwarding
**ANFOR**			Association Francaise de Normalisation (Gremien, Organisationen)
**ANI**				Access Network Interface
**ANI**				Automatic Number Identification --> Rufnummeridentifizierung (Telekommunikation)
**ANIS**			Analoger Anschluss am ISDN (ISDN)
**ANM**				Answer Message (Mobilfunknetze)
**ANMP**			Account Network Management Program (Netzmanagement)
**ANN**				Answer Signal, No Charge (Datenfelder)
**ANP**				Autonegotiation Protocol (Lokale Netze)
**ANR**				Advanced Network Routing (Hersteller-NW-Architekturen)
**ANS**				Advanced Network Services --> Internet-Backbone-Betreiber in USA
**ANS**				Answer Message
**ANSI**			American National Standards Institute
**ANSI**			American National Standards Institute --> Standardisierungsgremium in den USA (Gremien, Organisationen)
**ANU**				Answer Signal, Unqualified
**ANX**				Automotive Network Exchange (Internetworking)
**AO**				Abort Output (Datenfelder)
**AO/DI**			Always On / Dynamic ISDN --> Spezielle Anschlußvariante des ISDN für Internet-Nutzung
**AOC**				ADSL Overhead Control (xDSL)
**AOC**				Advice of Charge --> Dienstmerkmal zur Gebührenanzeige (GSM, ISDN)
**AOC**				Aeronautical Operational Communications (ICAO)
**AOCD**			AOC During Call (ISDN)
**AOCE**			AOC at End of Call (ISDN)
**AOCS**			AOC at Start of Call (ISDN)
**AODV**			Abort of Dialling --> Wahlabbruch
**AODV**			Ad-hoc On-demand Distance Vector
**AOL**				America Online (Internet-Diensteanbieter)
**AON**				Active Optical Network --> Aktives optisches Netz
**AON**				All Optical Network (Optische Netze, SDH, Sonet)
**AOPS**			Automated Office Protocol Standard (Protokolle)
**AOQ**				Average Outgoing Quality
**AOR**				Atlantic Ocean Region --> Ausleuchtzone geostationärer Satelliten
**AOZ**				Adresse ohne Zusatzinformationen
**AP**				Access Point (Wireless LAN)
**AP**				Administrative Point (Verzeichnisdienste)
**AP**				Advanced Prediction
**AP**				Application Part --> Anwendungsteil (ISDN)
**AP**				Application Process --> Anwendungsprozess (OSI-Referenzmodell)
**AP**				Application Profile
**AP**				Application Protocol (ISO/OSI)
**AP-CCIRN**			Asia/Pacific Coordinating Committee for Intercontinental Research Networks (Gremien, Organisationen)
**APA**				Adaptive Packet Assembly
**APAA**			Agressive Packet Adaptive Assembly
**APACS**			Association for Payment Clearing Services (Gremien, Organisationen)
**APC**				Adaptive Power Control
**APC**				Adaptive Predictive Coding (Codierung)
**APC**				Aeronautical Passenger Communications
**APC**				Angle Polished Convex (Verkabelung)
**APC**				Asynchronous Procedure Call
**APCG**			ATM Pilot Coordination Group (Gremien, Organisationen)
**APCI**			Advanced Power and Configuration Interface
**APCM**			Adaptive PCM (Modulation)
**APCN**			Asia Pacific Cable Network --> Glasfaser-Unterwasserkabel
**APCO**			Association of Public Safty Communication Officials (Gremien, Behörden)
**APD**				Avalange Photo Diode --> Lawinenfotodiode
**APDU**			Application Protocol Data Unit --> Anwendungsprotokoll-Dateneinheit (ISO/OSI)
**APE**				Abgesetzte Periphere Einheit (ISDN)
**APE**				All Paths Explorer Frame (Internetworking)
**APE**				Application Protocol Entity (ISO/OSI)
**APF**				All Plastic Fiber --> Plastikfaser (Verkabelung)
**API**				Application Programming Interface --> Programmierschnittstelle zur Unterstützung von Anwendungsprogrammen
**API**	                    	Application Programming Interface
**API/CS**			API Communication Services
**APL**				Anschlusspunkt (ISDN)
**APM**				Additional Packet Mode --> Paketvermittelnder Dienst im ISDN-D-Kanal
**APM**				Advaced Power Management
**APM**				Alternierende Pulsmodulation --> Übertragungsverfahren im Basisband
**APM**				Application Performance Manager (VMware)
**APMBS**			Associated Packet Mode Bearer Services (Telekommunikation)
**APN**				Access Point Name
**APNIC**			Asian Pacific Network Information Center (Gremien, Organisationen)
**APON**			ATM over Passive Optical Network (Optische Netze, SDH, Sonet)
**APOP**			Authenticated Post Office Protocol (Internet-Protokoll)
**APP**				Application Portability Profile
**APPC**			Advanced Program-to-Program Communication (IBM/LU6.2) (Hersteller- NW-Architekturen)
**APPI**			Advenced Peer to Peer Internetworking (Hersteller-NW-Architikturen, Internetworking)
**APPL**			Application-level Crypto
**APPLI**			Advanced Program to Program Communication (Internetworking)
**APPLI/COM**			Application/Communication
**APPN**			Advanced Peer-to-Peer Networking (IBM/SNA) (Hersteller-NW-Architekturen)
**APPNTAM**			APPN Topology and Accounting Manager (Hersteller-NW-Architekturen)
**APR**				Arbeitsplatzrechner
**APS**				Arbeitsplatz-System (Telekommunikations-Endgeräte)
**APS**				Automatic Protection Switch (SDH, FDDI, Optische Netze, Sonet)
**APT**				Advanced Persistent Threat
**AQ**				Adaptive Quantisation (Modulation)
**AQL**				Acceptable Quality Level
**AR**				Access Rate --> Zugriffsgeschwindigkeit
**AR**				Access Router
**AR**				Adaptive Routing
**AR**				Alarm Report
**AR**				Auto-Regressive
**AR**				Automatic Recall
**AR**				Availability Requirement
**ARA**				Apple Talk Remote Access Protocol (Internetworking)
**ARA**				Attribute Registration Authority (Mobilfunkdienste)
**ARBC**			Adaptive Rate-based Congestion Control
**ARC**				Attached Resource Computer (Telekommunikations-Endgeräte)
**ARCH**			Access Response Channel
**ARCnet**			Attached Resource Computer Network (Lokale Netze)
**ARE**				All Routes Explorer (Internetworking)
**ARF**				Acknowledgement Run Flag
**ARFCN**			Absolute Radio Frequency Channel Numbers (GSM)
**ARI/FCI**			Address Recognized Indicator / Frame Copied Indicator (Token Ring)
**ARIB**			Association of Radio Industries and Businesses (Japan) (Gremien, Organisationen)
**ARIN**			American Registry for Internet Numbers (Gremien Organisationen)
**ARIS**			Aggregate Route-based IP Switching (MPLS)
**ARL**				Adjusted Ring Length --> Einstellung der Ringleitungslänge (Token Ring)
**ARM**				Application Resource Management
**ARM**				Asynchronous Response Mode (HDLC)
**ARP**				Adaptive Routing Protocol (Internetworking)
**ARP**				Address Resolution Protocol
**ARP**				Address Resolution Protocol (Ethernet, RFC 826)
**ARP**				Finnisches analoges Mobilfunknetz aus der vor-zellularen Zeit
**ARPA**			Advanced Research Projects Agency (früher DARPA) (Gremien, Organisationen)
**ARPANET**			Advanced Research Projects Agency Network --> Erstes großes paketvermittelndes WAN
**ARPU**			Average Return per User
**ARQ**				Automatic Repeat Request --> Quittierungs-Verfahren (Übertragungstechnik)
**ARR**				Asynchronous Reply Requiered --> Asynchrone Antwort erforderlich (Zeichen, Zeichensatz)
**ARR**				Automatic Retransmission Re-routing --> Automatische Wegerneuerung (Internetworking)
**ARR**				Automatic Retransmission Request --> Anforderung einer wiederholten Übertragung
**ARRA**			Announced Retransmission Random Access (Sat-Kommunikation)
**ARS**				Adress Resolution Server (Internetworking)
**ARS**				Automatic Route Selection --> Automatische Wegwahl (Internetworking)
**ART**				Address Resolution Table (ISDN)
**ARTS**			American Radio Telephone System --> Vorläufer von AMPS
**ARTour**			Advanced Radio Communications on Tour (Mobilfunknetze)
**ARU**				Anrufumleitung (Telekommunikation)
**ARX**				Automatic Retransmission Exchange --> Vermittlung mit automatischer Weiterschaltung (Telekommunikation)
**AS**				Address Space
**AS**				Air Station --> Funkstation ( TFTS, Sat-Kommunikation)
**AS**				Aktiver Sternkoppler (Internetworking)
**AS**				Anrufsucher
**AS**				Application Service (ISO/OSI)
**AS**				Autonomes System (Internet)
**ASA**				American Standards Association --> Amerikanisches Normungsgremium (Gremien, Organisationen)
**ASAI**			Adjunct Switch Application Interface --> API von AT&T
**ASAM**			ATM Subscriber Access Multiplexer (ATM)
**ASAP**			As Soon As Possible --> Abkürzung für Redewendung (Chat)
**ASB**				Associated Body --> Assoziierte Organisation (Gremien, Organisationen)
**ASBR**			Autonomous System Boundary Router (Internet)
**ASC**				American Standards Committee --> Amerikanisches Normungsgremium (Gremien, Organisationen)
**ASC**				Automatic Sequence Control --> Automatische Steuerung der Reihenfolge
**ASCII**			American Standard Code for Information Interchange
**ASCII**			American Standard Code for Information Interchange (Zeichensatz)
**ASDSP**			Apple Talk Secure Data Stream Protocol (Apple, Protokolle)
**ASE**				Amplified Spontaneous Emission --> Rauschen in optischen Verstärkern
**ASE**				Application Service Element --> Anwendungs-Dienstelement (ISO/OSI)
**ASF**				Apache Software Foundation (Gremien, Organisationen)
**ASI**				Aktor-Sensor-Interface --> Feldbus zur Vernetzung im Maschinen- u. Anlagenbau
**ASI**				Asynchronous Serial Interface
**ASI**				Asynchronous/Synchronous Interface (Schnittstelle)
**ASIC**			Application Specific Integrated Circuit
**ASID**			Access, Searching and Indexing of Directories
**ASK**				Amplitude Shift Keying --> Digitale Amplitudenmodulation, Amplituden(um)tastung
**ASL**				Adaptive Speed Leveling (Modem)
**ASLA**			Application Service Level Agreement (QoS)
**ASM**				Anwendungsspezifisches Modul
**ASN**				Autonomous System Number (Datenfelder)
**ASN.1**			Abstract Syntax Notation One (ITU-T X.208, ISO/IEC 8824)
**ASO**				Address Supporting Organization --> ICANN
**ASO**				Application Service Object (OSI-Referenzmodell)
**ASO**				Automated System Operation
**ASON**			Automatically Switched Optical Network
**ASP**				Abstract Service Primitive
**ASP**				Abstract Syntax Processor
**ASP**				Active Server Page (Microsoft)
**ASP**				Active Splitting Point --> Bezeichnung für Splitter (Verkabelung)
**ASP**				AppleTalk Session Protocol (Apple, Protokolle)
**ASP**				Application Service Provider
**ASPIC**			ASP Industry Consortium (Gremien, Organisationen)
**ASR**				Automatic Send/Receive --> Automatisches Senden und Empfangen (Telekommunikation)
**ASR**				Automatic Speech Recognition --> Automatische Spracherkennung (Telekommunikation)
**ASU**				Asynchron/Synchronumsetzer (Netzkomponente)
**ASVD**			Asynchronous Simultaneous Voice and Data --> DSVD
**ASZ**				Aufschaltzeichen
**AT**				Advanced Technology (IBM/PC-Generation)
**AT**				Attention --> Befehlssatz für Modem
**AT**				Authentification Triplet (Weitverkehrsnetze)
**ATB**				ATM Termination Board
**ATC**				Adaptive Transform Coding --> Sprachcodierungsverfahren
**ATCP**			AppleTalk Control Protocol (Apple, Protokolle, Internetworking)
**ATDM**			Asynchronous Time Division Multiplexing --> Zeitmultiplex-Verfahren
**ATDP**			Attention Dial Pulse (Datenfelder)
**ATDT**			Attention Dial Tone (Datenfelder)
**ATE**				Application Test Environment
**ATE**				Automatic Test Equipment
**ATF 1**			Application Transport Failover (Speichernetze)
**ATF 1**			Autotelefoon Net 1 --> Erstes niederländisches analoges Mobilfunknetz
**ATFP**			Apple Talk Filing Protocol (Apple, Protokolle)
**ATL**				ATM Layer --> ATM-Schicht (ATM)
**ATM**				Asynchronous Transfer Mode --> Zellenbasierte Übertragungstechnologie
**ATM-CC**			ATM-Crossconnect
**ATM-SDU**			ATM Service Data Unit --> ATM-Dienstelement (ATM)
**ATM-VE**			ATM-Vermittlungseinheit
**ATMP**			Ascend Tunnel Management Protocol (Ascend) (Protokolle)
**ATN**				Aeronautical Telecommunication Network (ICAO)
**ATN**				Augmented Transition Network (Offene NW-Konzepte)
**ATP**				AppleTalk Transaction Protocol (Apple, Protokolle)
**ATPC**			Automatic Transmission Power Control
**ATRAC**			Adaptive Transform Acoustic Coding --> Kompressionsverfahren für akustische Signale (Sony)
**ATRT**			Ausschuss für technische Regulierung in der Telekommunikation (Gremien, Organisationen)
**ATS**				Abstract Test Suite
**ATS**				Analoge Teilnehmerschaltung --> a/b-Wandler (ISDN)
**ATSC**			Advanced Television Systems Committee (USA) (Gremien, Organisationen)
**ATSC**			Air Traffic Services Communications
**ATT**				Attached (Bit)
**ATT**				Attenuation --> Dämpfung
**ATU-C**			ADSL Transmission Unit – Central Office (xDSL)
**ATU-R**			ADSL Transmission Unit – Remote (xDSL)
**ATVEF**			Advanced Television Enhancement Forum (Gremien, Organisationen)
**AU**				Access Unit (ITU-T X.400)
**AU**				Administrative Unit --> Verwaltungselement (Optische Netze, SDH, Sonet)
**AUC**				Authentication Center (GSM, Mobilfunknetze)
**AUG**				Administrative Unit Group (Optische Netze, SDH, Sonet)
**AUI**				Attachment Unit Interface --> Schnittstelle zwischen MAU und Ethernet-Endgerät (IEEE 802.3)
**AUIF**			AUI Female --> "Weiblicher" AUI-Stecker (Verkabelung)
**AUIM**			AUI male --> "Männlicher" AUI-Stecker (Verkabelung)
**AUP**				Acceptable Use Policy
**AURP**			Apple Talk Update-based Routing Protocol (Apple, Protokoll, Internetworking)
**AUTH**			Authentification Service (IT-Sicherheit)
**AUX**				Auxiliary --> Hilfsfunktion (Datenfelder)
**AV**				Attack Vector
**AVA**				Attibute Value Assertion
**AVD**				Alternate Voice/Data --> Abwechselnd Sprache/Daten (Weitverkehrsnetze)
**AVI**				Audio Video Interleaved --> Dateiformat zur Speicherung von Ton- und Bildinformationen (Microsoft)
**AVIEN**			Anti-Virus Information Exchange Network (IT-Sicherheit)
**AVL**				Amtsverbindungsleitung (Weitverkehrsnetze)
**AVL**				Automatic Vehicle Location (Telematik)
**AVON**			Amtliches Verzeichnis der (Fernmelde-) Ortsnetzkennzahlen (Deutsche Telekom)
**AVP**				Attribute Value Pair
**AVRS**			Automatic Voice Response System
**AVT**				Active Voice Technology(Telekommunikation)
**AWAG**			Automatisches Wähl- und Ansagegerät
**AWD**				Automatische Wähleinrichtung für Datenverbindungen
**AWFQ**			Adaptive Weighted Fair Queuing (Weitverkehrsnetze)
**AWG**				American Wire Gauge --> Amerikanisches Kennzeichnungssystem für Durchmesser elektrischer Leiter (Verkabelung)
**AWG**				Arbitrary Waveform Generator
**AWG**				Arrayed Waveguide Grating (Optische Netze, SDH, Sonet)
**AWGN**			Additive White Gaussian Noise --> Additives weißes gaußverteiltes Rauschen
**AWS**				Anrufweiterschaltung (ISDN)
**AWUG**			Automatisches Wähl- und Übertragungsgerät (Telekommunikation)
**AWaDo**			Automatische Wechselschalter-Anschlussdose --> Anschalteeinheit am analogen Telefonanschluß (Deutsche Telekom)
**AX.25**			Amateur X.25 --> Variante von X.25, die im Amateurfunk zur paketorientierten Datenübertragung verwendet wird
**AZ**	                    	Availability Zone
**ActiveX**			Microsoft-Entwicklungswerkzeug für dynamische Internet-Anwendungen (Internet Explorer)
**Agt**				Anschaltgerät (Weitverkehrsnetze)
**AiMF**			Ader in Metall-Folie (Verkabelung)
**AoI**				ADSL over ISDN (ISDN, xDSL)
**Archie**			Internet-Datenbank für Dateien, die per Anonymous FTP zur Verfügung stehen
**AsB**				Anschlussbereich (Anschlussnetze)
**AsBVSt**			Anschlussbereichsvermittlungsstelle (Netzkomponente)
**Asl**				Anschlußleitung
**AslMux**			Anschlussleitungs-Multiplexer
**Auto Deploy**			Technique to automatically install ESXi to a host. (VMware)
**AutoFüFmNLw**			Automatisches Führungsfernmeldenetz der Luftwaffe (Bundeswehr)
**B-ICI**			B-ISDN Inter Carrier Interface --> ATM PNNI-Signalisierung (ATM)
**B-ISDN PRM**			Protocol Reference Model of Broadband ISDN --> Protokoll-Referenzmodell des Breitband-ISDN (B-ISDN)
**B-ISDN**			Broadband Integrated Services Digital Network --> Breitband-ISDN
**B-Kanal**			64-kbit/s-Nutzkanal (ISDN)
**B-LLI**			Broadband Low Layer Information (ATM)
**B-MTP**			Broadband MTP
**B-NT**			Broadband Network Termination --> Netzabschluss für B-ISDN (B-ISDN)
**B-NT1**			Broadband Network Termination Type 1 --> Netzabschluss 1 für B-ISDN
**B-NT2**			Broadband Network Termination Type 2 --> Netzabschluss 2 für B-ISDN
**B/W**				Bothway --> Zweiseitig
**B2B**				Business to Business --> Geschäftsabwicklung zwischen Unternehmen durch Nutzung elektronischer Interaktionen
**B2C**				Business to Consumer --> Geschäftsabwicklung zwischen Unternehmen und Kunden durch Nutzung elektronischer Interaktionen
**B8Z**				Bipolar Eight Zero Substitution --> Leitungscodierung (z.B. für T1) (Codierung)
**BA**				Basic Access --> Basisanschluß (ISDN)
**BA**				Behavior Aggregate
**BAC**				Balanced Asynchronous Class (HDLC)
**BAC**				Billing, Accounting and Charging (Netz-Management)
**BAC**				Binary Asymmetric Channel
**BACC**			Billing and Customer Care
**BACP**			Bandwidth Allocation Control Protocol --> Protokolle aus der PPP-Familie (Internet, Protokolle)
**BAF**				Bellcore Automatic Message Accounting Format (Bellcore)
**BAIC**			Barring All Incoming Calls --> Sperren von eingehenden Rufen (Mobilfunknetze)
**BAK**				Backup
**BAKOM**			Bundesamt für Kommunikation --> TK-Regulierungsbehörde der Schweiz
**BAKT**			Basisanschluss-Konzentrator (ISDN)
**BALUN**			Balanced/Unbalanced (Verkabelung)
**BAM**				Basic Telecommunications Access Method
**BAM**				Burst Mode Ampltude Modulation --> Bitserielles Wechseltakt-Übertragungsverfahren (Modulation)
**BAMx**			Basisanschluss-Multiplexer (ISDN)
**BAOC**			Barring All Outgoing Calls --> Sperren von ausgehenden Rufen (Mobilfunknetze)
**BAP**				Bandwidth Allocation Protocol --> Protokoll aus der PPP-Familie (Protokolle, Internet)
**BAPT**			Bundesamt für Post und Telekommunikation (Gremien, Organisationen)
**BAR**				Backward Adaptive Reencoding
**BAS**				Base Station (Mobilfunknetze)
**BAS**				Basic Activity Subset
**BAS**				Bit Rate Allocation Signal
**BAS**				Broadband Access Server
**BATCH**			Blockallocated Transfer Channel --> Block-Übertragungskanal (Datenverarbeitung)
**BAV**				Breitband-Anschlussvermittlungsstelle (Weitverkehrsnetze)
**BB**				Backbone (NW-Konzepte)
**BB**				Baseband --> Basisband
**BB**				Broadband --> Breitbandübertragung
**BBI**				Broadband Interactive System (Sat-Kommunikation)
**BBN**				Bolt, Beranek & Newman
**BBNS**			Broadband Network Services
**BBP**				Binary Backoff Procedure (IEEE 802.3, Ethernet)
**BBR**				Backbone Ring (NW-Konzepte)
**BBS**				Bulletin Board System (Internet)
**BBTAG**			Broadband TAG (Lokale Netze)
**BC**				Basic Call
**BC**				Basic Container
**BC**				Bearer Capability (ISDN)
**BC**				Binary Code --> Binärcode (Codierung)
**BC**				Broadcast
**BC**				Broandband Bearer Capability (ATM)
**BC**				Busy Condition --> Besetztzustand
**BCAM**			Basic Communication Access Method
**BCAST**			Broadcast Protocol (Novell) (Protokolle)
**BCC**				Bearer-Channel-Connection Protocol
**BCC**				Blind Carbon Copy
**BCC**				Block Check Character (Fehlerkorrektur)
**BCC**				Broadcast Call Control (Mobilfunknetze)
**BCCH**			Broadcast Control Channel --> Sendekontrollkanal (GSM, Mobilfunknetze)
**BCCS**			Billing (and) Customer Care System
**BCD**				Binary Coded Decimal --> Binär codiertes Dezimalzahl (Codierung)
**BCDIC**			Binary Code Decimal Interchange Code --> Binär codierter dezimaler Austausch-Code (Codierung)
**BCF**				Backward Control Field
**BCFSK**			Binary Code Frequency Shift Keying (Modulation)
**BCH**				Bose, Chadhuri, Hocquenghem --> Kanalcode
**BCH**				Broadcast Channel --> Sendekanal (GSM)
**BCLK**			Bit Clock
**BCM**				Back Channel Message
**BCM**				Block Code Modulation
**BCM**				Business Continuity Management
**BCOB**			Broadband Connection Oriented Bearer (ATM)
**BCP**				Basic Call Process (Weitverkehrsnetze)
**BCP**				Best Current Practices
**BCP**				Bridging Control Protocol (Protokolle, Internetworking)
**BCP**				Byte Control Protocol (IBM/BSC)
**BCPL**			Binary Coded Programming Language
**BCPN**			Business Customer Premises Network (NW-Konzepte)
**BCS**				Basic Combined Subset
**BCS**				Basic Control System
**BCS**				Block Check Sequence --> Blockprüfzeichenfolge (Fehlerkorrektur)
**BCS**				Block Control Signal --> Blockkontrollsignal
**BCSM**			Basic Call State Model (Weitverkehrsnetze)
**BCT**				Business Cordless Telephone (Mobilfunknetze)
**BCVT**			Basic Class Virtual Terminal (Datenkommunikations-Dienste)
**BD**				Building Distributor --> Gebäudeverteiler (Verkabelung)
**BDC**				Backup Domain Controller (IT-Sicherheit)
**BDC**				Basic Mode Control Procedures für Data Communication Systems (Protokolle)
**BDC**				Big Data Cluster
**BDE**				Below Deck Equipment
**BDLS**			Bidirectional Loop Switching --> Redundante Netzkonfiguration für Ringnetze
**BDSG**			Bundesdatenschutzgesetz
**BDSG**			Bundesdatenschutzgesetz (IT-Sicherheit)
**BDV**				Breitband-Durchgangs-Vermittlungsstelle --> Vermittlungsknoten der obersten Netzebene (Weitverkehrsnetze)
**BEB**				Binary Exponential Backoff --> Algorithmus zur Bestimmung der Wartezeit (IEEE 802.3, Ethernet)
**BECN**			Backward Explicit Congestion Notification (Frame Relay)
**BEEP**			Block Extensible Exchange Protocol (Protokolle)
**BEF**				Building Entrance Facilities --> Hausübergabepunkt (Anschlussnetze)
**BEL**				Bell --> Klingelzeichen
**BEP**				Back-End-Prozessoren
**BER**				Basic Encoding Rules (ISO/OSI)
**BER**				Bit Error Rate --> Bitfehlerrate
**BERKOM**			Berliner Kommunikationssystem --> Forschungsprojekt der Deutschen Telekom (NW-Konzepte)
**BERNET**			Berliner (Forschungs-)Netz bzw. Berliner Rechnernetz für die Wissenschaft
**BERT**			Bit Error Rate Test --> Bitfehlerratenmessung
**BERTS**			Basic Exchange Radio Telecommunications Service (Mobilfunknetze)
**BEXR**			Basic Exchange Radio (Mobilfunknetze)
**BF**				Boundary Functions --> Begrenzungsfunktion (Hersteller-NW-Architekturen)
**BFH**				Bitfehlerhäufigkeit
**BFI**				Bad Frame Indicator
**BFOC**			Bayonet Fibre Optic Connector --> Bajonet-Lichtwellenleiter-Steckverbinder (Verkabelung)
**BFR**				Bitfehlerrate
**BFS**				Business File System --> Kommerzielles Dateisystem
**BFT**				Binary File Transfer
**BFU**				Betriebsführungsumsetzer (Netzmanagement)
**BG**				Border Gateway
**BGMP**			Border Gateway Multicast Protocol (Protokolle, Internet)
**BGNW**			Benutzergruppe Netze (Gremien, Organisationen)
**BGP**				Border Gateway Protocol
**BGP**				Border Gateway Protocol --> Routingprotokoll im Internet (RFC 1163) (Protokolle, Internet)
**BH**				Block Header --> Blockanfangskennsatz
**BHC**				Busy Hour Calls (ISDN)
**BHCA**			Busy Hour Call Attempts (ISDN)
**BHLI**			Broadband High Layer Information (ATM)
**BI**				Business Intelligence
**BIB**				Backward Indication Bit --> Rückwärtsindikator (Zeichen, Zeichensatz)
**BIB**				Bus Interface Board (Schnittstelle)
**BIBA**			Bilingualer Basisanschluss (ISDN)
**BIC**				Barring Incoming Call (Mobilfunknetze)
**BIC**				Bus Interface Circuit --> Computer/Netz-Schnittstelle (Schnittstelle)
**BICC**			Bearer Independent Call Control
**BICI**			Broadband Inter-Carrier Interface (ATM)
**BIE**				Base Station Interface Equipment (GSM, Mobilfunknetze)
**BIFF**			Binary Interchange File Format --> Dateiformat für Spreadsheets
**BIGFERN**			Breitbandiges Integriertes Glasfaser-Fernmeldenetz (NW-Konzepte)
**BIGFON**			Breitbandintegriertes Glasfaser-Fernmelde-Ortsnetz (NW-Konzepte)
**BIIS**			Binary Interchange of Information and Signalling (Mobilfunknetze)
**BIM**				Backplane Interface Module (Schnittstelle)
**BIND**			Berkeley Internet Name Daemon --> Dämon für DNS-Server (Internet, Protokolle)
**BIOP**			Broadcast Inter ORB Protocol (Protokolle)
**BIOS**			Basic Input Output System
**BIOS**			Basic Input/Output System --> Einfaches Eingabe/Ausgabe-System (PC-Technik)
**BIP**				Bit Interleaved Parity --> Prüfsummenalgorithmus (SDH, Sonet)
**BIP-8**			Bit Interleaved Parity-8 (Optische Netze, SDH, Sonet)
**BIS**				Border Intermediate System (ATM)
**BIS**				Boundary Intermediate System (ISO/OSI)
**BIS**				Bracket Initiation Stopped
**BISAM**			Basic Indexed Sequential Access Method
**BISM**			Bit Interleaved Subrate Multiplexing --> Multiplexverfahren, das leitungs- und paketvermittelte Daten miteinander mischt
**BISO**			Business Information Security Officer
**BIST**			Built-in Self Test
**BISUP**			Broadband ISDN User Part (ATM)
**BITNET**			Akademisches Netz in USA, Südamerika und Japan
**BITNET**			Because It´s Time Network (Forschungsnetz)
**BIU**				Basic Information Unit (IBM/SNA) (Hersteller-NW-Architekturen)
**BIU**				Bus Interface Unit (Schnittstelle)
**BIX**				Binary Information Exchange
**BK**				Benutzerklasse (Weitverkehrsnetze)
**BK**				Breitband-Kommunikation
**BK**				Breitbandkabel-Netz (Anschlussnetze)
**BK**				Bürokommunikation (Telekommunikation)
**BKS**				Bundesverband Kabel und Satellit (Gremien, Organisationen)
**BKZ**				Bereichskennzahl
**BLA**				Blocking Acknowledgement, Signal/Message --> Sperrbestätigungskennzeichen (Zeichen, Zeichensatz)
**BLAM**			Binary Logarithmic Arbitration Methode (Übertragungstechnik)
**BLAST**			Blocked Asynchronous Transmission --> Blockweise Übertragung
**BLER**			Block Error Rate
**BLERT**			Block Error Rate Test --> Blockfehlerratentest
**BLO**				Blocking Signal --> Sperrzeichen (Zeichen, Zeichensatz)
**BLOB**			Binary Large Object (Datenbank)
**BLQAM**			Blackman Quadrature Amplitude Modulation --> Spezielles QAM-Verfahren (Modulation)
**BLSR**			Bidirectional Line Switch Ring (Weitverkehrsnetze)
**BLU**				Basic Link Unit (IBM/SNA) (Hersteller-NW-Architekturen)
**BM**				Bearer Mobile (Mobilfunknetze)
**BMAP**			Broadband Modem Access Protocol --> Zugangsprotokoll für Breitband-Modems (Protokolle)
**BML**				Business Management Layer (TMN)
**BMP**				Bitmap --> Grafikformat
**BMP**				Burst Mode Protocol (Novell) (Protokolle)
**BMPT**			Bundesministerium für Post und Telekommunikation (Gremien, Organisationen)
**BMS**				Basic Management System (Netzmanagement)
**BMUX**			Block Multiplexer Channel --> Block-Multiplexkanal (Offene NW-Konzepte)
**BMWi**			Bundesministerium für Wirtschaft und Technologie (Gremien, Organisationen)
**BN**				Backward Explicit Congestion Notification (Frame Relay)
**BN**				Bridge Number --> Brücken-Nummer (Internetworking)
**BNA**				Backbone Network Architecture (NW-Konzepte)
**BNC**				Bayonet Neill Concelmann --> Stecker für Koaxialkabel (Verkabelung)
**BNC**				Bayonet Nut Connector --> Stecker für Koaxialkabel (Verkabelung)
**BNK**				Bereichs-Netzknoten (Deutsche Telecom) --> Digitale Orts- und Kontenvermittlung (Weitverkehrsnetze)
**BNK**				Betriebsnetzknoten (Deutsche Telekom) --> Netzknoten für das Netzmanagement
**BNN**				Boundary Network Node (Hersteller-NW-Architekturen)
**BOA**				Basic Object Adapter
**BOC**				Barring all Outgoing Calls --> Sperren von ausgehenden Rufen (Mobilfunknetze)
**BOC**				Bell Operating Company --> Telefongesellschaft in USA
**BOD**				Bandwidth on Demand (Tk-Datendienste)
**BOIC**			Barring Outgoing International Calls --> Sperren von ausgehenden internationalen Rufen (Mobilfunknetze)
**BOLT**			Breitband Optical Line Termination
**BONDING**			Bandwidth on Demand Interoperability Group (ISDN)
**BONT**			Broadband Optical Network Termination --> Netzabschluß mit optisch/elektrischer Wandlung
**BONU**			Broadband Optical Network Unit
**BOOTP**			Bootstrap Protocol --> Anwendungsprotokoll im Internet (Protokolle, Internet)
**BOP**				Bit Oriented Protocol (Protokolle)
**BOS**				Behörden und Organisationen mit Sicherheitsaufgaben (Gremien, Organisationen)
**BP**				Backplane (Schnittstelle)
**BPDU**			Bridge Protocol Data Unit (Internetworking)
**BPF**				Best Path First (Internetworking)
**BPON**			Broadband Passive Optical Network --> Passives Optisches Breitbandnetzwerk (NW-Konzepte)
**BPP**				Bridge Port Pair (Internetworking)
**BPSK**			Binary Phase Shift Keying --> Zweiphasenumtastung (Modulation)
**BR**				Branch --> Verzweigung
**BR**				Bus Request
**BRA**				Basic Rate Acces (ISDN)
**BRAM**			Broadcast Recognition Access Method --> Sendeerkennungszugriffsmethode bei Ethernet (Ethernet)
**BRAN**			Broadband Radio Access Network (HiperLAN)
**BRAS**			Broadband Remote Access Server
**BRE**				Bridge Relay Encapsulation (Ascom) (Frame Relay, Protokolle)
**BRI**				Basic Rate Interface --> Basisanschluss, S0-Schnittstelle (ISDN)
**BRIM**			Bridge/Router Interface Module (Internetworking)
**BRK**				Break
**BRM**				Basic Reference Model (ISO/OSI)
**BROC**			Bayonet Fibre Optic Connector
**BRQ**				Bandwidth Request
**BRS**				Break Request Signal --> Abbruchanforderungssignal (Datenfelder)
**BS**				Backplane Segment (Schnittstelle)
**BS**				Backspace --> Rückwärtsschritt (Steuerzeichen)
**BS**				Base Station --> Basisstation (Mobilfunknetze)
**BS**				Bearer Service (Mobilfunknetze)
**BS**				Bitsynchronisation (Übertragungstechniken)
**BS**				British Standard
**BSA**				Basic Service Area (Wireless LAN)
**BSAM**			Basic Sequential Access Method --> Sequentielles Zugriffsverfahren (DK-Übertragungstechniken)
**BSB**				Blockade State Block
**BSC**				Base Station Controller --> Basisstationssteuereinheit (GSM, Mobilfunknetze)
**BSC**				Binary Symmetric Channel
**BSC**				Binary Synchronous Communication --> Bitsynchrones, zeichenorientiertes Schicht-2-Protokoll (IBM, Protokolle)
**BSC**				Binary Synchronous Control
**BSC**				Binary Synchronous Control --> Klassisches synchrones Übertragungsprotokoll (IBM, Protokolle)
**BSD**				Berkeley Software Distribution (Unix)
**BSHR**			Bidirectional Self-healing Ring
**BSI**				Boot Sector Infector --> Systemvirus (IT-Sicherheit)
**BSI**				British Standards Institute (Gremien, Organisationen)
**BSI**				Bundesamt für Sicherheit in der Informationstechnik
**BSI**				Bundesamt für Sicherheit in der Informationstechnik (Bonn) (Gremien, Organisationen)
**BSIC**			Base Station Identity Code (Mobilfunknetze)
**BSM**				Business Service Management
**BSMAP**			Base Station Management Application Part (Mobilfunknetze)
**BSS**				Base Station Subsystem (GSM)
**BSS**				Basic Service Set
**BSS**				Basic Synchronized Subset
**BSSAP**			Base Station Subsystem Application Part (GSM)
**BSSGP**			Base Station Subsystem GPRS Protocol (Mobilfunknetze, Protokolle)
**BSSMAP**			Base Station Subsystem Management Application Part (GSM)
**BT**				Bandwidth Time Product
**BT**				Bit Times
**BT**				Burst Tolerance
**BTN**				Basic Telephony Network --> Herkömmliches analoges Telefonnetz
**BTS**				Base Transceiver Station --> Basisstation (GSM)
**BTSM**			Base Transceiver Station Management (Mobilfunknetze)
**BTU**				Basic Transmission Unit (IBM/SNA)
**BTV**				Business Television
**BTX**				Bildschirmtext (Neu: Datex-J)
**BU**				Branching Unit
**BVB**				Bundesverband Büro- und Informationssysteme (Bad Homburg) (Gremien, Organisationen)
**BVCP**			Banyan Vines Control Protocol --> Protokoll der PPP-Familie (Protokolle)
**BVIT**			Bundesverband Informationstechnologien (und neue Medien e.V.) (Gremien, Organisationen)
**BVN**				Breitbandkabelverteilnetz
**BWC**				Bandwidth Control
**BWS**				Building Wiring Standard (Verkabelung)
**BYOD**			Bring Your Own Device
**BYOD**			Bring your own device
**BZT**				Bundesamt für Zulassungen in der Telekommunikation (Saarbrücken) (Gremien, Organisationen)
**BaAs**			Basisanschluß (ISDN)
**Bd**				Baud --> Maßeinheit für Symbolrate
**BdiMF**			Bündel in Metall-Folie (Verkabelung)
**Bisync**			Binary Synchronous Communication
**Bit**				Binary Digit --> Binäre Einheit (Zahlensysteme)
**BkVtSt**			Breitbandkabelverteilstelle
**BoM**				Beginning of Message --> Nachrichtenanfang (Weitverkehrsnetze)
**C**				C-Netz (Mobilfunknetze)
**C**				Confidentiality
**C**				Container
**C**				Control Bit (GSM)
**C**				Control Field --> Steuerfeld (Zeichen, Zeichensatz)
**C/I**				Carrier-to-Interference
**C/R**				Command/Response
**C/R**				Command/Response (Frame Relay)
**C/S**				Client Server Architecture (NW-Konzepte)
**C2C**				Consumer-to-Consumer --> Elektronische Interaktion zwischen Kunden
**CA**				Channel Adapter --> Kanaladapter
**CA**				Channel Aggregation
**CA**				Collision Avoidance --> Kollisionsvermeidung (Lokale Netze, IEEE 802)
**CA**				Competence Areas
**CA**				Conditional Access (Stadt-, Regionalnetze)
**CA**				Congestion Avoidance (Internetworking)
**CAA**				Certification Authority Authorization
**CAC**				Call Accepted --> Rufannahme (Datenfelder)
**CAC**				Call Admission Control (ATM)
**CAC**				Carrier Access Code
**CAC**				Computer Aided Communication --> Computerunterstützte Kommunikation
**CAC**				Connection Admission Control (ATM)
**CAC**				Customer Access Connection (Weitverkehrsnetze)
**CAD**				Computer Aided Design
**CADF**			Cloud Auditing Data Federation
**CADN**			Cellular Access Digital Network (Mobilfunknetze)
**CAE**				Common Application Environment (X/Open)
**CAE**				Computer Aided Engineering
**CAFM**			Computer Aided Facility Management
**CAI**				Common Air Interface (CT2) --> Funkschittstelle (Mobilfunknetze)
**CAI**				Computer Aided Instruction (Datenverarbeitung)
**CAL**				CAN Application Layer
**CALS**			Computer Aided Acquisition, Logistics and Support
**CAM**				Common Access Method (Schnittstelle)
**CAM**				Computer Aided Manufacturing --> Computerunterstützte Fertigung
**CAM**				Content Addressable Memory
**CAMC**			Customer Access Maintenance Centre --> Wartungszentrum mit Teilnehmerzugang
**CAMEL**			Customized Applications for Mobile Network Enhanced Logic EU-Projekt
**CAN**				Campus Area Network --> Campus-Netz
**CAN**				Cancel (Steuerzeichen)
**CAN**				Controller Area Network --> Feldbus
**CAN**				Customer Access Network (Stadt-, Regionalnetze)
**CAO**				Computer Aided Office --> Verwaltungsaufgaben mit dem Computer
**CAP**				CAMEL Application Part
**CAP**				Cable Access Point --> Kabelanschlusspunkt (Verkabelung)
**CAP**				Carrierless Amplitude- and Phasemodulation --> Digitales Modulationsverfahren (xDSL)
**CAPCS**			Cellular Auxiliary Personal Communications Service
**CAPG**			Control of Application Process Group
**CAPI**			Common Application Progamming Interface (Schnittstelle, ISDN)
**CAPS**			Call Attempts Per Second --> Verbindungsversuche pro Sekunde (Telekommunikation)
**CAR**				Competence Area Representative
**CARP**			Cache Array Routing Protocol (Internet, Protokolle)
**CARS**			Cable Television Relay Station --> TV-System in USA
**CAS**				Channel Associated Signalling --> In-Band-Signalisierung (ATM)
**CAS**				Communications Applications Specification (Schnittstelle)
**CASE**			Common Application Service Element (ISO/OSI)
**CASE**			Computer Aided Software Engineering
**CAST**			Competence Center for Applied Security Technology (IT-Sicherheit) (Gremien, Organisationen)
**CAST**			Computer Aided Software Testing
**CAT**				Cable Tester --> Kabeltester (Verkabelung)
**CAT**				Category --> Kategorie von Kupferkabeln (Verkabelung)
**CATM**			Cellular ATM (ATM)
**CATV**			Cable Television --> Kabelfernsehen
**CAU**				Controlled Access Unit (Token-Ring)
**CAVE**			Cellular Authentication and Voice Encryption Algorithm
**CAW**				Channel Address Word
**CB**				Carrier Band --> Trägerfrequenzband
**CB**				Cell Broadcast (Mobilfunknetze)
**CB**				Chargeback (VMware)
**CB**				Citizen Band --> Amateurfunk
**CB**				Control Block
**CB**				Crossbar-Switch
**CBA**				Campus Backbone --> Primärkabel (Verkabelung, NW-Konzepte)
**CBC**				Call Broker Client (Telekommunikation)
**CBC**				Changeback Acknowledgement Signal
**CBC**				Cipher Block Chaining --> Verfahren zur Blockverschlüsselung (IT-Sicherheit, DES)
**CBCH**			Cell Broadcast Channel (Mobilfunknetze)
**CBDS**			Connectionless Broadband Data Service --> Verbindungsloser Breitband-Datendienst (Stadt-, Regionalnetze)
**CBE**				Cell Broadcast Equipment (Mobilfunknetze)
**CBEMA**			Computer Business Equipment Manufactures Association --> Vereinigung von Herstellern für kommerzielle Computerausrüstung (Gremien, Organisationen)
**CBK**				Clearback Signal --> Schlusszeichen (Steuerzeichen)
**CBM**				Chargeback Manager (VMware)
**CBMS**			Computer Based Message System --> Nachrichtensystem auf Computerbasis
**CBO**				Continuous Bitstream Oriented (Übertragungsverfahren)
**CBQ**				Class Based Queuing (Weitverkehrsnetze)
**CBR**				Constant Bit Rate (ATM, ISDN)
**CBRC**			Content Based Read Cache (VMware)
**CBS**				Call Broker Server (Telekommunikation)
**CBS**				Common Base Station (Mobilfunknetze, GSM)
**CBT**				Core Based Tree (Protokoll, Internet)
**CBWFQ**			Class Based Weighted Fair Queuing (Weitverkehrsnetze)
**CBX**				Computerized Branch Exchange --> Computergestützte Nebenstellenanlage (Netzkomponente)
**CC**				Call Confirm
**CC**				Call Control --> Rufsteuerung (GSM, Weitverkehrsnetze)
**CC**				Calling Card (Mobilfunknetze)
**CC**				Carbon Copy (Email)
**CC**				Cell Controller
**CC**				Clearing Center (Mobilfunkdienste)
**CC**				Cluster-Controller (Netzkomponente)
**CC**				Code Conversion --> Code-Konvertierung (Codierung)
**CC**				Connection Confirmed --> Verbindungsbestätigung (Steuerzeichen)
**CC**				Continuity Check --> Kontinuitätsprüfung (ATM)
**CC**				Control Character --> Kontrollzeichen (Steuerzeichen)
**CC**				Countdown Counter (Stadt-, Regionalnetze)
**CC**				Country Code --> Landescode (GSM, Weitverkehrsnetze)
**CC**				Cross Connect (ATM)
**CC**				Customer Control
**CCA**				Conceptual Communication Area
**CCAF**			Call Control Agent Function (Weitverkehrsnetze)
**CCAP**			Call Control Access Point
**CCB**				Channel Command Block --> Kanalbefehlsblock (Datenfelder)
**CCB**				Customer Care and Billing System --> System für Kundenbetreuung und Abrechnung
**CCBS**			Call Completion to Busy Subscriber --> Rückruf bei Besetzt (ISDN)
**CCCH**			Common Control Channel --> Allgemeiner Steuerkanal (GSM, Mobilfunknetze)
**CCD**				Charge Couple Device
**CCDF**			Complementary Cumulative Distribution Function (GSM)
**CCDN**			Corporate Consolidated Data Network --> Übergeordnetes Unternehmensdatennetz (NW-Konzepte)
**CCE**				Configuration Control Element (FDDI)
**CCETT**			Centre Commun d´Etude de Télediffusion et Télecommunications
**CCF**				Call Control Function (Steuerzeichen)
**CCFG**			Configuration Control Frame Generator
**CCFP**			Central Control Fixed Part --> Zentrale Steuerungseinheit
**CCH**				Control Channel --> Steuerungskanal (GSM, Mobilfunknetze)
**CCI**				Co-Channel Interference
**CCI**				Contactless Chipcard Interface (PC-Technik)
**CCI**				Customer Care Integration
**CCIA**			Computer and Communication Industries Assocation (Gremien, Organisationen)
**CCIB**			Call Completion Internet Busy
**CCIP**			Control Card Interface Panel
**CCIR**			Comité Consultatif International des Radiocommunications --> Internationaler beratender Ausschuß für das Funkwesen, Genf (Gremien, Organisationen)
**CCIRN**			Coordinating Committee for International Research Networking (Gremien, Organisationen)
**CCIS**			Common Channel Interoffice Signalling --> Zeichengabeverfahren SS#6 (ATM)
**CCITT**			Comité Consultatif Internationale de Télégraphique et Téléphonique --> Internationaler beratender Ausschuß für Telegrafie und Telefonie (Genf) (Gremien, Organisationen)
**CCK**				Complimentary Code Keying --> Modulationsverfahren (Wireless LAN)
**CCM**				Cross Connect Multiplexer (ATM)
**CCN**				Cluster Control Nodes (Netzkomponente)
**CCN**				Cordless Communication Network (Mobilfunknetze)
**CCNR**			Completion of Call on Reply --> Rückruf bei Nichtmelden (ISDN)
**CCO**				Call Connected --> Anrufverbindung geschaltet (Telekommunikation)
**CCP**				Call Control Point
**CCP**				Central Communication Processor (Netzkomponente)
**CCP**				Compression Control Protocol --> Protokoll der PPP-Familie (Protokolle)
**CCP**				Cornet Communications Protocol (Telekommunikation)
**CCR**				Commitment Concurrency and Recovery (ISO/OSI)
**CCR**				Continuity Check Request --> Durchgangsprüfungsanfrage (Mobilfunknetze)
**CCR**				Current Cell Rate (ATM)
**CCS#7**			Common Channel Signalling No. 7 --> Signalisierungssystem Nr. 7 (ISDN)
**CCS**				Common Channel Signalling --> Zentralkanalsignalisierung
**CCS**				Common Communication Support --> Gemeinsame Kommunikationsunterstützung (Hersteller-NW-Architekturen)
**CCS**				Custom Calling Services
**CCSA**			Common Control Switching Arrangement
**CCSN7**			Common Channel Signalling Network No. 7
**CCSRL**			Control Channel Segmentation and Reassembly Layer
**CCSS**			Common Channel Signalling System --> Gemeinsames Kanalsignalsystem (DK-Übertragungstechniken)
**CCT**				Channel Check Test
**CCTS**			Coordinating Committee on Satellite Communications (Gremien, Organisationen)
**CCU**				Cable Control Unit (Stadt-, Regionalnetze)
**CCU**				Central Control Unit --> Zentrale Steuereinheit (Netzkomponente)
**CCW**				Channel Command Word (Datenfelder)
**CD**				Call Deflection --> Anrufweiterschaltung (ISDN)
**CD**				Campus Distributor --> Standortverteiler (Verkabelung)
**CD**				Carrier Detect --> Trägererkennung (Modem)
**CD**				Change Directory (Datenfelder)
**CD**				Chromatische Dispersion (LWL, Verkabelung)
**CD**				Coded Diphase (Codierung)
**CD**				Collision Detection --> Kollisionserkennung (Ethernet, IEEE 802.3)
**CD**				Committee Draft (ISO/OSI) (Standards, Verordnungen, Gesetze)
**CD**				Compact Disc
**CD**				Continuous Delivery or Continuous Deployment
**CD-ROM**			Compact Disk-Read Only Memory
**CDD**				Connection and Data Distribution
**CDDI**			Copper Distributed Data Interface --> FDDI-Variante auf Kupferkabeln
**CDE**				Common Desktop Environment (X/Open)
**CDF**				Channel Definition Format
**CDF**				Common Data Format
**CDF**				Cumulative Distribution Function (GSM)
**CDF**				Cut-off Decrease Factor (ATM)
**CDG**				CDMA Development Group (Organisationen, Gremien)
**CDH**				Cooperative Document Handling (Datenbank)
**CDI**				Change Direction Indicator (Hersteller-NW-Architekturen)
**CDIF**			Common Document Interchange Format
**CDL**				Coded Digital Control Channel Locator
**CDLC**			Cellular Data Link Control --> Zellulare Datenverbindungssteuerung (Mobilfunknetze)
**CDM**				Code Division Multiplexing --> Codemultiplex (Übertragungstragungstechniken)
**CDM**				Code Domain Power (UMTS)
**CDMA**			Code Division Multiple Access --> Kanal-Zugriffsverfahren (Mobilfunknetze)
**CDMA/FDD**			Code Division Multiple Access/Frequency Diversity Duplex
**CDMF**			Commercial Data Masking Facility
**CDN**				City-Datennetz (Stadt-, Regionalnetze)
**CDN**				Content Delivery Network (Internet)
**CDN**				Customer Dedicated/Defined Network (NW-Konzepte)
**CDP**				CRL Distribution Point
**CDP**				Code Domain Power
**CDPD**			Cellular Digital Packet Data (Celluplan II)(Mobilfunknetze)
**CDR**				Call Detail Record
**CDR**				Clock and Data Recovery
**CDS**				Cell Directory Service
**CDS**				Cellular Digital System
**CDSL**			Consumer Premises Digital Subscriber Line (xDSL, Anschlußnetze)
**CDT**				Connectionless Data Transmission --> Verbindungslose Datenübertragung
**CDT**				Credit Allocation
**CDV**				Cell Delay Variation --> Zellverzögerungsschwankung (ATM)
**CDV**				Committee Draft for Voting (Gremien, Organisationen)
**CDVT**			Cell Delay Variation Tolerance (ATM)
**CE**				Circuit Emulation (ATM)
**CE**				Comittée Européen
**CE**				Communauté Européenne --> CE-Zeichen (Verkabelung)
**CE**				Connection Element --> Verbindungsabschnitt (ISDN)
**CE**				Connection Endpoint (ATM)
**CEA**				Consumer Electronics Association Gremien, Organisationen)
**CEI**				Connection Endpoint Identifier (ATM)
**CELP**			Code Excited Linear Prediction (Telekommunikation)
**CELPA**			Code-Excited Linear Prediction Algorithm --> Sprachcodierungsverfahren
**CEM**				Configuration Element Management
**CEMA**			Consumer Electronics Manufacturers Association (Gremien, Organisationen)
**CEN**				Comité Européen de Normalisation --> Europäisches Normungsgremium, Brüssel (Gremien, Organisationen)
**CENA**			Cellular Network Analyzer (Mobilfunknetze)
**CENELEC**			Comité Européen de Normalisation Electrotechnique --> Europäisches Gremium für elektrotechnische Normung, Brüssel (Gremien, Organisationen)
**CENTRE**			Council of European National Top Level Domain Registries (Internet)
**CENTREX**			Central Office Exchange Service (Telekommunikation)
**CEOC**			Clear Embedded Opterations Channel
**CEP**				Connection End Point
**CEP**				Country Extended Code Page
**CEPO**			Certified Exchange Point Operator
**CEPT**			Conférence Européenne des Administrations des Postes et des Télecommunications --> Gremium der europ. Post- u. Fernmeldeverwaltungen, Bern (Gremien,Organisationen)
**CEQ**				Customer Equipment --> Teilnehmereinrichtung (Anschlussnetze)
**CER**				Cell Error Ratio (ATM)
**CER**				Character Error Rate --> Zeichenfehlerrate
**CERT**			Computer Emergency Response Team (IT-Sicherheit)
**CERT**			TLS Certificate Attributes and Management
**CES**				Circuit Emulation Service (ATM)
**CES**				Coast Earth Station (Satellitenkommunikation)
**CESG**			Communication and Electronic Security Group (UK) (Gremien, Organisationen)
**CF**				Call Forwarding --> Rufweiterschaltung/Rufumleitung (ISDN, Telekommunikation)
**CF**				Cloud Foundry
**CF**				Control Flags --> Steuerflags (Zeichen, Zeichensatz)
**CF**				Convergence Function
**CF**				Coordinating Function (IEEE 802.11, WLAN)
**CFA**				Carrier Failure Alarm
**CFB**				Call Forwarding Busy --> Rufumleitung bei Besetzt (ISDN)
**CFB**				Cipher Feed Back --> Verfahren zur Blockverschlüsselung (IT-Sicherheit, DES)
**CFI**				Calling Feature Indicator
**CFNR**			Call Forwarding no Reply --> Anrufweiterschaltung (ISDN)
**CFOR**			Call Forwarding on Reply --> Anrufweiterschaltung
**CFP**				Computer Fax Protocol
**CFR**				Cambridge Fast Ring --> Ring-Netz (Lokale Netze)
**CFR**				Channelized Frame Relay
**CFR**				Confirmation to Receive
**CFS**				Common Functional Specification
**CFU**				Call Forwarding Unconditional --> Sofortige Rufweiterschaltung (ISDN)
**CFV**				Call For Votes
**CFV**				Carrier-Festverbindung (NW-Konzepte)
**CGBA**			Circuit Group Blocking Acknowledgement Message (Mobilfunknetze)
**CGI**				Common Gateway Interface (Internetworking)
**CGM**				Computer Graphics Metafile
**CGMP**			Cisco Group Management Protocol (Protokolle, Internetworking)
**CGSR**			Cluster Gateway Switch Routing
**CGU**				Circuit Group Unblocking Message (Mobilfunknetze)
**CGUA**			Circuit Group Unblocking Acknowledgement Message (Mobilfunknetze)
**CGW**				Customer Gateway --> Kundengateway (Stadt-, Regionalnetze)
**CH**				Call Hold --> Halten einer Verbindung (ISDN)
**CH**				Channel
**CHAP**			Challenge Handshake Authentication Protocol --> Protokoll der PPP-Familie (IT-Sicherheit, Protokolle)
**CHD**				Channel Distributor
**CHI**				Channel Identification (Mobilfunknetze)
**CHILL**			CCITT High Level Language --> Programmiersprache für die Entwicklung von TK-Systemen
**CHM**				Change Management
**CHML**			Compact Hypertext Markup Language
**CI**				Call Indicator --> Anruf-Anzeige (Tk-Datendienste)
**CI**				Call Intrusion (ISDN)
**CI**				Carrier Identification
**CI**				Cell Identification (Mobilfunknetze)
**CI**				Coded Information (Codierung)
**CI**				Common Interface (Stadt-, Regionalnetze)
**CI**				Component Interface (Schnittstelle)
**CI**				Computer Inquiry --> Computerabfrage
**CI**				Congestion Indication
**CI**				Congestion Indicator (ATM)
**CI**				Continuous Integration
**CI**				Critical Infrastructure
**CIA**				Channel Interface Adaptor
**CIA**				Computer Industry Association --> Vereinigung der Comoputerindustrie (Gremien, Organisationen)
**CIAT**			Confidentiality (C), Integrity (I), Availability (A) and Traceability (T)
**CIC**				Circuit Identification Code (ISDN)
**CIC**				Control Interconnect Card
**CICS**			Customer Information Communication System
**CID**				Call Instance Data
**CID**				Configuration, Installation, Distribution
**CID**				Context Identification (IT-Sicherheit)
**CIDCW**			Calling Line Identification on Call Waiting (ISDN)
**CIDF**			Common Intermediate Data Format
**CIDIN**			Common ICAO Data Interchange Network (ICAO)
**CIDR**			Classless Inter-Domain Routing
**CIF**				Cells in Frames (ATM over LAN)
**CIF**				Common Interleaved Frame
**CIFS**			Common Internet File System
**CIFS**			Common Internet File System (Microsoft, Lokale Netze)
**CII**				Code for Information Interchange
**CII**				Critical Infrastructure Information
**CIM**				Common Information Model (Netzmanagement)
**CIM**				Common Interface Model
**CIM**				Communications Interface Module --> Kommunikationsschnittstellenmodul (Schnittstelle)
**CIM**				Compuserve Information Manager
**CIM**				Computer Integrated Manufacturing --> Rechnerintegrierte Fertigung (Datenverarbeitung)
**CIME**			Customer Installation Maintenance Entity --> Wartungsinstanzen der Teilnehmereinrichtung
**CINT**			Call Interception (ISDN)
**CIO**				Central Input/Output
**CIOCS**			Communication Input/Output Control System
**CIP**				Carrier Identification Parameter (ATM)
**CIP**				Channel Interface Processor
**CIPH**			Cipher Suite Components
**CIQ**			 	Capacity IQ (VMware)
**CIR**				Carrier-to-Interference Ratio
**CIR**				Committed Information Rate (Frame Relay)
**CIRSC**			Cross Interleave Reed Solomon Code --> Fehlerkorrekturverfahren
**CIS**				Compuserve Information Services (Online-Dienste)
**CIS**				Customer Interaction System (TK-Sprachdienste)
**CIS**			 	Cloud Infrastructure Suite (VMware)
**CISPR**			Comité International Special des Pertubations Radioélectriques --> Sonderausschuß der IEC für Funkstörungen, Genf (Gremien, Organisationen)
**CISS**			Certificate Issuance
**CIT**				Computer Integrated Telephony --> Computerintegriertes Telefonieren (Telekommunikation)
**CITT**			Computerintegrierte Telefonie und Telematik (ISDN)
**CIV**				Cell Interarrival Variation (ATM)
**CIX**				Commercial Internet Exchange (Internet)
**CK**				Checkbit --> Prüfbit (Zeichen, Zeichensatz)
**CK**				Cipher Key (IT-Sicherheit)
**CKT**				Chipkartenterminal (IT-Sicherheit)
**CL**				Cable Length --> Kabellänge (Verkabelung)
**CL**				Connectionless --> Verbindungslose Übertragung (DK-Übertragungstechniken)
**CLAN**			Cordless LAN (Wireless LAN)
**CLASS**			Custom Local Area Signalling Services --> ISDN-ähnlicher Dienst in USA
**CLC**				Clear Confirmation --> Löschbestätigung (Datenfelder)
**CLCC**			Closed Loop Congestion Control (ATM)
**CLEC**			Competitive Local Exchange Carrier --> Neuer lokaler TK-Anbieter (Telekommunikation)
**CLI**				Calling Line Identification (Identity)
**CLI**				Clear Indication --> Löschanzeige (Datenfelder)
**CLI**				Command Line Interface
**CLI**				Command Line Interface (Schnittstelle)
**CLID**			Calling Line Identification --> Anruferidentifikation (ISDN)
**CLIP**			Calling Line Identification Parameters
**CLIP**			Calling Line Identification Presentation --> Anzeige der Rufnummer des Anrufers (ISDN)
**CLIR**			Calling Line Identification Restriction --> Unterdrückung der Rufnummer des Anrufers (ISDN)
**CLIST**			Command List --> Befehlsliste (Datenfelder)
**CLK**				Clock --> Takt (Steuerzeichen)
**CLL**				Cellular Local Loop
**CLLM**			Consolidate Link Layer Management (Frame Relay)
**CLNAP**			Connectionless Network Access Protocol (B-ISDN)
**CLNIP**			Connectionless Network Interface Protocol (B-ISDN)
**CLNP**			Connectionless Network Protocol (ISO/IEC 8473)
**CLNS**			Connectionless Network Service --> (DK-Übertragungstechniken)
**CLNS**			Connectionless Network Service --> Verbindungsloser Netz-Dienst (ISO/OSI)
**CLP**				Cell Loss Priority --> Zellenverlustpriorität (ATM)
**CLPS**			Connectionless Packet Service
**CLQ**				Clear Request (Datenfelder)
**CLR**				Cell Loss Ratio (ATM)
**CLR**				Clear --> Löschen (Datenfelder)
**CLS**				Connectionless Server (ATM, Internet, B-ISDN)
**CLSF**			Connectionless Service Function (B-ISDN)
**CLTP**			Connectionless Transport Protocol (ISO/OSI)
**CLTS**			Connectionless Transport Service --> Verbindungsloser Transportdienst (ISO/OSI)
**CLV**				Code/Length/Value (ISO/OSI)
**CM**				Cable Modem --> Kabelmodem (Anschlussnetze)
**CM**				Channel Management
**CM**				Communication Manager (Netzmanagement)
**CM**				Configuration Management --> Konfigurationsmanagement (Netzmanagement)
**CM**				Connection Management
**CMB**				Common Management Bus
**CMC**				Common Mail Calls (Mitteilungsdienste)
**CMC**				Communication Management Configuration (Netzmanagement)
**CMC**				Computer Mediated Communication (Internet)
**CMDR**			Command Reject --> Befehlsverweigerung (HDLC)
**CME**				Circuit Multiplication Equipment
**CME**				Conformance Management Entity (Netzmanagement)
**CMF**				Call Manager Function
**CMF/PDB**			Capacity Management Facility/Perfomance Data Base
**CMI**				Coded Mark Inversion --> Pseudoternärer Leitungscode (Codierung)
**CMI**				Common Management Information
**CMI**				Controlled Mode Idle
**CMIP**			Common Management Information Protocol (ISO/OSI)
**CMIS**			Common Management Information Service (ISO/OSI)
**CMISE**			Common Management Information Service Element --> Allgemeines Management-Informationsdienstelement (Netzmanagement)
**CMISP**			Common Management Information Service and Protocol
**CMIT**			Common Management Information Transmission (Netzmanagement)
**CML**				Common Markup Language (Internet)
**CMM**				Capability Maturity Model
**CMN**				Customer Management Network (Netzmanagement)
**CMNS**			Connection Mode Network Service (X.25)
**CMOL**			CMIP over LLC --> CMIP-Variante zur Verwaltung von Ethernet- und Token Ring-Netzen (Netzmanagement)
**CMOT**			CMIP over TCP/IP --> CMIP-Variante zur Verwaltung von TCP/IP-Netzen (Netzmanagement)
**CMP**				Cluster Management Prozessor
**CMR**				Cell Misinsertion Ratio (ATM)
**CMR**				Common Mode Rejection --> Gleichtaktunterdrückung
**CMRS**			Commercial Mobile Radio Services --> Oberbegriff für öffentlichen kommerziellen Mobilfunk in USA
**CMS**				Commercial Mobile Service (Netzmanagement)
**CMS**				Computerized Message Switching
**CMS**				Corporate Mobile Server
**CMS**				Cryptographic Message Syntax (IT-Sicherheit)
**CMSE**			Common Management Information Service Element (ISO/OSI)
**CMT**				Connection Management (FDDI)
**CMTS**			Cabel Modem Termination System
**CMTS**			Connection-Mode Transport Service (ISO/OSI)
**CN**				Core Network (Weitverkehrsnetze)
**CN**				Corporate Network --> Firmennetz (NW-Konzepte)
**CN**				Customer Network --> Teilnehmernetz (NW-Konzepte)
**CNA**				Cascaded Network Architecture (NW-Konzepte)
**CNCF**			Cloud Native Computing Foundation
**CNET**			Centre National d’Etdudes des Telecommunications --> Forschungszentrum der France Telekom, Toulouse (Gremien, Organisationen)
**CNG**				Calling --> Erkennungston von Faxgeräten beim Verbindungsaufbau
**CNIDR**			Clearinghouse for Networked Information Discovery and Retrieval (Internet)
**CNIP**			Calling Name Indentification Presentation (ISDN)
**CNIR**			Calling Name Identification Restriction (ISDN)
**CNIU**			Customer Network Interface Unit
**CNM**				Communication Network Manager (Netzmanagement)
**CNM**				Customer Network Management (ATM)
**CNMA**			Communication Network for Manufacturing Application
**CNMI**			Communication Network Management Interface (Netzmanagement)
**CNN**				Composite Network Node
**CNP**				Connection not Possible --> Verbindung nicht möglich (Steuerzeichen)
**CNR**				Carrier-to-Noise Ratio
**CNRI**			Corporation for National Research Institute (Gremien, Organisationen)
**CNS**				Communication Network System (NW-Konzepte)
**CNT**				Coaxial Network Termination
**CO**				Call Offer
**CO**				Central Office --> Fernsprechamt, Vermittlungsstelle (Telekommunikation)
**CO**				Connection Oriented --> Verbindungsorientierte Übertragung
**COAX**			Coacial Cable --> Koaxialkabel (Verkabelung)
**COBRA**			Consultative, Objective and Bi-Functional Risk Analysis (IT-Sicherheit)
**COBS**			Connection Oriented Broadband Services
**COCF**			Connection Oriented Convergence Function (Stadt-, Regionalnetze)
**COD**				Connection Oriented Data (ATM)
**CODEC**			Coder/Decoder
**COFDM**			Coded Orthogonal Frequency Division Multiplexing --> Digitales Modulationsverfahren (xDSL, DAB)
**COI**				Connection-Oriented Interconnection (Zeichen, Zeichensatz)
**COLP**			Connected Line Identification Presentation --> Rufnummer-Anzeige (ISDN)
**COLR**			Connected Line Identification Restriction --> Rufnummer-Unterdrückung (ISDN)
**COM**				Communications
**COM**				Continuation of Message (ATM)
**COMAC**			TK-Unterwasserkabel zwischen Vancouver und Sydney
**COMSAT**			Communication Satellites Corporation --> Private Satelliten-Betreibergesellschaft in USA
**CON**				Connect Message (Mobilfunknetze)
**CONF**			Confirmation --> Bestätigung (Datenfelder)
**CONP**			Connected Name Indentification Presentation (ISDN)
**CONP**			Connection-Oriented Network Protocol (ISO/OSI)
**CONS**			Connection-Oriented Network Service (ISO/OSI)
**COP**				Byte Count Oriented Protocol (Protokoll)
**COPS**			Common Open Policy Service (Internetworking)
**COPS**			Connection Oriented Packet Service
**COPSK**			Coded Octal Phase Shift Keying --> Digitales Modulationsverfahren (Modulation)
**CORBA**			Common Object Request Broker Architecture --> Von der OMG standardisiertes Modell zur Beschreibung von verteilten objektorientierten Systemen
**CORS**			Cross-Origin Resource Sharing
**COS**				Communication Operating System --> Kommunikationsbetriebssystem
**COS**				Corporation for Open Systems --> Konsortium von OSI-Unterstützern (Gremien, Organisationen)
**COSE**			Common Object System Environment
**COSINE**			Cooperation for OSI Networking in Europe (Gremien, Organisationen)
**COSS**			Common Object Services Specification
**COT**				Class of Traffic --> Verkehrsklassenzeichen (Zeichen, Zeichensatz)
**COT**				Continuity Message --> Durchgangsmeldung (Datenfelder)
**COTM**			Circuit Oriented Transfer Mode (Übertragungstechniken)
**COTP**			Connection-Oriented Transport Protocol (ISO/OSI)
**COTS**			Commercial Off-the-Shelf --> Preisgünstige Standard-Software
**COTS**			Connection-Oriented Transport Service (ISO/OSI)
**COW**				Cluster of Workstation (NW-Konzepte)
**CP**				Circuit Priority
**CP**				Clock Pulse --> Taktimpuls
**CP**				Common Part
**CP**				Communication Processor
**CP**				Consolidation Point (Verkabelung)
**CP**				Content Provider --> Inhalte-Anbieter (Telekommun ikation)
**CP**				Control Plane
**CP**				Control Point
**CP**				Control Program
**CP**				Convergence Protocol
**CP**				Critical Path
**CPAP**			Customer Premises Access Profile
**CPBX**			Computerized Private Branch Exchange --> Computergesteuerte Nebenstellenanlage (Telekommunikation)
**CPC**				Call Processing Control
**CPC**				Customer Premises Cabling (Verkabelung)
**CPCS**			Common Part Convergence Sublayer (ATM)
**CPE**				Convergence Protocol Entity
**CPE**				Customer Premises Equipment --> Teilnehmer-Endgerät (ATM)
**CPFSK**			Continuous Phase Frequency Shift Keying --> Kontinuierliche Phasenfrequenzumtastung (Modulation)
**CPG**				Call Progress Message (Mobilfunknetze)
**CPI**				Characters Per Inch --> Zeichen pro Zoll (Speichertechnik)
**CPI**				Common Part Indicator
**CPI**				Common Programming Interface (Schnittstelle)
**CPIC**			Common Programming Interface for Communications (IBM)
**CPL**				Character per Line --> Zeichen pro Zeile (Datenverarbeitung)
**CPLD**			Complex Programmable Logic Device
**CPM**				Connection Point Manager
**CPM**				Continuous Phase Modulation --> Digitales Modulationsverfahren
**CPMS**			Control Point Management Services --> Managementdienste der Steuerzentrale (Netzmanagement)
**CPN**				Called Party Number
**CPN**				Calling Party Number --> Rufnummer der rufenden Endeinrichtung (ATM)
**CPN**				Customer Premises Network (Netzkonzept)
**CPODA**			Contention Priority Oriented Demand Assignment
**CPP**				Calling Party Pays (Mobilfunknetze)
**CPPP**			Compressed Point-to-Point Protocol (Protokolle)
**CPRP**			Connectivity Programming Request Block
**CPS**				Call Processing Server
**CPS**				Certificate Practice Statement (IT-Sicherheit)
**CPS**				Character per Second --> Zeichen pro Sekunde (Übertragungsgeschwindigkeit)
**CPS**				Common Part Sublayer
**CPS**				Cycles per Second --> Durchläufe pro Sekunde (Geschwindigkeit)
**CPSII**			Communications Protocol Stack Independent Interface
**CPSS**			Call Processing Subsystem (Telekommunikation)
**CPSS**			Control Packet Switching System --> Übertragungsprotokoll (Newbridge)
**CPU**				Central Processing Unit --> Zentraleinheit (PC-Technik)
**CPY**				Copy (Datenfeld)
**CR**				Call Reference --> Verbindungskennung (ISDN)
**CR**				Capture Radio
**CR**				Carriage Return --> Wagenrücklauf (Steuerzeichen)
**CR**				Cell Relay
**CR**				Circuit Reservation
**CR**				Collision Resolution
**CR**				Confidentiality Requirement
**CR**				Connection Request --> Verbindungsanforderung
**CR-LDP**			Constraint-based Label Distribution Protocol (MPLS, Internetworking)
**CRAMM**			Computer Risk Analysis and Management Method (IT-Sicherheit)
**CRC**				Cyclic Redundancy Check --> Bitfehlererkennungsverfahren
**CRD**				Custom Resource Definition
**CREN**			Corporation for Research and Educational Networking --> Netz in USA
**CRF**				Cable Retransmission Facility (Verkabelung)
**CRF**				Cell Relay Function (ATM)
**CRF**				Connection Refused --> Verbindungsablehnung
**CRF**				Connection Related Function --> Verbindungsbezogene Funktion (ATM)
**CRFP**			Cordless Radio Fixed Parts
**CRI**				Continuity Recheck Incoming --> Ankommende erneute Durchgangsprüfung
**CRJ**				Call Record Journal --> Rufdatenaufzeichnung (Telekommunikation)
**CRJE**			Conversational Remote Job Entry --> Jobverarbeitung im Dialog
**CRL**				Certificate Revocation List (IT-Sicherheit)
**CRM**				Cell Rate Margin (ATM)
**CRM**				Customer Relationship Management
**CRMA**			Cyclic Reservation Multiple Access --> Schicht-2-Protokoll (Lokale Netze)
**CRN**				Cellular Radio Network --> Zellulares Funknetz (Mobilfunknetze)
**CRQ**				Call Request --> Rufanforderung (Telekommunikation)
**CRS**				Cell Relay Service (ATM)
**CRS**				Configuration Report Server
**CRT**				Cathode Ray Tube --> Kathodenstrahlröhre
**CRT**				Computer Response Time --> Antwortzeit des Computers
**CRTP**			Compressed Real Time Protocol (Protokolle, Internet)
**CRTS**			Cellular Radio Telecommunications Service
**CRV**				Coding Rule Violation --> Verletzung der Codierungsregeln (Codierung)
**CRV**				Cryptography Verification --> Kryptographische Bestätigung (IT-Sicherheit)
**CS**				Capability Set (Telekommunikation)
**CS**				Channel Status
**CS**				Circuit Switched --> Leitungsvermittelt (Übertragungsverfahren, Weitverkehrsnetze)
**CS**				Clearing Center (Netzmanagement)
**CS**				Code Scheme (Codierung)
**CS**				Communication Software
**CS**				Communication Subsystem
**CS**				Communication Support
**CS**				Configuration Server (ATM)
**CS**				Control Station (ATM)
**CS**				Convergence Sublayer (ATM)
**CS-PDU**			Convergence Sublayer Protocol Data Unit --> Datenelement des Konvergenz-Teilschichtprotokolls
**CSA**				Carrier Serving Area (Mobilfunknetze)
**CSA**				Client Server Agent (Lokale Netze)
**CSA**				Cooperating Systems Architecture
**CSA-CELP**			Conjugate Structure Algebraic Code Exited Linear Prediction (Telekommunikation)
**CSAU**			Circuit Switched Access Unit (Stadt-, Regionalnetze)
**CSC**				Class Selector Code Point
**CSC**				Control Signal Code --> Steuerzeichen zur Zeichengabe (Steuerzeichen)
**CSC**				Customer Service Control (Weitverkehrsnetze)
**CSCW**			Computer Supported Cooperative Work
**CSD**				Circuit Switched Data
**CSDN**			Circuit Switched Data Network --> Leitungsvermittelndes Datennetz (Weitverkehrsnetze)
**CSFI**			Communication Subsystem for Interconnect
**CSG**				Call Signalling Gateway
**CSI**				Control Signal Indicator --> Einleitungszeichen für Steuerfolge (Zeichen, Zeichensatz)
**CSI**			 	Clustering Services Infrastructure (VMware)
**CSL**				Control Service Link
**CSLBA**			Continuous-State Leaky Bucket Algorithm
**CSLIP**			Compressed Serial Line Interface Protocol --> Variante von SLIP mit Komprimierung (Protokolle)
**CSM**				Call Supervision Message
**CSM**				Customer Service Management
**CSMA**			Carrier Sense Multiple Access --> Kanal-Zugriffsverfahren (Lokale Netze)
**CSMA/CA**			Carrier Sense Multiple Access (with) Collision Avoidance --> Zugriffsverfahren (IEEE 802.11, WLAN)
**CSMA/CD**			Carrier Sense Multiple Access (with) Collision Detection --> Zugriffsverfahren (IEEE 802.3, Ethernet)
**CSN**				Circuit Switched Network
**CSN**				Computer Science Network --> Forschungsnetz
**CSNET**			Content Storage Network --> Speichernetz
**CSNP**			Complete Sequence Numbers PDU (Internetworking)
**CSO**				Central Services Organisation
**CSO**				Composite Second Order (Stadt-, Regionalnetze)
**CSO**				Computing Services Office
**CSP**				Certified Service Provider (Internet)
**CSP**				Communication Service Provider (Telekommunikation)
**CSP**				Content Security Policy
**CSP**				Cross System Product
**CSPDN**			Circuit Switched Public Data Network --> Leitungsvermittelndes öffentliches Netz (Weitverkehrsnetze)
**CSR**                         Certificate Signing Request
**CSRCI**			Content Source Identifier (Netz-, Transportprotokolle)
**CSRF**			Cross-Site Request Forgery
**CSS**				Cascading Style Sheets
**CSS**				Cascading Style Sheets (Internet)
**CSS**				Connection Successful Signal
**CSS7**			Channel Signalling System No. 7 (B-ISDN)
**CSSRL**			Control Channel Segmentation and Reassembly Layer
**CST**				Computer Supported Telephony
**CSTA**			Computer Supported Telecommunications Application (Tk-Sprachdienste)
**CSTA**			Computer Supported Telephony Application
**CSU**				Channel Service Unit --> Kanaldiensteinheit (ATM)
**CSV**				Capability Set Values
**CSW**				Channel Status Word --> Kanalstatus-Wort
**CT**				Call Transfer (Mobilfunknetze)
**CT**				Certificate Transparency
**CT**				Conformance Testing --> Konformitätstest
**CT**				Cordless Telephony --> Schnurlose Telefonie (Mobilfunknetze)
**CT**				Customer Terminal
**CTA**				Cordless Terminal Adapter
**CTB**				Communication Tool Box
**CTBA**			Composite Triple Beat Area (Stadt- u. Regionlanetze)
**CTC**				Channel-to-Channel
**CTCA**			Channel-to-Channel Adapter
**CTCP**			Cell Transfer Delay (ATM)
**CTCP**			Client to Client Protocol (Protokolle)
**CTCP**			Communication and Transmission Control Program (IBM/SNA)
**CTD**				Cell Transfer Delay (ATM)
**CTE**				Channel Translating Equipment
**CTE**				Customer Terminal Equipment (Telekommunikations-Endgeräte)
**CTE**				Customer Trading Exchange (Online-Dienste)
**CTERM**			Command Terminal (DECnet)
**CTI**				Computer Telephony Integration --> Computer Telefonie Integration (Telekommunikation)
**CTIA**			Cellular Telecommunications Industry Association (Gremien, Organisationen)
**CTM**				Circuit Transfer Mode
**CTM**				Cordless Terminal Mobility
**CTN**				Circuit Transfer Mode (DK-Übertragungstechniken)
**CTN**				Corporate Telecommunications Network
**CTP**				Configuration Testing Protocol
**CTP**				Connection Termination Point
**CTR**				Common Technical Regulations (ETSI)
**CTRL**			Common Technical Regulations (ISDN)
**CTS**				Clear to Send --> Sendebereitschaft (Modem)
**CTS**				Clear-to-Send --> Sendebereitschaft (Modem)
**CTS**				Common Transport Semantics
**CTS**				Conformance Test Service (CCITT)
**CTS**				Cordless Telephone System
**CU**				Capacity Unit
**CU**				Control Unit --> Steuereinheit (Netzkomponente)
**CU**				Currently Unused --> Feld in IP-Header
**CU**				Kupfer (Verkabelung)
**CUA**				Common User Access --> Gemeinsamer Benutzerzugriff (Hersteller-NW-Architekturen)
**CUG**				Closed User Group --> Geschlossene Benutzergruppe (ITU-T X.25)
**CUSF**			Call-Unrelated Service Function
**CUSI**			Comprehensive User Search Interface
**CUSN**			Communication Controller Subarea Norde
**CUSP**			Call-Unrelated Service Point
**CUT**				Cluster Unit Terminal
**CUT**				Control Unit Terminal (Hersteller-NW-Architekturen)
**CUT**				Control Unit Terminal (IBM)
**CVE**				Common Vulnerabilities and Exposures 
**CVE**				Common Vulnerabilities and Exposures (IT-Sicherheit)
**CVP**				Content Vectoring Protcol (Internet, Protokolle)
**CVSD**			Continous Variable Slope Delta (Modulation)
**CVSDM**			Continuous Variable Slope Delta Modulation --> Verfahren zur Umwandlung analoger Sprache in Datenstrom (Bluetooth)
**CVSS**			Common Vulnerabilities Scoring System (IT-Sicherheit)
**CVSS**			Common Vulnerability Scoring System
**CW**				Call Waiting --> Anklopfen (ISDN)
**CW**				Carrier Wave --> Trägerfrequenz
**CW**				Continuous Wave
**CWD**				Change Working Directory --> Wechsel des Arbeitsverzeichnisses (Verzeichnisdienste)
**CWDM**			Coarse Wave Division Multiplex (Optische Netze, SDH, Sonet)
**CWE**				Common Weakness Enumeration
**CWI**				Call Waiting Internet Busy
**CWT**				Continuous Wavelet Transformation --> Codierungsverfahren
**Cdma2000**			TIA-/EIA-/ITU-Standard für Zugriffsverfahren für den zellularen Mobilfunk der 3. Generation
**CdmaOne**			Zellularer US-Mobilfunkstandard der 2. Generation
**CoS**				Class of Service (IBM/SNA)
**CuDA**			Kupfer-Doppelader (Anschlussnetze)
**D-AMPS**			Digital Advanced Mobile Phone System (Mobilfunknetze)
**D/A**				Digital/Analog
**DA**				Demand Assignment
**DA**				Destination Address --> Zieladresse
**DA**				Direct Access --> Direkter Telefonanschluss (Anschlussnetze)
**DA**				Doppelader (Verkabelung)
**DAA**				Data Access Arrangement (Übertragungstechniken)
**DAB**				Datenanschaltbaugruppe (Weitverkehrsnetze)
**DAB**				Digital Audio Broadcasting
**DAC**				Data Acquisition and Control
**DAC**				Digital-to-Analogue Converter
**DAC**				Dual Attachment Concentrator (FDDI)
**DACS**			Digital Access and Crossconnect System (Weitverkehrsnetze)
**DACTLU**			Deactivate Logical Unit --> Logische Einheit deaktivieren
**DACTPU**			Deactivate Physical --> Physikalische Einheit deaktivieren
**DACU**			Device Attachment Control Unit
**DAD**				Draft Addendum --> Zusatz zu einem Normenentwurf (Standards, Verordnungen, Gesetze)
**DAE**				Distributed Application Environment (NW-Konzepte)
**DAEDR**			Delimitation, Alignment, Error Detection, Reception
**DAEDT**			Delimitation, Alignment, Error Detection, Transmitting
**DAEMON**			Disk and Execution Monitor --> Dämonprozess
**DAF**				Destination Address Field --> Zieladresse
**DAG**				Datenanschlußgerät
**DAGt**			Datenanschlussgerät (Weitverkehrsnetze)
**DAI**				Digital Audio Interface --> Digitale Audioschnittstelle (Mobilfunknetze)
**DAL**				Drahtlose Anschlußleitung (Mobilfunknetze)
**DAM**				DECT Authentication Module (Mobilfunknetze)
**DAM**				Data Aquisition and Monitoring
**DAM**				Direct Access Method --> Direkte Zugriffsmethode
**DAM**				Draft Amendment
**DAMA**			Demand Assignment Multiple Access --> Mehrfach-Zugriffsmethode
**DAMQAM**			Dynamically Adaptive Multicarrier Quadrature Amplitude Modulation (Modulationsverfahren)
**DAN**				DECT Access Node
**DAN**				Datennetzabschlussgerät (Anschlussnetze)
**DAN**				Desk Area Network
**DANE**			DNS-based Authentication of Named Entities
**DANN**			Digital Network Architecture (Offene NW-Konzepte)
**DAO**				Data Offset
**DAP**				Data Access Protocol (DECnet) (Protokolle)
**DAP**				Distributed Access Point
**DAP**				Document Application Profiles
**DAPSK**			Differential Amplitude Phase Shift Keying (Modulationsverfahren)
**DAQ**				Data Acquisition --> Datenerfassung
**DAR**				Data Access Register
**DAR**				Dynamic Alternate Routing
**DARPA**			Defense Advanced Research Projects Agency (US DoDD, Gremien, Organisationen)
**DARS**			Digital Audio Radio Service (Sat-Kommunikation)
**DAS**				Database Access Service
**DAS**				Digital Accouncement System (Offene NW-Konzpte)
**DAS**				Direct Attached Storage (Speichernetze)
**DAS**				Directory Access Service --> Verzeichniszugangsdienst (Verzeichnisdienste)
**DAS**				Dual Attached Station (FDDI)
**DAS**			 	Distributed Availability Service
**DASAT**			Datenübertragung über Satellit (Sat-Kommunikation)
**DASE**			Directory Access Service Element (Verzeichnisdienste)
**DASS**			Digital Access Signalling System (Telekommunikation)
**DAST**			Datenaustauschsteuerung
**DAST**			Dynamic Application Security Testing
**DAT**				Digital Audio Tape
**DATE**			Dedicated Access to X.25 Transport Extension (Protokolle)
**DATEL**			Data Telecommunications
**DATEX**			Data Exchange
**DATEX-J**			Data Exchange Jxxxx --> Dienst der Deutschen Telekom im Fernsprechnetz
**DATEX-L**			Data Exchange Leitungsvermittlung --> Dienst der Deutschen Telekom im Fernsprechnetz
**DATEX-M**			Data Exchange Mxxxx --> Dienst der Deutschen Telekom im Fernsprechnetz
**DATEX-P**			Data Exchange Paketvermittlung --> Dienst der Deutschen Telekom im Fernsprechnetz
**DAV**				Data Above Voice --> Daten über Sprache (NW-Konzepte)
**DAV**				Data Valid --> Gültige Daten
**DAVIC**			Digital Audio and Video Council (Gremien, Organisationen)
**DAWS**			Digital Advanced Wireless Services (Mobilfunknetze)
**DAYTIME**			Daytime Protocol (RFC 867)
**DB**				Database --> Datenbank
**DB**				Dummy Burst (GSM)
**DBAM**			Directory Basic Access Method (Verzeichnisdienste)
**DBCS**			Double Byte Character Set --> Doppelbyte-Zeichensatz
**DBE**				Digital Branch Exchange
**DBER**			Data Block Error Rate
**DBMS**			Data Base Management System
**DBR**				Deterministic Bit Rate (ATM)
**DBS**				Digital Broadcast Satellite
**DBS**				Direct Broadcasting System
**DBX**				Digital Branch Exchange --> Digitale Nebenstellenanlage (Netzkomponente)
**DBaaS**		 	Database as a Service
**DC**				Data Center
**DC**				Data Communication --> Datenkommunikation (Telekommunikation)
**DC**				Device Control --> Gerätesteuerung
**DC**				Direct Current --> Gleichstrom
**DC**				Disconnect Confirm --> Verbindungsabbruchbestätigung (Steuerzeichen)
**DC**				Dounstream Channel
**DCA**				Defense Communications Agency (Gremien, Organisationen)
**DCA**				Distributed Communications Architecture --> Architektur verteilter Systeme (Offene NW-Konzepte)
**DCA**				Document Content Architecture (IBM)
**DCA**				Dynamic Channel Allocation (Mobilfunknetze)
**DCAF**			Distributed Console Access Facility
**DCAM**			Data Communications Access Method
**DCAP**			Data Link Switching Client Access Protocol
**DCB**				Data Control Block (Steuerzeichen)
**DCBX**			Distributed Computerized Branch Exchange --> Verteilte computerunterstützte Nebenstellenanlage (Netzkomponente)
**DCC**				Data Collection Centre
**DCC**				Data Communication Channel (Optische Netze, SDH, Sonet)
**DCC**				Data Country Code (ISO/OSI)
**DCC**				Digital Cross Connect
**DCCH**			Dedicated Control Channel --> Steuerkanal im Mobilfunk (GSM, Mobilfunknetze)
**DCCS**			Digital Cross Connect System
**DCCU**			Data Communication Control Unit
**DCD**				Data Carrier Detect --> Modemsignal
**DCE**				Data Circuit-Terminating Equipment --> Datenübertragungseinrichtung
**DCE**				Data Collection Engine
**DCE**				Data Communications Equipment
**DCE**				Distributed Computing Environment --> Umgebung für verteilte Datenverarbeitung (Offene NW-Konzepte)
**DCF**				Data Communications Function
**DCF**				Dispersion Compensating Fiber (Verkabelung)
**DCF77**			Digital Code Frequency on 77,5 kHz
**DCH**				Dedicated Channel --> Zugeordnete Kanäle im Mobilfunk (Mobilfunknetze)
**DCI**				Data Communication Interface
**DCIT**			Digital Compression of Increased Transmission
**DCK**				Derived Cipher Key (IT-Sicherheit)
**DCL**				Device Clear --> Gerät im Grundzustand
**DCM**				Data Communication Method
**DCME**			Digital Circuit Multiplication Equipment --> Leitungsvervielfacher
**DCMS**			Data Communication Management System (NW-Konzepte)
**DCN**				Data Communication Network
**DCOM**			Distributed Component Object Model
**DCP**				Data Compression Protocol over Frame Relay (Protokolle, Frame Relay)
**DCP**				Device Control Protocol --> Gerätesteuerungsprotokoll (Protokolle)
**DCP**				Distributed Communication Processor --> Rechner für verteilte Kommunikation
**DCP**				Domain Control Point
**DCPCP**			DCP Control Protocol (Protokolle, Frame Relay)
**DCPS**			Digital Copy Protection System (Speichertechnik)
**DCS 1800**			Digital Cellular System 1800 MHz --> DCS-1800-Netz (Mobilfunknetze)
**DCS**				Defined Context Set (DK-Dienste)
**DCS**				Digital Cellular System
**DCS**				Digital Command Signal
**DCS**				Digital Communications System
**DCS**				Digital Crossconnect System
**DCS**				Distributed Communications System
**DCT 900**			Digital Cordless Telephony at 900 MHz
**DCT**				Data Calling Tone
**DCT**				Digital Cordless Telephony
**DCT**				Discrete Clock Transmitter
**DCT**				Discrete Cosine Transformation
**DCV**				Digital Coded Voice --> Digital codierte Sprache (Codierung)
**DD**			 	Data Director (VMware)
**DDA**				Domain Defined Attribute (Mobilfunkdienste)
**DDB**				Distributed Database --> Verteilte Datenbank
**DDB**				Downlink Data Block
**DDC**				Dynamic Data Channel (Telekommunikation)
**DDCMP**			Digital Data Communications Message Protocol --> Nachrichtenorientiertes Punkt-zu-Punkt-Protokoll (DEC)
**DDD**				Direct Distance Dialling --> Selbstfernwähldienst (Weitverkehrsnetze)
**DDE**				Dynamic Data Exchange
**DDF**				Digital Distribution Frame (Weitverkehrsnetze)
**DDGL**			Device Dependent Graphic Layer (DK-Dienste)
**DDI**				DNS, :abbr:`DHCP (Dynamic Host Configuration Protocol)` and IPAM
**DDI**				Direct Dialing In --> Durchwahl (ISDN)
**DDLCN**			Distributed Double Loop Computer Network
**DDM**				Distributed Data Management Architecture (IBM/LU6.2)
**DDN**				Defense Data Network
**DDN**				Digital Data Network
**DDNS**			Dynamic Domain Name Server
**DDP**				Datagram Delivery Protocol (AppleTalk) (Protokolle)
**DDP**				Digital Data Processor
**DDP**				Distributed Data Processing --> Verteilte Datenverarbeitung
**DDR**				Dial on Demand Routing (Internetworking)
**DDS**				Dataphone Digital Services (Telekommunikation)
**DDS**				Digital Data Service
**DDS**				Digital Data System
**DDS**				Direct Digital Synthesis --> Realisierungsvariante für Modulationsverfahren
**DDS**				Document Distribution Services
**DDV**				Datendirektverbindung
**DDoS**			Distributed Denial Of Services
**DDoS**			Distributed Denial of Service (IT-Sicherheit)
**DE**				Datenerfassung
**DE**				Discard Eligibility (Frame Relay)
**DEA**				Data Encryption Algorithm (IT-Sicherheit)
**DEB**				Data Event Block
**DEC**				Decoder
**DEC**				Digital Echo Cancellation (DK-Übertragungstechniken)
**DEC**				Digital Equipment Corporation (Gremien, Organisationen)
**DECT**			Digital Enhanced Cordless Telecommunication
**DECnet**			Digital Equipment Corporation's Proprietary Network Architecture (NW-Konzepte)
**DED**				Dynamical Established Data
**DEE**				Datenendeinrichtung
**DEGt**			Datenendgerät (Telekommunikations-Endgeräte)
**DEKITZ**			Deutsche Akkreditierungsstelle für Informations- und Kommunikationstechnik (Gremien, Organisationen)
**DEL**				Delete --> Löschen (Steuerzeichen)
**DEL**				Direct Exchange Line
**DEM**				Demultiplex (DK-Übertragungstechniken)
**DEN**				Directory Enabled Network (Netzmanagement)
**DEN**				Distribution Eelement Nname --> Verteilungselementname
**DENIC**			Deutsches Network Information Center (Gremien, Organisationen)
**DEPL**			Deployment of Certificate and Key Material
**DEPSK**			Differential Encoded Phase Shift Keying (Modulationsverfahren)
**DES**				Data Encryption Standard
**DES**				Data Encryption Standard (IT-Sicherheit)
**DES**				Destination End Station (ATM, B-ISDN)
**DES**				Distributed Systems Environment
**DET**				Detach Nachricht (ISDN)
**DEU**				Data Exchange Unit --> Datenvermittlungseinrichtung (Netzkomponente)
**DF**				Data Flag --> Datenflag (Zeichen, Zeichensatz)
**DF**				Default Forwarding (ATM, B-ISDN)
**DF**				Don´t Fragment (IP-Protokollelement)
**DFB**				Datenfernschaltbaugruppe (Netzkomponente)
**DFB**				Distributed Feedback (Laser)
**DFC**				Data Flow Control --> Datenflusskontrolle (DK-Übertragungstechniken)
**DFDSM**			Data Facility Distributed Manager
**DFE**				Decision Feedback Equalisation
**DFFT**			Discrete Fast Fourier Transformation
**DFGt**			Datenfernschaltgerät (Anschlussnetze)
**DFI**				Domain Specific Part Format Identifier --> NSAP (ISO/OSI)
**DFIC**			Dual Fabric Interface Card
**DFN**				Deutsches Forschungsnetz
**DFP**				Distributed Functional Plane
**DFPDN**			Digital Fixed Public Data Network (Weitverkehrsnetze
**DFR**				Document Filing and Retrieval (DK-Dienste)
**DFS**				Deutscher Fernmeldesatellit
**DFS**				Distributed File System
**DFSG**			Datenfernschaltgerät (Netzkomponente)
**DFSK**			Direct Frequency Shift Keying --> Direkte Frequenzumtastung (Modulationsverfahren)
**DFSM**			Dispersion Flatted Single Mode (Verkabelung)
**DFT**				Diagnostic Function Test (Netzmanagement)
**DFT**				Discrete Fourier Transformation
**DFT**				Distribution Functional Terminal (Hersteller-NW-Architekturen)
**DFV**				Datenfernverarbeitung
**DFWMAC**			Distributed Foundation Wireless Media Access Control (IEEE 802.11)
**DFÜ**				Datenfernübertragung
**DGCRA**			Dynamic Generic Cell Rate Algorithm
**DGD**				Diffenential Group Delay --> Differenzielle Gruppenlaufzeit (Optische Netze, SDH, Sonet)
**DGN**				Distribution Group Name --> Verteilungsgruppenname
**DGPS**			Diffenential Global Positioning System
**DGT**				Digit --> Ziffer (Zeichen, Zeichensatz)
**DGW**				Decomposed Gateway
**DH**				Distribution Hub
**DHA**				Diffie Hellman Algorithm (IT-Sicherheit)
**DHCP**			Dynamic Host Configuration Protocol --> Anwendungsprotokoll im Internet
**DHCP**	            	Dynamic Host Configuration Protocol
**DHE**				Data Handling Equipment
**DHS**				Data Handling System
**DHSD**			Duplex High Speed Data
**DHTML**			Dynamic Hypertext Markup Language
**DI**				Device Interface
**DIA**				Document Interchange Architecture (IBM/SNA)
**DIAG**			Diagnostic Responder Protocol (Novell) (Protokolle)
**DIAM**			Digital Announcement Module
**DIANE**			Direct Information Access Network for Europe (Hersteller-NW-Architekturen)
**DIB**				Directory Information Base (X.500, Verzeichnisdienste)
**DID**				Direct Inward Dialing --> Anrufweiterleitung
**DIF**				Document Interchange Format (IBM/LU6.2)
**DIFS**			Distributed Coordination Function Inter Frame Space (Wireless LAN)
**DIGL**			Device-Independent Graphics Layer (DK-Dienste)
**DIGON**			Digitales Ortsnetz (Weitverkersnetze)
**DIGS**			Device-Independent Graphics Service (DK-Dienste)
**DII**				Downlink Information Indication
**DIIS**			Digital Interchange of Information and Signalling
**DIML**			Digitale Internationale Mietleitung
**DIN**				Deutsche Industrie Norm (Verbandszeichen des Deutschen Instituts für Normung e.V.)
**DIN**				Deutsche Institut für Normung e. V.
**DIN**				Deutsches Institut für Normung e.V. (Gremien, Organisationen)
**DIPRA**			Digital Professional Mobile Radio (Mobilfunk)
**DIR**				Diagnose Responder (Netzmanagement)
**DIR**				Direction
**DIR**				Directory --> Verzeichnis
**DIRC**			Digital Inter Relay Communication (Anschlußnetze)
**DIS**				Draft International Standard (Standards, Verordnungen, Gesetze)
**DISA**			Defense Information System Agency (Gremien, Organisationen)
**DISC**			Disconnected --> Beendet (Steuerzeichen)
**DISCO**			Digital-Subcarrier-On-Satellite Technology
**DISOSS**			Distributed Office Support System (IBM/LU6.2))
**DISP**			Directory Information Shadowing Protocol (Protokolle, Verzeichnisdienste)
**DISP**			Disposal
**DISP**			Draft International Standard Profile (Standards, Verordnungen, Gesetze)
**DISSOS**			Distributed Office Support System (Hersteller-NW-Architekturen)
**DIT**				Directory Information Tree (X.500, Verzeichnisdienste)
**DIU**				Digital Indoor Unit (Sat-Kommunikation)
**DIU**				Digital Interface Unit
**DIU**				Document Interchange Unit --> Dokumentenaustauscheinheit (Hersteller-NW-Architekturen)
**DIV**				Digitale Vermittlung
**DIVO**			Digitale Vermittlungsstelle für den Ortsdienst (Weitverkehrsnetze)
**DIW**				Delivery Interworking (Mobilfunknetze)
**DIX**				Digital, Intel, Xerox (Ethernet)
**DK**				Dienstekennung (ISDN)
**DKE**				Deutsche Kommission für Elektrotechnik in DIN und VDE (Gremien, Organisationen)
**DKIM**			Domain Keys Identified Mail
**DKZ**				Datenkonzentrator (Netzkomponente)
**DKZ**				Dienstekennzahl (Weitverkehrsnetze)
**DKZ-E**			Digitale Kennzeichengabe auf Endstellenleitungen (ISDN)
**DL**				Data Link
**DL**				Distribution List --> Verteilerliste (Mobilfunkdienste)
**DL**				Downlink
**DLC**				Data Link Control --> Datenverbindungssteuerung
**DLC**				Digital Leased Circuit --> Digitale Standleitung (Weitverkehrsnetze)
**DLC**				Digital Line Carrier (Telekommunikation)
**DLCI**			Data Link Connection Identifier (Frame Relay)
**DLCN**			Distributed Loop Computer Network
**DLE**				Data Link Escape --> Datenübertragungssteuerzeichen (Steuerzeichen)
**DLL**				Data Link Layer --> Sicherungsschicht (ISO/OSI)
**DLL**				Dial Long Lines --> Fernwahl
**DLM**				Data Line Monitor
**DLM**				Data Link Mapping
**DLM**				Distributed Lock Manager (Netzkoomponente)
**DLP**				Data Loss Prevention
**DLPD**			Data Link Protocol Description
**DLPDU**			Data Link Protocol Data Unit
**DLPI**			Data Link Provider Interface (ISDN)
**DLR**				Dependent Logical Requester (Hersteller-NW-Architekturen)
**DLS**				Data Link Switching (IBM/SNA)
**DLS**				Directory Location Service
**DLS**				Distributed Load Sharing --> Lastverteilung (ISDN)
**DLSAP**			Destination Link Service Access Point
**DLT**				Decision Logical Table
**DLU**				Dependent Logical Unit
**DLU**				Destination Logical Unit
**DLU**				Digital Line Unit
**DLUR**			Dependent Logical Unit Requester (Internetworking)
**DLUS**			Dependent Logical Unit Server
**DM**				Data Management
**DM**				Degraded Minutes
**DM**				Delta Modulation
**DM**				Dienstmerkmal (ISDN)
**DM**				Disconnect Mode --> Abgebrochen (HDLC)
**DM**				Disconnected Mode
**DM**				Distribution Manager
**DMA**				Direct Memory Access
**DMAP**			DECT Multimedia Access Profile
**DMAPI**			Data Management Application Programming Interface (Speichernetze)
**DMARC**			Domain-based Message Authentication Reporting and Conformance
**DMD**				Differential Mode Delay (Verkabelung)
**DMD**				Directory Management Domain (Verzeichnisdienste)
**DME**				Digital Multiplex Equipment (Netzkomponente)
**DME**				Distributed Management Environment (Netzmanagement)
**DMF**				Distributed Management Facility (Netzmanagement)
**DMI**				Definition of Management Information (Netzmanagement)
**DMI**				Desktop Management Interface (Netzmanagement)
**DMI**				Digital Multiplexed interface --> Digital-Multiplex-Schnittstelle (ISDN)
**DMIS**			Dimensional Measuring Interface Specification
**DMO**				Direct Mode Operation (Mobilfunknetze)
**DMO**				Domain Management Organization
**DMP**				Diagnostic and Monitoring Protocol (Protokolle, Netzmanagement)
**DMP**				Drive Management Protocol (Protokolle)
**DMPDU**			Derived MAC Protocol Data Unit
**DMS**				Document Management System
**DMSK**			Differential Minimum Shift Keying (Modulation)
**DMSP**			Distributed Mail System Protocol (Protokolle)
**DMT**				Discrete Multi Tone --> Digitales Modulationsverfahren (xDSL)
**DMTF**			Distributed Management Task Force (Gremien, Organisationen)
**DMZ**				De-Militarized Zone --> Durch Firewalls geschützter Bereich zwischen unsicherem Extranet und sicherem Intranet (IT-Sicherheit)
**DMZ**				demilitarized zone 
**DN**				Dedicated Network
**DN**				Directory Number
**DN**				Distinguished Name
**DN**				Distributed Network
**DNA**				Digital Network Architecture (DEC)
**DNAE**			Datennetzabschlusseinrichtung
**DNAE**			Datennetzabschlusseinrichtung (Anschlussnetze)
**DNCP**			DECnet Phase IV Control Protocol (DECnet) (Protokolle)
**DND**				Do Not Disturb (ISDN)
**DNDO**			Do Not Disturb Override (ISDN)
**DNG**				Datennetzabschlussgerät
**DNHR**			Dynamic Non-hierarchical Routing
**DNI**				Data Network Identification
**DNIC**			Data Network Identification Code (Weitverkehrsnetze)
**DNID**			Data Network Identity (Sat-Kommunikation)
**DNIS**			Dialed Number Identification Service (ISDN)
**DNKZ**			Datennetzkontrollzentrum (Netzmanagement)
**DNS**				Domain Name Service --> Anwendungsprotokoll im Internet (RFC 1034, 1035)
**DNS**                     	Domain Name System
**DNSO**			Domain Name Supporting Organization
**DNSSEC**			Domain Name System Security Extensions
**DOC**				Distributed Object Computing (NW-Konzepte)
**DOCSIS**			Data over Cable Service Interface Specification (Anschlussnetze)
**DOD**				Direct Outward Dialing (Telekommunikation)
**DOE**				Distributed Objects Everywhere
**DOI**				Digital Object Identifier
**DOMF**			Distributed Object Management Facility
**DOMSAT**			Domestic Satellite (Sat-Kommunikation)
**DOP**				Directory Operational Binding Management Protocol (Protokolle, Verzeichnisdienste)
**DOS**				Denial of Services
**DOS**				Disk Operating System
**DOSF**			Distributed Office Support Facility (DK-Dienste)
**DOV**				Data over Voice --> Modemtechnik
**DP**				Data Processing --> Datenverarbeitung
**DP**				Demand Priority (Übertragungsverfahren)
**DP**				Destination Port
**DP**				Detection Piont
**DP**				Dial Pulse --> Wählimpuls (Telekommunikation)
**DP**				Draft Proposal --> Vorschlagsentwurf (Standards, Verordnungen, Gesetze)
**DPA**				Demand Protocol Architecture (3Com)
**DPA**				Department of Defense Protocol Architecture --> 4-Schichten-Kommunikationsarchitektur
**DPAP**			Demand Priority Access Protocol (Protokolle, Lokale Netze)
**DPC**				Destination Point Code (ISDN)
**DPC**				Distributed Process Control
**DPCCH**			Dedicated Physical Control Channel (GSM, Mobilfunk)
**DPCH**			Dedicated Physical Channel (GSM, Mobilfunk)
**DPCM**			Differential Pulse Code Modulation (Modulation)
**DPDCH**			Dedicated Physical Data Channel (GSM, Mobilfunk)
**DPDU**			Data Link Protocol Data Unit
**DPE**				Data Processing Equipment --> Datenverarbeitungsanlage
**DPG**				Dedicated Packet Group (FDDI)
**DPG**				Destination Point Code (ISDN)
**DPL**				Digital Power Line (Anschlussnetze)
**DPLL**			Dual Phased Locked Loop
**DPM**				Decentralized Production Management (Online-Dienste)
**DPM**				Digital Phase Modulation (Modulation)
**DPM**			 	Distributed Power Management (VMware)
**DPN**				Data Packet-Switched Network (NW-Konzepte)
**DPNSS**			Digital Private Network Signalling System
**DPO**				Data Phase Optimization
**DPP**				Demand Priority Protocol (Protokolle, Ethernet)
**DPPX**			Distributed Processing Programming Executive
**DPRS**			DECT Packet Radio Service (Mobilfunknetze)
**DPS**				Data Presentation Service
**DPSK**			Differential Phase Shift Keying (Modulation)
**DPT**				Dynamic Packet Transfer
**DPVSt**			Datenpaketvermittlungsstelle (Weitverkehrsnetze)
**DQ**				Directory Enquiry
**DQDB**			Distributed Queue Dual Bus (IEEE 802.6)
**DQP**				Distributed Queue Protocol (Protokolle)
**DQPSK**			Differential Quadrature Phase Shift Keying (Modulation)
**DQS**				Deutsche Gesellschaft zur Zertifizierung von Managementsystemen mbH (Gremien, Organisationen)
**DR**				Definitive Response
**DR**				Designated Router (Internetworking)
**DR**				Disaster Recovery
**DR**				Disconnect Request --> Verbindungsabbruchanforderung (Steuerzeichen)
**DR**				Draft Recommendation --> Empfehlung für einen Normentwurf
**DR**				Draht (Verkabelung)
**DRA**				DECT Radio Access
**DRBG**			deterministic random bit generator
**DRC**				Data Access Control System Return Code
**DRCS**			Dynamically Redefinable Character Set
**DRCS**			Dynamically Redefinable Character Set --> Dynamisch frei veränderbarer Zeichensatz (ATM, B-ISDN)
**DRF**				Data Run Flag
**DRI**				Defense Research Internet (NW-Konzepte)
**DRI**				Definite Response Indicator
**DRM**				Data Replication Manager (Speichernetze)
**DRM**				Digital Radio Mondial
**DRP**				Distribution and Replicaton Protocol (Protokolle)
**DRR**				Disaster Risk Reduction
**DRS**				Data Rate Select (Steuerzeichen)
**DRS**				Data Retrieval System
**DRS**				Digital (Microwave) Radio System
**DRS**				Digitales Richtfunksystem
**DRS**				Distributed Resource Scheduler
**DRS**				Document Recognition System --> Aktenerkennungssystem
**DRS**			 	Distributed Resources Scheduler (VMware)
**DRX**				Discontinuous Reception --> Nichtkontinuierlicher Empfang
**DS 0, 1, 2, 3, 4**		Digital Signal Level No. 0, 1, 2, 3, 4
**DS**				Differentiated Services
**DS**				Digital Section --> Digitale Teilstrecke
**DS**				Digital Signal
**DS**				Digital Signaling Level --> Übertragungshierarchie in USA
**DS**				Digital Switsching
**DS**				Direct Sequence
**DS**				Directory Services (ITU-T X.500)
**DS**				Distributed System --> Verbundsystem (Offene NW-Konzepte)
**DS**				Distribution System
**DS**				Draft Standard --> Standard-Entwurf (Standards, Verordnungen, Gesetze)
**DS2Vt**			Digitaler Signalverteiler für 2 Mbit/s
**DSA**				Data Service Adapter (Mobilfunknetze)
**DSA**				Digital Signature Algorithm
**DSA**				Digital Signature Algorithm (IT-Sicherheit)
**DSA**				Directory System Agent (X.500, Verzeichnisdienste)
**DSA**				Distributed Systems Architecture (Offene NW-Konzepte)
**DSAF**			Destination Subarea Address Field
**DSAP**			Destination Service Access Point (ISO/OSI)
**DSB**				Datenschutzbeauftragter (IT-Sicherheit)
**DSB**				Double Side Band --> Zweiseitenband (Modulation)
**DSB-AM**			Double Side Band Amplitude Modulation (Modulation)
**DSBM**			Designated Subnet Bandwidth Manager (ATM, B-ISDN)
**DSCP**			Differentiated Services Code Point (IP Header Field)
**DSD**				Data Structure Definition
**DSDV**			Destination-Sequenced Distance-Vector
**DSE**				Data Switching Equipment
**DSE**				Data Switching Exchange --> Datenvermittlungsstelle (Netzkomponente)
**DSE**				Distributed Single Layer Embedded
**DSE**				Distributed Systems Environment
**DSF**				Directory System Function (Verzeichnisdienste)
**DSF**				Dispersion Shifted Fiber --> Kompensierte Einmodenfaser (Verkabelung)
**DSI**				Digital Speech Interpolator
**DSI**				Distributed System Interworking
**DSK**				Datenschutzkommission (IT-Sicherheit)
**DSL**				Digital Subscriber Line --> Digitale Übertragungstechnik im Telefonanschlußleitungsnetz (xDSL)
**DSL**				Distributed Service Logic (NW-Konzepte)
**DSL**				Distribution Service Level (Hersteller-NW-Architekturen)
**DSLAM**			Digital Subscriber Line Access Multiplexer --> DSL-Multiplexer in der Vermittlungsstelle (xDSL)
**DSM**				Distributed Storage Management
**DSMA**			Digital Sense Multiple Access (Lokale Netze)
**DSMCC**			Digital Storage Media Command and Control --> Audio Visual over ATM
**DSMP**			Distributed Session Management Protocol (Protokolle)
**DSMX**			Digital-Signal-Multiplexer/-Multiplexgerät (Weitvekehrsnetze)
**DSN**				Distributed Systems Network (NW-Konzepte)
**DSNX**			Distributed System Node Executive
**DSOM**			Distributed System Object Model
**DSP**				Data Stream Profile
**DSP**				Digital Signal Processing
**DSP**				Directory System Protocol (ISO/OSI)
**DSP**				Domain Specific Part --> NSAP (ISO/OSI)
**DSPU**			Downstream Physical Unit (Internet-Protokolle)
**DSR**				Data Set Ready --> Betriebsbereitschaft (Steuerzeichen)
**DSR**				Datenstationsrechner (Netzkomponente)
**DSR**				Digital Satellite Radio
**DSR**				Digitaler Satellitenrundfunk
**DSR**				Dynamic Source Routing
**DSRC**			Digital Short Range Communication (Mobilfunknetze)
**DSRR**			Digital Short Range Radio (Mobilfunknetze)
**DSS**				Data Security Standard
**DSS**				Digital Signature Standard (IT-Sicherheit)
**DSS**				Disconnect Send State
**DSS**				Distributed Sample Scrambler (ATM, B-ISDN)
**DSS**				Distributed System Service (Datenverarbeitung)
**DSS1**			Digital Signalling System No. 1 --> Standardisiertes Euro-ISDN-Protokoll
**DSS1**			Digital Subscriber System Number 1 (ISDN)
**DSS2**			Digital Subscriber Signalling System No. 2
**DSSE**			Directory System Service Element
**DSSR**			Digital Short Range Radio --> Kurzstreckenfunk
**DSSS**			Direct Sequence Spread Spectrum --> Spreizbandtechnik (Wireless LAN)
**DSSSL**			Document Style Semantics and Specification Language
**DST**				Datenstation (Weitverkehrsnetze)
**DST**				Dispatcher Station (Mobilfunknetze)
**DSU**				Data Service Unit
**DSU**				Data Service Unit --> Datendiensteinheit (Telekommunikations-Endgeräte)
**DSU**				Digital Switching Unit
**DSU**				Distribution ServicesUnit --> Verteilungsdiensteinheit
**DSV**				Digitalsignal-Verbindung
**DSV**				delimiter separated value e.g. CSV
**DSVD**			Digital Simultaneous Voice and Data
**DSX**				Digital Cross Connect (Weitverkehrsnetze)
**DSX**				Distributed System Executive (Weitverkehrsnetze)
**DT**				Data Transmission --> Datenübertragung
**DT**				Dateitransfer
**DT**				Deutsche Telekom
**DT**				Directory Traversal
**DTAG**			Deutsche Telekom AG (Gremien, Organisationen)
**DTAM**			Document Transfer and Manipulation (CCITT T.431/433)
**DTAP**			Direct Transfer Application Part (GSM, Mobilfunknetze)
**DTC**				Data Test Center --> Datentestzentrum (Netzmanagement)
**DTCH**			Dedicated Transport Channel (GSM, Mobilfunk)
**DTD**				Document Type Definition
**DTE**				Data Terminal Equipment --> Datenendeinrichtung (ISO/OSI)
**DTF**				Data Transfer Facility (Hersteller-NW-Architekturen)
**DTH**				Direct-to-Home (Sat-Kommunikation)
**DTL**				Designated Transit List (ATM, B-ISDN)
**DTM**				Dynamic Synchronous Transfer Mode (Weitverkehrsnetze)
**DTMF**			Dual Tone Multi Frequency --> Zweiton-Verfahren (Analoge Übertragung)
**DTO**				Direct-to-Office (Sat-Kommunikation)
**DTP**				Data Grade Twisted Pair (Verkabelung)
**DTP**				Data Transmission Process
**DTP**				Datagram Transport Protocol (Protokolle, ATM, B-ISDN)
**DTP**				Distributed Transaction Processing (DK-Dienste)
**DTR**				Data Terminal Ready --> Modemsignal (Steuerzeichen)
**DTR**				Dedicated Token Ring (Token Ring)
**DTS**				Data Transmission System
**DTS**				Digitale Teilnehmerschaltung
**DTS**				Distributed Time Service
**DTTB**			Digital Terrestrial Television Broadcasting
**DTU**				Data Transmission Unit
**DTV**				Digital Television --> Digitales Fernsehen
**DTX**				Discontinuous Transmission --> Nichtkontinuierliches Senden
**DU**				Data Unit --> Dateneinheit
**DUA**				Directory User Agent (X.500)
**DUA**				Distribution and Usage Authorization
**DUART**			Dual Universal Asynchronous Receiver/Transmitter
**DUE**				Datenumsetzereinrichtung
**DUP**				Data User Part
**DUS**				Datenübertragungssystem
**DUST**			Datenumsetzerstelle (Netzkoomponente)
**DUT**				Device Under Test
**DUV**				Data Under Voice (DK-Übertragungstechniken)
**DV**				Datenverarbeitung
**DV**				Domain Validation
**DVA**				Data Processing Equipment --> Datenverarbetujngsanlage (Datenverarbeitung)
**DVA**				Distance Vector Algorithm (Internetworking)
**DVB**				Digital Video Broadcasting
**DVB-C**			Digital Video Broadcasting – Cable
**DVB-RCC**			Digital Video Broadcasting – Return Channel for Cable
**DVB-RCS**			Digital Video Broadcasting – Return Channel System
**DVB-S**			Digital Video Broadcasting – Satellite
**DVB-T**			Digital Video Broadcasting – Terrestrial
**DVC**				Desktop Videoconferencing
**DVD**				Digital Versatile Disc
**DVE**				Datenvermittlungseinrichtung
**DVE**				Datenvermittlungseinrichtung (Netzkomponente)
**DVE-P**			Datenvermittlungseinrichtung mit Paketvermittlung (Netzkomponente)
**DVIN**			Data Voice Internetworking
**DVMRP**			Distance Vector Multicast Routing Protocol --> Routingprotokoll (RFC 1075)
**DVR**				Datenübertragungsvorrechner (Netzkomponente)
**DVRP**			Distance Vector Routing Protocol (Protokolle, Internetworking)
**DVS**				Data Switching System --> Datenvermittlungssystem (Netzkomponente)
**DVS**				Datenverarbeitungssystem
**DVS**			 	Distributed Virtual Switch (VMware)
**DVST-L**			Datenvermittlungsstelle mit Leitungsvermittlung (Netzkomponente)
**DVST-P**			Datenvermittlungsstelle mit Paketvermittlung (Netzkoomponente)
**DVSt**			Datenvermittlungsstelle (Netzkomponente)
**DW**				Data Warehousing
**DWDM**			Dense Wavelength Division Multiplexing --> Wellenlängen-Multiplex mit engem Wellenlängenabstand
**DWMT**			Discrete Wavelet Multi-Tone --> Mehrtonverfahren (Modulation)
**DWS**				Dynamic Wavelength Slicing
**DX**				Data Exchange --> Datenvermittlung (Internetworking)
**DXC**				Data Exchange Control
**DXC**				Digital Cross Connect (Optische Netze, SDH, Sonet)
**DXG**				Datex-Netzabschlussgerät (Telekommunikations-Endgeräte)
**DXI**				Data Exchange Interface (ATM, B-ISDN)
**DXT**				Digital Exchange for TETRA (Mobilfunknetze)
**DZ**				Datenzentrale (Netzmanagement)
**DZ**				Distanzzone
**DaaS**		 	Desktop as a Service
**DiMF**			Dreier in Metallfolie (Verkabelung)
**DiffServ**			Differentiated Services (Internet)
**DigÜNBw**			Digitales Übertragungsnetz der Bundeswehr (Bundeswehr)
**DirX**			Directory Extranet (Verzeichnisdienste)
**DoC**				Department of Communications (Mobilfunknetze)
**DoD**				Department of Defense --> US-Verteidigungsministerium
**DoS**				Denial of Service (IT-Sicherheit)
**Dx**				Duplex (DK-Übertragungstechniken)
**DÜ**				Datenübertragung
**DÜ**				Dienste-Übergang
**DÜE**				Datenübertragungseinrichtung
**DÜG**				Datenübertragungsgerät
**DÜST**			Datenübertragungssteuerung
**DÜVO**			Datenübertragungsvermittlungsverordnung
**E**				Exploit Code Maturity
**E-DSS1**			European Digital Subscriber Signaling System Nr. 1
**E-OSF**			Element Management, Operation System Function (Netzmanagement)
**E-TDMA**			Enhanced Time Division Multiple Access
**E-Zine**			Electronic Magazine
**E0, E1, ..., E5**		European (Digital Signal Level) No. 1, 2, 3, 4, 5
**E2E**				End-to-End
**EA**				Effective Address
**EA**				Electro Absorption (Optische Netze, SDH, Sonet)
**EA**				Extended Address (HDLC)
**EAB**				Extended Attributes
**EAC**				European Activities Committee (gremien, Organisationen)
**EACEM**			European Association of Consumer Electronics Manufacturers (Gremien, Organisationen)
**EACK**			Extended Acknowledgement
**EAD**				Ethernet-Anschlussdose (Ethernet)
**EAM**				Extended Answer Message
**EAP**				Extensible Authentication Protocol (IT-Sicherheit, Protokolle)
**EAP-MD5**			EAP - Message Digest No. 5
**EAP-TLS**			EAP - Transport Layer Security
**EAP-TTLS**			EAP - Tunneled Transport Layer Security
**EARN**			European Academic Research Network
**EAS**				Expert Agent Selection (TK-Sprachdienste)
**EAZ**				Endgeräte-Auswahlziffer (ISDN)
**EBCDIC**			Extended Binary Coded Decimal Interchange Code (Zeichensatz, Codierung)
**EBCI**			Explicit Backward Congestion Indication (ATM, B-ISDN)
**EBGP**			Exterior Border Gateway Protocol (Protokolle)
**EBI**				Enhanced Bios Interface
**EBM**				Ethernet Bridge Module (Lokale Netze)
**EBONE**			European Backbone (NW-Konzepte)
**EBU**				European Broadcasting Union (Gremien, Organisationen)
**EC**				Echo Cancellation --> Echokompensation (DK-Übertragungstechniken)
**EC**				Erase Character --> Löschzeichen
**EC-DH**			Elliptic Curve Diffie Hellman (IT-Sicherheit)
**ECA**				Emergency Changeover Acknowledgement
**ECB**				Electronic Code Book (IT-Sicherheit)
**ECB**				Ethernet Control Board
**ECB**				Event Control Block (Zeichen, Zeichensatz)
**ECC**				Elliptic Curve Cryptography 
**ECC**				Elliptic Curve Cryptography (IT-Sicherheit)
**ECC**				Embedded Control Channel
**ECC**				Embedded Control Channel (Optische Netze, jSDH, Sonet)
**ECC**				Error Correcting Code --> Fehlerkorrektur (Codierung)
**ECCO**			Equatorial Constellation Communications
**ECDH**			Elliptic Curve Diffie-Hellman 
**ECDSA**			Elliptic Curve Digital Signature Algorithm
**ECF**				Echo Frame
**ECF**				Enhanced Connectivity Facility (IBM/SNA)
**ECHO**			Echo Protocol (RFC 862)
**ECIC**			Electronic Communication Implementation Committee (Gremien, Organisationen)
**ECITC**			European Committee for Information Technology Certification (Gremien, Organisationen)
**ECM**				Emergency Changeover Message
**ECM**				Endpoint Congestion Management
**ECM**				Entitlement Control Message (Stadt-, Regionalnetze)
**ECM**				Equal Cost Multipath
**ECM**				Error Correction Mode
**ECM**				Exploit Code Maturity
**ECMA**			European Computer Manufacturers Association (Gremien, Organisationen)
**ECN**				Explicit Congestion Notification (DK-Übertragungstechniken)
**ECO**				Emeregency Changeover Order (Signal)
**ECP**				Encryption Control Protocol --> Protokoll aus der PPP-Familie (Protokolle)
**ECP**				Extended Capability Port (Schnittstellen, -Busse)
**ECR 900**			European Consortium Cellular Radio 900 (Mobilfunknetze)
**ECR**				Explicit Cell Rate (ATM)
**ECRC**			European Computer-Industry Research Center (Gremien, Organisationen)
**ECRM**			Electronic Customer Relationship Management
**ECS**				European Communication Satellite (Sat-Kommunikation)
**ECS**				European Communicaton Satellite (EUTELSAT)
**ECSD**			Enhanced Circuit Switched Data
**ECSP**			Electronic Communications Service Provider
**ECSQ**			Entropy Constrained Scalar Quantization
**ECTEL**			European Committee of Telecommunications and Electronic Professionals Industries
**ECTF**			Enterprise Computer Telephony Forum (Gremien, Organisationen)
**ECTRA**			European Committee on Telecommunications Regulatory Affairs (Gremien, Organisationen)
**ECTUA**			European Council of Telecommunications Users (Gremien, Organisationen)
**ECVQ**			Entropy Constrained Vector Quantization
**ED**				Ending Delimiter --> Endbegrenzer (Lokale Netze)
**EDAC**			Error Detection and Correction (DK-Übertragungstechniken)
**EDC**				Error Detection Code
**EDD**				Electronic Direct Debit
**EDD**				Electronic Document Distribution (Hersteller-NW-Architekturen)
**EDFA**			Erbium Dopped Fiber Amplifier --> Erbium-dotierter Faserverstärker (Optische Netze, SDH, Sonet)
**EDFG**			Edge Device Functional Group (ATM, B-ISDN)
**EDGE**			Enhanced Data Rates for GMS Evolution (GSM)
**EDH**				European Digital Hierarchy
**EDI**				Electronic Data Interchange --> Elektronischer Datenaustausch (DK-Dienste)
**EDIF**			Electronic Design Interchange Format (DK-Dienste)
**EDIFACT**			Electronic Data Interchange For Administration, Commerce and Transport (ISO/OSI)
**EDIMS**			Electronic Data Interchange Messaging Systems
**EDMD**			Electronic Document Message Directory (Mobilfunkdienste)
**EDMS**			Enterprise Defined Messaging Service (Mobilfunkdienste)
**EDP**				Electronic Data Processing --> Elektronische Datgenverarbeitung
**EDP**				Event Detection Point
**EDPS**			Electronic Data Processing System --> Elektronisches Datenverarbeitungssystem
**EDS**				Electronic Data Switching System --> Elektronisches Datenvermittlungssystem
**EDSD**			Electronic Document Segment Directory
**EDTV**			Extended Definition Television (PAL Plus)
**EDV**				Elektronische Datenverarbeitung
**EDX**				Electronic Data Exchange --> Elektronische Datenvermittlung
**EE**				Endeinrichtung
**EE**				Extended Edition
**EEA**				Electrical Equipment Association (Gremien, Organisationen)
**EECA**			European Electronic Component Manufacturers Association (Gremien, Organisationen)
**EEEMA**			European Electronic Messaging (Mail) Association (Gremien, Organisationen)
**EEI**				Electrical Interface Board
**EEP**				Equal Error Protection
**EEPG**			European Engineering Planning Group (Gremien, Organisationen)
**EES**				Escrow Encryption Standard (IT-Sicherheit)
**EET**				Equipment Engaged Tone
**EF**				Elementary Function (Weitverkehrsnetze)
**EF**				Expedited Flow
**EF**				Expedited Forwarding (ATM, B-ISDN)
**EFA**				Envelope Function Adress
**EFCI**			Explicit Forward Congestion Indication (Frame Relay, ATM, B-ISDN)
**EFD**				End Frame Delimiter
**EFF**				Electronic Frontier Foundation (Gremien, Organisationen)
**EFI**				Error Free Interval --> Bitfehlerfreie Intervalle (DK-Übertragungstechniken)
**EFI**				External File Interface
**EFM**				Ethernet First Mile (Anschlussnetze)
**EFR**				Enhanced Full Rate
**EFR**				Enhanced Full Rate (Codec)
**EFS**				End Frame Sequence (Token Ring)
**EFS**				Erdfunkstelle (Bodensegment, Sat-Komm)
**EFS**				Error Free Seconds
**EFS**				European Freephone Service
**EFT**				Electronic Funds Transfer
**EFT**				Euro File Transfer (ISDN)
**EFT**				European File Transfer
**EGA**				Enhanced Graphics Adapter
**EGC**				Enhanced Group Call --> Erweiterter Gruppenruf (Sat-Kommunikation)
**EGN**				Eingabegerät für Numerik (Mobilfunknetze)
**EGN**				Einzelgebührennachweis (Telekommunikation)
**EGP**				Exterior Gateway Protocol (Protokolle, Internet)
**EGPRS**			Enhanced General Packet Radio Service (GSM, Mobilfunknetze)
**EGRP**			Exterior Gateway Routing Protocol (Protokolle, Internet)
**EGW**				Edge Gateway
**EHF**				Extremely High Frequency
**EHKP**			Einheitliches Höheres Kommunikations-Protokoll
**EHLLAP**			Emulator High Level Language API
**EI**				Error Increments
**EI**				Error Indicator
**EIA**				Electronic Industries Association --> Standardisierungsgremium in den USA (Gremien, Organisationen)
**EIB**				Electrical Interface Board
**EIB**				Europäischer Installations-Bus
**EICAR**			European Institute for Computer Antivirus Research
**EID**				Equipment Identifier
**EIDE**			Enhanced Integrated Drive Electronic
**EIDQ**			European International Directory Query Forum (Gremien, Organisationen)
**EIES**			Electronic Information Exchange System
**EIGRP**			Enhanced Interior Gateway Routing Protocol (Cisco, Protokolle)
**EIM**				Ethernet Interconnection Module
**EIN**				European Informatics Network
**EIP**				Extended Internet Protocol (Protokolle, Internet)
**EIR**				Equipment Identity Register (GSM, Mobilfunk)
**EIR**				Equipment Identity Register -> Gerätedatenbank
**EIR**				Excess Information Rate
**EIR**				Excess Information Rate (Frame Relay)
**EIRP**			Equivalent Isotropically Radiated Power (Sat-Kommunikation)
**EIS**				Electronic Information Security (IT-Sierheit)
**EISA**			Extended Industrial Standard Architecture
**EIT**				Encoded Information Type
**EITO**			European Information Technology Observatory
**EIUF**			European ISDN User Forum (Gremien, Organisationen)
**EKT**				Envelope Kanalteiler (Netzkomponente)
**EL**				Endleitung
**EL**				Erase Line (Zeichen, Zeichensatz)
**ELAN**			Emulated LAN (ATM, B-ISDN)
**ELAP**			EtherTalk Link Access Protocol (Protokolle)
**ELBA**			Emergency Location Beacon
**ELF**				Extremely Low Frequency
**ELFEXT**			Equal Level Far End Crosstalk (Verkabelung)
**ELLC**			Enhanded LLC (Hersteller-NW-Architekturen)
**ELM**				Extended LAN-Manager
**EM**				End of Medium --> Ende der Aufzeichnung (Steuerzeichen)
**EM**				Environmental Module
**EM**				Event Management --> Ereignismanagement (Netzmanagement)
**EMA**				Electronic Messaging Association
**EMAC**			Ethernet Media Access Control (Lokale Netze)
**EMAIL**			Electronic Mail - Elektronische Post
**EMB**				Elektromagnetische Beeinflussung
**EMBB**			Electronic Mail Building Block
**EMC**				Electromagnetic Compatibility --> Elektromagnetische Verträglichkeit
**EMD**				Edelmetallmotordrehwähler (Netzkomponente)
**EMD**				Equilibrium Mode Distribution (Verkabelung)
**EMF**				Elektromagnetische Felder (Mobilfunknetze)
**EMF**				Enhanced Metafile
**EMI**				Electromagnetic Interference
**EMI**				Electromagnetic Interference --> Elektromagnetische Beeinflussung
**EMI**				External Machine Interface
**EMIF**			ESCON Multiple Image Facility (NW-Konzepte)
**EML**				Element Management Layer (Netzwserkmanagement)
**EMM**				Entitlement Management Message
**EMM**				Ethernet Management Module (Netzmanagement)
**EMMS**			Electronic Mail and Message System
**EMMS**			Electronic Media Management System (Multimedia)
**EMRP**			Effective Monopole Radiated Power
**EMS**				Electronic Message System --> Elektronisches Mitteilungssystem
**EMS**				Element Management System (ATM, B-ISDN)
**EMS**				Enhanced Messaging Service (Mobilfunknetze)
**EMS**				Enterprise Management System --> Managementsystem eines Unternehmens (Netzmanagement)
**EMSI**			Electronic Mail Standard Identification (Mobilfunkdienste)
**EMT**				Embedded Management Tool (Netzmanagement)
**EMUG**			European MAP User Group (Gremien, Organisationen)
**EMV**				Elektromagnetische Verträglichkeit
**EMVG**			Elektromagnetisches Verträglichkeitsgesetz
**EMVU**			Elektromagnetische Verträglichkeit zur Umwelt (Mobilfunknetze)
**EN**				End Code
**EN**				Europäische Norm (CEN/CENELEC) (Standards, Verordnungen, Gesetze)
**EN**				Europäischen Normen
**EN**				Exchange Number
**ENA**				Enterprise Network Architecture (NW-Konzepte)
**ENA**				Extended Network Addressing
**ENDAK**			End Acknowledgement (ATM, B-ISDN)
**ENIAC**			Electronic Numerical Integrator and Computer
**ENID**			EGC Network Identity (Sat-Kommunikation)
**ENISA**			European Union Agency for Cybersecurity
**ENQ**				Enquiry --> Stationsaufforderung (Steuerzeichen)
**ENS**				Enterprise Networking Services (NW-Konzepte)
**ENSAD**			Einheitliches Nachrichtensystem für analoge und digitale Vermittlung
**EOA**				End of Address (Steuerzeichen)
**EOB**				End of Block (Steuerzeichen)
**EOB**				End of Bus (Stadt-, Regionalnetze)
**EOC**				Embedded Operation Channel
**EOC**				End of Contents --> Ende des Inhalts (Zeichen, Zeichensatz)
**EOD**				End of Data --> Datenende (Datenfelder)
**EOF**				End Of File
**EOF**				End of File --> Dateiende (Zeichen, Zeichensatz)
**EOI**				End of Identify --> Ende der Identifizierung
**EOI**				End of Inquiry --> Ende der Anforderung
**EOJ**				End of Job --> Ende des Jobs
**EOL**				End of Line --> Ende der Zeile
**EOL**				Europe Online (Online-Dienste)
**EOM**				End of Message --> Nachrichtenende (ATM)
**EON**				End of Number --> Ende der Zahl
**EOR**				End of Record --> Ende des Datensatzes
**EOR**				End of Run --> Ende des Programmlaufes
**EOT**				End of Transmission --> Ende der Übertragung (Steuerzeichen)
**EOTC**			European Organization for Testing and Certification (Gremien, Organisationen)
**EOTD**			Enhanced Observed Time Difference (Mobilfunknetze)
**EOV**				End of Volume
**EP**				Emulation Program --> Emulationsprogramm
**EP**				Error Protocol --> Fehlerprotokoll (Protokolle)
**EPA**				Electrical Pre-Amplifier
**EPA**				Enhanced Performance Architecture (Lokale Netze)
**EPBX**			Electronic Private Branch Exchange (Netzkomponente)
**EPD**				Early Packet Discard (ATM, B-ISDN)
**EPHOS**			European Procurement Handbook on Open Systems
**EPON**			Ethernet Passiv Optical Network (Ethernet)
**EPP**				Enhanced Parallel Port
**EPRA**			Enhanced Performance Router Access Unit
**EPROM**			Erasable Programmable Read Only Memory
**EPS**				Encapsulated PostScript
**EPSS**			Experimental Packet-Switched System
**ER**				Edge Router --> Router an Netzgrenze
**ER**				Equipment Raum (Verkabelung)
**ER**				Error Recovery --> Wiederaufsetzen nach Fehler
**ER**				Error Report
**ER**				Explicit Rate (ATM, B-ISDN)
**ER**				Externer Rechner
**ERC**				European Radiocommunications Committee (Gremien, Organisationen)
**ERC**				Explicit Route Control (Hersteller-NW-Architekturen)
**EREP**			Environment Recording Event Program (Netzmanagement)
**ERI**				Exception Response Indicator
**ERION**			Ericsson Optical Networking
**ERIP**			Extended Routing Information Protocol (Protokolle)
**ERL**				Echo Return Loss
**ERM**				Ethernet Router Module
**ERM**				Explicit Rate Method (ATM, B-ISDN)
**ERMES**			European Radio Message System
**ERN**				Enterprise Resource Network (Speichernetze)
**ERN**				Explicit Route Number (Hersteller-NW-Architekturen)
**ERNet**			Educational and Research Network
**ERO**				European Radiocommunications Office (Gremien, Organisationen)
**ERO**				Explicit Router Object (Internetworking)
**EROS**			Emitter-Receiver for Optical Systems
**ERP**				Effective Radiated Power
**ERP**				Electronic Retail Puchasing
**ERP**				Enterprise Resource Planning
**ERP**				Error Recovery Procedure --> Wiederaufsetzprozedur
**ERP**				Exterior Routing Protocol (Protokolle)
**ERR**				Error --> Fehler
**ERS**				Emergency Response Service
**ERS**			 	Enterprise Ready Server (VMware)
**ES**				Echo Suppression --> Echo-Unterdrückung (DK-Übertragungstechniken)
**ES**				End System (ISO/OSI)
**ES**				Endstelle
**ES**				Errored Seconds (G.826)
**ES**				Extended Services --> Erweiterter Service
**ES-IS**			End System to Intermediate System --> OSI-Routing-Protokoll (ISO/OSI)
**ESA**				Enterprise System Architecture (IBM)
**ESA**				Extended Service Area
**ESA**				Extended Service Area (Wireless LAN)
**ESB**				Einseitenband
**ESB**				Erweiterter Sonderbereich (Anschlussnetze)
**ESC**				Escape Character --> Code-Umschaltung (Steuerzeichen)
**ESCON**			Enterprise Systems Connection (IBM)
**ESD**				Electrostatic Discharge --> Elektrostatische Entladung
**ESDI**			Enhanced Small Device Interface
**ESE**				Entladung statischer Elektrizität
**ESF**				Extended Service Frame
**ESI**				End System Identifier (ATM, B-ISDN)
**ESI**				Ethernet Serial Interface (Ethernet)
**ESIG**			European SMDS Interest Group (Gremien, Organisationen)
**ESMR**			Emergency Specialized Mobile Radio (Mobilfunknetze)
**ESMTP**			Enhanced Simple Mail Transfer Protocol (Protokolle, Internet)
**ESN**				Enterprise Storage Network (Speichernetze)
**ESP**				Encapsulation Security Payload
**ESP**				Enhanced Service Provider
**ESPRIT**			European Strategic Program for Research and Development Information Technology
**ESR**				Errored Second Ratio (G.826)
**ESRP**			Extreme Standby Router Protocol (Protokolle)
**ESS**				Electronic Switching System (Netzkomponente)
**ESS**				End Station Support
**ESS**				Extended Service Set (Wireless LAN)
**ESS1**			Electronic Switching System No. 1.
**ESSID**			Extended Service Set Identity --> “Netzname” eines Funk-LANs (WLAN)
**ESSM**			Electronic Switching System for Metropolitan Area Networks
**ESTELLE**			Extended State Transition Language
**ESX**				Ethernet Serial Router for X.25
**ESX**			 	Elastic Sky X (VMware)
**ESXi**			The vSphere Hypervisor from VMware. For extra trivia points, know that Elastic Sky was the original proposed name of the hypervisor and is now the name of a band made up of VMware employees. (VMware)
**ESXi**		 	Elastic Sky X Integrated (VMware)
**ET**				Enganged Tone --> Besetztzeichen
**ET**				Exchange Termination
**ET**				Exchange Termination --> Vermittlungsabschluss (ISDN)
**ETACS**			Extended Total Access Communications System
**ETB**				Elektronisches Telefonbuch
**ETB**				End of Transmission Block --> Ende des Datenübertragungsblocks (Steuerzeichen)
**ETCG**			Elapsed Time Code Generator
**ETCOM**			European Testing and Certification for Office and Manufacturing Protocol (Gremien, Organisationen)
**ETDM**			Electronic Time Division Multiplexing (Optische Netze, SDH, Sonet)
**ETE**				Equivalent Telephone Erlangs
**ETE**				Ethernet Switching Engine
**ETI**				Ensemble Transport Interface
**ETIM**			Elapsed Time
**ETIS**			European Telecommunications (Gremien, Organisationen)
**ETIS**			European Telecommunications Informatics Services
**ETL**				European Testing Laboratory (Netzmanagement)
**ETNO**			European Public Telecommunications Netzwork Operators Association (Gremien, Organisationen)
**ETNS**			European Telecommunications Numbering Scheme
**ETO**				European Telecommunications Office (Gremien, Organisationen)
**ETR**				ETSI Technical Report (Standards, Verordnungen, Gesetze)
**ETR**				Early Token Release --> Early-Token-Release-Verfahren (Token Ring)
**ETR**				European Technical Report
**ETS**				ETSI Technical Standard (Standards, Verordnungen, Gesetze)
**ETS**				Ethernet Terminal Server (Ethernet)
**ETS**				European Telecomminications Standard (Standards, Verordnungen, Gesetze)
**ETS**				Executable Test Suite (Mobilfunkdienste)
**ETSI**			European Telecommunication Standards Institute (Nizza) (Gremien, Organisationen)
**ETSI**			European Telecommunications Standards Institute
**ETX**				End of Text --> Textende (Steuerzeichen)
**EU**				End User --> Endbenutzer
**EUC**				End User Computing (VMware)
**EUCATEL**			European Conference of Associations of Telecommunication Industries (Gremien, Organisationen)
**EUI**				End User Interface
**EUI**				End-System Unique Identifier (Mobile Kommunikation)
**EURESCOM**			European Institute for Research and Strategic Studies in Telecommunications (Gremien, Organisationene)
**EURONET**			European Network
**EUTELSAT**			European Telecommunications Satellite Organization (Gremien, Organisationen)
**EUUG**			European Unix System User Group (Gremien, Organisationen)
**EUnet**			European Network (Forschungsnetze)
**EUnet**			European UNIX Network
**EV**				Etagenverteiler (Verkabelung)
**EV**				Extended Validation
**EVA**				Empfangsverteilanlage (Anschlussnetze)
**EVA**				Äthylen-Vinylacetat (Verkabelung)
**EVC**				Enhanced vMotion Compatibility (VMware)
**EVDC**			Elastic Virtual Data Center (VMware)
**EVI**				EDI Via Internet
**EVM**				Error Vector Magnitude --> Amplitude des Fehlervektors
**EVN**				Einzelverbindungsnachweis (ISDN)
**EVSt**			Endvermittlungsstelle
**EVUA**			European Virtual Private Network Users Association (Gremien, Organisationen)
**EVÜ**				Einzelverbindungsübersicht (ISDN)
**EWI**				Email Waiting Indication (Mobilfunkdienste)
**EWL**				Equivalent Working Length (Anschlussnetze)
**EWOS**			European Workshop in Open Systems (Gremien, Organisationen)
**EWS**				Elektronisches Wählsystem (Weitverkehrsnetze)
**EWSD**			Elektronisches Wählsystem Digital (Netzkoomponente)
**EX**				Exception Response
**EXU**				Extension Unit
**Eltg**			Endleitung
**EoS**				Ethernet over Sonet (Weitverkehrsnetze)
**EoVDSL**			Ethernet over VDSL (Anschlussnetze)
**Erl**				Erlang
**EuroCAIRN**			European Co-operation for Academic and Industrial Research Networking
**Evt**				Endverteiler
**Evz**				Endverzweiger
**F**				Frequenz
**F-PDCCH**			Forward Packet Data Control Channel (GSM, Mobilfunk)
**F-PDCH**			Forward Packet Data Channel (GSM, Mobilfunk)
**FA**				Final Address
**FA**				Functional Addressing (GSM, Mobilfunk)
**FAC**				Facility Accepted
**FACCH**			Fast Associated Control Channel --> Schneller assoziierter Steuerkanal (GSM)
**FACREJ**			Facility Reject
**FADU**			File Access Data Unit --> Dateizugriffsdateneinheit
**FAG**				Fernmeldeanlagengesetz (Telekommunikation)
**FAM**				File Access Method
**FAM**				Forward Address Message
**FAM**				Functional Architecture Model
**FANP**			Flow Attribute Notification Protocol --> Anwendungsprotokoll im Internet (Protokolle)
**FAP**				FAPD Anchored Path Field (Mobile Kommunikation)
**FAP**				File Access Protocol (Protokolle)
**FAPD**			Friend Assisted Path Discovery (Mobile Kommunikation)
**FAPDP**			Friend Assisted Path Discovery Protocol (Mobile Kommunikation)
**FAQ**				Frequently Asked Questions
**FAR**				Facility Request Message (Mobilfunknetze)
**FAR**				Failure Analysis Report --> Fehleranalysebericht
**FARNET**			Federation of Academic Research Networks
**FAS**				Flexible Access System
**FAS**				Frame Alignment Signal --> Rahmenkennwort (GSM)
**FAST**			Frame ATM over SDH Transport (ATM, B-ISDN)
**FAT**				File Allocation Table
**FAT**				Flexible Access Termination
**FAU**				Fixed Access Unit
**FAX**				Faksimile --> Fernkopieren (Talekommunikation)
**FB**				Fiber Backbone
**FB**				Frequency Burst (GSM)
**FBC**				Fully Buffered Channel
**FBE**				Fernbetriebseinheit (Weitverkehrsnetze)
**FBE**				Free Buffer Enquiry
**FBG**				Fiber Bragg Grating (Optische Netze, SDH, Sonet)
**FBS**				Fallback-Switch (Netzkomponente)
**FBS**				Funkbasisstation (Mobilfunknetze)
**FC**				Fiber Channel
**FC**				Frame Control (Token Ring)
**FC-AL**			Fiber Channel Arbitrated Loop (Fiber Channel)
**FCA**				Fiber Channel Association (Fibre Channel)
**FCA**				Flow Control Acknowledgement
**FCAPS**			Fault-, Configuration-, Account-, Performance- and Security-Management
**FCB**				File Control Block
**FCC**				Federal Communication Commission --> US-Bundesbehörde für Fernmeldewesen (Gremien, Organisationen)
**FCCH**			Frequency Correction Channel --> Frequenzkorrekturkanal (GSM, Mobilfunknetze)
**FCCSET**			Federal Coordinating Council for Science, Engineering and Technics (Gremien, Organisationen)
**FCFS**			First Come First Serve
**FCFS**			Frame Check Forward Sequence
**FCI**				Flow Control Indication
**FCIA**			Fiber Channel Industry Association (Gremien, Organisationen)
**FCIF**			Full Common Intermediate Format
**FCIP**			Fiber Channel Over IP
**FCM**				Flow Control Message
**FCO**				Flow Control Operator
**FCS**				Fast Circuit Switching
**FCS**				Fiber Channel Standard (FDDI)
**FCS**				Frame Check Sequence --> Rahmenprüfwort
**FCSP**			Fiber Channel Switched Protocol (Fiber Channel)
**FCT**				File Control Table
**FCoE**			Fibre Channel over Ethernet, a networking and storage technology.
**FD**				Floor Distributor --> Etagenverteiler (Verkabelung)
**FD**				Forward Direction
**FD**				Fotodiode
**FDCT**			Forward Discrete Cosine Transformation (Codierung)
**FDD**				Frequency Division Duplex
**FDDI**			Fiber Distributed Data Interface --> ANSI-Standard
**FDE**				Facility Deactivated Message (Mobilfunknetze)
**FDE**				Full Duplex Ethernet (Ethernet)
**FDF**				Fiber Distribution Frame
**FDI**				Feeder Distribution Interface --> Kabelverzweiger
**FDIR**			Failure Detection, Isolation and Recovery
**FDIS**			Final Draft International Standard
**FDM**				Fault Domain Manager (VMware)
**FDM**				Frequency Division Multiplexing --> Frequenzmultiplex, Frequenzgetrenntlage-Verfahren
**FDMA**			Frequency Division Multiple Access
**FDMA/FDD**			Frequency Division Multiple Access/Frequency Diversity Duplex
**FDSE**			Full Duplex Switched Ethernet (Ethernet)
**FDT**				Full Duplex Token Ring (Token Ring)
**FDVDI**			Fiber Distributed Video/Voice and Data Interface --> Lichtwellenleiterinterface für Bild, Ton und Daten
**FDX**				Full Duplex
**FE**				Fast Ethernet (Ethernet)
**FE**				Fernsprechen (Telekommunikation)
**FE**				Functional Entity
**FE**				Functional Entity, Funktionseinheit
**FEA**				Functional Entity Action
**FEBE**			Far End Block Error (ATM)
**FEC**				Fast Ethernet Channel (Lokale Netze)
**FEC**				Forward Error Correction --> Vorwärtsfehlerkorrektur
**FEC**				Forwarding Equivalence Class
**FECN**			Forward Explicit Congestion Notfication (Frame Relay, ATM)
**FED**				Fernsprechhauptanschluss für Direktruf (DK-Schnittstellen)
**FED**				Field Emission Display
**FEFO**			First Ended, First Out --> Warteschlangenverwaltung (DK-Übertragungstechniken)
**FEP**				Front End Processor
**FEP**				Teflon, Tetrafluorethylen (Verkabelung)
**FEPG**			Federal European Planning Group (Gremien, Organisationen)
**FER**				Frame Error Rate
**FERF**			Far End Receive Failure (ATM, B-ISDN)
**FET**				Functional Entity Type
**FEXT**			Far-End-Crosstalk --> Fernnebensprechen (Verkabelung)
**FF**				Form Feed --> Formularvorschub (Steuerzeichen)
**FFM**				Fiber FDDI Module
**FFM**				Fixed Frequency Modem --> Festfrequenzmodem
**FFOL**			FDDI Follow On LAN (FDDI-II)
**FFSK**			Fast Frequency Shift Keying (Modulation)
**FFT**				Fast Fourier Transformation
**FFTDCA**			Final-Form Text DCA (Hersteller-NW-Architekturen)
**FG**				Fernstrecken-Gateway
**FG**				Funktional Group (ATM, B-ISDN)
**FGF**				Forschungsgemeinschaft Funk
**FGt**				Fernschaltgerät (Weitverkehrsnetze)
**FH**				Fixed Host
**FH**				Frame Handler (ISDN)
**FH**				Frequency Hopping --> Frequenzsprungverfahren
**FHMA**			Frequency Hopping Multiple Access
**FHS**				Format Handling System
**FHSS**			Freqency Hopping Spread Spectrum --> Bandspreizung durch Frequenzsprungverfahren
**FIB**				Fiber
**FIB**				Forward Indication Bit --> Vorwärtsindikator (Zeichen, Zeichensatz)
**FIC**				Fabric Interface Card
**FIC**				Fast Information Channel
**FICON**			Fiber Channel Connection (Fiber Channel)
**FID**				Format Identifier Field -> Formatidentifikationsfeld (Hersteller-NW-Architekturen)
**FIFO**			First In, First Out (Speichertechnik)
**FILO**			First In, Last Out (Speichertechnik)
**FIM**				Facility Information Message (ISDN)
**FIN**				Final (Flag)
**FIN**				Finish
**FIPS**			Federal Information Processing Standard --> US-Standard für Informationsverarbeitung (Standards, Verordnungen, Gesetze)
**FIR**				Fast Infrared (Mobilfunknetze)
**FIR**				Finite Impulse Response
**FIRST**			Forum of Incident Response and Security Teams (IT-Sicherheit) (Gremien, Organisationen)
**FISU**			Fill in Signal Unit --> Füllzeicheneinheit (ISDN)
**FIT**				Failure in Time (Anschlussnetze)
**FITCE**			Fédération des Ingénieurs des Télécommunications de la Communauté Européene (Brüssel) (Gremien, Organisationen)
**FITL**			Fiber in The Loop (Anschlussnetze)
**FITUG**			Förderverein Informationstechnik und Gesellschaft (Gremien, Organisationen)
**FIX**				Federal Internet Exchange
**FKM**				Funkkanalmodem (Mobilfunknetze)
**FKS**				Fernmelde-Klein-Steckverbindung --> Western-Stecker
**FKZ**				Fernkennzeichen
**FL**				Fiber Link (Verkabelung)
**FLAP**			FDDI Link Access Protocol (Protokolle)
**FLE**				Fiber Link Extender (Weitverkehrsnetze)
**FLL**				Frequency Locked Loop
**FLOPS**			Floating Point Operations Per Second --> Fließkomma-Operationen pro Sekunde (Maßeinheit)
**FLP**				Fast Link Pulse
**FLR**				Frame Loss Ratio
**FM**				Fault Management --> Fehlermanagement (Netzmanagement)
**FM**				Frame Mode
**FM**				Frequency Management --> Frequenzmanagement
**FM**				Frequenzmodulation (Modulation)
**FM**				Friend Management (Mobile Kommunikation)
**FM**				Function Management --> Funktionsmanagement
**FMBS**			Frame Mode Bearer Service (ISDN)
**FMC**				Fixed Mobile Convergence (Mobilfunknetze)
**FMD**				Function Management Data
**FMDS**			Function Management Data Services
**FMG**				Fernmeldegesetz
**FMH**				Function Management Header
**FMI**				Fixed Mobile Integration (Mobilfunknetze)
**FMP**				File Maintenance Protocol (Protokolle)
**FMS**				File Management System
**FMS**				Fixed Mobile Substitution (Mobilfunknetze)
**FMUX**			Flexibler Multiplexer (Optische Netze, SDH, Sonet)
**FMX**				Frequenzmultiplex
**FN**				Fernnetz (Weitverkehrsnetze)
**FN**				Forwarded Notification
**FN**				Frame Number
**FNA**				Free Network Architecture
**FNB**				Flexible Network Bus
**FNC**				Federal Networking Council (USA) (Gremien, Organisationen)
**FNZDSF**			Flated Non Zero Dispersion Shifted Fiber (Verkabelung)
**FO**				Fernmeldeordnung
**FO**				Fiber Optics --> Lichtwellenleiter (Verkabelung)
**FOC**				Fiber Optic Communications --> Kommunikation über Lichtwellenleiter (Verkabelung)
**FOIRL**			Fiber Optic Inter-Repeater Link (IEEE 802.3, Ethernet)
**FOLAN**			Fiber Optic LAN --> LAN mit Lichtwellenleiter (Lokale Netze)
**FOMAU**			Fiber Optic Medium Attachment Unit --> Lichtwellenleiter-Anschlusseinheit (Ethernet)
**FORMAC**			Fiber Optic Ring Media Access Controller
**FOSA**			Fiber Optic Station Attachment
**FOTAG**			Fiber Optic TAG (Lokale Netze)
**FOTS**			Fiber Optic Transmission System --> Lichtwellenleiter-Übertragungssystem
**FOX**				Fiber Optic Transceiver --> Sender/Empfänger für Lichtwellenleiter (Telekommunikations-Endgeräte)
**FP**				Fast Poll
**FP**				Fiber Passiv (Verkabelung)
**FP**				Fixed Part (Mobilfunk)
**FP**				Format Prefix
**FPA**				Fast Packet Adaptation
**FPD**				Flat Panel Display
**FPGA**			Field Programmable Gate Array
**FPH**				Freephone --> Gebührenübernahme (ISDN)
**FPL**				Fabry Perot Laser (Verkabelung)
**FPLMTS**			Future Public Land Mobile Telecommunication System
**FPM**				Fast Packet Multiplexing
**FPS**				Fast Packet Switching
**FQDA**			Fully Qualified Domain Address (Internet)
**FQDN**			Fully Qualified Domain Name (Internet)
**FR**				Fixed Routing (Internetworking)
**FR**				Flame Retardant --> Flammwidrig (Verkabelung)
**FR**				Flat Rate
**FR**				Frame Rate
**FR**				Frame Relay
**FR**				Full Rate (Mobilfunknetze)
**FR-SSCS**			Frame Relay - Service Specific Convergence Sublayer (Frame Relay)
**FRA**				Fixed Radio Access
**FRAD**			Frame Relay Access Device (Frame Relay)
**FRAU**			Fixed Radio Access Unit (Anschlussnetze)
**FRC**				Federal Radio Commission (USA) (Gremien, Organisationen)
**FRED**			Front End Directory (Verzeichnisdienste)
**FRF**				Frame Relay Forum (Gremien, Organisationen)
**FRFH**			Frame Relay Frame Handler (Frame Relay)
**FRI**				Frame Relay Interface (Frame Relay)
**FRICC**			Federal Research Internet Coordination Committee (Gremien, Organisationen)
**FRJ**				Facility Rejected Message (Mobilfunknetze)
**FRMR**			Frame Reject --> Blockrückweisung (HDLC)
**FRNC**			Flame Retardant Non Corrosive (Verkabelung)
**FROA**			Frame Relay over ATM (Frame Relay)
**FRQ**				Facility Request Message
**FRR**				Frame Retransmission Rate
**FRS**				Frame Relay Service (Frame Relay)
**FRSI**			Frame Relay Service Interworking (Frame Relay)
**FRSU**			Frame Relay Service Unit (Frame Relay)
**FRTE**			Frame Relay Terminal Equipment (Frame Relay)
**FRTT**			Fixed Round Trip Time
**FRU**				Fixed Repeater Unit (DECT)
**FRUNI**			Frame Relay User Network Interface (Frame Relay)
**FS**				File Separator (Steuerzeichen)
**FS**				Finger Server (Internet)
**FS**				Frame Status (Token Ring)
**FS**				Frame Switching
**FS**				Functional Specification
**FSAN**			Full Service Access Network
**FSC**				Frame Synchronisation
**FSK**				Frequency Shift Keying --> Frequenzumtastung (Modulation)
**FSL**				Flexible Service Logic (Weitverkehrsnetze)
**FSM**				Finite State Machine
**FSM**				Frequency Shift Modulation (Modulation)
**FSMA**			Field Installable Subminiature Assembly (Verkabelung)
**FSN**				Forward Sequence Number
**FSN**				Frequency Subset Number (Mobilfunknetze)
**FSN**				Full Service Network
**FSOQ**			Frequency Shift Offset Quadrature (Modulation)
**FSP**				Fiber Service Platform
**FSP**				File Service Process
**FSP**				File Sevice Protocol (Protokolle)
**FSP**				Frequency Shift Pulsing (Modulation)
**FSP**				Full Service Provider
**FSPR**			Foundry Standby Router Protocol (Protokolle)
**FSS**				Fixed Satellite Services
**FST**				Fisheye State Routing
**FSYNC**			Frame Synchronisation
**FT**				Fault Tolerance (VMware)
**FT**				Fault Tolerance --> Fehlertoleranz
**FT**				File Transfer
**FT**				Fourier Transformation
**FT**				Frame Transfer
**FT-1**			Fractional T1 (Weitverkehrsnetze)
**FT-3**			Fractional T3 (Weitverkehrsnetze)
**FTAM**			File Transfer Access and Management (ISO/OSI)
**FTC**				Full Rate Traffic Channel
**FTD**				Frame Transfer Delay
**FTDM**			Fixed Time Division Multiplexing (Multiplex-Verfahren)
**FTN**				Fido-Technology Network
**FTNEA**			File Transfer Network Architecture (Offene NW-Konzepte)
**FTP**				File Transfer Protocol --> Anwendungsprotokoll im Internet (RFC 765, 959)
**FTP**				Foiled Twisted Pair (Verkabelung)
**FTS**				File Transfer Service
**FTS**				File Transfer Support (IBM)
**FTSC**			Federal Telecom Standard Committee (Gremien, Organisationen)
**FTTA**			Fiber to the Amplifier (Anschlussnetze)
**FTTB**			Fiber to the Building (Anschlussnetze)
**FTTC**			Fiber to the Cabinet (Anschlussnetze)
**FTTC**			Fiber to the Curb (Anschlussnetze)
**FTTD**			Fiber to the Desk (Anschlussnetze)
**FTTE**			Fiber to the Exchange (Anschlussnetze)
**FTTH**			Fiber to the Home --> Lichtwellenleiter bis in die Wohnung (Anschlussnetze)
**FTTL**			Fiber to the Loop (Anschlussnetze)
**FTTO**			Fiber to the Office (Anschlussnetze)
**FTTR**			Fiber to the Radio (Anschlussnetze)
**FTTSA**			Fiber to the Serving Area (Anschlussnetze)
**FTTT**			Fiber to the Terminal (Anschlussnetze)
**FTVSt**			Fern- und Teilnehmervermittlungsstelle
**FTZ**				Fernmeldetechnisches Zentralamt (Gremien, Organisationen)
**FTZ**				Forschungs- und Technologiezentrum (Deutschen Telekom)
**FUD**				Fully Undetectable
**FUNI**			Frame-based User Network Interface
**FV**				Festverbindung
**FVSat**			Festverbindung über Satellit (Sat-Kommunikation)
**FVSt**			Fernvermittlungsstelle (Weitverkehrsnetze)
**FVSt**			Funkvermittlungsstelle (Mobilfunknetze)
**FW**				Fernsprech-Wählnetz (Weitverkehrsnetze)
**FW**				Frequenzweiche
**FWA**				Fixed Wireless Access (Anschlussnetze)
**FWLST**			Fernwirkleitstelle
**FWM**				Four Wave Mixing --> Vierwellen-Mischung (Optische Netze, SDH, Sonet)
**FX**				Foreign Exchange
**FYI**				For Your Information (Chat)
**FYI**				for your information
**FZ**				Fernzone
**FeA**				Fernsprech-Apparat (Telekommunikations-Endgeräte)
**FmNstA**			Fernmeldenebenstellenanlage
**Fs**				Fernschreiber
**Fs**				Fernsprecher
**FuE**				Forschung und Entwicklung
**FuFSt**			Funkfeststation (Mobilfunknetze)
**FuN**				Funknetz
**FuRE**			Funkrufempfänger (Mobilfunknetze)
**FuRK**			Funkrufkonzentrator (Mobilfunknetze)
**FuRSe**			Funkrufsender (Mobilfunknetze)
**FuRVSt**			Funkrufvermittlungsstelle (Mobilfunknetze)
**FuTelG**			Funktelefongerät (Mobilfunknetze)
**FuVSt**			Funkvermittlungsstelle (Mobilfunknetze)
**FuÜ**				Funkübertragung (Mobilfunknetze)
**FÜV**				Fernmelde-Überwachungs-Verordnung (Standards, Verordnungen, Gesetze)
**G-WiN**			Gigabit-Wissenschaftsnetz
**GA**				Go Ahead --> Aufforderung
**GAL**				Global Address List
**GAN**				Global Area Network
**GAP**				Generic Access Profile (Bluetooth)
**GARP**			Generic Attribute Registration Protocol (Protokolle)
**GATE**			General Access to X.25 Transport Extension (ISO/OSI)
**GATS**			Global Automotive Telematics Standard
**GB**				Gigabyte (Zahlensysteme)
**GBG**				Geschlossene Benutzergruppe
**GBI**				Gebührenimpuls (Weitverkehrsnetze)
**GBIC**			Gigabit Interface Converter (Fiber Channel)
**GCAC**			Generic Connection Admission (ATM, B-ISDN)
**GCC**				Group Call Control (Mobilfunknetze)
**GCF**				GSM Certification Forum (GSM, Mobilfunk) (Gremien, Organisationen)
**GCI**				Guidelines for Communication Interfaces (DK-Schnittstellen)
**GCR**				Group Code Recording
**GCRA**			Generic Cell Rate Algorithm (ATM)
**GDA**				Global Directory Agent (Verzeichnisdienste)
**GDDM**			Graphic Data Display Manager
**GDF**				Group Distribution Frame
**GDMI**			Generic Definition of Management Information
**GDMO**			Guidelines for the Definition of Managed Objects (ISO/OSI) (Netzmanagement)
**GDP**				Gateway Discovery Protocol (Cisco) (Protokolle)
**GDS**				General Data Stream
**GDS**				Global Directory Service (Verzeichnisdienste)
**GDSS**			Global Distress Safety System (Sat-Kommunikation)
**GE**				Gebühreneinheit (DK-Übertragungstechniken)
**GE**				Geräteanschlußeinheit
**GE**				Gigabit Ethernet (Ethernet)
**GEA**				Gigabit Ethernet Alliance (Gremien, Organisationen)
**GEDAN**			Gerät zur dezentralen Anrufweiterschaltung (Deutsche Telekom)
**GEN**				Global European Network (NW-Konzepte)
**GEO**				Geostationary Earth Orbit (Sat-Kommunikation)
**GERAN**			GSM/EDGE Radio Access Network
**GES**				Ground Earth Station --> Bodenstation (Sat-Kommunikation)
**GF**				Glasfaser (Verkabelung)
**GFC**				Generic Flow Control (ATM)
**GFE**				GemFire Enterprise
**GFID**			General Format Identifier (X.25)
**GFID/LCGN**			General Format Identifier/Logical Channel Group Number
**GFK**				Glasfaserkabel (Verkabelung)
**GFN**				Glasfaserfernmeldenetz
**GFP**				Global Functional Plane (Weitverkehrsnetze)
**GFR**				Guaranteed Frame Rate (ATM, B-ISDN)
**GFSK**			Gaussian Frequency Shift Keying --> Modulationsverfahren
**GFT**				Generic Functional Transport
**GGA**				Groß-Gemeinschafts-Antenne (Anschlussnetze)
**GGP**				Gateway-to-Gateway Protocol (Protokolle, Internet)
**GGSN**			Gateway GPRS Support Node (GSM, Mobilfunknetze)
**GI**				Gradient Index --> Gradientenindex (Verkabelung)
**GIF**				Gradient Index Fiber --> Gradientenindex-Profilfaser (Verkabelung)
**GIF**				Graphics Interchange Format
**GIH**				Global Information Highway (NW-Konzepte)
**GII**				Global Information Infrastructure (NW-Konzepte)
**GIOP**			General Inter ORB Protocol (COBRA)
**GIPR**			Gigabit IP Router (Internet)
**GIX**				Global Internet Exchange (Internet)
**GK**				Gatekeeper (VoIP)
**GLONASS**			Global Navigation Satellite System bzw. Globalnaya Navigasionnay Sputnikovaya Sistema
**GLSB**			Global Load Server Balancing (Internet)
**GLT**				Gebäudeleittechnik
**GMA**				Gefahren-Melde-Anlage (Anschlussnetze)
**GMD**				Gesellschaft für Mathematik und Datenverarbeitung (Gremien, Organisationen)
**GMDSS**			Global Maritime Distress and Safety System
**GMF**				Generalized Monitoring Facility
**GMLC**			Gateway Mobile Location Center
**GMM**				GPRS Mobility Management
**GMPCS**			Global Mobile Personal Communications by Satellite (Sat-Kommunikation)
**GMPD**			Geographic Map-based Discovery (Mobile Kommunikation)
**GMPR**			GARP Multicast Registration Protocol (Protokolle)
**GMS**				Global Messaging Service
**GMSC**			Gateway Mobile Switching Centre --> Gateway-Vermittlungsstelle (Mobilfunknetze)
**GMSK**			Gaussian Minimum Shift Keying --> Digitales Modulationsverfahren
**GMT**				Greenwich Mean Time
**GND**				Ground --> Erdung (Verkabelung)
**GNN**				Global Network Navigator
**GNP**				Geographical Number Portability
**GNS**				Get Nearest Server (Internetworking)
**GOES**			Geostationary Operational Environmental Satellites (Sat-Komm)
**GOSIP**			Government Open Systems Interconnection Profile (UK, USA) (Standards, Verordnungen, Gesetze)
**GP**				Guard Period (Mobilfunknetze)
**GPD**				General Purpose Discipline --> Einfache Start/Stopp-Prozedur
**GPF**				Geodesic Packet Forwarding (Mobile Kommunikation)
**GPIB**			General Purpose Interface Bus --> IEC-Bus
**GPL**				General Public License (IT-Sicherheit)
**GPRS**			General Packet Radio Service (GSM, Mobilfunknetze)
**GPS**				Generic Presentation Service (ISO/OSI)
**GPS**				Global Positioning System
**GRE**				Generic Routing Encapsulation --> Tunneling-Protokoll im Internet (Internetworking)
**GRX**				Global Roaming Exchange
**GS**				Geographic Scope (Mobilfunknetze)
**GS**				Ground Station --> Bodenstation (Satellitenkommunikation)
**GS**				Group Separator --> Gruppentrennzeichen (Steuerzeichen)
**GSA**				GSM Systems Area (Mobilfunknetze)
**GSC**				Ground Switching Center --> Vermittlungszentrum (Mobilfunknetze)
**GSL**				Global Service Logic (Weitverkehrsnetze)
**GSM**				GPRS Session Management
**GSM**				Global System for Mobile Communications (ursprünglich: Groupe Spéciale Mobile) (Standard für Mobilfunknetze)
**GSMP**			General Switch Management Protocol (ATM) (Protokolle)
**GSMS**			GPRS Short Message Service (GSM)
**GSN**				GPRS Support Node (GSM)
**GSN**				Gigabyte System Network (NW-Konzept)
**GSN**				Global Subscriber Number (Mobilfunknetze)
**GSO**				Geostationary Satellite Orbit --> Geostationäre Satelliten-Umlaufbahn (Satellitenkommunikation)
**GSR**				Global State Routing
**GSS**				Ground Station Subsystem (Sat-Kommunikation)
**GSSAPI**			Generic Security Service Application Program Interface
**GSTN**			General Switched Telephone Network
**GSX**				Ground Storm X (VMware)
**GTFM**			Generalized Tamed Freqeuency Modulation --> Digitales Modulationsverfahren
**GTO**				Geostationary Transfer Orbit (Sat-Komm)
**GTP**				GPRS Tunneling Protocol (GSM) (Protokolle)
**GTS**				Global Telecommunications System (Telekommunikation)
**GUI**				Graphical User Interface
**GUM**				Gemeinschaftsumschalter
**GUS**				Guide to the Use of Standards (Standards, Verordnungen, Gesetze)
**GV**				Gebäudeverteiler (Verteiler)
**GVD**				Group Velocity Dispersion
**GVRP**			GARP VLAN Registration Protocol
**GVV**				Gewählte virtuelle Verbindung (Weitverkehrsnetze)
**GW**				Gateway (Internetworking)
**GW**				Gruppenwähler
**GWA**				Google Web Accelerator (Google)
**GWL**				Gateway Link (Sat-Kommunikation)
**GaAs**			Gallium Arsenid
**GbE**				Gigabit Ethernet (Ethernet)
**GfVrP**			Glasfaserverstärkerpunkt (Netzkomponente)
**GoBS**			Grundsätze ordnungsmäßiger DV-gestützter Buchführungssysteme
**GoS**				Grade of Service (Telekommunikation)
**GwG**				Geldwäschegesetz
**GÜP**				Gebäude-Übergabepunkt (Anschlussnetze)
**HA**				Hauptanschluss (Weitverkehrsnetze)
**HA**				High Availability
**HA**				High Availability (VMware)
**HA**				Home Agent (Sat-Kommunikation)
**HAL**				Hardware Abstraction Layer
**HAN**				Home Area Network (ATM, B-ISDN)
**HANFS**			Highly Available Network File System (NFS)
**HAs**				Hauptanschluss
**HBA**				Host Bus Adapter (Fiber Channel)
**HBA**				Host Bus Adapter for Fibre Channel storage networks.
**HBCI**			Home Banking Computer Interface (Online-Dienste)
**HBFG**			Host Behavior Functional Group (ATM, B-ISDN)
**HC**				Header Compression
**HC**				Horizontal Cable --> Horizontalkabel (Verkabelung)
**HCA**				Host Channel Adapter (Lokale Netze)
**HCD**				High Speed Copper Drop
**HCDM**			Hybrid Companding Delta Modulation (Modulation)
**HCF**				Host Command Facility
**HCI**				Host Controller Interface (Wireless LAN)
**HCL**				Hardware Compatibility List
**HCMTS**			High Capacity Mobile Telephone System
**HCS**				Hard Clad Silicon (Verkabelung)
**HCS**				Header Check Sequence
**HCS**				Header Checksum
**HCT**				Home Communication Terminal --> Set-Top-Box
**HD**				Halbduplex
**HD**				Halbduplex --> Übertragungsart
**HD**				Harmonisation Document --> Harmionisierungsdokument (CEN/CENELEC)
**HD-ML**			Handheld Markup Language (Mobilfunknetze)
**HDB**				High Density Bipolar (Codierung)
**HDB3**			High Density Bipolar Code of Order 3 --> Pseudoternärer Leitungscode (Codierung)
**HDF**				Host Data Facility
**HDH**				HDLC Distant Host
**HDLC**			High Level Data Link Control --> OSI-Schicht-2-Protokoll (Protokolle)
**HDMAC**			HDTV Multiplexed Analogue Components (Telekommunikation)
**HDPE**			Niederdruck-Polyäthylen (Verkabelung)
**HDR**				Header --> Anfangskennsatz (Protokolle)
**HDR**				High Data Rate
**HDSL**			High Bit-Rate Digital Subscriber Line (xDSL) (Anschlussnetze)
**HDTV**			High Definition Television --> Hochauflösendes Fernsehen
**HDW**				Hebdrehwähler (Weitverkehrsnetze)
**HDWDM**			High Density Wavelength Division Multiplex
**HDx**				Half Duplex --> Halbduplex-Betrieb (DK-Übertragungstechniken)
**HE**				Header Extension (Protokolle)
**HE**				Höheneinheit (Verkabelung)
**HEC**				Header Error Check
**HEC**				Header Error Control (ATM)
**HEL**				Header Extension Length
**HEMS**			High Level Entity Management System (Netzmanagement)
**HEO**				Highly Elliptical Orbit (Sat-Kommunikation)
**HEPNET**			High Energy Physics Network
**HEX**				Hexadezimal (Zahlensysteme)
**HF**				High Frequency --> Hochfrequenz
**HFBC**			High Frequency Broadcast Conference
**HFC**				Hybrid Fiber Coaxial --> Hybrides LwL-Koax-Kabel (Verkabelung)
**HFD**				Hauptanschluß für Direktruf (Deutsche Telekom)
**HFP**				Host-to-Front-End Protocol (Protokolle)
**HFR**				Hybrid Fiber Radio
**HFRG**			Gesetz über den Betrieb von Hochfrequenzgeräten (Standards, Verordnungen, Gesetze)
**HGB**				Handelsgesetzbuch
**HIC**				Highest Incomming Channel (Mobilfunknetze)
**HILI**			Higher Layer Interface (IEEE 802.1)
**HILI**			Higher Level Interface (Lokale Netze)
**HILN**			Harmonic and Individual Line plus Noise
**HIOC**			High Speed I/O-Card
**HIPERACCESS**			High Performance Radio Access (Anschlussnetze)
**HIPERLAN**			High Performance Radio Local Area Network (ETSI-Standard ETR 101 031 und ETR 101 683)
**HIPERLINK**			High Performance Radio Link (Anschlußssnetze)
**HIPERMAN**			High Performace Radio Metropolitan Network (Anschlussnetze)
**HIPPI**			High Performance Parallel Interface --> Schnelle parallele Schnittstelle (ANSI X3T9.3/88-023)
**HK**				Hauptkabel (Verkabelung)
**HKIX**			Hong-Kong Internet Exchange
**HL**				Header Length
**HL**				Hotline (Telekommunikation)
**HLA**				Home Location Area (Mobilfunknetze)
**HLC**				Higher Layer Capabilities
**HLF**				Halide-Free Insulation (Verkabelung)
**HLF**				High Layer Function --> Funktion der oberen Schichten
**HLLAPI**			High Level Language Application Programming Interface
**HLM**				Heterogenous LAN Management (Netzmanagement)
**HLPI**			High Layer Protocol Identifier
**HLR**				Home Location Register --> Heimatregister, Standortverzeichnis (GSM, Mobilfunknetze)
**HMA**				Human Machine Adaptation
**HMAC**			Hashed Message Authentification Code (IT-Sicherheit)
**HMI**				Hub Management Interface (Netzbetriebsysteme)
**HMMP**			HyperMedia Management Protocol (Protokolle)
**HMON**			HyperMedia Object Manager
**HMP**				Host Monitor Protocol (Protokolle)
**HMSD**			Higher Mode Signalling Deactivation
**HOC**				Highest Outgoing Channel
**HON**				Handover Number (Mobilfunknetze)
**HOPC**			High Order Path Crossconnection
**HP**				Hewlett-Packard
**HP**				High Priority
**HP-GL**			Hewlett Packard Graphics Language
**HP-PCL**			Hewlett Packard Printer Command Language
**HPA**				Horizontal Pod Autoscaler
**HPAD**			Host Packet Assembly and Disassembly
**HPC**				Handheld PC
**HPCA**			High Performance Computing Act
**HPF**				Highest Possible Frequency
**HPFS**			High Performance Filing System
**HPIB**			Hewlett Packard Interface Bus --> IEC-Bus
**HPKP**			HTTP Public Key Pinning
**HPLMN**			Home Public Land Mobile Network (Mobilfunknetze)
**HPN**				High Performance Network
**HPPI**			High Performance Parallel Interface
**HPPP**			High Performance Packet Processor
**HPR**				High Performance Routing (Internetworking)
**HPR-APPN**			High Performance Routing - Advanced Peer-to-Peer Network (IBM)
**HPSB**			High Performance Serial Bus (IEEE 1394)
**HPSK**			Hybrid Phase Shift Keying --> Modulationsverfahren
**HPSN**			High Performance Scalable Networking (NW-Konzepte)
**HQAM**			Hamming Quadrature Amplitude Modulation --> Digitales Modulationsverfahren
**HR**				Half Rate
**HRC**				Header Redundancy Check (ATM)
**HRC**				Horizontal Redundancy Check --> Horizontale Blockparitätsprüfung
**HRC**				Hybrid Ring Control (FDDI)
**HRL**				High Return Loss (Verkabelung)
**HRTC**			Half Rate Traffic Channel
**HS**				High Speed --> Hochgeschwindigkeit
**HS-DPCCH**			High Speed Dedicated Physical Control Channel --> HSDPA-Uplink-Steuerkanal (Mobilfunk)
**HS-PDSCH**			High Speed Physical Downlink Shared Channel --> HSDPA-Downlink-Datenkanal (Mobilfunk)
**HS-SCCH**			High Speed Shared Control Channel --> HSDPA-Downlink-Steuerkanal (Mobilfunk)
**HSCSD**			High Speed Circuit Switched Data (GSM, Mobilfunknetze)
**HSD**				High Speed Data
**HSDPA**			High Speed Downlink Packet Access
**HSDS**			High Speed Digital Subscriber
**HSLAN**			High Speed Local Area Network --> Hochgeschwindigkeits-LAN
**HSLPC**			High Speed Line Processing Card
**HSM**				Hardware Security Module
**HSM**				Hierarchical Storage Management
**HSM**				Hierarchical Storage Management (Speichernetze)
**HSP**				High Speed Protocol --> Paketorientiertes Datenübertragungsprotokoll (X.25)
**HSPFIC**			High Speed Peripheral Fabric Interface Card
**HSRP**			Hot Standby Router Protocol (Cisco) (Protokolle)
**HSSB**			High Speed Serial Bus
**HSSC**			High Speed Shelf Controller
**HSSDC**			High Speed Serial Data Connection (Fiber Channel)
**HSSE**			High Speed Switching Element (ATM, B-ISDN)
**HSSG**			Higher Speed Study Group (Ethernet) (Gremien, Organisationen)
**HSSI**			High Speed Serial Interface --> Bitserielles Schicht 1-Protokoll
**HST**				High Speed Technology
**HSTP**			High Speed Transport Protocol (Protokolle)
**HSTR**			High Speed Token Ring (Token Ring)
**HSTS**			HTTP Strict Transport Security
**HT**				Horizontal Tabulation --> Horizontal-Tabulator (Steuerzeichen)
**HT**				Hyper Text (Multimedia)
**HTML**			Hypertext Markup Language
**HTML**			Hypertext Markup Language (Internet)
**HTTP**			Hypertext Transfer Protocol --> Anwendungsprotokoll im Internet (RFC 2068)
**HTTPS**			Hypertext Transfer Protocol Secure --> Anwendungsprotokoll im Internet (RFC xxxx)
**HTTPSS**			HTTP specific Settings
**HV**				Hauptverteiler (Verkabelung)
**HVEC**			Harmonic Vector Excitation Coding (Codierung)
**HVSt**			Hauptvermittlungsstelle (Weitverkehrsnetze)
**HVV**				Hausverteil-Verstärker (Anschlussnetze)
**HVt**				Hauptverteiler (Anschlussnetze)
**HW**				Hardware
**Hifi**			High Fidelity
**Hkz**				Hauptanschlusskennzeichen (ISDN)
**HoB**				Head of Bus
**HoL**				Hands-On Labs (VMware)
**Host**			Profiles Feature to deploy a pre-determined configuration to an ESXi host. (VMware)
**Hz**				Hertz --> Frequenzmaßeinheit
**HÜP**				Hausübergabepunkt
**I**				Information Field --> Informationsfeld (Token Ring)
**I**				Information Frame (Datenfelder)
**I**				Initiating
**I**				Integrity
**I/F**				Interface
**I/O**				Input/Output
**I/O**				input/output
**I2S**				Inter-IC Sound Bus
**IA**				Implemention Agreement (Frame Relay)
**IA**				Incomming Access
**IA2**				Internationales Alphabet Nr. 2
**IA5**				Internationales Alphabet Nr. 5
**IAB**				Internet Advertising Bureau (Gremien, Organisationen)
**IAB**				Internet Architecture Board (Gremien, Organisationen)
**IAB**				Internet Architecture Board (Internet)
**IAC**				Information Asset Classification
**IAC**				Interconnection Access Charge (Telekommunikation)
**IAD**				Integrated Access Device
**IAE**				ISDN-Anschluss-Einheit (ISDN)
**IAM**				Identity & Access Management
**IAM**				Initial Address Message
**IAM**				Integrierter Anruf Manager
**IANA**			Internet Assigned Numbers Authority (Internet) (Gremien, Organisationen)
**IAOG**			International ADMD Operators Group (Mitteilungsdienste)
**IAP**				Internet Access Provider
**IAPP**			Inter-Access-Point Protocol (WLAN, Protokolle)
**IARP**			Inverse Address Resolution Protocol --> Internet-Protokoll (RFC 2390)
**IARU**			International Amateur Radio Union (Gremien, Organisationen)
**IAS**				Image Area Separation
**IAS**				Information Access Service (Wireless LAN)
**IASG**			Internetwork Address Sub-Group (ATM, B-ISDN)
**IB**				In-Band
**IB**				Information Base
**IBC**				Integrated Broadband Communication
**IBCN**			Integrated Broadband Communication Network --> Integriertes Breitband-Fernmeldenetz (NW-Konzepte)
**IBDN**			Integrated Building Distribution Network
**IBFN**			Integriertes Breitband-Fernmeldenetz (Deutsche Telekom)
**IBGP**			Interior Border Gateway Protocol (Protokolle)
**IBMS**			Integriertes Breitbandiges Mobilkommunikationssystem (Anschlussnetze)
**IBN**				Integrated Broadband Network
**IBS**				International Business Service (Mobilfunknetze)
**IBSS**			Independent Basic Service Set
**IBT**				Intrinsic Burst Tolerance
**IBTA**			Infiniband Trade Association (Gremien, Organisationen)
**IBTN**			Integrated Broadband Telecom Network (ISDN)
**IC**				Input Controller (ATM, B-ISDN)
**IC**				Integrated Circuit
**IC**				Interconnection
**ICA**				Independent Computing Architecture (NW-Konzepte)
**ICA**				Integrated Communicaton Adapter
**ICA**				Intelligent Communication Adaptor
**ICA**				International Communications Association (Gremien, Organisationen)
**ICAN**			Individual Customer Access Network
**ICANN**			Internet Corporation for Assigned Names and Numbers (Gremien, Organisationen)
**ICAP**			Internet Calender Access Protocol (Internet, Protokolle)
**ICAP**			Internet Content Adaption Protocol (Internet, Protokolle)
**ICC**				Integrated Circuit Card (IT-Sicherheit)
**ICCB**			Internet Control and Configuration Board (Gremien, Organisationen)
**ICCS**			Integrated Communications Cabling System (Verkabelung)
**ICD**				Intelligent Call Distribution
**ICD**				International Code Designator (ISO/OSI)
**ICDM**			Interactive Communication Data Management (Netzmanagement)
**ICE**				Information (and) Content Exchange
**ICF**				Information Conversion Function
**ICFG**			Internetwork Coordination Functional Group (ATM, B-ISDN)
**ICI**				Inter-Cell-Interference
**ICI**				Intercarrier Interface
**ICI**				Interface Control Information
**ICIP**			Inter Carrier Interface Protocol (Internetworking, Protokolle)
**ICM**				Intelligent Call Management (Telekommunikation)
**ICMP**			Internet Control Message Protocol --> Schicht-3-Internet-Protokoll (RFC 792)
**ICO**				Intermediate Circular Orbit (Sat-Kommunikation)
**ICP**				Interconnection Partner
**ICP**				Internet Cache Protocol (Internet, Protokolle)
**ICP**				Internet Control Protocol (Banyan) (Protokolle)
**ICP**				Intertask Commnication Protocol (Protokolle)
**ICPC**			International Cable Protection Commitee --> Zwischenstaatliches Gremium zum Schutz von Unterwasserkabeln (Gremien, Organisationen)
**ICQ**				I Seek You (Chat)
**ICR**				Initial Cell Rate (ATM, B-ISDN)
**ICR**				Intelligent Character Recognition --> Intelligente Zeichenerkennung
**ICS**				IBM Cabling System --> IBM-Verkabelungssystem (Verkabelung)
**ICS**				Implementation Conformance Statement
**ICS**				Internet Connector Service
**ICS**	                    	Industrial Control Systems
**ICSA**			International Computer Security Association --> Industrieverband der Hard- und Softwareindustrie (Gremien, Organisationen)
**ICT**				Information and Communication Technology --> Informations- und Kommunikationstechnik
**ICUG**			International Closed User Group
**ICV**				Integrity Check Value (Datenfelder)
**ICW**				Internet Call Waiting
**ID**				Identification
**ID**				Identifier
**ID**				Identity (IT-Sicherheit)
**ID**				Intrusion Detection (IT-Sicherheit)
**IDA**				Integrated Digital Access --> Integrierter, digitaler Zugriff (DK-Übertragungstechniken)
**IDC**				IBM Data Connector --> IBM-Datenstecker (Verkabelung)
**IDCT**			Inverse Discrete Cosine Transform (Modulation)
**IDDD**			International Direct Distance Dialling --> Internationaler Selbstfernwähldienst (Telekommunikation)
**IDEA**			International Data Encryption Algorithm --> Symmetrisches Verschlüsselungsverfahren (IT-Sicherheit)
**IDF**				Identify
**IDF**				Intermediate Distribution Frame
**IDFT**			Inverse Discrete Fourier Transformation
**IDI**				ISDN Direct Interface (ISDN)
**IDI**				Initial Domain Identifier (ISO/OSI)
**IDL**				Interface Definition Language
**IDMEP**			Intrusion Detection Message Exchange Protocol (Protokolle) (IT-Sicherheit)
**IDN**				Integrated Digital Network --> Integriertes Digitalnetz (Offene NW-Konzepte)
**IDN**				Integriertes Text- und Datennetz (Deutschen Telekom)
**IDN**				Interface Definition Notation
**IDN**				International Data Number (ITU-T X.121)
**IDNX**			Integrated Digital Network Exchange
**IDP**				Initial Domain Part (ISO/OSI)
**IDP**				Integrated Data Processing --> Integrierte Datenverarbeitung
**IDP**				Internet Datagram Protocol (Xerox) (Protokolle)
**IDP**				Intrusion Detection and Prevention (IT-Sicherheit)
**IDR**				Intermediate Data Rate
**IDRP**			Inter Domain Routing Protocol (ISO/IEC 10747) (Protokolle)
**IDRS**			Intrusion Detection and Response System (IT-Sicherheit)
**IDS**				Internet Distribution Service (Internet)
**IDS**				Intrusion Detection System (IT-Sicherheit)
**IDSL**			ISDN Rate Digital Subscriber Line (xDSL) (Anschlussnetze)
**IDU**				Indoor Unit
**IDU**				Interface Data Unit
**IE**				Informations-Elemente (ISDN)
**IE**				International Exchange --> Internationale Vermittlungsstelle (Telekommunikation)
**IE**				Internet Explorer (MIcrosoft)
**IEC**				Inter-Exchange Carrier (Weitverkehrsnetze)
**IEC**				International Electrotechnical Commission (Genf) (Gremien, Organisationen)
**IECC**			International Electronic Components Committee (CENELEC, Frankfurt/M.)
**IEE**				Institution of Electronic and Electrical Engineers (UK) (Gremien, Organisationen)
**IEEE**			Institute of Electrical and Electronics Engineers (New York) (Gremien, Organisationen)
**IEICE**			Institute of Electronics, Information and Communication Engineers (Japan) (Gremien, Organisationen)
**IEN**				Internet Engineering Notes (Standards, Verordnungen, Gesetze)
**IEPG**			Internet Engineering Planning Group (Gremien, Organisationen)
**IESG**			Internet Engineering Steering Group (Gremien, Organisationen)
**IETF**			Internet Engineering Task Force --> Standardsierungsgremium für das Internet (Gremien, Organisationen)
**IETS**			Intermediate European Technical Standard (Standards, Verordnungen, Gesetze)
**IF**				Interface --> Schnittstelle
**IF**				Intermediate Frequency
**IFBN**			Integriertes Fernmelde-Breitbandnetz (Deutsche Telekom)
**IFCP**			Internet Fiber Channel Protocol (Fiber Channel, Protokolle)
**IFD**				Interface Device --> Chipkartenterminal (IT-Sicherheit)
**IFFT**			Inverse Fast Fourier Transformation
**IFG**				Inter Frame Gap (Ethernet)
**IFIP**			International Federation of Information Processing (Genf) (Gremien, Organisationen)
**IFMP**			Ipsilon Flow Management Protocol (RFC 1953) (Protokolle)
**IFRB**			International Frequency Registration Board (Genf) (Gremien, Organisationen)
**IFS**				Inter Frame Space (Wireless LAN)
**IFU**				Internationale Fernmeldeunion (Gremien, Organisationen)
**IFVV**			Internationale Feste Virtuelle Verbindung (DK-Übertragungstechniken)
**IGC**				Institute for Global Communication (San Francisco) (Gremien, Organisationen)
**IGMP**			Internet Group Management Protocol 
**IGMP**			Internet Group Management Protocol --> Internet-Protokoll (RFC 1112) (Protokolle)
**IGN**				IBM Global Network (IBM)
**IGP**				Interior Gateway Protocol (Internet, Protokolle)
**IGP**				interior gateway protocol
**IGRP**			Interior Gateway Routing Protocol --> Routing-Protokoll (Cisco) (Protokolle)
**IGSO**			Inclined Geosynchronous Orbit (Sat-Komm)
**IHL**				IP Header Length (Datenfeld)
**IHL**				Initial Header Length (Datenfeld)
**IHMP**			Intelligent Hub Management Protocol (Protokolle)
**IIH**				IS-IS Hello (ISO/OSI)
**IILS**			Internet Information Locator Service
**IINTERNIC**			Internet Network Interface Controller
**IIOP**			Internet Inter-ORB Protocol (Internet, Protokolle)
**IIP**				Internet Imaging Protocol (HP, Intel) (Protokolle)
**IISP**			Interim Inter-Switch Signalling Protocol --> ATM-Signallisierungs-Protokoll (ATM, B-ISDN) (Protokolle)
**IIVR**			Integrated Interactive Voice Response
**IJF**				Intersymbol Jitter Free
**IKE**				Internet Key Exchange (IT-Sicherheit)
**IKMP**			Internet Key Management Protocol (Internet, Protokolle, IT-Sicherheit)
**IKS**				Informations- und Kommunikationssystem
**IKT**				Informations- und Kommunikationstechnik
**IKZ**				Impulskennzeichen (ISDN)
**IL**				Insertion Loss --> Einfügungsdämpfung (Verkabelung)
**ILAN**			Industrial LAN
**ILD**				Injection Laser Diode (Optische Netze, SDH, Sonet)
**ILEC**			Incumbent Local Exchange Carrier
**ILI**				Intelligent Link Interface
**ILM**				Information Lifecycle Management
**ILMI**			Interim Local Management Interface (ATM)
**ILPF**			Internet Law and Policy Forum (Gremien, Organisationen)
**ILR**				Interworking Location Register
**ILS**				Internet Locator Service
**IM-UAPDU**			Interpersonal Messaging User Agent Protocol Data Unit (Mobilfunkdienste)
**IMA**				Intermodulationsabstand
**IMA**				Inverse Multiplexing over ATM
**IMAC**			Isochronous Medium Access Control (FDDI)
**IMAP**			Internet Message Access Protocol --> Schicht-6-Protokoll (Protokolle)
**IMBE**			Improved Multiband Excitation
**IMC**				Internet Mail Consortium (Gremien, Organisationen)
**IMD**				Intermodulation Distortion --> Intermodulationsverzerrung
**IMDR**			Initiative Marketing Digital Radio (Gremien, Organisationen)
**IME**				Interface Management Entity
**IMEI**			International Mobile Equipment Identity --> Internationale Mobilfunk-Gerätekennung (GSM, Mobilfunknetze)
**IMEI**			International Mobile Station Equipment Identity --> Internationale
**IML**				Initial Machine Load
**IML**				Internationale Mietleitung
**IMN**				Intermediate Node --> Transitknoten
**IMP**				Interface Message Processor
**IMPDU**			Initial MAC Protocol Data Unit
**IMS**				Information Management System (Hersteller-NW-Architekturen)
**IMSI**			Inter MAN System Interface (Weitverkehrsnetze)
**IMSI**			International Mobile Subscriber Identity --> Internationale Mobilfunk-Teilnehmerkennung (GSM, Mobilfunknetze)
**IMSO**			International Mobile Satellite Organization (Sat-Kommunikation)
**IMSP**			Internet Message Support Protocol (Internet, Protokolle)
**IMT 2000**			International Mobile Telecommunications in the Year 2000
**IMT**				International Mobile Telecommunications
**IMTC**			International Multimedia Teleconferencing Consortium (Gremien, Organisationen)
**IMTS**			Improved Mobile Telephone Service (AT&T)
**IMUIMG**			ISDN Memorandum of Understanding Implementation Management Group (Gremien, Organisationen)
**IN**				Intelligent Network --> Intelligentes Netz (Weitverkehrsnetz)
**INA**				Information Network Architecture
**INA**				Interactive Network Adapter
**INAP**			Intelligent Network Application Part
**INAP**			Intelligent Network Application Protocol
**INB**				Internal Network Bus
**INCM**			Intelligent Network Conceptual Model
**IND**				Indication (Steuerzeichen)
**INDI**			Informations- und Dialogdienste (BTX)
**INES**			International Nuclear Event Scale
**INF**				Information (Zeichen, Zeichensatz)
**INLP**			Inactive Network Layer Protocol (Protokolle)
**INM**				Integrated Network Management (Netzmanagement)
**INMARSAT**			International Maritime Satellite Organization (London) (Gremien, Organisationen)
**INMS**			Integrated Network Management System (Netzmanagement)
**INN**				Intermediate Network Node (Hersteller-NW-Archtiekturen)
**INOBA**			Intelligent Node for Basic Access (ISDN)
**INOC**			Internet Network Operations Center
**INP**				Internetwork Nodal Processor
**INS**				Integrated Network System --> Integriertes Übertragungssystem (DK-Übertragungstechniken)
**INT**				Interrupt --> Unterbrechnung (Steuerzeichen)
**INTELSAT**			International Telecommunications Satellite Consortium --> Internationales Fernmelde-Satelliten Konsortium (Gremien, Organisationen)
**INTUG**			International Telecommunications User Group (Gremien, Organisationen)
**INUP**			Intelligent Network User Part (Weitverkehrsnetze)
**INWG**			Internetwork Working Group (Gremien, Organisationen)
**INXS**			Internet Exchange Service
**IOC**				Interoffice Channel (Weitverkehrsnetze)
**IOD**				Information on Demand (Online-Dienste)
**IODC**			International Operator Direct Calling
**IODM**			I/O Device Management
**ION**				Internetworking over NBMA (Internet-Protokolle)
**IONL**			International Organization of the Network Layer
**IOP**				Interoperability --> Interoperabilität
**IOPs**			Input/Outputs per second, detailed measurement of a drive's performance.
**IOR**				Indian Ocean Region (INMARSAT, Sat-Kommunikation)
**IOS**				Internetwork Operating System
**IOT**				Intelligent Optical Terminal (Anschlussnetze)
**IOTP**			Internet Open Trading Protocol (Protokolle)
**IOUG**			International Oracle Users Group (Gremien, Organisationen)
**IP X.25**			Interworking Port X.25 (ISDN)
**IP**				Information Provider
**IP**				Intelligent Peripheral
**IP**				Intelligent Peripheral (Weitverkehrsnetze)
**IP**				Internet Protocol (RFC 791, 1752)
**IP**	                    	Internet Protocol
**IP-ATM**			IP and ARP over ATM (RFC 2225, 2320)
**IP-FDDI**			IP and ARP over FDDI (RFC 1390)
**IP-HIPPI**			IP over HIPPI (RFC 2067)
**IP-IEEE**			IP over IEEE 802 Networks (RFC1042)
**IPAD**			Integrated PAD (Hersteller-NW-Architekturen)
**IPAX**			Internet Protocol for Aeronautical Exchange (Eurocontrol)
**IPBX**			Intelligent Private Branch Exchange
**IPC**				Interprocess Communication
**IPCDN**			IP over Cable Data Network (Internet, Protokolle)
**IPCP**			Internet Protocol Control Protocol (RFC 1332) (Protokolle)
**IPDA**			IP Distributed Architecture
**IPDC**			Internet Protocol Device Control
**IPDR**			IP Detail Record
**IPG**				Inter Packet Gap (Lokale Netze)
**IPHC**			Internet Protocol Header Compression
**IPI**				Intelligent Peripheral Interface
**IPI-3**			Intelligent Peripheral Interface (Protocol) No. 3
**IPIP**			IP Encapsulation within IP (RFC 2003)
**IPL**				Initial Program Load
**IPM UA**			Interpersonal Messaging User Agent (Mitteilungsdienste)
**IPM**				Interpersonal Messaging (ITU-T X.420)
**IPMI**			Intelligent Platform Management Interface
**IPMI**			Internet Protocol Multicast Initiative
**IPMS**			Interpersonal Messaging System (ITU-T X.400)
**IPNNI**			Integrated Private Network to Network Interface (ATM, B-ISDN)
**IPP**				Internet Presence Provider
**IPP**				Internet Printing Protocol (Protokolle)
**IPPM**			Internet Protocol Performance Metric
**IPS**				IP Storage
**IPS**				Intelligent Protection Switching
**IPS**				Internet Protocol Suite
**IPS**				Intrusion Prevention System (IT-Sicherheit)
**IPSO**			IP Security Option
**IPSRA**			IP Security Remote Access (IT-Sicherheit)
**IPSS**			International Packet Switching Service
**IPSec**			Internet Protocol Security
**IPSec**			Internet Protocol Security (Protokolle, IT-Sicherheit))
**IPTEL**			IP Telephony --> IP-Telefonie (IP-Dienste)
**IPU**				ISDN-Datex-P-Umsetzer (Deutsche Telekom)
**IPVC**			International Permanent Virtual Connection --> Internationale permanente Verbindung
**IPX**				Internetwork Packet Exchange --> Internetzwerk-Protokoll (Novell) (Protokolle)
**IPXCP**			Internetwork Packet Exchange Control Protocol (Novell) (Protokolle)
**IPng**			Internet Protocol Next Generation (IP Version 6) (RFC 1752)
**IPoA**			IP over ATM
**IPoP**			Integrated Point of Presence (Internet, Protokolle)
**IPoS**			IP over SDH (Netz-, Transportprotokolle)
**IPv4**			Internet Protocol Version 4 (Protokolle)
**IPv4**	            	Internet Protocol based on v4
**IPv6**			Internet Protocol Version 6 (Protokolle)
**IPv6**	            	Internet Protocol based on v6
**IPv6CP**			IPv6 PPP Control Protocol (Protokolle)
**IR**				Implementation Requirements
**IR**				Information Retrieval
**IR**				Infrared
**IR**				Instruction Register --> Befehlsregister
**IR**				Integrity Requirement
**IR**				Intermediate Range
**IR**				Internet Registry
**IRA**				International Reference Alphabet --> IA5-Alphabet
**IRC**				Internet Relay Chat
**IRCP**			Internet Relay Chat Protocol (RFC 1459)
**IRD**				Integrated Receiver/Decoder
**IRDP**			ICMP Router Discovery Protocol (Protokolle)
**IRE**				Institute of Radio Engineers (Gremien, Organisationen)
**IREG**			International Roaming Expert Group (Mobilfunknetze)
**IRL**				Inter Repeater Link (Ethernet)
**IRN**				Intermediate Routing Node (Hersteller-NW-Architekturen)
**IRP**				Interior Routing Protocol (Protokolle)
**IRP**				Internal Reference Point --> Interner Bezugspunkt
**IRQ**				Interrupt Request --> Unterbrechnungsanforderung (Steuerzeichen)
**IRS**				Intermediate Reference System
**IRSG**			Internet Research Steering Group (Gremien, Organisationen)
**IRT**				Institut für Rundfunktechnik (Gremien, Organisationen)
**IRTF**			Internet Research Task Force
**IRTF**			Internet Research Task Force (Gremien, Organisationen)
**IRTP**			Internet Reliable Transaction Protocol (Internet, Protokolle)
**IS**				Information Separator --> Informationstrennzeichen (Steuerzeichen)
**IS**				Integrated Services
**IS**				Interim Standard (Standards, Verordnungen, Gesetze)
**IS**				Intermediate System --> Transit-System (ISO/OSI)
**IS**				International Standard (Standards, Verordnungen, Gesetze)
**IS**				Internet Standard (Standards, Verordnungen, Gesetze)
**IS-IS**			Intermediate System to Intermediate System --> OSI-Routingprotokoll (ISO/IEC) (Protokolle)
**ISA**				Integrated Service Architecture
**ISA**				International Federation of the National Standardization Association (Gremien, Organisationen)
**ISAKMP**			Internet Security Association and Key Management Protocol (IT-Sicherheit, Protokolle)
**ISAM**			Index-Sequential Access Method --> Index-sequentieller Zugriff (Speichertechnik)
**ISAPI**			Internet Information Server API
**ISBN**			International Standard Book Number
**ISC**				International Switching Center --> Auslandvermittlungsstelle (Weitverkehrsnetze)
**ISC**				Internet Service Card
**ISCP**			Integrated Service Control Point
**ISCSI**			Internet Small Computer System Interface over IP --> iSCSI-Protokoll (Speichernetze)
**ISD**				International Subscriber Dialing
**ISDN**			Integrated Services Digital Network --> Diensteintegrierendes digitales Telekommunikationsnetz
**ISDN-BA**			Integrated Services Digital Network - Basic Rate Access (ISDN)
**ISDN-BaAs**			ISDN-Basisanschluß (ISDN)
**ISDN-NT**			ISDN-Netzabschluß (ISDN)
**ISDN-NTBA**			ISDN-Netzabschluß für Basisanschluß (ISDN)
**ISH**				Intermediate System Hello (ISO/OSI)
**ISI**				Inter System Interface (Mobilfunknetze)
**ISI**				Intersymbol-Interference --> Impulsintereferenz
**ISL**				ISDN-Steckerleiste (ISDN)
**ISL**				Inter Satellite Link (Sat-Kommunikation)
**ISL**				Inter Switch Link (Lokale Netze)
**ISLAN**			Integrated Services LAN
**ISLN**			Integrated Services Local Network (Lokale Netze)
**ISM**				ISDN Standards Management Group (Gremien, Organisationen)
**ISM**				Industrial, Scientific, Medical --> Frequenzband im 2 GHz- und 5 GHz-Bereich
**ISMA**			Internet Streaming Media Alliance (Gremien, Organisationen)
**ISMS**			Information Security Management System
**ISMS**			Information Security Management System --> Informationssicherheits-Managementsystem (IT-Sicherheit)
**ISN**				Information System Network (NW-Konzepte)
**ISNS**			Internet Storage Name Service Protocol (Speichernetze)
**ISO**				International Organization for Standardization
**ISO**				International Standardization Organization (Gremien, Organisationen)
**ISOC**			Internet Society (Gremien, Organisationen)
**ISODE**			ISO Development Environment (Protokolle)
**ISP**				Information Service Provider (Weitverkehrsnetze)
**ISP**				Inmarsat Service Provider (Sat-Komm)
**ISP**				Intermediate Service Part
**ISP**				International Standardized Profile (ISO/OSI)
**ISP**				Internet Service Provider --> Anbieter von Internet-Diensten
**ISPBX**			Integrated Services Private Branch Exchange --> ISDN-fähige Telekommunikationsanlage
**ISPC**			International Signalling Point Code (Telekommunikation)
**ISPO**			Information Society Project (mittlerweile Promotion) Office (Gremien, Organisationen)
**ISPSC**			Internet Service Provider Security Council (Gremien, Organisationen)
**ISR**				Intermediate Session Routing
**ISRC**			International Standard Recording Code
**ISRF**			International Standard Recording File
**ISS AE**			ISDN Supplementary Services Application Entity (ISDN)
**ISS**				International Switching Symposium
**ISSI**			Inter Switching System Interface
**ISSLL**			Integrated Services Over Specific Link Layer (Internet)
**IST**				Integrated Switching and Transmission Network
**IST**				International Telecommunications Society (Gremien, Organisationen)
**ISTF**			Internet Society Task Force (Gremien, Organisationen)
**ISU**				Integrated Service Unit (Weitverkehrsnetze)
**ISU**				Isochronous Service Unit (DK-Übertragungstechniken)
**ISUP**			ISDN User Part (ISDN)
**ISX**				Integrated Switching Exchange --> Integrierte Leitungsvermittlung
**IT**				Information Technology --> Informationstechnologie
**IT**				Information Type --> Informationstyp
**IT**				information technology
**ITA**				International Telegraph Alphabet --> Internationales Telegrafenalphabet (Zeichen, Zeichensatz)
**ITAEGT**			IT Expert Group For Private Telecommunications (Gremien, Organisationen)
**ITB**				Intermediate Text Block
**ITC**				International Telecommunications Convention (Standards, Verordnungen, Gesetze)
**ITC**				Interoperability Test Center (Netzmanagement)
**ITE**				Independent Trading Exchange (Online-Dienste)
**ITE**				Information Technology Equipment (Netzkomponente)
**ITE**				Informationstechnische Einrichtung (Telekommunikations-Endgeräte)
**ITE**				International Transmission Exchange (Weitverkehrsnetze)
**ITF**				Information Task Force (Gremien, Organisationen)
**ITFS**			Instructional Television Fixed Service (Anschlussnetze)
**ITG**				Informationstechnische Gesellschaft (im VDE) (Gremien, Organisationen)
**ITGS**			Intelligent Traffic Guidance System (Mobilfunknetze)
**ITIC**			Information Technology Industries Council (Gremien, Organisationen)
**ITIL**			Information Technology Infrastructure Library
**ITIL**			information technology infrastructure library
**ITN**				Internationales Transportnetz
**ITP**				Interactive Terminal Protocol (Protokolle)
**ITRS**			International Terrestrial Reference System
**ITSEC**			Information Technology Security Evaluation Criteria (IT-Sicherheit)
**ITSEC**			Information and Technology Security (IT-Sicherheit)
**ITSP**			Internet Telephony Service Provider
**ITSTC**			Information Technology Steering Commitee (Gremien, Organisationen)
**ITT**				Invitation to Transmit (Lokale Netze)
**ITTF**			Information Technology Task Force (Gremien, Organisationen)
**ITU**				International Telecommunications Union
**ITU**				International Telecommunications Union (Genf) (Gremien, Organisationen)
**ITU-R**			ITU - Radio Communication Sector (Gremien, Organisationen)
**ITU-T**			ITU - Telecommunications Sector (Gremien, Organisationen)
**ITUSA**			Information Technology Users Standards Association
**ITV**				Interactive Television --> Interaktives Fernsehen
**ITX**				Intermediate Text Block
**IU**				Integer Unit
**IUT**				Implementation Under Test
**IV**				Informationsverarbeitung
**IV**				Initialisierungsvektor (IT-Sicherheit)
**IVANS**			International Value Added Network Service (Telekommunikation)
**IVD**				Integrated Voice and Data
**IVDLAN**			Integrated Voice Data LAN (IEEE 802.9)
**IVDT**			Integrated Voice/Data Terminal (Weitverkehrsnetze)
**IVE**				Integrierte Verbindungsdatenerfassung
**IVN**				Intervening Network
**IVOD**			Interactive Video on Demand (Telekommunikation)
**IVPN**			International Virtual Private Network (NW-Konzepte)
**IVR**				Individual Voice Recognition
**IVR**				Interactive Voice Response (Tk-Sprachdienste)
**IVS**				IBM-Verkabelungssystem (Verkabelung)
**IVTS**			International Video-Teleconferencing Service
**IVTeV**			Interessenverband Telekommunikation e.V. (Braunschweig) (Gremien, Organisationen)
**IVoIP**			Improved Voice over IP (IP-Dienste)
**IWF**				Interworking Function
**IWMSC**			Internetworking Mobile Switching Center
**IWU**				Internet Working Units
**IWU**				Interworking Unit (Netzkomponente)
**IWV**				Impulswahlverfahren (DK-Übertragungstechniken)
**IX**				Internet Exchange Architecture (Intel)
**IXC**				Inter Exchange Carrier (Weitverkehrsnetze)
**IXC**				Interchange Channel
**IXI**				International X.25 Interconnect (X.25)
**IZ**				Interregionalzone
**IaaS**	            	Infrastructure-as-a-Service
**IntServ**			Integrated Services (Internet)
**InterNIC**			Internet Network Information Center
**InvG**			Investmentgesetz
**IoT**	                    	Internet of Things
**IrCOMM**			IrDA Communications (Wireless LAN)
**IrDA**			Infrared Data Association (Gremien, Organisationen)
**IrLAN**			Infrared LAN (Wireless LAN)
**IrLAP**			IrDA Link Access Protocol (Wireless LAN)
**IrLMP**			IrDA Link Management Protocol (Wireless LAN)
**IrMC**			Infrared Mobile Communications
**IrMC**			IrDA Mobile Communication (Wireless LAN)
**IrOBEX**			IrDA Object Exchange (Wireless LAN)
**IrPHY**			IrDA Physical Layer (Wireless LAN)
**IrWW**			IrDA Wrist Watch (Wireless LAN)
**IuK**				Informations- und Kommunikationstechnik
**IuKDG**			Informations- und Kommunikationsdienste Gesetz (Standards, Verornungen, Gesetze)
**JBIG**			Joint Bi-Level Image Group (Gremien, Organisationen)
**JCP**				Java Community Process
**JCP**				Job Control Program (Datenverarbeitung)
**JDK**				Java Development Kit
**JFC**				Java Foundation Class
**JISC**			Japanese Industrial Standards Committee --> Japanisches Normierungsinstitut (Tokio) (Gremien, Organisationen)
**JPEG**			Joint Photographic Expert Group --> Kompressionsverfahren für Standbilder
**JPEG**			Joint Photographic Experts Group
**JRE**				Java Runtime Environment
**JSM**				Job Service Model
**JSON**			JavaScript Object Notation
**JSP**				Job Service Provider
**JSU**				Job Service User
**JTAG**			Joint Test Action Group
**JTC**				Joint Technical Committee (Gremien, Organisationen)
**JTM**				Job Transfer and Manipulation --> Schicht-7-Protokoll (ISO 8824, ISO 8825, ISO 8831, ISO 8832)
**JUNET**			Japan Unix Network
**JVM**				Java Virtual Machine
**KA**				Kommunikationsanschluss
**KA9Q**			Variante der TCP- und IP-Protokolle für Funkübertragung (RFC 1208)
**KAPI**			Kryptographic Application Programming Interface (IT-Sicherheit)
**KDC**				Key Distribution Center (IT-Sicherheit)
**KDG**				Kabel Deutschland GmbH (Gremien, Organisationen)
**KFS**				Kabelfernsehen (Anschlussnetze)
**KGEN**			Key Generation
**KGRZ**			Kommunales Gebietsrechenzentrum (Datenverarbeitung)
**KI**				Künstliche Intelligenz
**KIP**				Kinetics Internet Protocol (Protokolle)
**KIT**				Kernel for Intelligent Communication Terminals
**KKL**				Kanäle kleiner Leistung (Mobilfunknetze)
**KMA**				Kreuzmodulations-Abstand
**KMIP**			Key Management Interoperability Protocol
**KMP**				Key Management Protocol (Protokolle, IT-Sicherheit)
**KMS**				Kabelmanagement-System (Verkabelung)
**KP**				Kommunikationsprotokoll (Protokolle)
**KPI**				Key Performance Indicator
**KQML**			Knowledge Query and Manipulation Language
**KR**				Kommunikationsrechner (Netzkomponente)
**KSDS**			Key Sequenced Entry Set --> Variante von VSAM
**KT**				Kanalteiler
**KTV**				Kabel-Television
**KV**				Kabelverteiler (Verkabelung)
**KVM**				Kernel-based Virtual Machine
**KVSt**			Knotenvermittlungsstelle (Weitverkehrsnetze)
**KVz**				Kabelverzweiger
**KVz**				Knotenverzweiger (Anschlussnetze)
**KW**				Kurzwelle
**KWG**				Kreditwesengesetz
**KZU**				Kennzeichenumsetzer (Telekommunikation)
**KüFu**			Küstenfunk (Mobilfunknetze)
**L2CAP**			Logical Link Control and Adaptation Protocol (Protokolle, Bluetooth, Wireless LAN)
**L2F**				Layer 2 Forwarding --> Protokoll aus der PPP-Familie (Protokolle)
**L2ML**			Layer 2 Management Link
**L2Sec**			Layer 2 Security (IT-Sicherheit)
**L2TP**			Layer 2 Tunneling Protocol (Protokolle)
**LA**				Leitungsausrüstung
**LA**				Location Area --> Aufenthaltsbereich (Mobilfunknetze)
**LAA**				Locally Administrated Address
**LAC**				L2TP Access Concentrator
**LAC**				Link Aggregation Control
**LAC**				Location Area Code
**LACP**			Link Aggregation Control Protocol (Protokolle, IT-Sicherheit)
**LADT**			Local Access Data Transport (Weitverkehrsnetze)
**LAG**				Link Aggregation Group
**LAG**				Listen Address Group --> Gruppe von Empfangsadressen
**LAI**				Location Area Identity --> Funkverkehrbereichskennung (Mobilfunknetze)
**LAM**				LAN Adapter Module
**LAM**				Local Attachment Module (Token Ring)
**LAN**				Local Area Network --> Lokales Netz (Lokale Netze)
**LAN/RM**			LAN Reference Model --> LAN-Schichtenmodell (Lokale Netze)
**LANCE**			LAN Controller for Ethernet
**LANDP**			LAN Distributed Platform
**LANE**			LAN-Emulation (ATM, B-ISDN)
**LANRES**			LAN Ressource Extention and Services
**LAP**				Link Access Procedure (Protokolle, X.25)
**LAP-B**			Link Access Procedure Balanced --> HDLC-Derivat, X.25-Schicht-2-Protokoll (Protokolle)
**LAP-Bm**			Link Access Procedure for Bm-Channel (Protokolle, Mobilfunknetze)
**LAP-D**			Link Access Procedure on D-Channel --> Sicherungsprotokoll für den D-Kanal (Protokolle, ISDN)
**LAP-Dm**			Link Access Procedurel for Dm-Channel (Protokolle, Mobilfunknetze)
**LAP-F**			Link Access Procedure Frame Relay (Protokolle, Frame Relay)
**LAP-M**			Link Access Protocol For Modems (Protokolle, Modem)
**LAP-X**			Link Access Procedure Adapted to Half Duplex
**LARP**			Local Address Resolution Protocol (Protokolle)
**LAS**				Leitungsanschlusseinheit (Netzkomponente)
**LASER**			Light Amplification by Stimulated Emission (Verkabelung)
**LAT**				Local Area Transport --> Terminalserver-Protokoll (DECnet) (Protokolle)
**LATA**			Local Area and Transport Area
**LATM**			LAN over ATM
**LAVC**			Local Area Vax Cluster --> Kommunikation zwischen DEC VAX Computer (DECnet) (Protokolle)
**LAWN**			Local Area Wireless Network (Wireless LAN)
**LB**				Leaky Bucket --> Leaky-Bucket-Algorthmus (ATM, B-ISDN)
**LB**				Line Buffer
**LB**				Load Balancing
**LBA**				Logical Block Addressing
**LBR**				Low Bitrate Header (Wireless LAN)
**LBRV**			Low Bit Rate Voice (Codierung)
**LBS**				LAN Bridge Server (Token Ring)
**LBS**				Location Based Service (Mobilfunknetze)
**LBT**				Listen Before Talk (Ethernet)
**LBZ**				Leitungsbezeichnung
**LC**				Link Control --> Verbindungssteuerung (DK-Übertragungstechniken)
**LC**				Link Controller (Bluetooth)
**LCA**				Load Controled Amplifier
**LCB**				Liquid Crystal Display --> Flüssigkeitsanzeige
**LCC**				Lost Calls Cleared (Telekommunikation)
**LCD**				Liquid Crystal Display --> Flüssigkristallanzeige
**LCD**				Lost Calls Delayed (Telekommunikation)
**LCF-PMD**			Low Cost Fiber - Physical Medium Dependent (FDDI)
**LCFS**			Last Come First Serve
**LCGN**			Logical Channel and Group Number --> Logische Kanal- und Gruppennummer (X.25)
**LCI**				Logical Channel Identifier (X.25)
**LCID**			Local Character Set Identification
**LCM**				Line Control Module
**LCN**				Local Communication Network
**LCN**				Logical Channel Number (X.25)
**LCP**				Link Control Protocol --> Protokol aus der PPP-Familie (Protokolle)
**LCR**				Least Cost Routing (Internetworking)
**LCR**				Line Control Register (Weitverkehrsnetze)
**LCS**				Local Coordination System
**LCS**				Location Services
**LCT**				Last Conformance Time
**LCV**				Logical Connection Verification
**LD**				Laser Diode --> Laserdiode (Verkabelung)
**LD**				Long Distance (Weitverkehrsnetze)
**LD**				Loop Disconnect
**LD-CELP**			Low Delay - Code Excited Linear Prediction --> Sprachkompressionsverfahren (ITU-T G.728)
**LDA**				Location Dependent Address (GSM-R)
**LDAP**			Lightweight Directory Access Protocol --> Schicht-5-Protokoll im Internet (RFC 2251)
**LDDI**			Local Distributed Data Interface (ISO/OSI)
**LDIB**			Local Directory Information Base
**LDIF**			Lightweight Data Interchange Format (Verzeichnisdienste)
**LDM**				Limited Distance Modem (Modem)
**LDM**				Linear Delta Modulation (Modulation)
**LDM**				Lineare Deltamodulation
**LDM**				Long Data Message (Mobilfunknetze)
**LDMOS**			Lateral Diffused Metal Oxyde Silicon
**LDP**				Label Distribution Protocol (Protokolle, MPLS)
**LDP**				Loader Debugger Protocol (Protokolle)
**LDPE**			Hochdruck-Polyäthylen (Verkabelung)
**LDR**				Light Dependent Resistor (Verkabelung)
**LDR**				Low Data Rate
**LDSG**			Landesdatenschutzgesetz (Standards, Verordnungen, Gesetze)
**LDTV**			Low Definition Television (Multimedia)
**LE**				Layer Entity
**LE**				Local Exchange --> Ortsvermittlungsstelle
**LE-ARP**			LAN Emulation - Address Resolution Protocol (ATM, B-ISDN)
**LEA**				Last Error Address
**LEC**				LAN Emulation Client (ATM, B-ISDN)
**LEC**				Local Exchange Carrier (Weitverkehrsnetze)
**LECID**			LAN Emulation Client Identifier (ATM, B-ISDN)
**LECS**			LAN Emulation Configuration Server
**LED**				Light Emitting Diode --> Leuchtdiode (Verkabelung)
**LEM**				Link Error Module (FDDI)
**LEN**				Low Entry Networking (IBM)
**LEO**				Low Earth Orbit --> Niedrige Erdumlaufbahn (Sat-Kommunikation)
**LEO**				Lyons Electronic Office
**LEPM**			Leitungsendgerät für den PMXA
**LER**				Label Edge Router (Internetworking, MPLS)
**LER**				Link Error Rate (FDDI)
**LERG**			Local Exchange Routing Guide
**LES**				LAN Emulation Server (ATM, B-ISDN)
**LES**				Land Earth Station --> Land-Erdfunkstelle (Sat-Kommunikation, INMARSAT)
**LEX**				Local Exchange --> Ortsvermittlungsstelle (Weitverkehrsnetze)
**LF**				Line Feed --> Zeilenvorschub (Steuerzeichen)
**LF**				Loop Filter
**LF**				Low Frequency --> Langwelle
**LFC**				Local Function Capabilities --> Örtliche Funktionsfähigkeiten
**LFI**				Local File Inclusion
**LFS**				LAN Files Services
**LFSR**			Linear Feedback Shift Register
**LGN**				Logical Group Node (ATM, B-ISDN)
**LGN**				Logical Group Number (X.25)
**LH**				Line Hunting (ISDN)
**LH**				Link Header (Zeichen, Zeichensatz)
**LI**				Length Indicator --> Längenindikator
**LI**				Line Interface
**LIB**				Line Interface Base
**LIB**				Line Interface Board
**LIB**				Linear Incremental Backoff
**LIC**				Line Interface Card
**LIC**				Lowest Incoming Channel
**LID**				Local Injection and Detection (Verkabelung)
**LIDB**			Line Information Data Base
**LIF**				Low Intermediate Frequency
**LIFO**			Last In, First Out --> Stackspeicher (Speichertechnik)
**LIJ**				Leaf Initiated Join (ATM, B-ISDN)
**LIJP**			Leaf Initiated Join Parameter
**LILO**			Last In, Last Out
**LIMOP**			Linearly Frequency Modulated Pulse
**LIP**				Large Internet Packet (Novell, NetWare)
**LIP**				Loop Initialization Primitive (Fiber Channel)
**LIPS**			Lightweight Internet Person Schema
**LIR**				Local Internet Registry
**LIS**				Logical IP Subnet (ATM, B-ISDN)
**LIT**				Line Insulation Test
**LIU**				Lobe Insertion Unit
**LJE**				Local Job Entry
**LKZ**				Landeskennzahl (Weitverkehrsnetze)
**LL**				Local Loop (Anschlussnetze)
**LL**				Logical Link --> Logische Verbindung (DK-Übertragungstechniken)
**LLA**				Logical Layered Architecture
**LLAP**			Local Talk Link Access Protocol (Protokolle)
**LLB**				Line Loopback
**LLC**				Logical Link Control
**LLC**				Logical Link Control (IEEE 802.2)
**LLC**				Lower Layer Capability
**LLCDU**			Logical Link Control Data Unit
**LLF**				Line Link Frame
**LLF**				Lower Layer Function
**LLI**				Logical Link Identifier
**LLP**				Link Level Protocol (Protokolle)
**LM**				Link Manager (Bluetooth)
**LM**				Listen Mode
**LMCS**			Local Multipoint Communication System
**LMDS**			Local Multipoint Distribution Services (GSM)
**LME**				Layer Management Entity (ISO/OSI)
**LMEI**			Layer Management Entity Identifier
**LMI**				Layer Management Interface (ISO/OSI)
**LMI**				Local Management Interface
**LMP**				LAN Management Protocol (IBM/Token Ring)
**LMSI**			Local Mobile Subscriber Identifier (Mobilfunknetze)
**LMT**				Local Maintenance Terminal
**LNM**				LAN Network Manager (Netzmanagement)
**LNME**			LAN Network Management Entry
**LNNI**			LAN Emulation Network Node Interface (ATM, B-ISDN)
**LNP**				Local Number Portability
**LNS**				L2TP Network Server
**LOC**				Loss of Cell (ATM, B-ISDN)
**LOC**				Loss of Cell Delineation (ATM)
**LOC**				Loss of Channel
**LOC**				Loss of Continuity
**LOC**				Lowest Outgoing Channel (DK-Übertragungstechniken)
**LOF**				Loss of Frame (ATM)
**LOI**				Low Order Interface
**LON**				LAN Outer Networks (Lokale Netze)
**LON**				Local Operating Network
**LOP**				Loss of Payload (ATM, B-ISDN)
**LOP**				Loss of Pointer (ATM, B-ISDN)
**LOS**				Line of Sight --> Sichtverbindung
**LOS**				Loss of Signal (ATM, B-ISDN)
**LOTOS**			Language of Temporal Ordering Specifications
**LP**				Layer Protocol
**LP**				Line Printer
**LP**				Low Pass --> Tiefpass
**LP**				Low Priority
**LPC**				Linear Predictive Coding --> Sprachkomprimierungs-Verfahren
**LPC**				Longitudinal Parity Check (DK-Übertragungstechniken)
**LPCP**			Lightweight Phone Control Protocol (Internet, Protokolle)
**LPDA**			Line Problem Determination Access
**LPI**				Layer Protocol Implementation
**LPP**				Lightweight Presentation Protocol --> Schicht-6-Protokoll (Protokolle)
**LPT**				Line Print Terminal
**LQM**				Link Quality Monitor (Internetworking)
**LQR**				Link Quality Report (Internetworking)
**LR**				Local Register (Mobilfunknetze)
**LR**				Location Register
**LR**				Long Range
**LRC**				Longitudinal Redundancy Check --> Blockprüfverfahren
**LRCLK**			Left Right Clock
**LRG**				Location Reference Group
**LRM**				LAN Reporting Mechanism
**LRa**				Lokale Referenz a (ISDN)
**LS**				Link State
**LS**				Link Station
**LS**				Low Smoke --> Geringe Rauchentwicklung (Verkabelung)
**LSA**				Link Signalling Activation
**LSA**				Link State Advertisement (Internetworking)
**LSA**				Link State Algorithm --> Link-State-Algorithmus (Internetworking)
**LSAP**			Link Service Access Point
**LSAPI**			License Service Application Programming Interface (Microsoft)
**LSB**				Least Significant Bit --> Niederwertigstes Bit (Zahlensysteme)
**LSC**				Least Significant Character --> Niederwertigstes Zeichen
**LSC**				Link State Control --> Verbindungszustandskontrolle (DK-Übertragungstechniken)
**LSD**				Least Significant Digit --> Niederwertigste Ziffer
**LSD**				Low Speed Data
**LSDU**			Link Service Data Unit
**LSDV**			Link Segment Delay Value (Ethernet)
**LSF**				Low Smoke and Fume --> Geringe Rauch- und Dampfentwicklung (Verkabelung)
**LSL**				Link Support Layer (Netzbetriebssysteme)
**LSM**				LAN Station Management
**LSN**				Large Scale Networking (Group)
**LSNAT**			Load Sharing Network Address Translation (IT-Sicherheit)
**LSP**				Label Switched Path (Internetworking, MPLS)
**LSP**				Link State Packet (Internetworking)
**LSP**				Link State Protocol (Protokolle)
**LSR**				Label Switch Router (Internetworking, MPLS)
**LSR**				Leaf Setup Request (Offene NW-Konzepte)
**LSS**				Link Status Signal (ATM, B-ISDN)
**LSSU**			Link State Signal Unit (ISDN)
**LSU**				Link State Update
**LT**				Line Termination --> Leitungsabschluss (ISDN)
**LT**				Link Trailer --> Schlusskennsatz
**LT**				Lower Tester (Netzmanagement)
**LTC**				Line Test Unit Connector
**LTC**				Lowest Two-way Channel (DK-Übertragungstechniken)
**LTE**				Line Terminating Equipment (ATM, B-ISDN)
**LTG**				Line Trunk Group (Anschlussnetze)
**LTH**				Length Field --> Längenfeld
**LTM**				LAN Traffic Monitor (Netzmanagement)
**LTM**				Line Terminal Multiplexer
**LTM**				Line Terminal Multiplexer (Anschlussnetze)
**LTP**				Long Term Prediction --> Langzeitvorhersage (Mobilfunknetze)
**LTU**				Line Terminating Unit --> Leitungsendeinrichtung (Verkabelung)
**LTU**				Line Test Unit
**LU**				Line Unit
**LU**				Logical Unit (IBM)
**LU6.2**			Logical Unit 6.2 --> Sitzungsprotokoll (SNA)
**LUA**				Logical Unit Application Programming Interface
**LUF**				Lowest Usable Frequency
**LUN**				Logical Unit Name (Speichernetze)
**LUN**				Logical unit number, identifies shared storage (Fibre Channel/iSCSI).
**LUNI**			LAN Emulation User to Network Interface (ATM, B-ISDN)
**LUNS**			Logical Unit Network Service
**LUP**				Location Update (Mobilfunknetze)
**LVDS**			Low Voltage Differential Signalling
**LW**				Langwelle
**LW**				Leitungswähler (Weitverkehrsnetze)
**LWL**				Lichtwellenleiter (Verkabelung)
**LWT**				Listen While Talking (Ethernet)
**LZH**				Lempel, Ziv und Haruyasu --> Kompressionsverfahren für Daten (Codierung)
**LZH**				Lempel, Ziv und Huffman --> Kompressionsverfahren für Daten (Codierung)
**LZS**				Lempel, Ziv und Stac --> Kompressionsverfahren für Daten (Codierung)
**LZW**				Lempel, Ziv und Welsh --> Kompressionsverfahren für Daten (Codierung)
**LfD**				Landesbeauftragter für den Datenschutz (IT-Sicherheit)
**Lg**				Lagenverseilung (Verkabelung)
**Ltg**				Leitung (Verkabelung)
**LuftVZO**			Luftverkehrs-Zulassungs-Ordnung (translated Air Traffic Licensing Regulations)
**M**				Mega --> Präfix für Million (Zahlensysteme)
**M2M**				Machine to Machine
**MA**				Medium Adapter (Verkabelung)
**MA**				Modified Availability
**MAC**				Mandatory Access Control
**MAC**				Medium Access Control --> Kanalzugriffsverfahren (Lokale Netze, IEEE 802)
**MAC**				Message Authentication Code (IT-Sicherheit)
**MAC**				Modified Attack Complexity
**MAC**				Multiplier/Accumulator
**MACF**			Multiple Association Control Function
**MADS**			Multiple Access Data System --> Datensystem mit Mehrfachzugriff
**MADU**			Medium Access Data Unit
**MAE**				Metropolitan Area Exchange (Point)
**MAF**				Management Application Function (Netzmanagement)
**MAF**				Multiple Access Facility
**MAG**				Metropolitan Area Gateway
**MAG**				Multiple Address Group
**MAHO**			Mobile Assisted Handover
**MAL**				Management Application Layer
**MAN**				Metropolitan Area Network --> Stadtnetz (Stadt-, Regionalnetze)
**MANET**			Mobile Ad-Hoc Network
**MAO**				Maximum Acceptable Outage
**MAP**				Manufacturing Automation Protocol (General Motors)
**MAP**				Manufacturing Automation Protocol --> MAP-Protokoll (Protokolle)
**MAP**				Mobile Access Protocol
**MAP**				Mobile Access Protocol (Mobilfunknetze)
**MAP**				Mobile Application Part (Mobilfunknetze)
**MAP**				Mobile Application Part (SS#7)
**MAP**				Multi-User Authentication Policy (IT-Sicherheit)
**MAPDU**			Management Application Protocol Data Unit (Netzmanagement)
**MAPI**			Messaging Application Programming Interface (Microsoft)
**MAPOS**			Multiple Access Protocol over SONET/SDH (Protokolle)
**MARP**			Multilan Address Resolution Protocol (Protokolle)
**MARS**			Multicast Address Resolution Server (RFC 1755)
**MAS**				Multi Amplitude Signalling (Ethernet)
**MAS**				Multiple Access System (NW-Konzepte)
**MAS**				Multiprotocol Access Service (Hersteller-NW-Architekturen)
**MASE**			Message Administration Service Element --> Nachrichtenverwaltungs-Dienstelement (Mitteilungs-dienste)
**MATV**			Master Antenna Television
**MAU**				Medium Attachment Unit --> Komponente zwischen Medium und LAN-Endgerät (IEEE 802, Ethernet)
**MAU**				Multiple Access Unit (FDDI)
**MAU**				Multistation Access Unit (IBM/Token Ring)
**MAV**				Modified Attack Vector
**MB**				Mega Byte
**MBGP**			Multiprotocol Border Gateway (Internetworking)
**MBONE**			Multicast BackBONE
**MBONE**			Multicast Backbone (Internet-Protokolle)
**MBS**				Maximum Burst Size (ATM)
**MBS**				Mobile Broadband System --> Mobiles Breitbandsystem (Mobilfunknetze)
**MBX**				Mailbox
**MC**				Message Code (Mobilfunknetze)
**MC**				Micro Channel
**MC**				Modified Confidentiality
**MC**				Multicast
**MC**				Multipoint Controller
**MCA**				Micro Channel Architecture (IBM)
**MCB**				Message Command Block --> Nachrichtensteuerblock
**MCC**				Master Control Center (Sat-Kommunikation)
**MCC**				Mobile Country Code (GSM, Mobilfunknetze)
**MCD**				Maintenance Cell Description --> Wartungszellen-Beschreibung
**MCDDD**			Multi-Carrier Demultiplexer/Demodulator/Decoder
**MCDN**			Meridian Customer Defined Network (Nortel)
**MCDN**			Micro Cellular Data Network
**MCDV**			Maximum Cell Delay Variance (ATM, B-ISDN)
**MCE**				Management Communication Engine
**MCF**				MAC Convergence Function (Stadt-, Regionalnetze)
**MCF**				Message Communications Function
**MCHO**			Mobile Controlled Handover (Mobilfunknetze)
**MCHT**			Medium Call Hoding Time
**MCI**				Multimedia Control Interface (Microsoft/IBM)
**MCID**			Malicious Call Identification (ISDN)
**MCLR**			Maximum Cell Loss Ratio (ATM, B-ISDN)
**MCM**				Multicarrier Modulation --> Mehrträger-Modulation (Synonym für COFDM) (Mobilfunknetze)
**MCN**				Micro Cellular Network (Mobilfunknetze)
**MCNS**			Multimedia Cable Network System --> Breitbandkabelverteilnetz
**MCPC**			Multi Channel per Carrier
**MCR**				Maximum Cell Rate (ATM, B-ISDN)
**MCR**				Minimum Cell Rate (ATM, B-ISDN)
**MCS**				Mobile Communication System (Mobilfunknetze)
**MCS**				Multi-Category Security (SELinux)
**MCS**				Multicast Server
**MCS**				Multipoint Communication Service
**MCT**				Mobile Communication Terminal --> Mobilstation (Mobilfunknetze)
**MCTD**			Maximum Cell Transfer Delay (ATM, B-ISDN)
**MCU**				Multipoint Control Unit (ISDN)
**MCVD**			Modified Chemical Vapor Deposition (Verkabelung)
**MD**				Management Domain (ITU-T X.400)
**MD**				Management Domain --> Versorgungsbereich (Mitteilungsdienste)
**MD**				Meditation Device --> Umsetzungseinrichtung, Vermittlungsgerät
**MD**				Message Digest --> Hash-Funktion (IT-Sicherheit)
**MD-IS**			Mobile Data Intermediate System
**MD-PDU**			Management Data PDU (ATM, B-ISDN)
**MD2**				Message Digest No. 2 --> MD2-Algorithmus (IT-Sicherheit)
**MD4**				Message Digest No. 4 --> MD4-Algorithmus (IT-Sicherheit)
**MD5**				Message Digest No. 5 --> MD5-Algorithmus (IT-Sicherheit)
**MDAS**			Mobile Data Access System (Mobilfunknetze)
**MDC**				Manipulation Detection Code (Codierung)
**MDCP**			Media Device Control Protocol (Lucent)
**MDD**				Maximum Drive Distance (Token Ring)
**MDE**				Mobile Data Acquisition --> Mobile Datenerfassung (Mobilfunknetze)
**MDF**				Main Distribution Frame (Weitverkehrsnetze)
**MDF**				Management Defined Field
**MDF**				Mehrkanal-Datenübertragung mit Frequenzmultiplex --> Wechselstromtelegrafie
**MDG**				Message Development Group (Mobilfunkdienste)
**MDI**				Medium Dependent Interface --> Teil der MAU
**MDI**				Mobile Data Initiative --> Zusammenschluß zur Nutzung von Datenübertragung in GSM-Netzen (Gremien, Organisationen)
**MDI**				Mobile Data International (Mobilfunknetze)
**MDL**				Management Data Link
**MDM**				mobile device management
**MDNS**			Managed Data Network Service (Netzmanagement)
**MDPSK**			Modified Differential Phase Shift Keying --> Modifizierte differenzielle Phasenumtastung (Modulation)
**MDS**				Multichannel Distribution Service (Anschlussnetze)
**MDSE**			Message Delivery Service Element --> Dienstelement für Nachrichtenübergabe
**MDSL**			Medium Bitrate Digital Subscriber Line (xDSL)
**MDT**				Mean Down Time
**MDT**				Mittlere Datentechnik (Datenverarbeitung)
**MDTRS**			Mobile Digital Trunked Radio System (TETRA)
**MDZ**				Mehrkanal-Datenübertragung mit Zeitmultiplex (IDN)
**ME**				Magnitude Error
**ME**				Management Engine (Intel)
**ME**				Management Entity
**ME**				Mobile Equipment --> Mobiles Endgerät (GSM, Mobilfunknetze)
**MEC**				Multiport Ethernet Controller
**MED**				Multi-Exit Discriminator
**MEF**				Metro Ethernet Forum (Gremien, Organisationen)
**MEGACO**			Media Gateway Control Protocol (Protokolle)
**MEM**				Micro Electromechanical Mirror (Verkabelung)
**MEMS**			Micro Electromechanical System (Optische Netze, SDH, Sonet)
**MEO**				Medium Earth Orbit (Sat-Kommunikation)
**MER**				Modulation Error Ratio
**MES**				Master Earth Station (Sat-Kommunikation)
**METRAN**			Managed European Transmission Network (Offene NW-Konzepte)
**MF**				Matched Filter --> Signalangepasstes Filter
**MF**				Mediation Function
**MF**				Medium Frequency --> Mittelwelle
**MF**				More Fragments (Datenfeld)
**MF**				Multi Frequency
**MFA**				Management Functional Area
**MFA**				Multi-Factor Authentication
**MFC**				Multi Frequency Code --> Mehrfrequenzwahlverfahren (Weitverkehrsnetze)
**MFH**				Modified Frequenzy Hopping
**MFM**				Modified Frequency Modulation --> Aufzeichnungsverfahren für Festplatten
**MFN**				Multi Frequency Network
**MFPA**			Multi Functional Peripheral Association --> Konsortium für LAN-Peripheriegeräte (Gremien, Organisationen)
**MFPB**			Multifrequency Pushbutton
**MFSK**			Multiple Frequency Shift Keying (Modulation)
**MFSS**			Multiple Frequency Signalling System
**MFT**				Mobile Frequency Technologies --> Mobilfunktechnologien (Mobilfunknetze)
**MFV**				Mehrfrequenz-Wahlverfahren
**MG**				Media Gateway
**MGC**				Media Gateway Controller
**MGCP**			Media Gateway Control Protocol (Bellcore/Cisco) (Protokolle)
**MGT**				Management --> Management (Netzmanagement)
**MGT**				Mobile Global Title (Mobilfunknetze)
**MGW**				Media Gateway
**MH**				Message Handling --> Mitteilungs-Übermittlung (Mobilfunkdienste)
**MH**				Mobile Host
**MHC**				Modified Huffman Code --> Quellencodierungs-Verfahren (Codierung)
**MHEG**			Multimedia and Hypermedia Experts Group (Gremien, Organisationen)
**MHP**				Multimedia Home Platform
**MHS**				Message Handling Service (Mitteilungsdienste)
**MHS**				Message Handling System
**MHS**				Message Handling System (ITU-T X.400)
**MHX**				Mobitex Head Exchange (Mobitex)
**MHz**				Megahertz
**MI**				Management Interface (ATM, B-ISDN)
**MI**				Modified Integrity
**MIA**				Multivendor Integrated Architecture
**MIB**				Management Information Base (ISO/OSI) (Netzmanagement)
**MIC**				Managed Identity Controller
**MIC**				Medium Interface Connector --> Stecker für Lichtwellenleiter (Verkabelung)
**MIC**				Message Integrity Check --> Kryptographischer Integritätsschutzmechanismus
**MID**				Message Identifier
**MID**				Multiplexing Identification -> Multiplex-Kennzeichnung
**MIDI**			Musical Instrument Digital Interface
**MIF**				Management Information File (Netzmanagement)
**MIF**				Management Information Format (Netzmanagement)
**MIF**				Minimum Internetworking (Internetworking)
**MIFR**			Masters International Frequency (Gremien, Organisationen)
**MII**				Medium Independet Interface (Ethernet)
**MIL-STD**			Military Standard
**MILNET**			Military Network --> Teil des Defense Data Network in USA
**MIM**				Management Information Model (Netzmanagement)
**MIM**				Mobile Instant Messaging (Mobilfunknetze)
**MIM**				Modacom Identification Module (Mobilfunknetze)
**MIMD**			Multiple Instruction, Multiple Data
**MIME**			Multipurpose Internet Mail Extension (RFC 1521, 1522)
**MIN**				Multiple Interaction Negotiation (DK-Dienste)
**MIN**				Multipurpose Interconnection Network
**MIP**				Mega-frame Initialization Packet
**MIPS**			Million Instructions per Second --> Millionen Befehle pro Sekunde
**MIR**				Management Information Repository (Netzmanagement)
**MIR**				Minimum Information Rate (ATM)
**MIS**				Management Information System (Netzmanagement)
**MIT**				Management Information Tree (Netzmanagement)
**MIT**				Massachusetts Institute of Technology (Gremien, Organisationen)
**MITL**			Management Information Transport Layer (Netzmanagement)
**MITM**			Man in the Middle Attack
**MITS**			Management Information Transfer Service (Netzmanagement)
**MJ**				Modular Jack --> Steckerform (hauptsächlich in USA gebräuchlich) (Verkabelung)
**MK**				Magnetkarte (Speichertechnik)
**MK**				Mobiler Knoten
**ML**				Multilink (Mobilfunknetze)
**ML-PPP**			Multi-Link-Point-to-Point-Protocol (Protokolle)
**MLA**				Multileaving Adaptor
**MLC**				Multi Level Coding
**MLC**				Multilink Control Field
**MLI**				Multiple Line Interface (ODI)
**MLI**				Multiple Link Interface
**MLL**				Maximum Lobe Length (Token Ring)
**MLMA**			Multi Level Multi Access
**MLP**				Message Link Protocol (Protokolle)
**MLP**				Mirror Link Protocol (ATM, B-ISDN) (Protokolle)
**MLP**				Multi Layer Protocol (Protokolle)
**MLP**				Multi Link Procedure --> Mehrfachübermittlungsverfahren, Unterschicht von LAPB (X.25)
**MLS**				Modulated Light Source
**MLS**				Multi Level Security (IT-Sicherheit)
**MLT**				Multi Level Transmission --> Leitungscodierungsverfahren (Codierung)
**MLTU**			Monitoring (and) Line Transmission Unit
**MM**				Management Module
**MM**				Mobility Management (Mobilfunknetze)
**MM**				Multimedia
**MM**				Multimode
**MMA**				Main Mission Antenna (Sat-Kommunikation)
**MMAC**			Multimedia Access Center
**MMAP**			Multimedia Access Profile (Mobilfunknetze)
**MMAPI**			Multimedia Application Programming Interface
**MMC**				Microchannel to Mainframe Connection (Offene NW-Konzepte)
**MMC**				Mobile to Mobile Call (Mobilfunknetze)
**MMC**				Multimedia Collaboration
**MMCIP**			Media Manager Control Interface Protocol --> (Protokolle, Datenbank)
**MMDF**			Multi Channel Memorandum Distribution Facility (Mobilfunkdienste)
**MMDS**			Multichannel Multipoint Distribution System (Anschlußnetze)
**MMDS**			Multipoint Microwave Distribution System
**MME**				Mobile Management Entity (Mobilfunknetze)
**MMF**				Multi Mode Fiber --> Multimode-Lichtwellenleiter (Verkabelung)
**MMFS**			Manufacturing Message Format Standard (Standards, Verordnungen, Gesetze)
**MMI**				Man Machine Interface
**MMIP**			Media Manager Interchange Protocol --> (Protokolle, Datenbank)
**MMJ**				Modified Modular Jack --> Steckerform ähnlich zur RJ-Serie (Verkabelung)
**MMM**				Multimedia Mail
**MMP**				Media Management Protocol (Protokolle)
**MMR**				Meet-me room
**MMR**				Modified Modified Read --> Zweidimensionales Quellencodierungsverfahren (Codierung)
**MMS**				Manufacturing Message Specification (DK-Dienste)
**MMS**				Media Management System (Datenbank)
**MMS**				Microsoft Media Streaming Protocol (Protokolle)
**MMS**				Multimedia Message Service (GSM, UMTS)
**MMS**				Multimedia Messaging Service (Mobilfunknetze)
**MMS43**			Modified Monitored Sum 43 --> Leitungscodierungsverfahren aus der Familie der 4B/3T-Codes (Codierung)
**MMSC**			Multiparty Multimedia Session Control --> IETF-Gegenstück zu ITU-T H.323
**MMSK**			Modified Minimum Shift Keying --> Digitales Modulationsverfahren (Modulation)
**MN**				Mobile Node
**MNC**				Mobile Network Code (GSM, (Mobilfunknetze)
**MNO**				Mobile Network Operator
**MNP**				Microcom Networking Protocol --> Fehlerkorrekturprotokol für analoge Wählmodems
**MNRU**			Modulated Noise Reference Unit
**MO**				Managed Object (Netzmanagement)
**MO**				Mobile Originated (Mobilfunknetze)
**MOA**				Mobitex Operators Association (Gremien, Organisationen)
**MOB**				Managed Object Reference, a technique vCenter uses to classify every item. (VMware)
**MOC**				Managed Object Class (ISO/OSI) (Netzmanagement)
**MOC**				Mobile Originated Call (Mobilfunknetze)
**MOCS**			Managed Objects Conformance Statement (Netzmanagement)
**MOD**				Movies on Demand (Online-Dienste)
**MODAS**			Mobile Open Data Access Standard (Mobilfunknetze)
**MODEM**			Modulator/Demodulator
**MOF**				Managed Object Format
**MOH**				Music on Hold --> Externe Wartemusik (Telekommunikation)
**MOM**				Message Oriented Middleware
**MOP**				Maintenance Operation Protocol (DECnet) (Protokolle)
**MOP**				Mandatories, Optionals, Parameters (ATM, B-ISDN)
**MOS**				Mean Opinion Score --> Qualitätsmaß für Sprachübertragung (Telekommunikation)
**MOSPF**			Multicast Open Shortest Path First (Routing-Protokoll)
**MOST**			Media Oriented System Transport --> Optisches Bussystem mit Ringtopologie im Kfz-Bereich
**MOTIF**			Message Oriented Transfer Integrated File (DK-Schnittstellen)
**MOTIS**			Message Oriented Text Interchange System (ISO/IEC 10021)
**MOX**				Mobitex Area Exchange (Mobitex)
**MP**				Multipoint Processor
**MP**				Multiprotocol (Protokolle)
**MP+**				Multichannel Protocol Plus (Ascend)
**MP-iBGP**			Multi-Protocol Extension for BGP (Protokolle)
**MPC**				Mercury Personal Communication (Mobilfunknetze)
**MPC**				Multi Protocol Client (ATM, B-ISDN)
**MPC-MLQ**			Multipulse Coding Maximum Likelihood Quantization --> Verfahren zur Digitalisierung von Sprache
**MPCC**			Multiprotocol Personal Communications Controller
**MPDR**			Multi-Point Digital Radio
**MPDU**			Management Protocol Data Units (Wireless LAN)
**MPDU**			Message Protocol Data Unit (Mitteilungsdienste)
**MPE**				Multi-Point Extension (IEEE 802.3)
**MPE**				Multi-Protocol Encapsulation
**MPED**			MPoA Edge Device (ATM, B-ISDN)
**MPEG**			Motion Picture Experts Group --> Videokompressionsverfahren (Codierung)
**MPF**				Machine Processable Format
**MPH**				MPoA Host (ATM, B-ISDN)
**MPI**				Minimum Picture Interval
**MPI**				Multiple Protocol Interface (DK-Schnittstellen)
**MPKI**			Mobile Public Kex Infrastructure (IT-Sicherheit)
**MPLPC**			Multi-Pulse Linear Predictive Coding
**MPLS**			Multi-Protocol Label Switching (Internet, RFC 3031 – 3038, RFC 3063)
**MPMLQ**			Multiple Maximum Likelihood Quantization (Codierung)
**MPMP**			Microwave Point to Multipoint (Anschlussnetze)
**MPNP**			Multi-Protocol Network Programm
**MPPC**			Microsoft Point-to-Point Compression Protocol (Protokolle)
**MPPE**			Microsoft Point-to-Point Encryption (IT-Sicherheit)
**MPPP**			Multilink Point-to-Point Protocol (Internetworking) (Protokolle)
**MPR**				Modified Privileges Required
**MPR**				Multi Path Routing
**MPR**				Multi Protocol Router
**MPR**				Multi-Port Repeater
**MPS**				Multiprotocol Server (ATM, B-ISDN)
**MPTN**			Multiprotocol Transport Network (IBM, SNA)
**MPTP**			Mobile Phone Telematics Protocol
**MPTS**			Multiprotocol Transport Service
**MPX**				Multiplexer
**MPoA**			Multi-Protocol over ATM (ATM, B-ISDN)
**MQI**				Message Queuing Interface
**MQM**				Message Queuing Middleware
**MQV**				Menezes Qu Va --> Algorithmus (IT-Sicherheit)
**MR**				Modem Ready (Steuerzeichen)
**MR**				Modified Read (Telekommunikation)
**MRB**				Management Request Broker
**MRC**				Modified Read Code --> Zweidimensionales Quellencodierungs-Verfahren (Codierung)
**MRCC**			Maritime Rescue Coordination Center (Sat-Kommunikation)
**MRRU**			Maximum Received Reconstructed Unit
**MRSE**			Message Retrieval Service Element (Mitteilungsdienste)
**MRT**				Message Routing (Internetworking)
**MRU**				Maximum Receive Unit (Internetworking)
**MS**				Message Store --> Mitteilungsspeicher (ITU-T X.400) (Mitteilungsdienste)
**MS**				Message Switching --> Nachrichtenvermittlung (Weitverkehrsnetze)
**MS**				Mobile Service (Mobilfunknetze)
**MS**				Mobile Station (Mobilfunknetze)
**MS**				Mobile Subscriber (Mobilfunknetze)
**MS**				Modified Scope
**MSA**				Management Services Architecture
**MSAP**			Message Store Access Protocol (Mobilfunkdienste)
**MSAP**			Mini Slotted Alternating Priorities
**MSAU**			Multistation Access Unit --> Ringleitungsverteiler (Token Ring)
**MSB**				Bit mit der höchsten Wertigkeit (Zahlensysteme)
**MSB**				Most Significant Bit --> Höchstwertigstes Bit (Zahlensysteme)
**MSB**				Swedish Civil Contingency Agency
**MSC**				Main Service Channel
**MSC**				Main System Controller (Mobilfunknetze)
**MSC**				Message Sequence Chart (SDL)
**MSC**				Message Switching Center
**MSC**				Mobile Switching Center --> Mobilfunkvermittlungsstelle (Mobilfunknetze)
**MSC**				Multi Service Concentrator
**MSC**				Multi Switching Concentrator
**MSD**				Maximum Splitting Degree
**MSDP**			Mobile and Service Data Point
**MSDSL**			Multirate Symmetric Digital Subscriber Line (xDSL)
**MSE**				Management Service Element
**MSF**				Multiservice Switching Forum (ATM)
**MSG**				Message --> Nachricht
**MSIC**			Mobile Subscriber Identity Code (GSM)
**MSIN**			Mobile Subcriber Identification Number (GSM, Mobilfunknetze)
**MSISDN**			Mobile Station ISDN Number (GSM)
**MSK**				Minimum Shift Keying --> Digitales Modulationsverfahren
**MSL**				Maximale Segment-Lebenszeit
**MSL**				Mirrored Server Link (Netzbetriebssysteme)
**MSM**				Multi-System Manager (Netzmanagement)
**MSN**				Microsoft Network (Mircosoft)
**MSN**				Multiple Subscriber Number --> Mehrfachrufnummer (ISDN)
**MSN**				Multiple Systems Network (Offene NW-Konzepte)
**MSNF**			Multi-System Network Facility
**MSNet**			Microsoft Network (Online-Dienste)
**MSO**				Multiple System Operator
**MSOH**			Multiplex Section Overhead (Optische Netze, SDH, Sonet)
**MSP**				Maintenance Service Provider --> Wartungsdienstanbieter
**MSP**				Multi-Service Platform
**MSR**				Mobile Support Router
**MSRN**			Mobile Station Roaming Number --> Aufenthaltsnummer (Mobilfunknetze)
**MSS**				MAN Switching System (Weitverkehrsnetze)
**MSS**				Maximum Segment Size
**MSS**				Mobile Satellite Services (Sat-Kommunikation)
**MSU**				Message Service Unit (Mobilfunkdienste)
**MSU**				Message Signal Unit --> Nachrichten-Zeicheneinheit (ISDN)
**MSV**				Medium Speed Version
**MT**				Message Transfer --> Mitteilungsübertragung (DK-Übertragungstechniken)
**MT**				Message Type --> Nachrichtentyp (ATM, B-ISDN)
**MT**				Mobile Terminal (Mobilfunknetze)
**MT**				Mobile Terminated (Mobilfunknetze)
**MTA**				Media Terminal Adapter
**MTA**				Message Transfer Agent (ITU-T X.400)
**MTAE**			Message Transfer Agent Entity --> Mitteilungs-Transfer-Systemteil-Instanz (Mobilfunkdienste)
**MTBF**			Mean Time Between Failures --> Mittlere Ausfallzeit (Netzmanagement)
**MTBM**			Meantime Between Maintenance
**MTC**				Message Type Code (ISDN)
**MTC**				Message Type Code (SS#7)
**MTC**				Mobile Terminated Call (GSM, (Mobilfunknetze)
**MTDD**			Mean Time Between Disclosure and Diagnosis (Netzmanagement)
**MTDL**			Mean Time to Data Loss (Telekommunikations-Endgeräte)
**MTDR**			Mean Time Between Diagnosis and Repair (Netzmanagement)
**MTF**				Message Transfer Facility
**MTFD**			Mean Time Between Failure and Disclosure (Netzmanagement)
**MTFR**			Mean Time Between Failure and Repair (Netzmanagement)
**MTL**				Message Transfer Layer --> Mitteilungs-Transfer-Schicht (Mitteilungsdienste)
**MTOR**			Mean Time of Repair
**MTP**				Mail Transfer Protocol (Protokolle)
**MTP**				Message Transfer Part (SS#7)
**MTP-n (n = 2 .. 3)**		Message Transfer Part Level n --> Signalisierungsübertragung (ISDN, SS7)
**MTPL**			Management Presentation Layer
**MTPoD**			Maximum Tolerable Period of Disruption
**MTR**				Multiple Token Ring --> Mehrfach-Token-Ring
**MTS**				Message Transfer Service --> Mitteilungs-Transfer-Dienst (Mobilfunkdienste)
**MTS**				Message Transfer System/Service (ITU-T X.400)
**MTSE**			Message Transfer Service Element --> Mitteilungs-Transfer-Dienstelement (Mitteilungsdienste)
**MTSM**			Multiservice Traffic Shaping Module
**MTSO**			Mobile Telephone Switching Office
**MTTD**			Mean Time to Diagnosis
**MTTF**			Mean Time to Fix (Netzmanagement)
**MTTR**			Mean Time to Repair
**MTU**				Maximum Transfer Unit
**MTU**				Maximum Transmission Unit
**MUA**				Message User Agent (Mobilfunkdienste)
**MUD**				Multi User Dungeon
**MUD**				Multi-User Dungeon
**MUF**				Maximum Usable Frequency
**MUI**				Modified User Interaction
**MUL**				Mobile User Link
**MUP**				Mobile User Part (Mobilfunknetze)
**MUP**				Multiple UNC Providers
**MUP**				Multiple Upstream
**MUSE**			Muti-User Shared Environment
**MUSICAM**			Masking Universal Subband Integrated Coding and Multiplexing
**MUX**				Multiplexer (Netzkomponente)
**MVDS**			Microwave Video Distribution Service
**MVI**				Multivendor Interaction (Mobilfunknetze)
**MVIP**			Multi Vendor Integration Protocol (Tk-Sprachdienste)
**MVP**				VMware Mobile Virtualization Platform (VMware)
**MVSt**			Muttervermittlungsstelle
**MW**				Mittelwelle
**MWI**				Message Waiting Indication
**MWIF**			Mobile Wireless Internet Forum (Gremien, Organisationen)
**MZAP**			Multicast-Scope Zone Announcement Protocol (Protokolle)
**MacOS**			Macintosh Operating System
**Mbit/s**			Million Bit pro Sekunde
**Mbps**			Millions of Bits Per Second
**MoU**				Memoradum of Understanding (Standards, Verordnungen, Gesetze)
**Modacom**			Mobile Data Communication --> Datenfunkdienst der Deutschen Telekom
**Mx (x= 1, 2, 3, 4, 5)**	Netzmanagement-Schnittstelle von ATM-Netzen
**MÜW**				Monopol-Übertragungsweg
**N(R)**			Empfangsnummer (ISDN)
**N(S)**			Sendenummer (ISDN)
**N**				Negative Flag
**N**				Neutralleiter/Nullleiter bei der Stromversorgung (Verkabelung)
**N-OSF**			Network Operation System Function (NetzmanagementNP)
**NA**				Numerische Apertur
**NA-CCIRN**			North America CCIRN
**NAC**				Negative Acknowledgement
**NAC**				Network Access Control
**NAC**				Null Attachment Concentrator (FDDI)
**NACC**			Network Access and Connectivity Channel (Mobilfunknetze)
**NACK**			Negative Acknowledgement --> Negatitive Quittierung
**NACS**			NetWare Asynchronous Communication Services (Netzbetriebsysteme)
**NAD**				Node Address --> Knotenadresse
**NADC**			North American Digital Cellular (Mobilfunknetz)
**NAH**				Network Administration Host (Modacom)
**NAI**				Network Access Identifier
**NAK**				Negative Acknowledgment --> Negative Quittierung
**NAMPS**			Narrowband Advanced Mobile Phone System
**NANP**			North American Numbering Plan
**NAP**				Network Access Point --> Netzzugangspunkt (Internet)
**NAP**				Network Access Protocol --> Netzzugangsprotokoll (Protokolle)
**NAP**				Network Layer Access Point
**NAPT**			Network Address and Port Translation
**NARP**			NBMA Address Reolution Protocol --> Routing-Protokoll im Internet (Protokolle)
**NAS**				Netware Access Server (Netzbetriebsysteme)
**NAS**				Network Access Server
**NAS**				Network Access Station (Weitverkehrsnetze)
**NAS**				Network Attached Storage (Speichernetze)
**NASI**			Netware Asynchronous Service Interface (Netzbetriebssysteme)
**NAT**				Network Access Terminal
**NAT**				Network Address Translation --> Umsetzung von Netzadressen (RFC 1631, 1918, 2663)
**NAU**				Network Addressable Unit --> Adressierbares Netzelement (IBM/SNA)
**NAUN**			Next Active Upstream Neighbour (Token Ring)
**NAV**				Net Allocation Vector (Wireless LAN)
**NAYY**			PVC-isoliertes Energiekabel mit vier Leitersektoren aus Aluminium (Verkabelung)
**NB**				Narrowband --> Schmalbandübertragung
**NB**				Netzbetreiber
**NB**				Normal Burst (GSM)
**NBAR**			Network Based Application Recognition
**NBFCP**			NetBIOS Frames Control Protocol (Protokolle)
**NBFM**			Narrow Band Frequency Modulation --> Schmalbandfrequenzmodulation (Modulation)
**NBMA**			Non-Broadcast, Multiple Access (Weitverkehrsnetze)
**NBNS**			NetBIOS Name Server (Internet, Protokolle)
**NBP**				Name Binding Protocol (AppleTalk) (Protokolle)
**NBR**				Nitril-Kautschuk (Verkabelung)
**NBS**				National Bureau of Standards (USA) (Gremien, Organisationen)
**NBS**				Netzbetriebssystem
**NBT**				NetBIOS over TCP/IP (Protokolle)
**NC**				Network Computer
**NC**				Network Concentrator
**NC**				Network Congestion --> Datenstau im Netz
**NC**				Network Connection --> Netzverbindung
**NC**				Network Control --> Netzüberwachung (Netzmanagement)
**NC**				Network Coordinator
**NC**				No Circuit
**NC**				Non Corrosive --> Nicht-rostend (Verkabelung)
**NC-FDMA**			Narrow Channel Frequency Division Multiple Access (Motorola)
**NCA**				Network Computing Architecture (IBM)
**NCA**				Network Configuration Application
**NCB**				Node Control Block --> Knotensteuerblock
**NCC**				National Colour Code (Mobilfunknetze)
**NCC**				Network Coordination Center
**NCC**				Notwork Control Center --> Netz-Kontrollzentrum (Netzmanagement)
**NCCF**			Network Communications Control Facility (Netzmanagement)
**NCCI**			Network Control Connection Identifier (ISDN)
**NCD**				Network Computing Device --> Netzrechner
**NCE**				Network Control Engine
**NCF**				Network Command Facility (Netzmanagement)
**NCI**				Non-Coded Information --> Nicht-codierte Information (Codierung)
**NCIA**			Native Client Interface Architecture
**NCL**				Network Control Language --> Netzsteuerungssprache
**NCMS**			Network Connection Management Subprotocol
**NCOS**			Network Computer Operating System
**NCP**				NetWare Core Protocol (Novell) (Protokolle)
**NCP**				Network Control Program (IBM/SNA)
**NCP**				Network Control Protocol (Protokolle)
**NCPD**			Network Control Protocol Description
**NCS**				Network Control System (Netzmanagement)
**NCS**				Network Coordination System (Netzmanagement)
**NCSA**			National Center for Supercomputing Application (Gremien, Organisationen)
**NCSI**			Network Communications Services Interface
**NCT**				Network Control Terminal (Stadt-, Regionalnetze)
**NCTE**			Network Channel Terminating Equipment
**NCV**				Narrowband Compressed Voice (Telekommunikation)
**ND**				Neighbour Discovery (Internetworking)
**NDAP**			Novell Directory Access Protocol (Protokolle, Verzeichnisdienste)
**NDC**				National Destination Code --> Bereichskennzahl (Mobilfunknetze)
**NDIS**			Network Device Interface Specification (Microsoft, IBM, 3Com)
**NDM**				Normal Disconnected Mode (HDLC)
**NDMP**			Network Data Management Protocol (Legato, Network Appliance) (Speichernetze)
**NDMS**			NetWare Distributed Management Server (Netzbetriebssysteme)
**NDPS**			Novell Distributed Print Services (Netzbetriebsysteme)
**NDR**				Network Data Representation
**NDS**				NetWare Directory Service; später Novell Directory Service (Novell) (Verzeichnisdienste)
**NDSF**			Non Dispersion Shifted Fiber (Verkabelung)
**NE**				Network Element
**NE**				Netzebene
**NE**				Netzelement
**NEA**				Network Architecture (Offene NW-Konzepte)
**NEC**				National Electrical Code
**NECP**			Network Element Control Protocol (Protokolle)
**NEE**				Next-Generation Education Environment (VMware)
**NEF**				Network Element Funktion (Netzmanagement)
**NEL**				Network Element Layer
**NEM**				Network Equipment Manufacturer
**NEML**			Network Element Management Layer (TMN)
**NEMP**			Network Error Management Protocol (Protokolle, Netzmanagement)
**NEP**				Noise Equivalent Power
**NEPS**			Novell Enterprise Print Services (Netzbetriebssysteme)
**NESCOM**			New Standard Committee (Gremien, Organisationen)
**NET**				Network Entity Title (ISO/OSI)
**NET**				Network Equipment Technologies
**NET**				Normes Europeennes de Telecommunications (Standards, Verordnungen, Gesetze)
**NETID**			Network Identifications (Hersteller-NW-Architekturen)
**NETIOC**			Network I/O Control (VMware)
**NEXT**			Near End Crosstalk --> Nahnebensprechen (Verkabelung)
**NF**				Niederfrequenz
**NFA**				Network File Access (IBM/Intel/Microsoft)
**NFGt**			Nachrichtenfernschaltgerät (Deutsche Telekom)
**NFM**				Narrowband Frequency Modulation --> Schmalbandfrequenzmodulation
**NFMFR**			Non-Fully Meshed Frame Relay Support (Frame Relay)
**NFN**				Dreifach-Kodierung der TAE (für ein Telefon sowie zwei Datenübertragungseinrichtungen oder Zusatzeinrichtungen)
**NFS**				Network File System (Sun) (RFC 1094)
**NG**				Netz Gateway (Netzkomponente)
**NGAF**			Non GPRS Alert Flag (Mobilfunknetze)
**NGI**				Next Generation Internet (Internet)
**NGMF**			Netview Graphic Management Facilities
**NGN**				Next Generation Network (Offene NW-Konzepte)
**NGNP**			Non-Geographical Number Portability (Mobilfunknetze)
**NGSO**			Non Geostationary Orbit (Sat-Kommunikation)
**NGTRANS**			New Generation Transition (Internet)
**NH**				Network Layer Header
**NHRP**			Next Hop Resolution Protocol --> Routingprotokoll im Internet (RFC 2332)
**NHS**				Next Hop Server (Internetworking)
**NI**				Network Indicator (ISDN)
**NI**				Notification Indicator (Mobilfunknetze)
**NI-2**			Nationales ISDN-2 (Deutsche Telekom)
**NIA**				Network Interface Adapter (ISO/OSI)
**NIB**				Network Interface Block
**NIC**				National ISDN Committee (Gremien, Organisationen)
**NIC**				Nearly Instantaneous Coding (Codierung)
**NIC**				Network Independent Clocking (Mobilfunknetze)
**NIC**				Network Information Center
**NIC**				Network Interface Card --> Netzkarte, Netzadapter
**NIC**				Network Interface Controller --> Netz-Adapterkarte (Netzkomponente)
**NID**				Network Information Database
**NIF**				Neighbour Information Frame (FDDI)
**NIFTP**			Network Independent File Transfer Protocol (Protokolle)
**NII**				National Information Infrastructure --> Nationale Informationsinfrastruktur (Offene NW-Konzepte)
**NIL**				Network Interface Layer
**NILS**			Network Internal Layer Service
**NIM**				Network Interface Module
**NIM**				Network Inventory Management
**NIOC**			Network I/O Control (VMware)
**NIP**				Network Integrated Processing (Offene NW-Konzepte)
**NIS**				Network Information Service --> Verzeichnisdienst-Protokoll (Sun)
**NISDN**			Narrow-Band ISDN --> Schmalband-ISDN (ISDN)
**NISO**			National Information Security Officer
**NIST**			National Institute of Standards and Technology (USA) (Gremien, Organisationen)
**NIU**				Network Interface Unit --> Netz-Schnittstelleneinheit
**NIU**				Network Interface Unit --> Netzanschlußeinheit
**NIUF**			North American ISDN Users Forum (USA) (Gremien, Organisationen)
**NJE**				Network Job Entry
**NK**				Netzknoten
**NK**				Nutzkanal
**NKR**				Netzknotenrechner (Netzkomponente)
**NKS**				Netzkontrollserver (Netzkomponente)
**NKÜ**				Netzknoten der Übertragungstechnik
**NL**				Network Layer --> Netzschicht (ISO/OSI)
**NL**				No Line
**NLA**				Next Level Aggregation
**NLDM**			Network Logical Data Manager (Netzmanagement)
**NLM**				Netware Loadable Module (Netzbetriebssysteme)
**NLP**				Network Layer Protocol (ISO/OSI)
**NLP**				Normal Link Pulse (Ethernet)
**NLPID**			Network Layer Protocol Identifier
**NLPID**			Network Layer Protocol Identifier (Protokolle)
**NLRI**			Network Layer Reachability Information (ISO/OSI)
**NLSP**			NetWare Link State Protocol (Novell) (Protokolle)
**NLSP**			Network Layer Security Protocol (Protokolle)
**NM**				Network Management (ISO/OSI)
**NM**				Network Monitor
**NMA**				Network Management Architecture --> Netzmanagement-Archtektur (Hersteller-NW-Architekturen)
**NMA**				Network Management and Administration
**NMA**				Network Management and Administration (Netzmanagement)
**NMAP**			Network Management Application Program (Netzmanagement)
**NMC**				Network Management Center
**NMCC**			Network Management Control Center --> Netzmanagement-Steuerzentrum (Netzmanagement)
**NMDB**			Network Management Data Base (Netzmanagement)
**NME**				Network Management Entity (Netzmanagement)
**NMF**				Network Management Forum (Gremien, Organisationen)
**NMI**				Network Management Interface (Mobilfunknetze)
**NMI**				Node Management Identity
**NMIS**			Network Management Information System (Netzmanagement)
**NML**				Native Mode LAN
**NML**				Network Management Layer (Netzmanagement)
**NMM**				Network Management Module (Netzmanagement)
**NMO**				Network Management Option (Netzmanagement)
**NMP**				Network Management Protocol (Protokolle, Netzmanagement)
**NMPF**			Network Management Productivity Facility (Netzmanagement)
**NMS**				Network Management Station (Netzmanagement)
**NMS**				Network Management System (Netzmanagement)
**NMSI**			National Mobile Subscriber Identity (Mobilfunknetze)
**NMT**				Nordic Mobile Telephone (Mobilfunknetze)
**NMTI**			Node Management Terminal Interface
**NMVT**			Network Management Vector Transport (Netzmanagement)
**NN**				Network Node (IBM/SNA)
**NNB**				Netznachbildung
**NNE**				Network Node Equipment (Netzkomponente)
**NNI**				Network Node Interface --> Schnittstelle zwischen Netzknoten (ATM, B-ISDN)
**NNP**				Neighbour Notification Protocol (Protokolle)
**NNS**				National Numbering Scheme --> Länderspezifische Nummerierungspläne
**NNTP**			Network News Transfer Protocol --> Anwendungsprotokoll im Internet (RFC 977)
**NO**				Network Operator --> Netzbetreiber
**NOC**				Network Operation Center --> Netzbetriebszentrum (Netzmanagement)
**NOS**				Network Operating System --> Netz-Betriebssystem
**NP**				Network Performance --> Leistungsfähigkeit des Netzs
**NP**				Network Protocol --> Netzprotokoll (Protokolle)
**NP**				Nummernportabilität
**NPA**				Network Performance Analyzer
**NPA**				Numbering Plan Area (USA)
**NPAI**			Network Protocol Address Information
**NPC**				Network Parameter Control (ATM, B-ISDN)
**NPC**				Network Protocol Control (IBM/SNA)
**NPCI**			Network Layer Protocol Control Information
**NPDA**			Network Problem Determination Application
**NPDU**			Network Protocol Data Unit --> Protokoll-Dateneinheit der OSI-Schicht 3 (ISO/OSI)
**NPM**				NetView Perfomance Monitor
**NPM**				Network Performance Monitor (Netzmanagement)
**NPMA**			Non-Preemptive Multiple Access (HiperLAN)
**NPS**				Network Processing System --> Netz-Steuerungssystem
**NPS**				Number Portability Server
**NPSI**			Network Packet Switching Interface (Hersteller-NW-Architekturen)
**NPV**				Network Present Value
**NQS**				Network Queuing System
**NR**				Notruf
**NR**				Nyberg Rueppel (IT-Sicherheit)
**NR/SBR**			Natur-Kautschuk/Styrol (Verkabelung)
**NRAs**			Notrufanschluss
**NRE**				Netz-Rückschalteinrichtung
**NREN**			National Research and Education Network (USA)
**NRM**				Normal Response Mode (HDLC)
**NRR**				Network Resource Reservation
**NRR**				No Reply Required --> Keine Antwort erforderlich
**NRS 73**			Notrufsystem 73
**NRT**				Network Response Time --> Reaktionszeit des Netzs
**NRZ**				Non-Return-to-Zero --> Leitungscodierung (Codierung)
**NRZI**			Non-Return-to-Zero Inverted --> Leitungscodierung (Codierung)
**NS**				Network Service --> Netzdienst
**NS**				Network and Switching System
**NSA**				National Security Agency (USA) (IT-Sicherheit)
**NSA**				Non-Sequenced Acknowledgement --> Nicht folgegebundene Quittung
**NSAP**			Network Service Access Point (ISO/OSI)
**NSAPI**			Netscape Server Application Programming Interface
**NSC**				National Service Code (Weitverkehrsnetze)
**NSC**				National Service Code --> Präfix in Nummerierungsplänen
**NSC**				Network Service Class --> Netzdiensteklasse (Anschlussnetze)
**NSC**				Network Switching Centre --> Netzschaltzentrale
**NSD**				Network Statistics Database
**NSDU**			Network Service Data Unit --> Netzdienst-Dateneinheit (ISO/OSI)
**NSF**				National Science Foundation --> Internet-Betreiber in USA
**NSFNet**			National Science Foundation Network
**NSI**				National Security Information
**NSI**				Non SNA Interconnection
**NSI**				Non-Sequenced Information
**NSMS**			Network Systems Management Software
**NSO**				National Standards Organization (Gremien, Organisationen)
**NSP**				Name Server Protocol (Protokolle)
**NSP**				Network Service Part
**NSP**				Network Service Platform
**NSP**				Network Service Provider (Internet)
**NSP**				Network Services Protocol (DECnet) (Protokolle)
**NSP**				Non-Sequenced Poll --> Nicht folgegebundener Sendeaufruf (Steuerzeichen)
**NSPC**			National Signalling Point Code (Telekommunikation)
**NSPE**			Network Service Procedure Error
**NSR**				Non-Source Routed (Internetworking)
**NSRU**			Network Service Request Unit
**NSS**				Network Subsystem (GSM, Mobilfunknetze)
**NSS**				Nodal Switching System
**NSt**				Nebenstelle (Netzkomponente)
**NT**				Nachrichtentechnik
**NT**				Nachrichtentyp (ATM, B-ISDN)
**NT**				Network Termination --> Teilnehmerseitiger Netzabschluß (ISDN)
**NT**				Nutzungszeitabhängige Tarifierung
**NTA**				Network Termination Analogue (Weitverkehrsnetze)
**NTBA**			Network Termination for Basic Access (ISDN)
**NTDS**			Non-Transparent Data Services
**NTFS**			Network File System (Windows NT)
**NTG**				Nachrichtentechnische Gesellschaft (mittlerweile umbenannt in ITG) (Gremien, Organisationen)
**NTIS**			Network Driver Interface Specification
**NTLM**			NT LAN Manager (Windows) (IT-Sicherheit)
**NTN**				Network Terminal Number (Telekommunikation)
**NTP**				Network Termination Point --> Netz-Endstelle (Telekommunikation)
**NTP**				Network Time Protocol --> Anwendungs-Protokoll im Internet (RFC 1305)
**NTR**				Network Timing Reference
**NTSC**			National Television System Committee (Gremien, Organisationen)
**NTU**				Network Terminating Unit --> Netzabschlusseinheit
**NUA**				Network User Address (ITU-T X.25)
**NUI**				Network User Identification --> Benutzerkennung für Nutzer von LAN-Stationen
**NUI**				Network User Identification --> Netzzugangsberechtigung für Teilnehmer an X.25-Netzen
**NUL**				Null (Steuerzeichen)
**NUMA**			Non-uniform memory access, when multiple processors are involved their memory access is relative to their location.
**NV**				Network Video
**NVP**				Network Voice Protocol (Protokolle)
**NVP**				Nominal Velocity of Propagation --> Ausbreitungsgeschwindigkeit von elektrischen Signalen auf physikalischen Medien (Verkabelung)
**NVT**				Network Virtual Terminal --> Virtuelles Netzterminal
**NVoD**			Near Video on Demand (Mulimedia)
**NWDM**			Narrow Wave Division Multiplexing
**NWM**				Netzmanagement
**NYY**				PVC-isoliertes Energiekabel mit vier Leitersektoren aus Kupfer (Verkabelung=
**NZ**				Negative & Zero Flag
**NZDSF**			Non-Zero Dispersion Shifted Fiber (LWL) (Verkabelung)
**NZV**				Netzzugangsverordnung (Standards, Verordnungen, Gesetze)
**NetBIOS**			Network Basic Input/Output System (IBM/Microsoft) (RFC 1088)
**NetRPC**			NetRemote Procedure Call (Vines)
**Nrt-VBR**			Non Real Time Variable Bit Rate (ATM, B-ISDN)
**NstA**			Nebenstellenanlage (Telekommunikations-Endgeräte)
**NstAnl**			Nebenstellenanlage (Deutsche Telekom)
**Nw**				Nachricht wartet
**NÜ**				Netzübergang (ISDN)
**O&M**				Operation and Maintenance --> Bedienung und Wartung
**O**				Object --> Objekt (Dateiformate)
**O-BCSM**			Originating BCSM
**OA**				Office Automation --> Büro-Automation (Datenverarbeitung)
**OA**				Optical Amplifier --> Optischer Verstärker (Optische Netze, SDH, Sonet)
**OAD**				Object Management and Definitions
**OADM**			Optical Add-Drop-Multiplexer --> Optischer Multiplexer zum Abzweigen und Einfügen einzelner Kanäle (Optische Netze, SDH, Sonet)
**OAF**				Origin Address Field
**OAI**				Open Application Interface
**OAM**				Operations, Administration and Maintenance (Netzmanagement)
**OAM**				Operations, Administration, Maintenance and Provisioning (ATM, B-ISDN)
**OAMC**			Operations, Administration and Maintenance Center --> Betriebs-, Verwaltungs- u. Wartungszentrum (Netzmanagement)
**OAN**				Optical Access Network --> Zugangsnetz auf Glasfaserbasis (Anschlussnetze)
**OAS**				Optical Access Switch (Anschlussnetze)
**OASIS**			Organization for the Advancement of Structured Information Standards (Gremien, Organisationen)
**OAsk**			Ortsanschlusskabel (Anschlussnetze)
**OAsl**			Ortsanschlussleitung (Anschlußnetze)
**OBI**				Open Buying on the Internet (Online-Dienste)
**OBR**				Optical Burst Router
**OBW**				Occupied Bandwidth
**OC**				Optical Carrier
**OC**				Output Controller (ATM, B-ISDN)
**OC-n**			Optical Carrier Type n (Optische Netze, SDH, Sonet)
**OC-x (x =1, 3, 12, 24, 48)**	Optical Carrier Type x --> Bitratenhierarchie für synchrone Transportnetze
**OCA**				Object Contents Architecture
**OCA**				Open Communications Architecture (IBM)
**OCAL**			Online Cryptanalytic Aid Language (IT-Sicherheit)
**OCAP**			Open Cable Application Platform (Gremien, Organisationen)
**OCB**				Outgoing Call Blocked --> Programmierbare Sperre für abgehende Gespräche (ISDN)
**OCC**				Operational Control Center (Inmarsat) (Sat-Kommunikation)
**OCC**				Optical Connection Controller
**OCC**				Optical Cross Connect
**OCC**				Other Common Carrier
**OCCF**			Operator Communication Control Facility
**OCDM**			Optical Code Division Multiplexing --> Optisches Codemultiplex (Optische Netze, SDH, Sonet)
**OCH**				Optical Channel (Optische Netze, SDH, Sonet)
**OCI**				Open Container Initiative
**OCM**				Optical Channel Multiplexer --> Optischer Kanalmultiplexer
**OCQPSK**			Orthogonal Complex Quadrature Phase Shift Keying --> Digitales Modulationsverfahren
**OCR**				Optical Character Recognition
**OCS**				Originating Call Screening
**OCSP**			Online Certificate Status Protocol (Protokolle, IT-Sicherheit)
**OD**				On Demand (Online Dienste)
**ODA**				Office Document Architecture (ISO/IEC 8613)
**ODA**				Open Document Architecture (ITU-T I.410)
**ODAM**			Open Distributed Application Model
**ODAPI**			Open Database Application Programming Interface (Borland)
**ODB**				Object (-oriented) Database
**ODBC**			Open Database Connectivity (Datenbank)
**ODCS**			Open Distributed Computing Structure
**ODETTE**			Organization for Data Exchange by Teletransmission in Europe (Gremien, Organisationen)
**ODI**				Open Data Interface
**ODI**				Open Datalink Interface (Novell/Apple)
**ODI**				Open Document Interchange
**ODIF**			Office Document Interchange Format (ISO/CCITT)
**ODIN**			Online Daten- und Informationsnetz
**ODIN**			Open Data Link Interface (Netzbetriebsysteme)
**ODL**				Office Document Language
**ODL**				Optical Data Link
**ODLI**			Open Data Link Interface (Netzbetriebssysteme)
**ODMG**			Object Database Management Group (Gremien, Organisation)
**ODN**				Optical Distribution Network --> Optisches Verteilnetz (Anschlussnetze)
**ODP**				Open Distributed Processing --> Offene verteilte Verarbeitung (ISO, ITU-T)
**ODSA**			Open Distributed System Architecture
**ODSI**			Optical Domain Service Interconnect (Anschlussnetze)
**ODU**				Optical Data Unit (Optische Netze, SDH, Sonet)
**ODU**				Outdoor Unit
**ODgVSt**			Ortsdurchgangsvermittlungsstelle
**OEI**				Outgoing Error Indication
**OEM**				Original Equipment Manufacturer (Telekommunikations-Endgeräte)
**OEO**				Optical Electronic Optical (Verkabelung)
**OF**				Optical Fibre --> Lichtwellenleiter (Verkabelung)
**OFA**				Optical Fiber Amplifier --> Optische Faserverstärker
**OFB**				Output Feedback --> Betriebsart des DES (IT-Sicherheit)
**OFC**				Optical Fibre Control (Fiber Channel)
**OFDM**			Orthogonal Frequency Division Multiplexing --> Multiplex-Verfahren (Wireless LAN)
**OFTP**			Odette File Transfer Protocol (EDI) (Protokolle)
**OFX**				Open Financial Exchange
**OGM**				Outgoing Message
**OH**				No Halogen --> Halogenfreies Material (Verkabelung)
**OH**				Off-Hook
**OHAN**			Optical Hybrid Access Network (Anschlussnetze)
**OHR**				Optical Handwriting Recognition
**OHU**				Overhead Unit
**OIA**				Office Information Architecture
**OIF**				Optical Internetworking Forum (Gremien, Organisationen)
**OIM**				OSI Internet Management (Gremien, Organisationen)
**OIR**				Online Insertion and Removal (Telekommunikations-Endgeräte)
**OIS**				Office Information System
**OISL**			Optical Inter-Satellite Link (Sat-Kommunikation)
**OIW**				OSI Implementors Workshop (Gremien, Organisationen)
**OL**				Online --> Online-Betrieb (DK-Übertragungstechniken)
**OL**				Overload (Bit)
**OLB**				Optical Loss Budget --> Dämpfungsbudget (Verkabelung)
**OLD**				Optical Line Distributor --> Optischer Leitungsverteiler
**OLDI**			Online Data Interchange (ICAO)
**OLE**				Object Linking and Embedding (Datenbank)
**OLI**				Optical Line Inlet
**OLO**				Optical Line Outlet
**OLT**				Optical Line Termination --> Glasfasernetzabschluß in der Orts-VSt (Ansschlussnetze)
**OLTP**			Online Transaction Processing
**OLU**				Origin LU (IBM/SNA)
**OM**				Optical Multimode (Verkabelung)
**OMA**				Open Management Architecture (Netzmanagement)
**OMA**				Open Mobile Alliance (Gremien, Organisationen)
**OMAP**			Operations Maintenance Administration Part (ISDN)
**OMC**				Operations and Maintenance Center (Netzmanagement)
**OMG**				Object Management Group (Netzmanagement)
**OMI**				Optical Modulation Index --> Optischer Modulationsindex
**OML**				Operation and Maintenance Link
**OMR**				Optical Mark Recognition
**OMS**				Optical Multiplex Section
**OMSS**			Operations and Maintenance Subsystem --> Betriebs- und Wartungs-Subsytem (Mobilfunknetze)
**OMTR**			Optical Multiplexer and Transceiver
**OMU**				Operation and Maintenance Unit
**ON**				Optical Network --> Optisches Netz (Optische Netze, SDH, Sonet)
**ON**				Ortsnetz
**ONA**				Open Network Architecture (Offene NW-Konzepte)
**ONC**				Open Network Computing (Offene NW-Konzepte)
**ONDS**			Open Network Distribution System
**ONE**				Optical Network Element
**ONKz**			Ortsnetzkennzahl (ISDN)
**ONM**				Open Enterprise Network Management
**ONN**				Optical Network Node --> Optischer Netzknoten (Optische Netze, SDH, Sonet)
**ONP**				Open Network Provision --> Offener Netzzugang für Dienste-Anbieter (Weitverkehrsnetze)
**ONT**				Optical Network Termination
**ONU**				Optical Network Unit --> Letzter Punkt eines Glasfasernetzes (Anschlussnetze)
**OOB**				Out of Band (Übertragungstechniken)
**OODBMS**			Object Oriented Database Management System
**OOF**				Out of Frame (Übertragungstechniken)
**OOK**				On-Off-Keying --> Digitale Amplitudenmodulation (Modulationsverfahren)
**OP**				Operating Procedure
**OPAC**			Online Public Access Catalog
**OPAL**			Optische Anschlussleitung (Anschlussnetze)
**OPAMP**			Operational Amplifier --> Operationsverstärker (Netzkomponente)
**OPC**				Originating Point Code (ISDN, SS#7)
**OPD**				Optical Path Difference
**OPGW**			Optical Ground Wire --> Unterseekabel auf Glasfaserbasis (Verkabelung)
**OPM**				Optical Passive Multiplexer --> Optischer, passiv arbeitender Multiplexer
**OPN**				Open Network Provision
**OPPW**			Optical Phase Wire --> Hochspannungskabel mit integrierter Glasfaser (Verkabelung)
**OPS**				OTM Physical Section (Optische Netze, SDH, SONET)
**OPS**				Open Profiling Standard (Standards, Verordnungen, Gesetze)
**OPSE**			Operation Protocol Service Element
**OPT**				Open Protocol Technology (Protokolle)
**OPTIS**			Overlapped Pulse Amplitude Modulation with Interlocking Spectra
**OPU**				Optical Payload Unit (Optische Netze, SDH, SONET)
**OPV**				Operational Amplifier --> Operationsverstärker (Netzkomponente)
**OPX**				Off Premises Extension (Telekommunikation)
**OQPSK**			Offset Quadrature Phase Shift Keying -->Digitales Modulationsverfahren
**OR**				Optical Repeater --> Optischer Signalaufbereiter (Optische Netze, SDH, Sonet)
**ORB**				Object Request Broker
**ORFS**			Output Radio Frequency Spectrum (GSM, Mobilfunknetze)
**ORU**				Optical Repeater Unit --> Optischer Signalaufbereiter (Optische Netze, SDH, Sonet)
**ORZ**				Organisations- und Rechenzentrum
**OS**				Object Services
**OS/2**			Operating System 2 (IBM/Microsoft)
**OSA**				Open Systems Architecture
**OSAF**			Origin Subarea Address Field
**OSB**				Oberer Sonderkanalbereich --> Breitbandkabelverteilnetz (Anschlussnetze)
**OSC**				Optical Supervisory Channel --> Separater Kanal zur Übertragung von Managementinformation (Netzmanagement)
**OSCAR**			Orbital Satellite Carrying Amateur Radio --> Satelliten für Amateurfunk-Übertragung (Sat-Kommunikation)
**OSCI**			OS Command Injection
**OSD**				Optimal Subnet Design
**OSDM**			Optical Space Division Multiplexing --> Optischer Raummultiplex (Optische Netze. SDH, Sonet)
**OSF DCE**			Open Software Foundation Distributed Computing Environment (Offene NW-Konzepte)
**OSF**				Open Software Foundation (Gremien, Organisationen)
**OSF**				Operation System Function (Netzmanagement)
**OSGI**			Open Services Gateway Initiative
**OSI**				Open Systems Interconnection --> Architekturmodell der ISO
**OSI/NM**			OSI Network Management Forum (Gremien, Organisationen)
**OSIE**			OSI Management Environment (Netzmanagement)
**OSIRM**			OSI-Referenzmodell --> Architekturmodell der ISO
**OSITOP**			OSI Technical Office Protocol --> Europäische TOP User Group (Gremien, Organisationen)
**OSITP**			OSI Transport Protocol (Protokolle)
**OSMA/CD**			Optical Sense Multiple Access with Collision Detection (Optische Netze, SDH, Sonet)
**OSMAE**			Open System Management Application Entity (Netzmanagement)
**OSMAP**			Open System Management Process (Netzmanagement)
**OSME**			Open Systems Message Exchange (IBM)
**OSN**				Office System Node
**OSNA**			Open Service Network Architecture
**OSNR**			Optical Signal to Noise Ratio --> Optischer Signal-Rauschabstand (Optische Netze, SDH, Sonet)
**OSNS**			Open Systems Network Support
**OSP**				Open Service Platform
**OSPF**			Open Shortest Path First --> Routingprotokoll im Internet (RFC 1131, 1583, 2178, 2328)
**OSS**				One Stop Shopping
**OSS**				Operating Subsystem (Mobilfunknetze)
**OSS**				Operating Support System (Netzmanagement)
**OSTA**			Optical Storage Technologie Association (Gremien, Organisationen)
**OT**				Operation Technology
**OT**				Optical Transceiver (Ethernet)
**OTD**				Optical Time Domain
**OTDM**			Optical Time Division Multiplexing --> Optisches Zeitmultiplex (Optische Netze, SDH, Sonet)
**OTDR**			Optical Time Domain Reflectometry --> Optische Reflektionsmessung (Optische Netze, SDH, Sonet)
**OTI**				Original Transmitter Identification
**OTM**				Optical Transport Modul (Optische Netze, SDH, Sonet)
**OTMX**			Optical Terminal Multiplexer (Anschlussnetze)
**OTN**				Optical Transport Network (Optische Netze, SDH, Sonet)
**OTP**				One Time Password (IT-Sicherheit)
**OTP**				Open Trading Protocol (Protokolle)
**OTR**				Optical Transceiver
**OTS**				Optical Transmission Section (Optische Netze, SDH, Sonet)
**OTS**				Orbitaler Telekommunikations-Satellit (EUTELSAT) (Sat-Kommunikation)
**OTSS**			Open Systems Transport/Session Support
**OTU**				Optical Transport Unit (Optische Netze, SDH, Sonet)
**OU**				Organizational Unit --> Administrations-Einheit (Verzeichnisdienste)
**OUI**				Organizationally Unique Identifier (Lokale Netze)
**OV**				Organization Validation
**OV**				Overflow --> Überlauf
**OVD**				Outside Vapor Deposition (Verkabelung)
**OVDC**			Organization Virtual Data Center (VMware)
**OVL**				Ortsverbindungsleitung (Anschlussnetze)
**OVP**				Overvoltage Protection (Verkabelung)
**OVSF**			Orthogonal Variable Spreading Factor (WCDMA)
**OVSt**			Ortsvermittlungsstelle
**OVW**				OpenView Windows (Netzmanagement)
**OWASP**			Open Web Application Security Project
**OWF**				Optical Working Frequency (Optische Netze, SDH, Sonet)
**OWG**				Optical Waveguide (Optische Netze, SDH; Sonet)
**OXC**				Optical Crossconnect --> Optischer Vermittlungsknoten (Optische Netze, SDH, Sonet)
**OZ**				Ortszentrale
**OZ**				Ortszone
**P(R)**			Packet Receive Sequence Number --> Datenpaket-Empfangsnummer
**P(S)**			Packet Send Sequence Number --> Datenpaket-Sendenummer
**P**				Peta --> Präfix für Billiarde (Zahlensysteme)
**P**				Presentation Layer --> Darstellungsschicht
**P**				Primary Station --> Primärstation (Weitverkehrsnetze)
**P**				Priority (Bit, Datenfelder)
**P-P**				Point-to-Point
**P1**				P1-Protokoll (Mitteilungsdienste)
**P2P**				Peer-to-Peer
**P2P**				Point-to-Point Connection --> Punkt-zu-Punkt-Verbindung (Offene NW-Konzepte)
**P2V**				Physical to Virtual
**P3**				P3-Protokoll (Mitteilungsdienste)
**P3P**				Platform for Privacy Preferences Project (Internet)
**P7**				P7-Protokoll (Mitteilungsdienste)
**PA**				Polyamid (Verkabelung)
**PA**				Power Amplifier --> Leistungsverstärker
**PA**				Pre-Arbitrated Function (Stadt-, Regionalnetze)
**PA**				Primary Access (ISDN)
**PABX**			Private Automatic Branch Exchange (Telekommunikations-Endgeräte)
**PAC**				Paging Area Controller (Ermes) (Mobilfunknetze)
**PAC**				Perceptual Audio Coder --> Verfahren zur Kompression von Musik und Sprache
**PAC**				Privilege Attribute Certificate
**PAC**				partner activation code  (VMware)
**PACCH**			Packet Associated Control Channel (GPRS)
**PACE**			Priority Access Control Enabled --> Deterministisches Zufriffsprotokoll (3Com)
**PACS**			Personal Access Communications System (ANSI J-STD-014)
**PACS**			Picture Archive and Communication System (Telekommunikation)
**PACT**			PABX and Computer Timing
**PAD**				Packet Assembler/Dissassembler --> Paketierer/Depaketierer (ITU-T X.25)
**PAD**				Padding --> Füll-Bit (Datenfelder)
**PAE**				Propero Application Environment
**PAGCH**			Packet Access Grant Channel (GPRS)
**PAI**				Protocol Address Information (ISO/OSI)
**PAL**				Phase Alternation Line --> PAL-Fernsehstandard (Multimedia)
**PAM**				Privileged Account Management
**PAM**				Pulse Amplitude Modulation --> Pulsamplitudenmodulation (Modulation)
**PAMA**			Preassigned Multiple Access --> Verfahren zur Zuteilung von Satellitenkanälen zu Bodenstationen (Sat-Kommunikation)
**PAMD**			Public Access Mobile Data Network
**PAMR**			Private Access Mobile Radio --> Privater Bündelfunk
**PAMS**			Perceptual Analysis Measurement System (Telekommunikation)
**PAN**				Personal Access Number (IT-Sicherheit)
**PAN**				Personal Area Network
**PAN**				Public Access Network
**PAP**				Password Authentication Protocol --> Protokoll aus der PPP-Familie (Protokolle, IT-Sicherheit)
**PAP**				Printer Access Protocol (AppleTalk) (Protokolle)
**PAP**				Public Access Profile (Mobilfunknetze)
**PAPX**			Private Automatic Branch Exchange --> Programmierbare Nebenstellenanlage
**PAR**				Parameter
**PAR**				Parity Check --> Paritätsprüfung (Codierung)
**PAR**				Positive Acknowledgement with Retransmission
**PAR**				Project Authorization Request (Standards, Verordnungen, Gesetze)
**PARC**			Palo Alto Research Center
**PARK**			Portable Access Rights Key
**PARS**			Private Advanced Radio System
**PAS**				Public Avialable Specification
**PASC**			Portable Applications Standards Committee (Gremien, Organisationen)
**PAT**				Port Address Translation --> Adressumsetzungsverfahren für ISDN-Router (ISDN)
**PATS**			Parameterised Abstract Test Suite (Mobilfunkdienste)
**PAX**				Private Automatic Exchange
**PBCC**			Packet Binary Convolution Code (Wireless LAN)
**PBCCH**			Packet Broadcast Control Channel (GPRS)
**PBM**				Portable Bitmap --> Dateiformat für Grafiken
**PBN**				Policy Based Networking (Internetworking)
**PBS**				Personal Business Services (Mobilfunknetze)
**PBTP**			Polybutylentherephthalat (Verkabelung)
**PBX**				Private Branch Exchange --> Private Nebenstellenanlage (Telekommunikations-Endgeräte)
**PC**				Path Control --> Pfadsteuerung (ATM, B-ISDN)
**PC**				Personal Computer
**PC**				Physical Contact (Verkabelung)
**PC**				Point Coordinator
**PC**				Priority Control (ATM, B-ISDN)
**PC**				Protocol Control (ATM, B-ISDN)
**PC/SC**			Personal Computer Smartcard (IT-Sicherheit)
**PCA**				Policy Certification Authority (IT-Sicherheit)
**PCC**				Protocol Converter --> Protokollkonverter (Netzkomponenten)
**PCCPCH**			Primary Common Control Physical Channel (GSM)
**PCD**				Protocol Identifier
**PCDE**			Peak Code Domain Error (UMTS)
**PCF**				Packet Control Facility
**PCF**				Plastic Cladding Silica Fiber (Verkabelung)
**PCF**				Point Coordination Function (Wireless LAN)
**PCH**				Paging Channel --> Rufkanal (Mobilfunknetze)
**PCI DSS**			Payment Card Industry Data Security Standard
**PCI**				PABX Computer Interface (Netzkomoponenten)
**PCI**				Payment Card Industry
**PCI**				Peripheral Component Interconnect --> Bussystem
**PCI**				Programmable Communication Interface --> PCI-Schnittstelle (ISDN)
**PCI**				Protocol Control Information --> Protokoll-Steuerinformation (ISO/OSI)
**PCI**				Protocol Control Interconnect (Intel)
**PCIA**			Personal Communications Industry Association (USA) (Gremien, Organisationen)
**PCIM**			Policy Core Information Model (Netzmanagement)
**PCL**				Printer Command Language (HP)
**PCM**				Physical Connection Management (FDDI)
**PCM**				Pulse Code Modulation --> Verfahren zur Digitalisierung und Übertragung analoger Signale
**PCMCIA**			Personal Computer Memory Card International Association (Gremien, Organisationen)
**PCN**				Personal Communication Network
**PCNE**			Protocol Converter for Non-SNA Equipment (Netzkomponenten)
**PCO**				Point of Control and Observation (Mobilfunkdienste)
**PCP**				Primary Cross-Connection Point --> Kabelverzweiger (Verkabelung)
**PCPICH**			Primary Common Pilot Channel (GSM)
**PCR**				Peak Cell Rate (ATM)
**PCR**				Preventive Cyclic Retransmission (ISDN)
**PCR**				Program Clock Reference
**PCS**				Personal Communication System (Mobilfunknetze)
**PCS**				Physical Coding Sublayer (Ethernet)
**PCS**				Plesiochronous Connection Supervision
**PCS**				Polymer Cladding Silica (Verkabelung)
**PCTR**			Protocol Conformance Test Report (Netzmanagement)
**PCU**				Packet Control Unit --> Paketsteuerungseinheit (Mobilfunknetze)
**PCVD**			Plasma Activated Chemical Vapor Deposition (Verkabelung)
**PCX**				Private CTI Based Call Exchange (TK-Sprachdienste)
**PCX**				Private Communication Exchange
**PD**				Protocol Discriminator --> Feld zur Kennzeichnung des Protokolls
**PD**				Public Domain
**PDA**				Personal Digital Assistant --> Taschencomputer
**PDAM**			Proposed Draft Amendment (Standards, Verordnungen, Gesetze)
**PDAU**			Physical Delivery Access Unit (Mitteilungsdienste)
**PDC**				Personal Digital Cellular --> Japanisches Mobilfunksystem (Mobilfunknetze)
**PDC**				Personal Digital Communications
**PDC**				Pod Disruption Budget
**PDC**				Primary Domain Controller (IT-Sicherheit)
**PDCH**			Packet Data Channel (GPRS)
**PDD**				Post Dialing Delay
**PDF**				Portable Document Format
**PDF**				Portable Document Format --> Dateiformat (Adobe)
**PDFA**			Praseodymium-Doped Fiber Amplifier
**PDG**				Packet Data Gateway
**PDG**				Packet Data Group
**PDH**				Plesiochrone Digitale Hierarchie --> Digitale Übertragungstechnik
**PDIF**			Product Definition Interchange Format
**PDL**				Packet Description Language
**PDL**				Permanent Device Loss
**PDM**				Phasendifferenzmodulation (Modulation)
**PDM**				Product Data Modelling (Online-Dienste)
**PDM**				Pulsdauermodulation (Modulation)
**PDN**				Public Data Network --> Öffentliches Datennetz (Weitverkehrsnetze)
**PDO**				Packet Data Optimized (Mobilfunknetze)
**PDP**				Packet Data Protocol (Protokolle)
**PDP**				Path Discovery Packet (Internetworking)
**PDP**				Policy Decision Point (Internetworking)
**PDS**				Packet Driver Specification
**PDS**				Physical Delivery System
**PDS**				Premises Distribution System (Verkabelung)
**PDSL**			Powerline Digital Subscriber Line (Anschlussnetze)
**PDTCH**			Packet Data TCH (GPRS)
**PDU**				Protocol Data Unit (ISO/OSI)
**PDV**				Path Delay Value (Internetworking)
**PDV**				Prozessdatenverarbeitung
**PDX**				Private Digital Exchange --> Private digitale Nebenstellenanlage
**PE**				Phase Error
**PE**				Physical Element/Entity
**PE**				Polyäthylen (Verkabelung)
**PEAN**			Pan European ATM Network (ATM)
**PEAP**			Protected Extensible Authentication Protocol --> Variante zur gegenseitigen Authentisierung (IT-Sicherheit, Protokolle)
**PEB**				Peripheral Extension Bus (Dialogic)
**PEF**				Packet over Ethernet over Fiber (Stadt-, Regionalnetze)
**PEG**				Port Entry Guide
**PEM**				Privacy Enhanced Mail --> Verfahren zur kryptographisch geschützten Email-Übertragung, (RFC 989, 1113-1115, 1421-1424)
**PEO**				Polar Earth Orbit (Sat-Kommunikation)
**PEP**				Packet Exchange Protocol (Xerox) (Protokolle)
**PEP**				Packetized Ensemble Protocol (Telebit) (POTS, Modem)
**PEP**				Pan European Paging (Mobilfunknetze)
**PEP**				Partitional Emulation Program
**PEP**				Policy Enforcement Point (Internetworking)
**PEP**				Protocol Extensions Protocol
**PER**				Packed Encoding Rules (ASN.1)
**PER**				Packet Error Rate
**PERL**			Practical Extraction and Report Language
**PERT**			Program Evaluation Review Technique
**PES**				Packet over Ethernet over Sonet (Stadt-, Regionalnetze)
**PES**				Packetized Elementary Stream (Codierung)
**PETP**			Polyäthylenterephthalat (Verkabelung)
**PETS**			Parameterised Executable Test Suite (Mobilfunkdienste)
**PEW**				Packet over Ethernet over WDM (Stadt-, Regionalnetze)
**PF**				Pulse Frequency (Modulation)
**PFA**				Perfluoralkoxy-Polymer (Verkabelung)
**PFC**				Public Network for Fixed Connections
**PFM**				Pulsfrequenzmodulation --> Analoges Modulationsverfahren
**PFOS**			Passive Fiber Optical Segment
**PFSK**			Phase-Coherent FSK --> Digitales Modulationsverfahren
**PG**				Peer Group (ATM, B-ISDN)
**PG**				Prozessgewinn (Spread Spectrum)
**PGL**				Peer Group Leader (ATM, B-ISDN)
**PGP**				Pretty Good Privacy --> Software zur kryptografisch geschützten Email-Übertragung (IT-Sicherheit)
**PH**				Packet Handler (ISDN-X.25-Übergang)
**PH**				Packet Header --> Anfangskennsatz eines Paketes (Protokolle)
**PH**				Presentation Header --> Anfangskennsatz der Präsentationsschicht (Protokolle)
**PHAMOS**			Philips Advanced Management and Operation System
**PHASAR**			Phase Array (Optische Netze, SDH, Sonet)
**PHB**				Per Hop Behavior (QoS)
**PHI**				Packet Handler Interface (ISDN)
**PHOTON**			Pan-European Photonic Transport Overlay Network (Optische Netze, SDH, SONET)
**PHP**				Pacific Home Phone System (Mobilfunknetze)
**PHP**				Personal Home Page
**PHP/FI**			Personal Home Page/Form Interpreter
**PHS**				Personal Handyphone System --> Japanisches digitales schnurloses Telefonsystem (Mobilfunknetze)
**PHSAP**			Physical Service Access Point --> Physikalischer Dienstzugangspunkt
**PHY**				Physical (Layer) --> Bitübertragungsschicht
**PI**				Page Impression
**PI**				Primary In --> Primärring-Eingang (FDDI)
**PI**				Procedure Interface
**PI**				Progress Indicator (ISDN)
**PI**				Protocol Interpreter (Mobilfunknetze)
**PI/F**			Polymidfolie/FEP Kapton (Verkabelung)
**PIB**				Policy Information Base (Internetworking)
**PIC**				Peripheral Shelf Interconnection Card
**PIC**				Personal Intelligent Communicator
**PICS**			Platform for Internet Content Selection
**PICS**			Protocol Implementation Conformance Statement (ISO/OSI)
**PID**				Packet Identifier
**PID**				Protocol Identifier Point Coordination (Internetworking)
**PIFS**			Point Coordination Function Interframe Space (Wireless LAN)
**PII**				Personally Identifiable Information
**PILC**			Performance Implications of Link Characteristics (Wireless LAN)
**PIM**				Protocol Independent Multicast
**PIM**				Protocol Independent Multicast (Internet, Protokolle)
**PIM**				Pulse Interval Modulation
**PIMS**			Personal Information Management Services
**PIN**				Personal Identification Number --> Persönliche Identifikationsnummer (IT-Sicherheit)
**PIN**				Personal Information Network (Offene NW-Konzepte)
**PING**			Packet Internet Groper --> ICMP-Echo-Nachricht und ihre Antwort
**PINT**			PSTN/Internet Interworking --> Internet-Telefonie
**PINX**			Private Integrated Services Network Exchange
**PIO**				Programmed Input Output
**PIR**				Packet Insertion Rate
**PIR**				Peak Information Rate
**PIR**				Protocol Independent Routing (Internetworking)
**PISN**			Private Integrated Services Network (Offene NW-Konzepte)
**PIT**				Programmable Interval Timer
**PIU**				Path Information Unit (Hertsteller-NW-Architekturen)
**PIU**				Programmable Interface Unit
**PIXIT**			Protocol Implementation Extra Information for Testing
**PK**				Protokollkonverter (Netzkomponenten)
**PKCS**			Public Key Cryptography Standard (IT-Sicherheit)
**PKI**				Public Key Infrastructure --> Infrastruktur zur Schlüsselverteilung (IT-Sicherheit)
**PKIX**			Public Key Infrastructure Exchange (IT-Sicherheit)
**PKP**				Primärer Konzentrationspunkt
**PKS**				Public Key System (IT-Sicherheit)
**PL**				Padding Length
**PL**				Payload --> Nutzlast
**PL**				Permanent Link
**PL**				Physical Layer --> Bitübertragungsschicht
**PL-OAM**			Physical Layer Operation,Administration and Maintenance --> Betrieb- und Wartung der physikalischen Schicht (Netzmanagement)
**PLC**				Packet Loss Concealment
**PLC**				Powerline Communications (Anschlussnetze)
**PLC**				Programmable Logic Controller
**PLCF**			Physical Layer Convergence Function (ISO/OSI)
**PLCI**			Physical Link Connection Identifier (ISDN)
**PLCP**			Physical Layer Convergence Protocol (ISO/OSI)
**PLD**				Programmable Logic Device
**PLK**				Primary Link --> Erststrecke
**PLL**				Phase Locked Loop --> Phasenstarre Regelschleife
**PLM**				Payload Label Mismatch
**PLM**				Physic Length Modulation (Modulation)
**PLMN**			Public Land Mobile Network --> Öffentliches Mobilfunknetz (Mobilfunknetze)
**PLP**				Packet Layer Protocol --> Schicht-3-Protokoll von X.25 (ITU-T X.25)
**PLP**				Presentation Level Protocol (Protokolle)
**PLR**				Packet Loss Rate --> Paketverlustrate
**PLS**				Physical Layer Signalling (IEEE 802)
**PLS**				Primary Link Station (Hersteller-NW-Architekturen)
**PLT**				Payload Type (ATM, B-ISDN)
**PLT**				Powerline Termination (Anschlussnetze)
**PLU**				Periodic Location Update (Mobilfunknetze)
**PLU**				Primary Logical Unit
**PLVT**			Physical Layer Voice Telephony (Anschlussnetze)
**PLW**				PLCP-PDU Length Word (Wireless LAN)
**PM**				Performance Management --> Leistungsmanagement (Netzmanagement)
**PM**				Phasenmodulation (Modulation)
**PM**				Physical Medium (ATM, B-ISDN)
**PM**				Presentation Manager
**PM**				Problemmanagement (Netzmanagement)
**PMA**				Physical Medium Attachment --> Physikalischer Medienzugang (Ethernet)
**PMAP**			Port Mapper Protocol (Sun) (Protokolle)
**PMBS**			Packet Mode Bearer Service --> Paketorientierter Übermittlungsdienst
**PMC**				Personal Mobile Communication --> Persönliche Mobilkommunikation (Mobilfunknetze)
**PMC**				Physical Media Component
**PMD**				Physical Layer Medium Dependent (FDDI)
**PMD**				Polarisationsmodendispersion (LWL)
**PMF**				Parameter Management Frame (Netzmanagement)
**PMI**				Physical Medium Interface
**PMMA**			Polymethylmethacrylat (Verkabelung)
**PMOH**			Path Monitoring Overhead (Optische Netze, SDH, Sonet)
**PMP**				Punkt-zu-Multipunkt
**PMR**				Private Mobile Radio
**PMR**				Professional Mobile Radio (Mobilfunknetze)
**PMR**				Public Mobile Radio --> Öffentlicher Mobilfunk (Mobilfunknetze)
**PMUX**			Primary Multiplexer --> Primärmultiplexer
**PMXA**			Primärmultiplex-Anschluss (ISDN)
**PMxFAs**			Primärmultiplex-Festanschluss (ISDN )
**PN**				Personal Number (Telekommunikation)
**PN**				Pseudo Noise --> Pseudozufälliges Rauschen
**PNA**				Parallel Network Architecture (Mobilfunknetze)
**PNA**				Phoneline Networking Alliance (Gremien, Organisationen)
**PNC**				Paging Network Controller (ERMES) (Mobilfunknetze)
**PNC**				Personal Number Calling
**PNCP**			Peripheral Node Control Point --> Steuerzentrale für periphere Knoten --> Hersteller-NW-Architekturen)
**PNCQPSK**			Pseudo Noise Complex Quadrature Phase Shift Keying (Modulationsverfahren)
**PNG**				Portable Network Grafics --> Dateiformat für Grafiken
**PNI**				Permit Next Increase (ATM, B-ISDN)
**PNNI**			Private Network-to-Network Interface (ATM, B-ISDN)
**PNO**				Public Network Operator (Telekommunikation)
**PNP**				Private Numbering Plan --> Privater Nummernplan (Mobilfunknetze)
**PNR**				Power Network Repeater (Anschlussnetze)
**PNS**				Primary Name Server
**PNT**				Private Network Termination (ISDN)
**PNU**				Power Network Unit (Anschlussnetze)
**PO**				Primary Out --> Primärring-Ausgang (FDDI)
**POCSAG**			Post Office Codes Special Advisory Group (Gremien, Organisationen)
**PODA**			Priority Oriented Demand Assignment
**POF**				Plastic Optical Fiber --> Kunststoff-Lichtwellenleiter (Verkabelung)
**POH**				Path Overhead (Optische Netze, SDH, Sonet)
**POI**				Point of Interconnection (Internetworking)
**PON**				Passives optisches Netz (Offene NW-Konzepte)
**POP**				Point of Presence --> Zugangs-/Einwahlknoten in Telekommunikationsnetzwerk (Anschlussnetze)
**POP**				Post Office Protocol --> Mailzugriffs-Protokoll im Internet (RFC 1939) (Protokolle)
**POP3**			Post Office Protocol Version 3 (Protokolle)
**POR**				Pacific Ocean Region (Inmarsat) (Sat-Kommunikation)
**POR**				Point of Return
**POS**				Packet over SONET/SDH
**POS**				Personal Operating Space
**POS**				Point of Sale (Online-Dienste)
**POS**				Point of Synchronization
**POST**			Power-On-Self Test
**POTS**			Plain Old Telephone System --> Herkömmliches analoges Telefonnetz
**PP**				Packet Processor
**PP**				Patch Panel (Netzkomponenten)
**PP**				Physical Plane
**PP**				Polypropylen (Verkabelung)
**PP**				Portable Part --> DECT-Mobilstation
**PP**				Presentation Protocol --> OSI-Schicht-6-Protokoll (Protokolle)
**PPA**				Passiver Prüfanschluss
**PPC**				Pay per Channel (Telekommunikation)
**PPCH**			Packet Paging Channel --> Rufkanal bei GPRS
**PPD**				Partial Packet Discard (ATM, B-ISDN)
**PPDU**			Physical Protocol Data Unit (Wireless LAN)
**PPDU**			Presentation Layer Protocol Data Unit (ISO/OSI)
**PPF**				Paging Proceed Flag (Mobilfunknetze)
**PPM**				Packet Purse Model
**PPM**				Pulse Phase Modulation --> Pulsphasenmodulation (Modulation)
**PPM**				Pulse Position Modulation --> Analoges Modulationsverfahren (Modulation)
**PPP**				Point-to-Point Protocol --> Verbindungsprotokoll zwischen zwei Endpunkten (RFC 1661) (Protokolle)
**PPPoA**			PPP over ATM
**PPPoE**			PPP over Ethernet
**PPS**				Packets per Second
**PPS**				Precision Positioning System (GPS)
**PPS**				Pulse per Second (IWV)
**PPTP**			Point-to-Point Tunneling Protocol --> Protokoll aus der PPP-Familie (Protokolle)
**PPV**				Pay per View (Telekommunikation)
**PPX**				Parallel Packet Express
**PPX**				Private Packet Switching Exchange --> Privates Paketvermittlungssystem (Offene NW-Konzepte)
**PQ**				Primary Questionaire
**PQ**				Priority Queuing (Internetworking)
**PQA**				Palm Query Applications (GSM)
**PR**				Path Replacement
**PR**				Persönliche Rufnummer (Weitverkehrsnetze)
**PR**				Privileges Required
**PRA**				Preambel --> Präambel (Ethernet)
**PRA**				Primary Rate Access --> Primärmultiplexanschluß (ISDN)
**PRACH**			Packet Random Access Channel (GPRSl)
**PRBS**			Pseudo Random Binary Sequence --> Binäre Quasi-Zufallsfolge
**PRC**				Primary Rate Channel
**PRC**				Primary Reference Clock (Optische Netze, SDH, Sonet)
**PRC**				Provisioning Class (Netzmanagement)
**PRCP**			Path Reply Control Packet
**PRF**				Pulse Repetition Frequency --> Pulswiederholungsfrequenz
**PRI**				Primary Rate Interface --> Primärmultiplexanschluss (S2M-Schnittstelle) (ISDN)
**PRI**				Provisioning Instance (Netzmanagement)
**PRID**			Provisioninig Instance Identifier (Netzmanagement)
**PRM**				Portable Mobile Radio Modem (Mobilfunknetze)
**PRM**				Protocol Reference Model -> Protokoll-Referenzmodell (ATM, B-ISDN)
**PRMA**			Packet Reservation Multiple Access
**PRMD**			Private Management Domain (ITU-T X.400)
**PRN**				Pseudo Random Numerical (Wireless LAN)
**PRNET**			Packet Radio Network --> Paketorientiertes Funknetz (Mobilfunknetze)
**PRNG**			pseudorandom number generator
**PROFS**			Professional Office System (Hersteller-NW-Architekturen)
**PRR**				Pulse Repetition Rate --> Pulswiederholungsrate
**PRS**				Premium Rate Service (Telekommunikation)
**PRT**				Portable Radio Termination
**PS**				Packet Switching --> Paketvermittlung (Weitverkehrsnetze)
**PS**				Paging System
**PS**				Polystryrol (Verkabelung)
**PS**				PostScript
**PS**				Power Supply --> Stromversorgung
**PS**				Presentation Service --> Dienst der Darstellungsschicht
**PS**				Proposed Standard (Standards, Verordnungen, Gesetze)
**PSA**				Point of Service Activation
**PSACR**			Power Sum Attenuation Crosstalk Ratio
**PSAP**			Presentation Service Access Point --> Dienstzugriffspunkt der Darstellungsschicht (ISO/OSI)
**PSC**				Peripheral Shelf Controller
**PSC**				Physical Signalling Component
**PSCH**			Primary Synchronisation Channel (GSM)
**PSD**				Power Spectral Density
**PSDN**			Packet Switched Data Network --> Paketvermitteltes Datennetz (X.25)
**PSDU**			Presentation Service Data Unit --> Dienstdateneinheit der Darstellungsschicht (ISO/OSI)
**PSE**				Packet Switching Exchange --> Datenpaketvermittlung ITU-T (X.25)
**PSE**				Path Switching Element (FDDI)
**PSE**				Personal Security Environment --> Persönliche Sicherheitsumgebung (IT-Sicherheit)
**PSE**				Personal Service Environment
**PSELFEXT**			Power Sum Equal Level Far End Crosstalk (Verkabelung)
**PSF**				PLCP-PDU Signalling Field (Wireless LAN)
**PSFEXT**			Power Sum Far End Crosstalk (Verkabelung)
**PSH**				Push (Flag, Steuerzeichen)
**PSI**				Packetnet System Interface (DEC/VAX)
**PSI**				Programmable Serial Interface
**PSI**				Programmspezifische Information
**PSI**				Protocol Service Information
**PSK**				Phase Shift Keying --> Phasenumtastung (Modulation)
**PSM**				Product Specific Module
**PSN**				Packet Switch Node --> Netzknoten in paketvermittelndem Netz (Weitverkehrsnetze)
**PSN**				Packet Switched Network --> Paketorientiertes, vermittelndes Netz
**PSN**				Public Switched Network --> Öffentliches vermittelndes Netz (Weitverkehrsnetze)
**PSNEXT**			Power Sum Near End Crosstalk (Verkabelung)
**PSNP**			Partial Sequence Numbers PDU (Internetworking)
**PSO**				Private Service Operator
**PSO**				Professional Services Organisation
**PSO**				Protocol Supporting Organization (ICANN) (Gremien, Organisationen)
**PSP**				Packet Switching Processor
**PSP**				Pod Security Policy
**PSPDN**			Packet Switched Public Data Network --> Öffentliches Datenpaketvermittlungsnetz (Offene NW-Konzepte)
**PSQM**			Perceptual Speech Quality Measurement (Telekommunikation)
**PSR**				Previous Slot Received
**PSR**				Previous Slot Reuse
**PSS 1**			Private Signalling System Number 1 (Telekommunikation)
**PSS**				Packet Switched Stream (ITU-T X.25)
**PSS**				Packet Switching Service
**PSS**				Packet Switching System --> Datenpaketvermittlungssystem
**PSS**				Public Safety and Security (Mobilfunknetze)
**PSTN**			Public Switched Telephone Network --> Öffentliches, vermittelndes Telefonnetz
**PSTP**			PNNITopology State Packet (ATM, B-ISDN)
**PSU**				Power Supply Unit --> Stromversorgungseinheit
**PT**				Path Traversal
**PT**				Payload Type --> Nutzsignalart
**PT**				Penetration Testing --> Penetrationstest (IT-Sicherheit)
**PT**				Personal Telecommunication (Mobilfunknetze)
**PTAT**			Private Trans Atlantic Cable --> Glasfaserkabel zwischen Europa und Nordamerika (Verkabelung)
**PTCRB**			PCS Type Certification Review Board (Gremien, Organisationen)
**PTE**				Path Terminal Equipment (Optische Netze, SDH, Sonet)
**PTE**				Private Trading Exchange (Online-Dienste)
**PTF**				Program Temporary Fix
**PTFE**			Polytetrafluorethylen (Verkabelung)
**PTI**				Payload Type Identifier
**PTL**				Physical Transport Layer (Hersteller-NW-Architekturen)
**PTM**				Packet Trade Model
**PTM**				Packet Transfer Mode
**PTM**				Packet Transport Mode (ATM, B-ISDN)
**PTM**				Point-to-Multipoint
**PTN**				Private Telecommunication Network
**PTNX**			Private Telephone Network Exchange (Weitverkehrsnetze)
**PTO**				Public Telecommunication Operator (Telekommunikation)
**PTO**				Public Telephone Operator
**PTP**				Point-to-Point
**PTR**				Pointer --> Zeiger (Optische Netze, SDN, Sonet)
**PTS**				Presentation Time Stamp (ATM, B-ISDN)
**PTSE**			PNNI Topology State Element (ATM, B-ISDN)
**PTSP**			PNNI Topology State Packet (ATM, B-ISDN)
**PTT**				Post, Telephone and Telegraph
**PTT**				Push to Talk
**PU**				Physical Unit
**PU**				Portable Unit --> Mobilteil (Mobilfunknetze)
**PUCP**			Peripheral Unit Control Point (IBM/SNA)
**PUCP**			Physical Unit Control Point
**PUK**				Personal Unblocking Key (Mobilfunknetze)
**PUMS**			Physical Unit Management Services
**PUP**				Parc Universal Packet
**PUP**				Parc Universal Protocol (Protokolle)
**PUR**				Polyrethan (Verkabelung)
**PV**				Packetized Voice --> Paketvermittelte Sprache
**PV**				Paketvermittlung (Weitverkehrsnetze)
**PVC**				Permanent Virtual Circuit --> Dauerhaft eingerichtete, virtuelle Verbindung (X.25)
**PVC**				Permanent Virtual Connection
**PVC**				Ployvinylchlorid (Verkabelung)
**PVCC**			Permanent Virtual Channel Connection (ATM, Be-ISDN)
**PVCL**			Permanent Virtual Channel Link
**PVDC**			Provider Virtual Data Center (VMware)
**PVDF**			Polyvinylidenfluorid (Verkabelung)
**PVER**			Password Verification
**PVP**				Permanent Virtual Path (ATM, B-ISDN)
**PVPC**			Permanent Virtual Path Connection (ATM, B-ISDN)
**PVPL**			Permanent Virtual Path Link
**PVV**				Path Variability Value
**PVXC**			Pulse Vector Exciting Coding
**PW**				Pulse Width --> Pulsbreite
**PWA**				Predicted Wordlength Assignment
**PWD**				Print Working Directory
**PWM**				Pulse Width Modulation --> Pulsbreitenmodulation (Modulation)
**PX**				Private Exchange --> Privatvermittlung
**PXP**				Packet Exchange Protocol (Protokolle)
**PaaS**			Plattform-as-a-Service
**PiMF**			Pair in Metal Foil --> Paar in Metallfolie (Verkabelung)
**Q-Bit**			Qualifier Bit (ITU-T X.25)
**QA**				Queued Arbitrated Function
**QAF**				Q-Adapter Function (Netzmanagement)
**QAM**				Quadrature Amplitude Modulation --> Digitales Modulationsverfahren
**QAM**				Queued Access Method
**QAPSK**			Quadrature Amplitude Phase Shift Keying --> Digitales Modulationsverfahren
**QBtx**			Qualitäts-Bildschirmtext
**QCB**				Queue Control Block --> Warteschlangensteuerungsblock (Datenfelder)
**QD**				Queuing Delay (ATM, B-ISDN)
**QFC**				Quantum Flow Control (ATM, B-ISDN)
**QL**				Querleitung (Weitverkehrsnetze)
**QL**				Query Language --> Abfragesprache (Datenverarbeitung)
**QL**				Queue Length
**QLLC**			Qualified Logical Link Control --> Protokoll um SNA über X.25-Netze abzuwickeln (IBM/SNA) (Protokolle)
**QMF**				Query Management Facility (Netzmanagement)
**QPSK**			Quadrature Phase Shift Keying --> Digitales Modulationsverfahren
**QPSX**			Queued Packet and Synchronous Exchange --> Synchrone Koppeleinrichtung mit Paketwarteschlangenbetrieb
**QRI**				Queued Response Indicator
**QSAM**			Quadrature Sideband Amplitude Modulation (Modulation)
**QSAM**			Queued Sequential Access Method
**QSIG**			Q-Reference Point Signalling Protocol --> Von ETSI standardisiertes Signalisierungsverfahren zwischen Nebenstellenanlagen (ETS 300.239)
**QSIG-GF**			QSIG Generic Function (Telekommunikation)
**QTAM**			Queued Telecommunication Access Method
**QV**				Querverbindung
**QoS**				Quality of Service --> Dienstgüte
**R**				Recommendation --> Normempfehlung (Standards, Verordnungen, Gesetze)
**R**				Reservation (Bit, Steuerzeichen)
**R/S**				Reset/Set
**R1**				Regional (Signalling System) No. 1
**R2**				Regional (Signalling System) No. 2
**RA**				Radio Access
**RA**				Rate Adaption
**RA**				Real Audio (Online-Dienste)
**RA**				Registration Authority --> Registrierungsstelle
**RA**				Remote Access
**RA**				Routing Arbiter
**RA**				Routing Area --> Routing-Bereich
**RAA**				Ressourcen-Allokationsanfrage
**RACCH**			Random Access Channel (Mobilfunknetze)
**RACE**			Research on Advanced Communications in Europe --> EU-Forschungsförderprogramm, 1987-1995
**RACF**			Resource Access Control Facility
**RACH**			Random Access Channel --> Zufalls-Zugriffskanal (GSM, Mobilfunknetze)
**RAD**				Remote Access Device (Netzkomponenten)
**RADIUS**			Remote Authentication Dial In User Service --> Authentifizierungsmechanismus (RFC 2138) (IT-Sicherheit)
**RADSL**			Rate Adaptive Digital Subscriber Line (Anschlussnetze)
**RAF**				Resouce Allocation Frame
**RAI**				Remote Alarm Indication
**RAID**			RAID means Redundant Array of Inexpensive Disks
**RAID**			Redundant Array of Inexpensive Disks --> System aus mehreren Festplattenlaufwerken mit automatischer Redundanz (Speichertechnik)
**RAID**			also an acronym for Risks, Assumptions, Issues, and Dependencies
**RAIL**			Redundant Array of Inexpensive Libraries (Speichertechnik)
**RAIT**			Redundant Array of Inexpensive Tapes (Speichertechnik)
**RAM**				Random Access Momory --> Beschreibbarer Speicher mit schnellem Zugriff
**RAN**				Radio Access Network --> Funkzugangsnetz (Mobilfunknetze)
**RAN**				Remote Access Node (Mobilfunknetze)
**RANAP**			Radio Access Network Application Part (Mobilfunknetze, Protokolle)
**RAND**			Random Number --> Zufallsnummer (Mobilfunknetze)
**RAP**				Radio Access Point (Anschlussnetze)
**RAP**				Radio Access Profile (DECT)
**RAP**				Route Access Protocol (Protokolle, Internetworking)
**RAPID**			Reserve Alternate Path with Immediate Diversion
**RARC**			Regional Administrative Radio Conference (Gremien, Organisationen)
**RARE**			Réseaux Associés pour la Recherche Européen (Gremien, Organisationen)
**RARP**			Reverse Address Resolution Protocol (Protokolle)
**RAS**				Registration, Admission and Status
**RAS**				Remote Access Server --> Einwähl-Server, meist eine Modem-Bank des Diensteanbieters
**RAS**				Remote Access Service (Microsoft)
**RASCI**			(R) Responsible, (A) Accountable, (S) Supports, (C) Consulted, (I) Informed
**RAT**				Radio Access Technology
**RAT**				Remote Administration Tool
**RATP**			Reliable Asynchronous Transfer Protocol (Protokolle)
**RAU**				Remote Access Unit
**RAX**				Remote Access --> Fernzugriff
**RB**				Reserved Bandwidth
**RBAC**			Role Based Access Control (IT-Sicherheit)
**RBAC**			role-based access control
**RBB**				Residental Broadband (ATM, B-ISDN)
**RBDS**			Radio Broadcast Data System (Mobilfunknetze)
**RBE**				Remote Batch Entry
**RBG**				Random Bit Generator
**RBM**				Regelbasierte Maschine
**RBS**				Radio Base Station (Mobilfunknetze)
**RBS**				Radiobasisstation
**RBS**				Remote Batch System
**RBS-F**			Radiobasisstation-Funkteil
**RBS-V**			Radiobasisstation-Vermittlungsteil
**RC**				Report Confidence
**RC**				Request Counter (ATM, B-ISDN)
**RC**				Ring Coupler
**RC**				Route Control (Datenfelder)
**RC2**				Rivest Cipher No 2 --> Stromchiffrierverfahren von Ron Rivest (IT-Sicherheit)
**RC4**				Rivest Cipher 4
**RC4**				Rivest Cipher No 4 --> Stromchiffrierverfahren von Ron Rivest (IT-Sicherheit)
**RC5**				Rivest Cipher No 5 --> Stromchiffrierverfahren von Ron Rivest (IT-Sicherheit)
**RCC**				Routing Control Center --> Routing-Steuerungszentrum (Internetworking)
**RCF**				Registration Confirm
**RCO**				Registration Confirmation Packet --> Registrierungsbestätigung (ITU-T X.25)
**RCP**				Remote Copy
**RCS**				Return Channel System --> Rückkanalsystem (Sat-Kommunikation)
**RCSI**			Remote Control Service Inband
**RCU**				Remote Concentrator Unit
**RCV**				Receive
**RD**				Receive Data --> Datenempfang (Steuerzeichen)
**RD**				Receive Data --> Signal der V.24-Schnittstelle
**RD**				Routing Domain --> Routing-Domäne (Internetworking)
**RD-LAP**			Radio Data - Link Access Protocol (Motorola)
**RDA**				Remote Data Access
**RDA**				Remote Database Access --> Datenbankfernzugriff
**RDA**				Rufdatenaufzeichnung (Telekommunikation)
**RDBMS**			Relationales Datenbank-Managementsystem (Datenbank)
**RDE**				Route Determination Entity (Token-Ring)
**RDF**				Rate Decrease Factor (ATM, B-ISDN)
**RDF**				Repeater Distribution Frame
**RDF**				Resource Denied Frame (FDDI)
**RDI**				Remote Defect Indication (Netzmanagement)
**RDI**				Routing Domain Identifier (ISO/OSI)
**RDMA**			Remote DMA (Fiber Channel)
**RDN**				Radio Data Network (Mobilfunknetze)
**RDN**				Relative Distinguished Name (Verzeichnisdienste)
**RDP**				Reliable Datagram Protocol (Protokolle)
**RDP**				Remote Desktop Protocol
**RDP**				Remote Desktop Protocol (Protokolle)
**RDR**				Radio Downstream Receiver (Stadt-, Regionalnetze)
**RDR**				Receive Data Register --> Empfangsdatenregister
**RDS**				Radio Data System --> Datenübertragung im UKW-Rundfunk
**RDS**				Remote Data Services (IBM)
**RDS**				Remote Device System (Telekommunikations-Endgeräte)
**RDS**				Running Digital Sum --> Verfahren zur Überwachung von Leitungscodes
**RDSS**			Radio Determination Satellite Service --> Positionsbestimmung mittels Satelliten
**RDT**				Radio Downstream Transmitter
**RDU**				Radio Distribution Unit
**RDY**				Ready --> Bereit (Steuerzeichen)
**RE**				Recall (ISDN)
**READ**			Relative Element Address Destinate (Codierung)
**RECO**			Recoverability
**RED**				Random Early Detection (Internetworking)
**REI**				Remote Error Indicator
**REL**				Release (ISDN)
**RELP**			Residually Excited Linear Predictive --> Quellencodierungsverfahren für Sprache (Codierung)
**REM**				Remote Error Monitor
**REM**				Ring Error Monitor
**REM**				Router Engine Module (Internetworking)
**REN**				Remote Enable --> Fernbedienung ermöglichen
**REQ**				Request --> Anfrage (Datenfelder)
**RER**				Residual Error Rate --> Restfehlerrate
**RER**				Reverse Error Reporting (Sat-Kommunikation)
**RES**				Radio Equipment and Systems (Mobilfunknetze)
**RES**				Regional Earth Station (Sat-Kommunikation)
**RES**				Reserved
**RES**				Resume Message (Mobilfunknetze)
**REV-S**			Reverse Charging --> Entgeltübernahme durch den Angerufenen
**REXEC**			Remote Execute
**RF**				Radio Frequency --> Funkfrequenz
**RF**				Rundfunk
**RFA**				Radio Fixed Access
**RFA**				Remote File Access --> Dateifernzugriff
**RFB**				Radiofrequente Beeinflussung
**RFC**				Request for Comments --> Standardisierungsdokument im Internet (Standards, Verordnungen, Gesetze)
**RFC**				Request for Connection --> Verbindungsanforderung (Steuerzeichen)
**RFC**				Requests for Comments
**RFCOMM**			Radio Frequency Communication (Wireless LAN)
**RFD**				Request for Discussion --> Diskussionsvorschlag im Internet (Standards, Verordnungen, Gesetze)
**RFI**				Radio Frequency Interference --> Interferenz elektromagnetischer Wellen
**RFI**				Remote Failure Indicator
**RFI**				Remote File Inclusion																	
**RFI**				Request for Information (Datenfelder)
**RFID**			Radio Frequency Identification
**RFM**				Radio Frequency Modem
**RFNM**			Ready for Next Message --> Bereit für die nächste Nachricht
**RFNM**			Request for Next Message --> Anfrage nach der nächsten Nachricht (Datenfelder)
**RFOH**			Radio Frequency Overhead
**RFP**				Radio Fixed Part
**RFP**				Request for Proposal
**RFPI**			Radio Fixed Part Identity
**RFS**				Remote Failure Signalling
**RFS**				Remote File Sharing
**RFS**				Remote File System (Datenverarbeitung)
**RFU**				Radio Frequency Unit (Mobilfunknetze)
**RG 58**			Koaxialkabeltyp (50 Ohm)
**RG**				Rechner-Gateway
**RG**				Regenerator (Optische Netze, SDH, Sonet)
**RGA**				Reset Circuit Group Acknowledgement (Mobilfunknetze)
**RGN**				Routing Group Name --> Pfadsteuerungselement-Name
**RGW**				Residential Gateway
**RH**				Request Header --> Anfangskennsatz für Anfrage (Hersteller-NW-Architekturen)
**RH**				Response Header - Anfangskennsatz für Antwort (Hersteller-NW-Architekturen)
**RHT**				Reroute Hold Timer
**RI**				Reliability Index
**RI**				Ring Indicator --> Signal der V.24-Schnittstelle
**RI**				Ring Input --> Ringeingang (Token Ring)
**RI**				Routing Information --> Wegewahlinformation (Internetworking)
**RIB**				Routing Information Base (Internetworking)
**RIC**				Radio Identify Code (Mobilfunknetze)
**RIF**				Rate Increase Factor (ATM, B-ISDN)
**RIF**				Routing Information Field (Token Ring)
**RIFU**			Richtfunk
**RII**				Routing Information Indicator (Internetworking)
**RIM**				Request for Initialization Mode
**RIN**				Relative Intensity Noise
**RIP**				Routing Information Protocol --> Routingprotokoll (RFC 1058, 2453)
**RIPE**			Réseaux IP Européenne
**RIPE-NCC**			RIPE Network Control Center
**RIPL**			Remote Initial Program Loaded
**RIPSO**			Revised Internet Protocol Security Option (DoD, USA) (IT-Sicherheit)
**RIR**				Regional Internet Registry (ICANN) (Gremien, Organisationen)
**RIS**				Reference Information Store
**RISC**			Reduced Instruction Set Computer
**RITL**			Radio in the Loop
**RJ 11**			Vierpoliger Western-Stecker (für analogen Telefonanschluß)
**RJ 14**			Vierpoliger Western-Stecker mit 2 Steckverbindungen
**RJ 45,**			Achtpoliger Western-Stecker (z.B. für ISDN, Ethernet, ATM)
**RJ 48**			Achtpoliger Western-Stecker
**RJ**				Residual Jack --> Steckerform, auch Western-Stecker genannt
**RJE**				Remote Job Entry --> Jobferneingabe (Datenverarbeitung)
**RJEP**			Remote Job Entry Protocol --> Protokoll für Jobferneingabe (Protokolle)
**RKL**				Rufklasse (Mobilfunknetze)
**RKW**				Rahmenkennungswort (TDM)
**RL**				Remediation Level
**RL**				Return Loss --> Rückflussdämpfung (Verkabelung)
**RLA**				Remote LAN Access
**RLAM**			Remote Lobe Attachment Module
**RLC**				Radio Link Control (GSM/GPRS) (Mobilfunknetze)
**RLC**				Release Complete (ISDN)
**RLE**				Run Length Encoding --> Lauflängencodierung (Codierung)
**RLL**				Radio (in the) Local Loop (Anschlussnetze)
**RLOGIN**			Remote Login (Anwendungsdienst)
**RLP**				Radio Link Protocol (Mobilfunknetze, Protokolle)
**RLP**				Resource Location Protocol
**RLSD**			Received Line Signal Indicator
**RLSD**			Released --> Freigegeben (ISDN)
**RLV**				Ringleitungsverteiler (Token Ring)
**RM**				Referenzmodell (ISO/OSI)
**RM**				Resource Management
**RM**				Resource Management (ATM)
**RMC**				Reference Measurement Channel
**RMF**				Resource Management Facility
**RMI**				Remote Method Invocation
**RMON**			Remote Monitoring --> Netzmanagementprotokoll (RFC 1743, 1751) (Netzmanagement)
**RMT**				Ring Management (FDDI)
**RMU**				Remote Multiplexer Unit --> Fernmultiplexer
**RMoA**			Realtime Multimedia over ATM (ATM, B-ISDN)
**RN**				Rechnernetz
**RN**				Regionalnetz
**RN**				Remote Node (Netzkomponenten)
**RN**				Ring Number
**RNA**				Remote Network Access
**RNC**				Radio Network Controller --> Funknetzsteuerung (Mobilfunknetze)
**RNG**				Radio Network Gateway (Mobilfunknetze)
**RNG**				Random Number Generator
**RNID**			Return Node Identification (Speichernetze)
**RNP**				Regional Network Provider
**RNR**				Receiver Not Ready (HDLC)
**RNT**				Radio Network Termination
**RO**				Read Only --> Nur lesen (Steuerzeichen)
**RO**				Receive Only --> Nur Empfang (Steuerzeichen)
**RO**				Remote Operation
**RO**				Ring Output --> Ringausgang (Token Ring)
**RO**				Router (Internetworking)
**ROA**				Recognized Operating Agency
**ROAD**			Routing and Addressing Group (Gremien, Organisationen)
**ROADS**			Running Out of Address Space
**ROCCO**			Robust Checksum-based Header Compression
**RODM**			Resource Object Data Manager
**RONA**			Redirect on no Answer
**ROR**				Rate of Return
**ROS**				Remote Operation Service (Mitteilungsdienste)
**ROSE**			Remote Operations Service Element (ISO/OSI)
**ROSI**			Radio Signalling Protocol (Mobitex)
**RP**				Radio Port --> Funkfeststation (Mobilfunknetze)
**RP**				Reference Point
**RP**				Rendezvouz Point (Internetworking)
**RP**				Route Port (Internetworking)
**RP**				Routing Protocol
**RPB**				Reverse Path Broadcast (Internetworking)
**RPC**				Radio Port Controller (Mobilfunknetze)
**RPC**				Remote Procedure Call (Sun) (RFC 1057)
**RPC**				Root Path Costs
**RPCU**			Radio Port Controller Unit (Mobilfunkknetze)
**RPE**				Radio Paging Equipment (Mobilfunknetze)
**RPE**				Regular Pulse Excitation (Mobilfunknetze)
**RPE**				Remote Procedure Error --> Ablauffehler der Gegenstelle
**RPE-LTP**			Regular Pulse Excitation with Long Term Prediction
**RPF**				Reverse Path Forward (DK-Übertragungstechniken)
**RPL**				Remote Program Load (Datenverarbeitung)
**RPM**				Radio Packet Modem (Mobilfunknetze)
**RPM**				Reverse Path Multicasting (DK-Übertragungstechniken)
**RPO**				Recovery Point Objective
**RPOA**			Recognized Private Operating Agency
**RPP**				Remote Printing Protocol (Protokolle)
**RPR**				Resilient Packet Ring (Lokale Netze)
**RPRSG**			Resilient Packet Ring Study Group (Gremien, Organisationen)
**RPS**				Ring Parameter Server (Token Ring)
**RPSU**			Redundant Power Supply Unit
**RPU**				Radio Port Unit (Mobilfunknetze)
**RQ**				Repeat Request
**RR**				Radio Regulations --> Funkvorschriften (Standards, Verordnungen, Gesetze)
**RR**				Radio Resource (GSM)
**RR**				Ready to Receive (HDLC)
**RR**				Relative Rate (ATM, B-ISDN)
**RRAS**			Routing and Remote Access Service (Protokolle)
**RRB**				Radio Regulations Board (ITU) (Gremien, Organisationen)
**RRC**				Radio Resource Control (GSM)
**RRC**				Regional Radiocommunication Conferences (ITU) (Gremien, Organisationen)
**RRC**				Root Raised Cosine --> Wurzel-Kosinus(-Filter)
**RREP**			Route Reply
**RREQ**			Route Request
**RRJ**				Registration Reject
**RRM**				Radio Resource Management (GSM, Mobilfunknetze)
**RRO**				Record Route Object
**RRQ**				Registration Request
**RRTAG**			Radio Regularoty Technicyal Advisory Group (Gremien, Organisationen)
**RRU**				Request Response Unit --> Anfrage/Antwort-Einheit
**RRV**				Rechner-Rechner-Verbund
**RS**				Recommended Standard --> Empfohlene Norm (Standards, Verordnungen, Gesetze)
**RS**				Reconciliation Sublayer
**RS**				Record Seperator --> Untergruppen-Trennzeichen (Steuerzeichen)
**RS**				Reed-Solomon --> Code zur Kanalcodierung (Codierung)
**RS**				Regenerator Section --> Regeneratorabschnitt
**RS**				Reset --> Rücksetzen (Datenfelder)
**RS**				Resource Sharing --> Ressourcenteilung
**RS-232**			Recommended Standard No. 232 --> Asynchrone serielle Schnittstelle
**RSA**				Rivest Shamir Adelman --> Kryptographisches Public-Key-Verfahren (IT-Sicherheit)
**RSA**				Rivest-Shamir-Adelman
**RSB**				Restseitenband (Modulation)
**RSC**				Reset Circuit Message (Mobilfunknetze)
**RSCS**			Remote Spooling and Control Subsystem
**RSE**				Real System Environment
**RSFG**			Route Server Functional Group (ATM, B-ISDN)
**RSG**				Reset Circuit Group Message (Mobilfunknetze)
**RSL**				Radio Signal Link (GSM)
**RSM**				Remote Subscriber Multiplexer (Anschlussnetze)
**RSM**				Removable Storage Management (Speichernetze)
**RSO**				Remote Spooling Option
**RSOH**			Repeater Section Overhead (Optische Netze, SDH, Sonet)
**RSP**				Response --> Antwort (Datenfelder)
**RSP**				Routing Switching Platform
**RSRB**			Remote Source Route Bridging (Internetworking)
**RSS**				Radio Subsystem --> Funkfeststationssystem (Mobilfunknetze)
**RSS**				Remote Subscriber Switch (Anschlussnetze)
**RSSCP**			Remote System Service Control Point
**RSSI**			Received Signal Strength Indicator (Mobilfunknetze)
**RST**				Reset (Flag, Datenfeld, Steuerzeichen)
**RST**				reStructuredText
**RSTP**			Rapid Spannung Tree Protocol (Protokolle)
**RSU**				Remote Subscriber Unit
**RSU**				Remote Switching Unit
**RSVD**			Reserved
**RSVP**			Resource Reservation Setup Protocol --> Internetworkig-Protokoll (RFC 2205, 2208)
**RSVP-TE**			Resource Reservation Protocol Trafic Engineering (Internetworking)
**RT**				Radio Telephone --> Funktelefon (Telekommunikation)
**RT**				Radio Termination
**RT**				Realtime --> Echtzeit (Telekommunikation)
**RT**				Reliable Transfer
**RT**				Remote Terminal (Telekommunikations-Endgeräte)
**RT**				Routing Table --> Routing-Tabelle (Internetworking)
**RTAM**			Remote Telecommunication Access Method
**RTC**				Real Time Clock
**RTCP**			Real Time Control Protocol (Internet, Protokolle)
**RTD**				Round Trip Delay --> Umlaufverzögerung (Lokale Netze)
**RTE**				Remote Terminal Emulator
**RTFM**			Realtime Flow Measurement
**RTIC**			Realtime Interface Coprocessor
**RTICM**			Realtime Interface Coprocessor Multiport
**RTIN**			Return Topology Information (Speichernetze)
**RTM**				Response Time Management (Netzmanagement)
**RTMP**			Routing Table Maintenance Protocol (AppleTalk) (Protokolle)
**RTMS**			Radio Telephone Mobile System --> Zellulares Mobilfunksystem in Italien (Mobilfunknetze)
**RTO**				Recovery Time Objective
**RTO**				Retransmission Time-out (TCP)
**RTO**				Round Trip Time-out
**RTP**				Rapid Transport Protocol (Hersteller-NW-Architekturen)
**RTP**				Realtime Transport Protocol --> Internet-Protokol zur Unterstützung von Echtzeitanwendungen (RFC 1889, 1890)
**RTP**				Regulierungsbehörde für Telekommunikation und Post (Gremien, Organisationen)
**RTP**				Routing Update Protocol (Banyan) (Protokolle)
**RTR**				Ready to Receive
**RTS**				Reliable Transfer Service (ITU-T X.400)
**RTS**				Reliable Transfer Service (Mitteilungsdienste)
**RTS**				Request to Send --> Signal der V.24-Schnittstelle
**RTSE**			Reliable Transfer Service Element
**RTSP**			Real Time Streaming Protocol --> Protokoll zur Übertragung von Echtzeitdaten im Internet (Protokolle, Online-Dienste)
**RTT**				Round Trip Time
**RTTY**			Radio Teletype
**RTU**				Remote Terminal Unit --> Fernbedienungsterminal
**RU**				Remote Unit (Netzkomponenten)
**RU**				Request/Response Unit (Hersteller-NW-Architekturen)
**RUA**				Remote User Agent
**RUB**				Router Hub (Internetworking)
**RUDP**			Reliable User Datagram Protocol (Protokolle)
**RUR**				Radio Upstream Receiver (Stadt-, Regionalnetze)
**RUT**				Radio Upstream Transmitter (Stadt-, Regionalnetze)
**RVP over IP**			Remote Voice Protocol over Internet Protocol (Protokolle)
**RVP**				Remote Voice Protocol (Protokolle)
**RVP**				Rendezvous Protocol (Microsoft)
**RW**				Read/Write (Steuerzeichen)
**RWA**				Routing and Wavelength Assignment
**RWP**				Remote Write Protocol (Protokolle)
**RXLEV**			Received Signal Level (Mobilfunknetze)
**RXQUAL**			Received Signal Quality (Mobilfunknetze)
**RZ**				Rechenzentrum
**RZ**				Regionalzone
**RZ**				Return-to-Zero --> Verfahren zur Leitungscodierung (Codierung)
**RZ-I**			Return to Zero Inverted --> Verfahren zur Leitungscodierung (Codierung)
**RegTP**			Regulierungsbehörde für Telekommunikation und Post (Gremien, Organisationen)
**Rt-VBR**			Real Time Variable Bit Rate (ATM, B-ISDN)
**Rx**				Receiver
**S**				Scope
**S-AL**			Slotted Aloha
**S-DBM**			Satellite - Digital Multimedia Broadcasting
**S-DSMA**			Slotted Digital Sense Multiple Access --> Medium-Zugriffsverfahren (Modacom)
**S-HTTP**			Secure Hypertext Transfer Protocol (Protokolle)
**S-ISDN**			Schmalband-ISDN (ISDN)
**S/MIME**			Secure Multipurpose Internet Mail Extension (IT-Sicherheit)
**S/N**				Signal to Noise Ratio --> Signal-zu-Rauschleistungs-Verhältnis
**S/P-DIF**			Sony/Philips Digital Interface Format
**S/STP**			Screened Shielded Twisted Pair --> Geschirmte verdrillte Leitung mit Kabel-Gesamtschirmung (Verkabelung)
**S/UTP**			Screened Unshielded Twisted Pair --> Nicht-geschirmte verdrillte Leitung mit Kabel-Gesamtschirmung (Verkabelung)
**S12**				Digitales Vermittlungssystem (Alcatel)
**S2**				SpringSource (VMware)
**S2M**				Primary Rate Interface --> Teilnehmerschnittstelle mit einer Kapazität von 30×B+D64 (ISDN)
**SA**				Security Association --> Sicherheitsvereinbarung
**SA**				Service Area --> Versorgungsbereich (Mobilfunknetze)
**SA**				Source Address --> Quelladresse
**SAA**				System Aspects Application (ATM, B-ISDN)
**SAA**				Systems Application Architecture (IBM/SNA)
**SAA-AMS**			System Aspects Application - Audiovisual Multimedia Services (ATM, B-ISDN)
**SAA-VTOA**			System Aspects Application - Voice and Telephony over ATM (ATM, B-ISDN)
**SAAC**			Syntax-based Adaptive Arithmetic Coding
**SAAL**			Signalling ATM Adaption Layer (ATM, B-ISDN)
**SAC**				Service Access Code
**SAC**				Single Attached Concentrator
**SAC**				Single Attachment Concentrator (FDDI)
**SAC**				Subscriber Access Control
**SAC**				Switching Access Card
**SACCH**			Slow Associated Control Channel --> Langsamer asoziierter Steuerkanal (GSM)
**SACF**			Single Association Controlling Function
**SACK**			Selective Acknowledgement
**SAD**				Security Association Database (IT-Sicherheit)
**SAD**				Single Administrative Document
**SADL**			Synchronous Auto Dial Language
**SAFI**			Subsequent Address Family Identifier
**SAGF**			Service Application Gateway Function
**SAI**				Serving Area Interface
**SAID**			Secure Association Identifier (IT-Sicherheit)
**SAID**			Serving Area Identification (Number)
**SAM**				Service Access Multiplexer
**SAM**				Software Asset Management
**SAM**				Subsequent Address Message (ISDN)
**SAML**			Security Assertion Markup Language
**SAN**				Storage Area Network
**SAN**				Storage Area Network --> Dediziertes Netz zur Verbindung von Speichermedien (Speichernetze)
**SANC**			Signalling Area Network Code
**SAO**				Single Association Object
**SAP**				Segmentation Application Point
**SAP**				Service Access Point --> Dienstzugangspunkt (ISO/OSI)
**SAP**				Service Advertising Protocol (Novell) (Protokolle)
**SAP**				Session Announcement Protocol (Protokolle)
**SAPI**			Service Access Point Identifier (ISDN/LAP-D)
**SAPI**			Speech Application Programming Interface (Telekommunikation)
**SAR**				Segmentation and Reassembly (Sublayer) (ATM)
**SARA**			Satellite Radio --> Digitaler Hörrundfunk über Eutelsat-Satelliten
**SAS**				Sammelanschlussschaltung (Telekommunikation)
**SAS**				Server Attached Storage (Speichernetze)
**SAS**				Single Attached Station --> Einfach angeschlossene Station (FDDI)
**SAS**				Subscriber Alerting Signal
**SASE**			Specific Application Service Element --> Spezielles Anwendungs-Dienstelement (ISO/OSI)
**SASL**			Simple Authentification and Security Layer (IT-Sicherheit)
**SAST**			Static Application Security Testing
**SAT**				Subscriber Accesss Termination
**SATAN**			Security Administrator Tool for Analyzing Networks (IT-Sicherheit)
**SATS**			System Conformance Test Suite (Netzmanagement)
**SAVD**			Simultaneous or Alternating Voice Data
**SAVE**			Satellitenverteildienst (Sat-Kommunikation)
**SAW**				Surface Acoustic Wave (Mobilfunknetze)
**SB**				Synchronisation Burst (GSM)
**SBA**				Synchrounous Bandwidth Allocator
**SBC**				Sub-Band Coding
**SBCCS**			Single Byte Command Code Set
**SBCS**			Single Byte Character Set --> 1 Byte-Zeichensatz
**SBE**				Single Byte Extension
**SBI**				Sub-Bitstream Indicator
**SBM**				Subnet Bandwidth Manager (ATM, B-ISDN)
**SBR**				Statistical Bit Rate (ATM, B-ISDN)
**SBS**				Satellite Business System
**SC**				Sending Complete (Mobilfunknetze)
**SC**				Sequence Count --> Sequenznummer (ATM, B-ISDN)
**SC**				Service Center (Sat-Kommunikation)
**SC**				Session Control
**SC**				Standardising Committee (Gremien, Organisationen)
**SC**				Subscriber Connector --> Stecker für Glasfaser (Verkabelung)
**SC**				Synchronisation Channel
**SCAC**			Service Creation and Accounting Center (Mobilfunknetze)
**SCADA**			Supervisory Control and Data Acquisition
**SCAP**			Security Content Automation Protocol
**SCC**				Satellite Control Center (Sat-Kommunikation)
**SCCH**			Single Cell Control Channel
**SCCP**			Signalling Connection Control Part (ATM, B-ISDN, SS7)
**SCCS**			Source Code Control System
**SCD**				Source Code Disclosure
**SCDMA**			Synchronous Code Division Multiple Access (Mobilfunknetze)
**SCE**				Service Creation Environment
**SCEF**			Service Creation Environment Function
**SCELP**			Spike Code Excited Linear Prediction
**SCEP**			Simple Certificate Enrollment Protocol (Protokolle)
**SCF**				Service Control Function
**SCF**				Service Creation Function (Weitverkehrsnetze)
**SCH**				Synchronization Channel -> Synchronisationskanal (Mobilfunknetze)
**SCI**				Subsystem Control Indicator (FDDI)
**SCK**				Serial Clock
**SCK**				Static Cipher Key (IT-Sicherheit)
**SCLAN**			Super Computer Local Area Network (Offene NW-Konzepte)
**SCM**				Sub Carrier Multiplexing --> Multiplextechnik für analoge Kabelfernsehnetze
**SCM**				Switch Controller Module
**SCO**				Synchronous Connection Oriented --> Synchron verbindungsorientiert (Wireless LAN)
**SCP**				Secure Copy Protocol
**SCP**				Service Control Point --> Dienstesteuerungsknoten (Weitverkehrsnetze)
**SCP**				Session Control Protocol (DECnet) (Protokolle)
**SCP**				Signalling Control Point --> Signalisierungs-Steuerungsknoten (Mobilfunknetze)
**SCPC**			Single Channel Per Carrier (Sat-Kommunikation)
**SCPS**			Space Communication Protocol Specification (Sat-Kommunikation)
**SCR**				Standard Context Routing (Mobilfunknetze)
**SCR**				Sustainable Cell Rate --> Dauerzellrate (ATM)
**SCS**				Service Capability Server
**SCS**				Service Capability Set
**SCS**				Signalling Capability Set (Offene NW-Konzepte)
**SCS**				Structured Cabling System --> Strukturierte Verkabelung (Verkabelung)
**SCSA**			Signal Computing System Architecture (Tk-Sprachdienste)
**SCSI**			Small Computer System Interface --> Bussystem für PC (Schnittstellen, Busse)
**SCT**				Signed Certificate Timestamp
**SCT**				Smartcard Terminal --> Smartcard-Lesegerät (IT-Sicherheit)
**SCTP**			Stream Control Transmission Protocol (Protokolle)
**SCTPC**			Simple Computer Telephony Protocol Client (Telekommunikation)
**SCTPS**			Simple Telephony Protocol Server (Telekommunikation)
**SCTR**			System Conformance Test Report (Netzmanagement)
**SCUAF**			Service Control User Agent Function
**SD**				Service Data
**SD**				Signal Degraded
**SD**				Single Density
**SD**				Starting Delimiter --> Anfangsbyte eines MAC-Frames (Ethernet)
**SD-PDU**			Sequenced Data PDU (ATM, B-ISDN)
**SDAP**			Service Discovery Application Profile (Bluetooth)
**SDAP**			Session Description (and) Announcement Protocol (Protokolle)
**SDARS**			Satellite Digital Audio Radio Services
**SDB**				Switched Digital Broadcast
**SDBAS**			Switched Digital Broadband Access System (Stadt-, Regionalnetze)
**SDCCH**			Stand-alone Dedicated Control Channel --> Eigenständiger gewidmeter Steuerkanal (GSM)
**SDCP**			Serial Data Control Protocol (Protokolle)
**SDDC**			Software-Defined Data Center
**SDDI**			Shielded Distributed Data Interface (FDDI)
**SDE**				Secure Data Exchange
**SDE**				Submission and Delivery Entity (Mitteilungsdienste)
**SDF**				Service Data Function (Weitverkehrsnetze)
**SDFT**			Selective Discrete Fourier Transform
**SDH**				Synchrone Digitale Hierarchie --> Digitale Multiplex-Übertragungstechnik (Optische Netze, SDH, Sonet)
**SDI**				Serial Data Interface
**SDI**				Storage Device Interface (Netzbetriebsysteme)
**SDI**				Subscriber Distribution Interface --> Endverzweiger (Anschlussnetze)
**SDL**				Signalling Data Link
**SDL**				Simple Data Link
**SDL**				Specification and Description Language (CCITT)
**SDLC**			Synchronous Data Link Control --> Bitorientiertes, synchrones Schicht-2-Protokoll (IBM/SNA) (Protokolle)
**SDLP**			Standard Device Level Protocol
**SDM**				Short Data Message (Mobilfunknetze)
**SDM**				Space Division Multiplex --> Raummultiplex
**SDMA**			Space Division Multiple Access
**SDMS**			Switch Data Management System (Mobilfunknetze)
**SDMT**			Synchronized DMT --> Zeitgetrenntlageverfahren
**SDP**				Service Data Point
**SDP**				Service Discovery Protocol (Wireless LAN)
**SDP**				Session Description Protocol (Protokolle)
**SDRS**			Storage Distributed Resource Scheduling (VMware)
**SDSI**			Simple Distribution Security Infrastructure (IT-Sicherheit)
**SDSL**			Symmetric Digital Subscriber Line (xDSL) (Anschlussnetze)
**SDT**				Start Data Traffic --> Beginn des Datenverkehrs
**SDT**				Structured Data Transfer (ATM, B-ISDN)
**SDTV**			Standard Definition Television (Multimedia)
**SDU**				Satellite Data Unit
**SDU**				Service Data Unit --> Dienstdatenelement (ISO/OSI)
**SE**				Service Element --> Dienstelement
**SE**				Switching Element (ATM, B-ISDN)
**SE**				Systems Engineering
**SEAL**			Simple and Efficient Adaption Layer (ATM, B-ISDN)
**SEAS**			Signalling, Engineering and Administration System
**SEB**				Single End Billing
**SEC**				Security Aspects (ATM, B-ISDN)
**SEC**				Synchronisation Equipment Clock (Optische Netze, SDH, Sonet)
**SECAM**			Stème en Couleur à Mémoire (Multimedia)
**SECB**			Severely Errored Cell Blocks
**SEF**				Source Explicit Forwarding
**SEH**				Stackable Ethernet Hub
**SEL**				Selector (ATM, B-ISDN)
**SEM**				Spectrum Emission Mask
**SEP**				SCSI Encapsulated Protocol (Speichernetze)
**SEP**				Signalling End Point (ISDN) (SS#7)
**SEQ**				Sequence Number --> Sequenznummer
**SER**				Symbol Error Rate --> Symbolfehlerrate
**SES**				Satellite Earth Station --> Satelliten-Bodenstation (Sat-Kommunikation)
**SES**				Severely Errored Seconds (ATM, B-ISDN)
**SES**				Source End Station (ATM, B-ISDN)
**SESE**			Security-sensitive activity
**SET**				Secure Electronic Transaction (Protokolle, IT-Sicherheit)
**SETS**			Selected Executable Test Suite (Mobilfunkdienste)
**SF**				Service Feature
**SF**				Service Feature (Offene NW-Konzepte)
**SF**				Status Field --> Statusfeld (Datenfelder)
**SF**				Store and Forward --> Vermittlungsverfahren (DK-Übertragungstechniken)
**SF**				Superframe (ATM, B-ISDN)
**SFD**				Simple Formitable Document (Mitteilungsdienste)
**SFD**				Start Frame Delimiter --> Rahmen-Anfangsfeld (Datenfelder)
**SFDH**			Start Frame Delimiter High Priority (Datenfelder)
**SFDN**			Start Frame Delimiter Normal Priority (Datenfelder)
**SFET**			Synchronous Frequency Encoding Technology --> Frequenzsynchrones Codierverfahren
**SFF**				Small Form Factor --> Stecker (Verkabelung)
**SFH**				Slow Frequency Hopping
**SFMA**			Specific Functional Management Area
**SFN**				Single Frequency Network --> Gleichwellennetz
**SFP**				Small Form Factor Pluggable --> Stecker (Verkabelung)
**SFPS**			Secure Fast Packet Switching
**SFR**				Status Report Frame
**SFS**				Start Frame Sequence (Lokale Netze)
**SFT**				Simple File Transfer
**SFT**				System Fault Tolerance (Netzbetriebsysteme)
**SFTP**			Shielded Field Twisted Pair --> Mit Metallfolie geschirmte verdrillte Zweidrahtleitung (Verkabelung)
**SFTP**			Simple File Transport Protokoll (Protokolle)
**SFV**				Standardfestverbindung
**SG**				Signal Ground (Verkabelung)
**SG**				Study Group (Gremien, Organisationen)
**SGCP**			Simple Gateway Control Protocol --> Vorläufer von SNMP
**SGM**				Segmentation Message (Mobilfunknetze)
**SGML**			Standardized Generalized Markup Language
**SGMP**			Simple Gateway Management Protocol (Protokolle, Netzmanagement)
**SGSN**			Service Gateway Support Node (GPRS) (Mobilfunknetze)
**SGW**				Signalling Gateway
**SH**				Session Layer Header (ISO/OSI)
**SH**				Shift (Steuerzeichen)
**SHA**				Secure Hash Algorithm --> Kryptographische Hashfunktion (IT-Sicherheit)
**SHB**				Spectral Hole Burning (Verkabelung)
**SHDSL**			Single-Pair High Bit Rate Digital Subscriber Line --> Einpaarige digitale Teilnehmerleitung mit hoher Bitrate (Anschlussnetze)
**SHF**				Super High Frequency
**SHG**				Segmented Hypergrahics --> Dateiformat mit mehreren Hyperlinks
**SHM**				Short Hold Mode
**SI 0**			Sequenced Information 0 (ISDN)
**SI 1**			Sequenced Information 1 (ISDN)
**SI**				Secondary In --> Sekundärring-Eingang (FDDI)
**SI**				Serial Input --> Serieller Eingang
**SI**				Service Indicator --> Diensteanzeiger (ISDN)
**SI**				Shift In --> Rückschaltung (Steuerzeichen)
**SI**				Spring Integration (VMware)
**SI**				Système Internationale --> Internationales System physikalischer Einheiten (Normung)
**SIA**				Serial Interface Adapter
**SIB**				Service Independent Block
**SIB**				Status Indication Busy (Mobilfunknetze)
**SIDE**			System Independent Data Format (Netzbetriebsysteme)
**SIDL**			Synchron IDL (Ethernet)
**SIE**				Status Indication Emergency (Mobilfunknetze)
**SIEM**			Security Information and Event Management
**SIF**				SONET Interoperability Forum (Gremien, Organisationen)
**SIF**				Signalling Information Field (ISDN)
**SIF**				Station Information Frame (FDDI)
**SIF**				Step Index Fiber --> Stufenindex-Profilfaser (Verkabelung)
**SIFS**			Short Interframe Space (Wireless LAN)
**SIG**				Signalling --> Signalisierung
**SIG**				Signalling User Interface (ATM, B-ISDN)
**SIG**				Special Interest Group (Bluetooth) (Gremien, Organisationen)
**SIHP**			Service Interaction Handling Process
**SILS**			Standard for Interoperability LAN Security (Lokale Netze)
**SIM**				Subscriber Identity Module --> Teilnehmerkennungsmodul (GSM)
**SIN**				Status Indication Normal (ISDN)
**SINA**			Sichere Inter-Netz Architektur (IT-Sicherheit)
**SINAD**			Signal-to-Noise and Distortion
**SINP**			Session Identity Notification Protocol (Protokolle)
**SIO**				Serial Input/Output --> Serieller Ein- und Ausgang
**SIO**				Service Information Octet (ISDN, SS#7)
**SIOC**			Storage I/O Control (VMware)
**SIP**				SMDS Interface Protocol (Weitverkehrsnetze)
**SIP**				Session Initiation Protocol --> Protokol zum Verbindungsmanagement zwischen Server und Client (VoIP, RFC 2543)
**SIP**				Simple IP
**SIP-T**			Session Initiation Protocol for Telephones
**SIPO**			Serial In, Parallel Out --> Seriell/Parallelwandlung
**SIPP**			SMDS Interface Protocol (ATM, B-ISDN)
**SIPP**			Simple Internet Protocol Plus (Protokolle)
**SIPS**			Session Initiation Protocol Secure (Protokolle, IT-Sicherheit)
**SIR**				Serial Infrared
**SIR**				Signal-to-Interference Ratio
**SIR**				Silikonkautschuk (Verkabelung)
**SIR**				Sustained Information Rate (ATM)
**SIS**				Satellite Information Services
**SIS**				Structure Information Store (DK-Dienste)
**SIW**				Submission Interworking (Mobilfunknetze)
**SK**				Synchron Node --> Synchronknoten (Netzkomponenten)
**SKE**				Seekabelendstelle
**SKIP**			Simple Key Management Protocol (IT-Sicherheit)
**SKP**				Sekundärer Konzentrationspunkt
**SKid**			Script Kiddie
**SL**				Service Logic
**SL**				Signalling Link --> Zeichengabestrecke (ISDN)
**SLA**				Satellite Link Adapter --> Adapter für Satellitenverbindungen (Sat-Kommunikation)
**SLA**				Service Level Agreement
**SLA**				Service Level Agreement (QoS)
**SLA**				Site Level Aggregation
**SLARP**			Serial Line Address Resolution Protocol (Cisco)
**SLB**				Server Load Balancing
**SLC**				Signalling Link Code --> Zeichengabeabschnittcode
**SLEE**			Service Logic Execution Environment (Offene NW-Konzepte)
**SLI**				Service Logic Interpreter
**SLI**				Session Level Interface
**SLIC**			Subscriber Line Interface Card
**SLIC**			Subscriber Line Interface Circuit
**SLIP**			Serial Line Interface Protocol --> Protokoll zur Nutzung von TCP/IP über Punkt-zu-Punkt-Strecken, wie z.B. Modemverbindung (RFC 1055)
**SLM**				Service Level Management (Netzmanagement)
**SLM**				Signalling Link Management --> Abschnittsverwaltung (Netzmanagement)
**SLMA**			Subcriber Line Module Analogue --> Teilnehmerschaltung
**SLMD**			Subscriber Line Module Digital
**SLMS**			Single Line Multi Service (Anschlussnetze)
**SLP**				Service Location Protocol (Protokolle)
**SLP**				Service Logic Programme (Offene NW-Konzepte)
**SLP**				Single Link Procedure --> Verfahren für den Einfach-Übermittlungsabschnitt (X.25)
**SLS**				Secondary Link Station (Hersteller-NW-Architekturen)
**SLS**				Service Level Specification
**SLS**				Signalling Link Selection (ISDN, SS7)
**SLSAP**			Source Link Service Access Point
**SLT**				Slot Type
**SLT**				Subscriber Line Termination (Anschlussnetze)
**SLTC**			Signalling Link Test Control (Netzmanagement)
**SLTE**			Submarine Line Termination Equipment
**SLU**				Secondary Logical Unit (Hertsteller-NW-Architekturen)
**SLX**				Synchroner Leitungsmultiplexer (SDH)
**SM**				Security Management --> Sicherheitsmanagement (Netzmanagement)
**SM**				Service Manager (VMware)
**SM**				Service Module
**SM**				Session Management
**SM**				Short Message --> Kurzmitteilung (Mobilfunknetze)
**SM**				Single Mode
**SMA**				Stright Medium Adaptor --> Steckertyp (Verkabelung)
**SMAC**			Source MAC (Ethernet)
**SMAE**			System Management Application Entity (ISO/OSI)
**SMAP**			Service Management Access Point (Weitverkehrsnetze)
**SMAP**			Service Management Agent Point
**SMAP**			Station Management Application Program (FDDI)
**SMAP**			Systems Management Application Process (Netzmanagement)
**SMAS**			Service Management Application System (Netzmanagement)
**SMASE**			System Management Application Service Element (Netzmanagement)
**SMB**				Server Message Block --> Darstellungsschicht-Protokoll von Microsoft (Protokoll)
**SMC**				Spectral Management Class (Anschlussnetze)
**SMCP**			Service Management and Control Point
**SMDR**			Station Message Detailed Recording
**SMDS**			Switched Multi-Megabit Data Service --> Zellorientierter, verbindungsloser Hoch-geschwindigkeits-Breitband-Datendienst in USA
**SME**				Short Message Entity (Mobilfunknetze)
**SME**				Station Management Entity
**SME**				Storage Management Engine (Netzbetriebssysteme)
**SMF**				Service Management Function
**SMF**				Single Mode Fiber --> Monomodefaser bzw. Einmodenfaser (Verkabelung)
**SMF**				Standard Message Format (Mobilfunkdienste)
**SMF**				System Management Function (Netzmanagement)
**SMFA**			Specific Management Functional Area (Netzmanagement)
**SMG**				Special Mobile Group (Mobilfunknetze)
**SMH**				Short Message Handler (Mobilfunknetze)
**SMH**				Signalling Message Handling --> Meldungsabwicklung (Mobilfunkdienste)
**SMI**				Station Management Interface (Netzmanagement)
**SMI**				Structure and Identification of Management Information (RFC 1155)
**SMI**				System Management Information (Netzmanagement)
**SMIF**			Stream-Based Model Interchange Format
**SMIL**			Synchronized Multimedia Integration Language
**SMIP**			Specific Management Information Protocol --> Spezifisches Management-Informationsprotokoll (Netzmanagement)
**SMIS**			Specific Management Information Services --> Spezifische Management-Informationsdienste (Netzmanagement)
**SMIT**			System Management Information Tool (Netzmanagement)
**SMK**				Shared Management Knowledge
**SML**				Service Management Layer (TMN)
**SMM**				System Management Mode
**SMO**				Systems Management Overview (Netzmanagement)
**SMON**			Switched Monitoring (Lokale Netze)
**SMP**				Service Management Point (Weitverkehrsnetze)
**SMP**				Session Management Protect (Netzmanagement)
**SMP**				Simple Management Protocol --> Vorläufer von SNMP (Netzmanagement)
**SMP**				Symmetric Multiprocessing (Telekommunikations-Endgeräte)
**SMP**				Symmetrical Multi Processing
**SMP**				System Management Process
**SMPDU**			Service Management Protocol Data Unit
**SMPP**			Service Management and Provisioning Point
**SMR**				Specialized Mobile Radio --> Bündelfunkanwendungen in USA
**SMRSE**			Short Message Relay Service Element (GSM)
**SMS**				Service Management System
**SMS**				Service Management System (Microsoft)
**SMS**				Short Message Service --> Kurznachrichtendienst (Mobilfunknetze)
**SMS**				Storage Management Service (Netzbetriebsysteme)
**SMS**				Subscriber Management System (Stadt-, Regionalnetze)
**SMS-CB**			SMS Cell Broadcast (Mobilfunknetze)
**SMS-PP**			SMS Point-to-Point (Mobilfunknetze)
**SMSC**			Short Message Service Center (GSM)
**SMSTP**			Short Message Service Transfer Layer Protocol (Mobilfunknetze)
**SMT**				Satellite Mobile Terminal (Mobilfunknetze)
**SMT**				Short Message Terminal (Mobilfunknetze)
**SMT**				Station Management (FDDI)
**SMT**				Synchrone Multiplextechnik
**SMT**				Synchrones Multiplex Terminal (SDH)
**SMTP**			Simple Mail Transfer Protocol --> Anwendungsprotokoll im Internet (RFC 788, 821, 822)
**SMU**				System Management Unit
**SN T&M**			Signalling Network Testing & Maintenance
**SN**				Sequence Number --> Laufnummer (ATM, B-ISDN)
**SN**				Service Node (Weitverkehrsnetze)
**SN**				Subnetz
**SN**				Subscriber Number
**SN**				Switching Network
**SNA**				Systems Network Architecture (IBM)
**SNACP**			SNA Control Protocol (IBM/SNA) (Protokolle)
**SNADS**			SNA Distribution Services (IBM/SNA)
**SNAP**			Sub Network Access Protocol (ISO/OSI)
**SNAP**			Subnetwork Attachment Point
**SNAcF**			Subnetwork Access Function (ISO/OSI)
**SNAcP**			Subnetwork Access Protocol (ISO/OSI)
**SNC**				Subnetwork Connection (ATM, B-ISDN)
**SNCP**			Subnetwork Connection Protection (ATM, B-ISDN)
**SNCP**			System Node Control Point (Netzmanagement)
**SNDCF**			Subnetwork Dependent Convergence Function (ISO/OSI)
**SNDCP**			Subnetwork Dependent Convergence Protocol (ISO/OSI)
**SNG**				Satellite News Gathering
**SNI**				SMDS Network Interface (Weitverkehrsnetze)
**SNI**				SNA Network Interconnection (IBM/SNA)
**SNI**				Service Node Interface (Anschlussnetze)
**SNI**				Subscriber Network Interface --> Teilnehmerschnittstelle zum Kommunikationsnetz
**SNIA**			Storage Networking Industry Association (Gremien, Organisationen)
**SNICF**			Subnetwork Independent Convergence Function (ISO/OSI)
**SNICP**			Subnetwork Independent Convergence Protocol (ISO/OSI)
**SNM**				Signalling Network Management (ISDN)
**SNMP**			Simple Network Management Protocol
**SNMP**			Simple Network Management Protocol --> Netzmanagementprotokoll im Internet (RFC 1156 - 1158, 1212, 1213, 1901 – 1908, 2263, 2264, 2273, 2274)
**SNMPv2**			Simple Network Management Protocol Version 2 (Netzmanagement)
**SNMPv3**			Simple Network Management Protocol Version 3 (Netzmanagement)
**SNMR**			Set Normal Response Mode
**SNP**				Sequence Number PDU
**SNP**				Sequence Number Protection --> Laufnummerschutz (ATM, B-ISDN)
**SNP**				Subnetwork Protocol
**SNPA**			Subnetwork Point of Attachment (ISO/OSI)
**SNR**				Signal-to-Noise Ratio --> Signal-zu-Rauschleistungs-Verhältnis
**SNS**				Secondary Name Server
**SNS**				Secondary Network Server
**SNS**				Session Network Service
**SNTP**			Simple Network Time Protocol --> Protokol zur Synchronisation der Uhrzeiten im Internet (RFC 1769)
**SO**				Secondary Out --> Sekundärring-Ausgang (FDDI)
**SO**				Service Operator
**SO**				Shift Out --> Dauerumschaltung (Steuerzeichen)
**SOA**				Semiconductor Optical Amplifier --> Optischer Verstärker in Glasfasernetzen (Optische Netze, SDH, Sonet)
**SOAG**			Standards Promotion and Application Group (Gremien, Organisationen)
**SOAP**			Simple Object Access Protocol (Protokolle)
**SOB**				Start of Block --> Blockanfang (Steuerzeichen)
**SOC**				Security Operations Center
**SOCC**			Serial Optical Channel Converter (IBM)
**SOCC**			Service Operation Control Center (Sat-Kommunikation)
**SOH**				Section Overhead (Optische Netze, SDH, Sonet)
**SOH**				Start of Header (Datenfelder)
**SOHO**			Small Office, Home Office --> Kleine Büros oder Heimarbeitsplätze
**SOM**				Start of Message --> Mitteilungsanfang (Steuerzeichen)
**SOM**				Systems Object Model
**SONET**			Synchronous Optical Network --> Synchrone Multiplex-Übertragungstechnik (ANSI T1.105/106)
**SOVA**			Soft Output Viterbi Algorithm
**SOX**				Sarbanes-Oxley Act of 2002
**SP**				Segment Priority (Stadt-, Regionalnetze)
**SP**				Service Point
**SP**				Service Provider --> Dienstanbieter
**SP**				Signalling Point --> Zeichengabepunkt (ISDN)
**SP**				Source Port
**SP**				Space --> Zwischenraum (Steuerzeichen)
**SPA**				Self-Positioning Algorithm
**SPANS**			Simple Protocol for ATM Network Signalling (FORE) (ATM, Protokolle)
**SPC**				Signalling Point Code (Telekommunikation)
**SPC**				Stored Program Control --> 1. Generation von speicherprogrammierter Vermittlung
**SPC**				Super Physical Contact (Verkabelung)
**SPCN**			Satellite Personal Communication Network (Sat-Kommunikation)
**SPCS**			Single Per Carrier System (Mobilfunknetze)
**SPD**				Security Policy Database (IPSec)
**SPDU**			Session Protocol Data Unit (ISO/OSI)
**SPE**				Synchronous Payload Envelope (ATM, B-ISDN)
**SPF**				Sender Policy Framework
**SPF**				Short Pass (Optical) Filter
**SPF**				Shortest Path First (Internetworking)
**SPI**				Security Parameter Index
**SPI**				Service Provider Interface
**SPI**				Synchronous Parallel Interface
**SPIFFE**			Secure Production Identity Framework For Everyone
**SPKI**			Simple Public Key Infrastructure (IT-Sicherheit)
**SPL**				Self Provision Licence
**SPL**				Service Provider Link --> Dienstanbieter-Strecke
**SPM**				Self Phase Modulation (Verkabelung)
**SPM**				Session Protocol Machine
**SPM**				System Performance Monitor
**SPN**				Service Provider Node
**SPN**				Subscriber Premises Network
**SPOF**			Single Point of Failure
**SPOOL**			Simultaneous Peripheral Operations Online
**SPP**				Sequence Packet Protocol --> Schicht-4-Protokoll (XNS) (Protokolle)
**SPP**				Sequenced Packet Protocol (Protokolle)
**SPP**				Serial Port Profile (Bluetooth)
**SPR**				Single Protocol Router (Internetworking)
**SPS**				Signalling Protocols and Switching
**SPS**				Standard Positioning Service (Sat-Kommunikation)
**SPT**				Spanning Tree (Internetworking)
**SPTS**			Single Program Transport Stream (ATM, B-ISDN)
**SPV**				Semi-Permanente Verbindung (ISDN)
**SPVC**			Soft Permanent Virtual Circuit (ATM, B-ISDN)
**SPVC**			Soft Permanent Virtual Connection
**SPVCC**			Soft Permanent Virtual Channel Connection
**SPVP**			Soft Permanent Virtual Path (ATM, B-ISDN)
**SPVPC**			Soft Permanent Virtual Path Connection
**SPX**				Sequenced Packet Exchange --> Transportschicht-Protokoll (Novell) (Protokolle)
**SQA**				System Queue Area
**SQAM**			Staggered Quadrature Amplitude Modulation (Modulation)
**SQE**				Signal Quality Error (Ethernet)
**SQL**				Structured Query Language
**SQLF**			SQLFire
**SQLi**			SQL Injection
**SQORC**			Staggered Quadrature Overlapped Raised Cosine
**SQPSK**			Staggered Quadrature Phase Shift Keying --> Digitales Modulationsverfahren
**SR**				Source Routing --> Vorgabe der Wegewahl durch das sendende System (Internetworking)
**SR-IOV**			Single Root I/O Virtualization (VMware)
**SR-UAPDU**			Status Report User Agent Protocol Data Unit (Mobilfunkdienste)
**SRB**				Source Route Bridging (Internetworking)
**SRE**				Self Routing Switch Element (ATM, B-ISDN)
**SRES**			Signed Response (IT-Sicherheit)
**SRF**				Special Resource Function (Weitverkehrsnetze)
**SRF**				Specific Routed Frame (Internetworking)
**SRF**				Status Report Frame (FDDI)
**SRL**				Structure Return Loss
**SRM**				Site Recovery Manager (VMware)
**SRM**				Storage Resource Management (Speichernetze)
**SRN**				Source Recipient Node (Hersteller-NW-Architekturen)
**SRP**				Spatial Reuse Protocol (Cisco)
**SRP**				Specialized Resource Point
**SRR**				Short Range Radio --> Funkübertragung über kurze Distanzen (Mobilfunknetze)
**SRT**				Source Routing Transparent (Internetworking)
**SRTB**			Source Routing Translational Bridging (Internetworking)
**SRV**				Synchrones Raumvielfach
**SS**				Service Subscriber (Telekommunikation)
**SS**				Signalling System
**SS**				Spread Spectrum --> Bandspreizung
**SS**				Supplementary Service (Telekommunikation)
**SS**				Switching System --> Vermittlungssystem
**SS-CDMA**			Spread Spectrum - Code Division Multiple Access
**SS7**				Signalling System No. 7 --> Zeichengabesystem Nr. 7
**SSA**				Serial Storage Architecture (IBM) (Speichernetze)
**SSAIP**			Session Security, Authentication, Initialization Protocol (Datenbank)
**SSAP**			Session Service Access Point (ISO/OSI)
**SSAP**			Source Service Access Point (Ethernet)
**SSB**				Single Side Band --> Einseitenbandmodulation
**SSCF**			Service Specific Convergence (Coordination) Function (ATM)
**SSCH**			Secondary Synchronisation Channel
**SSCOP**			Service Specific Connection Oriented Protocol (ATM)
**SSCP**			Service Switching and Control Point
**SSCP**			System Service Control Point (IBM/SNA)
**SSCR**			Synchronisation Source Identifier
**SSCS**			Service Specific Convergence Sublayer (ATM, B-ISDN)
**SSD**				Service Support Data
**SSDP**			Simple Service Discovery Protocol (Protokolle)
**SSDU**			Session Service Data Unit (ISO/OSI)
**SSE**				Server Side Encryption
**SSF**				Service Subscriber Field (ISDN, SS#7)
**SSF**				Service Switching Function (Weitverkehrsnetze)
**SSF**				Supported Solution Forum (Gremien, Organisationen)
**SSG**				Service Selection Gateway
**SSH**				Secure Shell --> Protokoll zur kryptographisch geschützten Kommunikation von und zu Servern (IT-Sicherheit)
**SSHFP**			Secure Shell Fingerprint Record
**SSHS**			SSH (Secure Shell) specific Settings
**SSI**				Server Side Includes
**SSID**			Service Set Identity --> "Netzname" des Funk-LAN (WLAN)
**SSL**				Secure Socket Layer --> Protokoll zur kryptographisch geschützten Übertragung im Internet (Netscape) (IT-Sicherheit)
**SSL**				Secure Sockets Layer
**SSLCP**			Secure Socket Layer Control Protocol (IT-Sicherheit)
**SSM**				Single Segment Message --> Einzelsegment-Nachricht (ATM-B-ISDN)
**SSM-F**			Standard Singlemode-Fiber --> Einmoden-Faser (Verkabelung)
**SSMA**			Spread Spectrum Multiple Access
**SSO**				Single Service Operator (USA)
**SSO**				Single Sign On (IT-Sicherheit)
**SSO**				Single Sign-On
**SSP**				Service Switching Point
**SSP**				Service Switching Point --> Dienstvermittlungsknoten (Weitverkehrsnetze)
**SSP**				Storage Service Provider
**SSP**				Switch-to-Switch Protocol (Protokolle)
**SSRC**			Synchronisation Source Identifier
**SSRP**			Simple Server Redundancy Protocol (Protokolle)
**SSS**				Switching Subsystem (GSM) (Mobilfunknetze)
**SSS**				Synchronized Sample Scrambler (ATM, B-ISDN)
**SST**				Spread Spectrum Technology --> Bandspreiztechnik
**SSTP**			Service Support Transport Protocol (Protokolle)
**SSU**				Synchronisation Unit
**SSU**				Synchronization Supply Equipment (Optische Netze, SDH, Sonet)
**SSV**				Schnittstellenvervielfacher (Weitverkehrsnetze)
**ST**				Segment Type --> Segmenttyp (ATM, B-ISDN)
**ST**				Straight Tip --> Steckertyp für Glasfaser (Verkabelung)
**ST**				Streaming (Protocol) (MIT)
**ST2**				Stream (Protocol) Version 2 (RFC 1819)
**STA**				Spanning Tree Algorithm (Ethernet, IEEE 802.1d)
**STB**				Set Top Box
**STC**				Sub-Technical Committee (Gremien, Organisationen)
**STC**				System Time Clock (ATM)
**STD**				Internet Standard
**STD**				Standard
**STDM**			Synchronous Time Division Multiplexing
**STIF**			Swiss Federal Institute of Technology (Gremien, Organisationen)
**STM-1**			Synchronous Transport Module Level 1
**STP**				Shielded Twisted Pair --> Geschirmte verdrillte Leitung (Verkabelung)
**STP**				Spanning Tree Protocol (Ethernet, Protokolle)
**STS**				Security Token Service
**STS**				SpringSource Tool Suite (VMware)
**STS**				Synchronous Transport Signal
**STX**				Start of Text
**SUID**			Super User Identification
**SVC**				Switched Virtual Channel --> Geschaltete virtuelle Verbindung (X.25)
**SVC**				Switched Virtual Connection
**SVCC**			Switched Virtual Channel Connection
**SWAN**			Structured Wireless Aware Network (Cisco)
**SYN**				Synchronization
**SaaS**			Software-as-a-Service
**SatFu**			Satellitenfunk (Sat-Kommunikation)
**SatFv**			Satellitenfestverbindung (Sat-Kommunikation)
**SatKom**			Satellitenkommunikation
**Satcom**			Satellite Communications
**SigG**			Signaturgesetz (Standards, Verordnungen, Gesetze) (IT-Sicherheit)
**SigV**			Signaturverordnung (Standards, Verordnungen, Gesetze) (IT-Sicherheit)
**So**				Physikalische Endgeräteschnittstelle des ISDN-Basisanschlusses (ISDN)
**SoD**				Service-on-Demand
**SoIP**			Storage Over IP
**Storage DRS Cluster**		A collection SDRS objects (volumes, VMs, configuration). (VMware)
**Storage vMotion**		A VM storage migration technique from one datastore to another. (VMware)
**T**				Tracebility
**T-DAB**			Terrestrial Digital Audio Broadcasting
**T-UMTS**			Terrestrial UMTS
**T1**				Transmission Signal Level 1 --> Digitales Übertragungsverfahren mit 1,536 Mbit/s (USA)
**T3**				Transmission Signal Level 3 --> Digitales Übertragungsverfahren mit 44,768 Mbit/s (USA)
**TA a/b**			Terminal Adapter für Endeinrichtungen mit a/b-Schnittstelle (ISDN)
**TA**				Terminal Adapter --> Endgeräteanpassung an ISDN-Bus (ISDN)
**TA**				Time Alignment (GSM)
**TAC**				Terminal Access Controller
**TACACS**			Terminal Access Controller Access Control System
**TACH**			Traffic Channel
**TACS**			Total Access Communications System --> Zellulares, analoges Mobilfunknetz in Europa
**TAE**				Telekommunikationsanschalteeinrichtung
**TALI**			Transport Adapter Layer Interface
**TAM**				Technical Account Manager
**TAN**				Transaktionsnummer
**TAP**				Terminal Access Point
**TAPI**			Telephone Application Programming Interface
**TARM**			Telephone Answering Recording Machine --> Anrufbeantworter
**TASI**			Time Assignment Speech Interpolation
**TAT**				Theoretical Arrival Time
**TAT**				Trans Atlantic Trunk --> Unterwasserkabel im Nordatlantik (Verkabelung)
**TAXI**			Transparent Asynchronous Receive/Transmit Interface
**TB**				Tail Bit
**TB**				Token Bus
**TB**				Transparent Bridge
**TBC**				Token Bus Controller
**TBE**				Transient Buffer Exposure
**TBEB**			Truncated Binary Exponential Backoff (Ethernet, IEEE 802.3)
**TBF**				Temporary Block Flood (GPRS)
**TBR**				Technical Basis for Regulation (Standards, Verordnungen, Gesetze)
**TBS**				TETRA Base Station (Digitaler Bündelfunk)
**TC**				Technical Committee (Gremien, Organisationen)
**TC**				Telecommunications
**TC**				Transmission Clock
**TC**				Trellis Coding (Codierung)
**TCA**				Threshold Crossing Alarm
**TCAP**			Transaction Capabilities Application Part (ISDN, SS7)
**TCH**				Traffic Channel --> Nutzkanal (GSM)
**TCL**				Tool Command Language
**TCM**				Time Compression Multiplexing
**TCM**				Trellis Coded Modulation --> Kombiniertes Modulations- und Kanalcodierungsverfahren
**TCNS**			Thomas Conrad Network System --> 100 Mbit/s-LAN der Thomas Conrad Corporation
**TCP**				Transmission Control Protocol
**TCP**				Transmission Control Protocol --> Verbindungsorientiertes Transportprotokoll im Internet (RFC 761, 768, 793, 1323)
**TCP/IP**			Transmission Control Protocol/Internet Protocol
**TCR**				Tagged Cell Rate
**TCR**				Test Command Response (IEEE 802.2)
**TCSEC**			Trusted Computer System Evaluation Criteria (IT-Sicherheit)
**TCU**				Trunk Coupling Unit (IEEE 802)
**TD-CDMA**			Time Division Code Division Multiple Access
**TD-SCDMA**			Time Division Synchronous Code Division Multiple Access
**TDD**				Time Division Duplex --> Zeitduplex
**TDDGS**			Teledienstedatenschutzgesetz (Standards, Verordnungen, Gesetze)
**TDG**				Teledienstegesetz (Standards, Verordnungen, Gesetze)
**TDHS**			Time Domain Harmonic Scaling
**TDM**				Time Division Multiplexing --> Zeitmultiplex, Zeitgetrenntlage-Verfahren
**TDMA**			Time Division Multiple Access
**TDP**				Tag Distribution Protocol (Protokolle)
**TDP**				Telocator Data Protocol
**TDP**				Trigger Detection Points
**TDR**				Time Domain Reflectometer
**TDS**				Telegrammdienstsystem
**TDS**				Transparent Data Services
**TDSV**			Telekommunikationsdienstunternehmen-Datenschutzverordnung (Standards, Verordnungen, Gesetze)
**TDT**				Time Domain Transmission
**TE**				Terminal Equipment --> Teilnehmer-Endgerät (ISDN)
**TE**				Transit Exchange
**TEI**				Terminal Equipment Identifier (ISDN/LAP-D)
**TELNET**			Telnet Protocol --> Protokoll zur Terminal-Emulation (RFC 318, 764, 854)
**TEMEX**			Telemetry Exchange
**TEN**				Trans-European Network --> Infrastruktur-Programm der EU
**TETRA**			Terrestrial Trunked Radio --> Digitaler Bündelfunk (ETSI)
**TEntgV**			Telekommunikations-Entgeltregulierungsverordnung (Standards, Verordnungen, Gesetze)
**TF**				Trägerfrequenz
**TFCI**			Transport Block Combination Indicator
**TFE**				Türfreisprecheinrichtung
**TFH**				Trägerfrequenztechnik auf Hochspannungsleitungen
**TFM**				Tamed Frequency Modulation --> Digitales Modulationsverfahren
**TFM**				Telefon-Fax-Modem-Umschalter
**TFS**				Tunnelfunksystem (Deutsche Bahn)
**TFTP**			Trivial File Transfer Protocol --> Anwendungsprotokoll im Internet (RFC 1350)
**TFTS**			Terrestrial Flight Telecommunication System --> Aeronautisches Passagiertelefonsystem
**TFU**				Telefon-Fax-Umschalter
**TG**				Teilnehmergerät
**TGC**				Transmission Group Control (IBM/SNA)
**TGN**				Telekom Global Network
**THDR**			Transport Layer Header (Datenfelder)
**TIA**				Telecommunications Industry Association (Gremien, Organisationen)
**TIC**				Token Ring Interface Card (IBM)
**TIME**			Time Server Protocol (RFC 868)
**TINA**			Telecommunications Integrated Network Architecture
**TIPHON**			Telecommunication and Internet Protocol Harmonisation Over Networks
**TK**				Telekommunikation
**TKAnl**			Telekommunikationsanlage
**TKG**				Telekommunikationsgesetz
**TKG**				Telekommunikationsgesetz (Standards, Verordnungen, Gesetze)
**TKIP**			Temporal Key Integrity Protocol (IT-Sicherheit, Protokolle)
**TKO**				Telekommunikationsordnung (Standards, Verordnungen, Gesetze)
**TKP**				Tertiärer Konzentrationspunkt (Verkabelung)
**TLA**				Top Level Aggregation
**TLD**				Top Level Domain
**TLI**				Transport Layer Interface (UNIX)
**TLP**				Transmission Level Point
**TLR**				Terminode Local Routing
**TLS**				Transport Layer Security
**TLS**				Transport Layer Security (IT-Sicherheit)
**TLSS**			TLS specific Settings
**TM**				Terminal Multiplexer
**TM**				Traffic Management
**TMG**				Telemediengesetz
**TMN**				Telecommunication Management Network --> Netzmanagement-Architektur der ITU-T für Weitverkehrsnetze
**TMS**				Token Management System
**TMSI**			Temporary Mobile Subscriber Identity --> Zeitlich begrenzte Teilnehmeridentität (GSM)
**TNC**				Terminal Node Controller
**TNIC**			Telex Network Identification Code (ITU-T F.69)
**TO**				Telecommunications Operator
**TOFU**			Trust On First Use
**TOP**				Technical Office Protocols
**TP**				Transmission Priority
**TP**				Transport Protocol --> OSI-Transportprotokoll (ISO/IEC)
**TP**				Twisted Pair --> Verdrillte Kupferleitung (Verkabelung)
**TP-PMD**			Twisted Pair-Physical Medium Dependent (FDDI)
**TP4**				OSI Transport Protocol Class 4
**TPAD**			Transaction Packet Assembler Disassembler
**TPC**				Trans-Pacific Cable (Verkabelung)
**TPC**				Transmission Power Control (UMTS)
**TPDDI**			Twisted Pair Distributed Data Interface
**TPDU**			Transport Protocol Data Unit (ISO/OSI)
**TPS**				Transmission Parameter Signalling
**TPUI**			Temporary User Identity
**TR**				Technische Richtlinie (Standards, Verordnungen, Gesetze)
**TR**				Token Ring (IEEE 802.5)
**TRAU**			Transcoding Rate and Adaptation Unit (GSM)
**TRG**				Trägerrückgewinnung
**TRIP**			Telophony Routing over IP --> Routingprotokoll im Internet (Protokolle)
**TRPB**			Truncated Reverse Path Broadcast
**TRR**				Terminode Remote Routing
**TRT**				Token Rotation Time (FDDI)
**TRT**				Tonfrequenzrundsteuertechnik
**TRU**				Transceiver Unit --> Sendeeinheit
**TRX**				Transceiver --> Sender
**TS**				Time Slot (GSM)
**TS**				Transfer Syntax (ISO/OSI)
**TS**				Transportstrom
**TSAP**			Transport Service Access Point (ISO/OSI)
**TSAPI**			Telephony Service Application Programming Interface
**TSB**				Technical Standards Bulletin
**TSB**				Telecommunication Standardization Bureau (ITU) (Gremien, Organisationen)
**TSC**				Trunked Site/System Controller (TETRA)
**TSDU**			Transport Service Data Unit (ISO/OSI)
**TSI QPSK**			Two Symbol Interval Quadrature Phase Shift Keying --> Digitales Modulations­verfahren
**TSI**				Telekom Systems International (Gremien, Organisationen)
**TSL**				Telephone Service Licence
**TSMA/CD**			Transition Sense Multiple Access Collision Detection
**TSPI**			TSAPI Service Provider Interface
**TT&C**			Telemetry, Tracking and Control
**TTC**				Telemetry and Telecommand
**TTCN**			Tree and Tabular Combined Notation (ISO/IEC 9646 Teil 3)
**TTI**				Time Transmission Interval
**TTL**				Time To Live
**TTL**				Time To Live (Datenfelder)
**TTS**				Trouble Ticket System
**TTW**				Tontastenwahl
**TTX**				Teletex
**TTY**				Teletype
**TU**				Traffic Unit --> Verkehrseinheit
**TU**				Transmission Unit
**TUP**				Telephone User Part (GSM)
**TV**				Television --> Fernsehen
**TVSt**			Teilnehmervermittlungsstelle
**TWTA**			Travelling Wave Tube Amplifier --> Wanderfeldröhrenverstärker
**TXT**				text
**Terminode**			Terminal and Node
**Tln**				Teilnehmer
**ToA**				Time of Arrival
**ToM**				Tunneling of Messages (Mobilfunknetze)
**ToS**				Type of Service
**Tx**				Transmitter
**UA**				User Agent (ITU-T X.400)
**UAA**				Universally Administrated Address
**UAC**				User Agent Client (ITU-T X.400)
**UADSL**			Universal Asymmetric Digital Subscriber Line (xDSL)
**UAE**				Universelle Anschlusseinrichtung
**UAE**				User Agent Entity
**UAK**				User Authentication Key (IT-Sicherheit)
**UAL**				User Access Line (ITU-T X.25)
**UAL**				User Agent Layer
**UAN**				Universal Access Number
**UAPDU**			User Agent Protocol Data Unit
**UART**			Universal Asynchronous Receiver and Transmitter
**UAS**				Unavailable Second
**UAS**				User Agent Server (ITU-T X.400)
**UAT**				Unavailable Time
**UAWG**			Universal ADSL Working Group (xDSL) (Gremien, Organisationen)
**UBIDEP**			Universal Building Information and Data Exchange Protocol (Protokolle)
**UBL**				Unblocked
**UBR**				Unspecified Bit Rate --> Nicht festgelegte Bitrate (ATM, B-ISDN)
**UC**				Upstream Channel
**UC**				User Class
**UCC**				User Communication Channel
**UCS**				Unified Communication Service
**UCS**				Universal Cabling System (Verkabelung)
**UCS**				Universal Card Slot
**UDB**				User Data Block
**UDI**				Universal Digital Interface
**UDLR**			Unidirectional Link Routing
**UDP**				User Datagram Protocol
**UDP**				User Datagram Protocol --> Verbindungsloses Transportprotokoll im Internet (RFC 768, 793)
**UDSL**			Unidirectional Digital Subscriber Line (xDSL)
**UDT**				Unchannelized Data Transfer
**UE**				User Element
**UE**				User Equipment --> Teilnehmer-Endgerät (UMTS)
**UFS**				Unix File System
**UG**				Universal Gateway
**UGV**				Universelle Gebäudeverkabelung (Verkabelung)
**UHF**				Ultra High Frequency
**UHTTP**			Unidirectional Hypertext Transfer Protocol --> Anwendungsprotololl im Internet
**UI**				Unnumbered Information
**UI**				User Interaction
**UI**				User Interface
**UIC**				User Identification Code
**UID**				Unique Identifier
**UIFN**			Universal International Freephone Number (Service)
**UIM**				User Identity Module
**UIM**				User Information Message
**UKO-Schnittstelle**		Leitungsschnittstelle des ISDN-Basisanschlusses für Kupferleiter; deutscher Schnittstellenstandard auf der Teilnehmeranschlußleitung
**UKW**				Ultrakurzwelle
**UL**				Uplink
**ULA**				Upper Layer Architecture
**ULH**				Ultra Long Haul
**ULL**				Unbundled Local Loop
**ULP**				Upper Layer Protocol
**UM**				Unified Messaging
**UMD**				Uniform Mode Distribution
**UME**				UNI Management Entity
**UMI**				User MAN Interface
**UML**				Unified Message Language
**UMS**				Unified Messaging Service
**UMTS**			Universal Mobile Telecommunications System
**UNA**				Upstream Neighbour´s Address (Token-Verfahren)
**UNDRR**			UN Office for Disaster Risk Reduction (formerly known as United Nations International Strategy for Disaster Reduction)
**UNI**				User Network Interface --> Schnittstelle zw. Netzknoten und Teilnehmer (ATM)
**UNISDR**			United Nations International Strategy for Disaster Reduction
**UNIX**			Multitasking- und Multiuser-Betriebssystem
**UNMA**			Unified Network Management Architecture (AT&T)
**UNT**				Update Notification Table
**UP**				User Part
**UPAM**			User Primary Access Method
**UPC**				Usage Parameter Control (ATM)
**UPI**				User Protocol Interpreter
**UPM**				User Profile Management
**UPN**				Universal Personal Number
**UPSR**			Unidirectional Path Switching Ring
**UPT**				Universal Personal Telecommunications
**UR2**				Schnittstelle bei T-DSL
**URG**				Urgent (Flag, Datenfeld)
**URI**				Uniform Resource Identifier (WWW)
**URL**				Uniform Resource Locator (WWW)
**URN**				Uniform Resource Name (WWW)
**URT**				User Response Time
**USART**			Universal Synchronous/Asynchronous Receiver/Transmitter
**USAT**			Ultra Small Aperture Terminal (Sat-Kommunikation)
**USB**				Universal Serial Bus --> Hochratige, serielle Schnittstelle
**USBS**			User Signalling Bearer Service
**USDC**			US Digital Cellular
**USI**				User Specific Interface
**USIM**			Universal Subscriber Indentity Module
**USO**				Universal service Obligation
**USOC**			Universal Service Ordering Code
**USSD**			Unstructured Supplementary Service Data
**USV**				Unterbrechungsfreie Stromversorgung
**UT**				Universal Time
**UT**				Upper Tester
**UTC**				Universal Time Coordinated --> Weltzeit
**UTC**				Universeller Teletex Controller
**UTF**				Universal Transformation Format
**UTM**				Universal Transaction Monitor
**UTOPIA**			Universal Test and Operation PHY-interface for ATM
**UTP**				Unshielded Twisted Pair --> Verdrillte Kupferdoppelader ohne Abschirmung (Verkabelung)
**UTRA**			UMTS Terrestrial Radio Access (UMTS)
**UTRAN**			UMTS Terrestrial Radio Access Network (UMTS)
**UUCICO**			Unix-to-Unix Copy-in-Copy-out
**UUCP**			UNIX-to-UNIX Communication Protocol --> Asynchrones Standardprotokoll in UNIX-Umgebung
**UUI**				Uder-to-User Information
**UUNET**			Unix-to-Unix Network --> Weltweites, IP-basiertes Kommunikationsnetz für geschäftliche Kunden
**UUS**				User-to-User Signalling
**UUT**				Unit Under Test
**UV**				Ultraviolet
**UWC**				Universal Wireless Consortium (GSM)
**UWCC**			Universal Wireless Communications Consortium (Gremien, Organisationen)
**UWDM**			Ultra-dense Wavelength Division Multiplex --> Wellenlängenmultiplex-Verfahren mit sehr engen Wellenlängenabständen
**Upn**				Anlagen-Systemschnittstelle (Firmenbezeichnung)
**V2M**				Schnittstelle zwischen Primärmultiplexanschluß und Teilnehmervermittlungsstelle (ISDN)
**V2V**				Virtual to Virtual
**VA**				Vulnerability Assessment
**VAAI**			vStorage API for Array Integration (VMware)
**VACC**			Value Added Common Carrier
**VAD**				Voice Activity Detection
**VADM**			vCenter Application Discovery Manager (VMware)
**VADP**			vSphere APIs for Data Protection, a way to leverage the infrastructure for backups. (VMware)
**VADS**			Value Added Data Services
**VADSL**			Very High Bit Rate Asymmetrical Digital Subscriber Line (xDSL)
**VAN**				Value Added Network --> Netz mit zusätzlichen Mehrwertdiensten
**VANS**			Value Added Network Services
**VAPC**			Vector Adaptive Predictive Coding
**VARP**			Vines Address Resolution Protocol (Banyan) (Protokolle)
**VAS**				Value Added Service --> Mehrwertdienst
**VASP**			Vallue Added Service Provider
**VATM**			Verband der Anbieter von Telekommunikations u. Mehrwertdiensten e.V. (Gremien, Organisationen)
**VBN**				Vermittelndes Breitbandnetz (ab 1989), zuvor: Vorläufer-Breitbandnetz
**VBN**				Virtual Backbone Network
**VBR**				Variable Bit Rate --> Nicht-konstante Bitrate (ATM)
**VBW**				Video Bandwidth
**VC**				Vector Quantization Coding (Codierung)
**VC**				Virtual Center (VMware)
**VC**				Virtual Channel --> Virtueller Kanal (ATM)
**VC**				Virtual Circuit --> Virtuelle Verbindung
**VC**				Virtual Connection
**VC**				Virtual Container
**VC-n**			Virtueller Container-Class n (SDH)
**VCA4-DT**			VMware Certified Associate 4 - Desktop (VMware)
**VCAC**			vCloud Automation Center (VMware)
**VCAP**			VMware Certified Advanced Professional (VMware)
**VCAP-CID**			VMware Certified Advanced Professional – Cloud Infrastructure Design (VMware)
**VCAP-DTD**			VMware Certified Advanced Professional - Desktop Design (VMware)
**VCAP4-DCA**			VMware Certified Advanced Professional 4 - Datacenter Administration (VMware)
**VCAP4-DCD**			VMware Certified Advanced Professional 4 - Datacenter Design (VMware)
**VCAP5-DCA**			VMware Certified Advanced Professional 5 - Datacenter Administration (VMware)
**VCAP5-DCD**			VMware Certified Advanced Professional 5 - Datacenter Design (VMware)
**VCAP6-DCA**			VMware Certified Advanced Professional 6 - Datacenter Administration (VMware)
**VCAP6-DCD**			VMware Certified Advanced Professional 6 - Datacenter Design (VMware)
**VCAPI**			Virtual CAPI
**VCAT**			vCloud Architecture Toolkit (VMware)
**VCC**				Virtual Channel Connection (ATM)
**VCCE**			Virtual Channel Connection Endpoint (ATM)
**VCD**				vCloud Director (VMware)
**VCDX**			VMware Certified Design Expert (VMware)
**VCDX-DT**			VMware Certified Design Expert – Desktop (VMware)
**VCDX4-DV**			VMware Certified Design Expert 4 - Datacenter Virtualiziation (VMware)
**VCDX5-DV**			VMware Certified Design Expert 5 - Datacenter Virtualiziation (VMware)
**VCELP**			Vector Code Excited Linear Prediction
**VCI**				Virtual Channel Identifier (ATM)
**VCIM**			vCloud Integration Manager (VMware)
**VCL**				Virtual Channel Link
**VCLI**			vSphere Command Line Interface (VMware)
**VCM**				vCenter Configuration Manager (VMware)
**VCMux**			Virtual Channel Multiplexer --> Logischer Verbindungsabschnitt mit Multiplexfunktion
**VCN**				Virtual Circuit Number
**VCO**				Voltage Controlled Oscillator
**VCO**				vCenter Orchestrator (VMware)
**VCOPS**			vCenter Operations (VMware)
**VCP**				Videotext Control Program
**VCP4-DT**			VMware Certified Professional 4 - Desktop (VMware)
**VCP4-DV**			VMware Certified Professional 4 - Datacenter Virtualization (VMware)
**VCP5-DT**			VMware Certified Professional 5 - Desktop (VMware)
**VCP5-DV**			VMware Certified Professional 5 - Datacenter Virtualization (VMware)
**VCS**				Virtual Circuit Switch
**VCS**				Voice Communication System
**VCS**	                    	Vattenfall Container Service
**VCSEL**			Vertical Cavity Surface Emitting Laser
**VCSN**			vCloud Security and Networking (VMware)
**VD**				Virtual Destination
**VDC**				Virtual Data Center (VMware)
**VDD**				Virtual Device Driver
**VDE**				Verband Deutscher Elektrotechnik, Elektronik und Informationstechnik e.V. (Gremien, Organisationen)
**VDI**				Verein Deutscher Ingenieure (Gremien, Organisationen)
**VDIA**			Verband Deutscher Internet Anbieter (Gremien, Organisationen)
**VDL**				VHF Data Link (ICAO)
**VDP**				Video Datagram Protocol
**VDP**				vSphere Data Protection (VMware)
**VDR**				VMware Data Recovery (VMware)
**VDS**				vNetwork Distributed Switch (VMware)
**VDSL**			Very High Bitrate Digital Subscriber Line (xDSL)
**VDTA**			Verband Deutscher Telekommunikatonsanwender (Gremien, Organisationen)
**VDo**				Verbindungsdose (Deutsche Telekom)
**VE**				Vermitttlungseinheit
**VEN**				Vermittlungseinrichtung mit Netzübergabepunkt
**VEPC**			Voice Excited Predictive Coding
**VF**				Vattenfall
**VF**				Virtual Filestore
**VF**				Voice Frequency
**VFIR**			Very Fast Infrared
**VFM**				Variable Frequency Modem
**VFN**				Vendor Feature Node
**VFS**				Virtual File Storage
**VFW**				Video for Windows
**VG**				Voice Grade
**VGA**				Video Graphics Adapter
**VGCS**			Voice Group Call Service
**VHDL**			Very High Speed Hardware Description Language
**VHDSL**			Very High Speed Digital Subscriber Line (Anschlussnetze)
**VHDWDM**			Very High Density Wavelength Division Multiplexing
**VHE**				Virtual Home Environment
**VHF**				Very High Frequency
**VI**				Virtual Interface
**VIA**				Vendors ISDN Association (Gremien, Organisationen)
**VIC**				Video Conferencing
**VICS**			Voluntary Inter-Industry Communication Standards
**VIM**				Virtual Infrastructure Management (VMware)
**VIN**				vCenter Infrastructure Navigator (VMware)
**VINES**			Virtual Network System
**VIP**				Vattenfall Integration Platform
**VISYON**			Variables Intelligentes Synchrones Optisches Netz
**VIX**				Virtual Infrastructure eXtension (VMware)
**VL**				Verbindungsleitung
**VLAN**			Virtual Local Area Network
**VLC**				Variable Length Coding --> Quellencodierungsverfahren
**VLF**				Very Low Frequency (3 kHz – 30 kHz)
**VLM**				Virtual Loadable Module
**VLR**				Visitor Location Register --> Besucherregister (GSM)
**VLS**				Vehicle Location System
**VLSI**			Very Large Scale Integration
**VLSM**			Variable Length Subnet Mask
**VLT**				VDSL Line Termination
**VM**				Snapshot: A point-in-time representation of a VM. (VMware)
**VM**				Virtual Machine
**VM**				Virtual Memory
**VMA**				vSphere Management Assistant (VMware)
**VME**				Versa Module Europe
**VMFS**			Virtual Machine File System (VMware)
**VMRC**			VMware Remote Console (VMware)
**VMS**				Voice Mail System
**VMS**				vFabric Management Service (VMware)
**VMSA**			VMware Security Advisory (VMware)
**VMTN**			VMware Technology Network (VMware)
**VMTP**			Versatile Message Transaction Protocol (Protokolle)
**VMW**				VMware (VMware)
**VMX**				Virtual Machine eXecutable (VMware)
**VNCA**			VTAM Node Control Application
**VNK**				Vermittelnder Netzknoten (OSPF)
**VNM**				Virtual Network Machine
**VNN**				Virtual Network Navigator
**VNT**				VDSL Network Termination
**VOFDM**			Vector Orthogonal Frequency Division Mulitplexing
**VON**				Voice on (the) Net
**VP**				Virtual Path (ATM)
**VPA**				Vertical Pods Autoscaler
**VPC**				Virtual Path Connection (ATM)
**VPC**	                    	Vattenfall Private Cloud – the :abbr:`VF (Vattenfall)` Infrastructure Platform for providing IaaS.
**VPCE**			Virtual Path Connection Endpoint (ATM)
**VPDN**			Virtual Private Data Network
**VPDN**			Virtual Private Dial-In Network (VPN)
**VPI**				Virtual Path Identifier (ATM)
**VPIM**			Voice Profile for Internet Mail --> Standard für Email-Austausch zwischen unterschiedlichen Mail-Servern (RFC 1911)
**VPL**				Virtual Path Link
**VPN**				Virtual Private Network 
**VPN**				Virtual Private Network (IT-Sicherheit)
**VPOP**			Virtual Point of Presence --> Virtueller Einwahlknoten
**VPT**				Virtual Path Termination
**VPU**				Voice Processing Unit
**VPX**				Virtual Provisioning X (VMware)
**VPXA**			Virtual Provisioning X Agent (VMware)
**VPXD**			Virtual Provisioning X Daemon (VMware)
**VR**				Virtual Route
**VR**				vSphere Replication (VMware)
**VRC**				Vertical Redundancy Check --> Kanalcodierungsverfahren
**VRF**				VPN Routing and Forwarding
**VRID**			Virtual Router Identification
**VRM**				vCloud Request Manager (VMware)
**VRML**			Virtual Reality Modelling Language
**VRPRS**			Virtual Route Pacing Response (IBM/SNA)
**VRRP**			Virtual Router Redundancy Protocol 
**VRRP**			Virtual Router Redundancy Protocol (Protokolle)
**VS**				Virtual Scheduling
**VS**				Virtual Source
**VSA**				vSphere Storage Appliance (VMware)
**VSAM**			Virtual Storage Access Method
**VSAN**			Virtual SAN, a new VMware announcement for making DAS deliver SAN features in a virtualized manner. (VMware)
**VSAT**			Very Small Aperture Terminal
**VSB**				Vestigal Sideband
**VSELP**			Vector Sum Excited Linear Prediction
**VSM**				VMware Service Manager (VMware)
**VSMSC**			Virtual Short Message Service Center (Mobilfunknetze)
**VSN**				Virtual Switched Network
**VSP**				VMware Sales Professional (VMware)
**VSt**				Vermittlungsstelle
**VT**				Vermittlungstechnik
**VT**				Virtual Terminal --> OSI-Anwendungsprotokoll (ISO/IEC)
**VTAM**			Virtual Terminal Access Method --> Netzbetriebssystem (IBM/SNA)
**VTC**				Virtual Terminal Control
**VTP**				Virtual Terminal Protocol (ISO/OSI)
**VTS**				Virtual Terminal Service (ISO/OSI)
**VTSP**			VMware Technical Sales Professional (VMware)
**VTSU**			Virtual Terminal Support Unit
**VTX**				Videotext
**VUM**				vCenter Update Manager (VMware)
**VXC**				Vector Excitation Coding
**VXLAN**			Virtual Extensible Local Area Network
**Virtual Appliance**		A pre-packed VM with an application on it.
**Virtual NUMA**		Virtualizes NUMA with VMware hardware version 8 VMs. (VMware)
**VoATM**			Voice over ATM
**VoD**				Video on Demand --> Videoabfragedienst
**VoDSL**			Voice over Digital Subscriber Line
**VoFR**			Voice over Frame Relay
**VoIP**			Voice over Internet Protocol
**Vzk**				Verzweigerkabel (Verkabelung)
**W-APN**			WLAN Access Point Name
**W3C**				World Wide Web Consortium (Internet)
**W3S**				World Wide Web Server (Internet)
**WAC**				Wide Area Center (Mobilfunknetze)
**WACA**			Write Access Connection Acceptor (Datenfelder)
**WACI**			Write Access Connection Initiator (Datenfelder)
**WACS**			Wireless Access Communication System (Mobilfunknetze)
**WAD**				Wohnungsanschlussdose (Anschlussnetze)
**WAE**				Wireless Application Environment (Mobilfunknetze)
**WAF**				Web Application Firewall
**WAG**				WLAN Access Gateway
**WAIS**			Wide Area Information Server (Internet)
**WAL**				Wohnungsanschlussleitung (Anschlussnetze)
**WAN**				Wide Area Network 
**WAN**				Wide Area Network --> Weitverkehrsnetz
**WAP**				Web Application Protection
**WAP**				Wide Area Paging (Mobilfunknetze)
**WAP**				Wireless Application Protocol (Protokolle, Mobilfunknetze)
**WARC**			World Administrative Radio Conference (Gremien, Organisationen)
**WARP**			Wavelength Routing Protocol (Protokolle, Optische Netze, SDH, Sonet)
**WATM**			Wireless ATM --> Funk-ATM (ATM, B-ISDN)
**WATS**			Wide Area Telecommunication Service (Weitverkehrsnetze)
**WBC**				Wideband Channel --> Weitbandkanal (FDDI)
**WBEM**			Web Based Enterprise Mangement (Netzmanagement)
**WBL**				Work Backlog Recovery
**WBS**				Work Breakdown Structure
**WC**				Wiring Closet --> Verteilerraum (Verkabelung)
**WCAC**			Wireless CAC (Mobilfunknetze)
**WCCP**			Web Cache Coordination Protocol --> Anwendungsprotokoll im Internet (Protokolle)
**WCDMA**			Wideband Code Division Multiple Access (Mobilfunknetze)
**WCM**				Web Content Management (Internet)
**WDA**				Wireless Digital Assistant (Telekommunikations-Endgeräte)
**WDLC**			Wireless Data Link Control (Mobilfunknetze)
**WDM**				Wavelength Division Multiplexing --> Wellenlängenmultiplex (Optische Netze, SDH, Sonet)
**WDOG**			Watchdog Protocol (Novell) (Protokolle)
**WDP**				Wireless Datagram Protocol --> (Protokolle, Mobilfunknetze)
**WDSL**			Wireless Digital Subscriber Line (Anschlussnetze)
**WECA**			Wireless Ethernet Compatibility Alliance (Wireless LAN) (Gremien, Organisationen)
**WEP**				Wired Equivalent Privacy (Protokolle, Wireless LAN, IT-Sicherheit)
**WEPA**			Wired Equivalent Privacy Algorithm (Wireless LAN)
**WER**				Word Error Rate
**WFQ**				Weighted Fair Queuing (Weitverkehrsnetze)
**WIM**				Wireless Identification Module (Mobilfunknetze)
**WIN**				Wissenschaftsnetz (Forschungsnetze)
**WINS**			Windows Internet Naming Service (Internet, Microsoft)
**WIS**				WAN Interface Sublayer (Ethernet)
**WITL**			Wireless in the Loop (Anschlussnetze)
**WLAN**			Wireless LAN (IEEE 802.11)
**WLAN**			Wireless Local Area Network
**WLANA**			Wireless LAN Association (Gremien, Organisationen)
**WLFS**			Workstation LAN File Service
**WLIF**			Wireless LAN Interoperability Forum (Gremien, Organisationen)
**WLL**				Wireless Local Loop (Anschlussnetze)
**WLM**				Wave Length Modulation (Optische Netze, SDH, Sonet)
**WM**				Western Modular --> Steckertyp (Verkabelung)
**WM**				Winkelmodulation (Modulation)
**WMAC**			Wireless Media Access Control (Wireless LAN)
**WMAN**			Wireless Metropolitan Area Network (Stadt-, Regionalnetze)
**WMF**				Windows Meta File
**WML**				Wireless Markup Language
**WMM**				Wifi Multimedia
**WNM**				Workstation Networking Module
**WOSA**			Windows Open Service Architecture
**WPA**				Wi-Fi Protected Access (WLAN, IT-Sicherheit)
**WPABX**			Wireless Private Automatic Branch Exchange (Mobilfunknetze)
**WPAN**			Wireless Personal Area Network (Wireless LAN)
**WPKI**			Wireless Public Key Infrastructure (IT-Sicherheit)
**WR**				Write (Zeichen, Zeichensatz)
**WRED**			Weighted Random Early Detection (Internetworking)
**WSDL**			Web Services Description Language (Internet)
**WSF**				Workstation Function (Netzmanagement)
**WSFL**			Web Service Flow Language (Online-Dienste)
**WSO**				Web Sign On (IT-Sicherheit)
**WSP**				Wireless Session Protocol (Protokolle, Mobilfunknetze)
**WTA**				Wireless Telephony Application (Mobilfunknetze)
**WTAI**			Wireless Telephony Application Interface (Mobilfunknetze)
**WTDM**			Wavelength Time Division Multiplexing (Verkabelung)
**WTLS**			Wireless Transport Layer Security (Mobilfunknetze)
**WTP**				Wireless Transaction Protocol (Protokolle, Mobilfunknetze)
**WTSC**			World Telecommunication Standardization Conference (Gremien, Organisationen)
**WUBR**			Weighted Unspecified Bit Rate (ATM, B-ISDN)
**WV**				Wellenlängenvielfach (Optische Netze, SDH, Sonet)
**WWAN**			Wireless Wide Area Network (Weitverkehrsnetze)
**WWDM**			Wide Wavelength Division Multiplex
**WWW**				World Wide Web
**WWW**				World Wide Web (Internet)
**WYSIWYG**			What You See Is What You Get
**WiFi**			Wireless Fidelity (Wireless LAN)
**WinSock**			Windows Socket
**WpHG**			Wertpapierhandelsgesetz
**WÜP**				Wohnungs-Übergabepunkt (Anschlussnetze)
**X.25**			ITU-T-Standard für paketvermittelndes Netz
**X.400**			ISO/OSI-Standard für den Nachrichtenaustausch
**X.75**			ITU-T-Standard zur Kopplung paketvermittelnder Netze
**X25NET**			X.25-Network
**XAML**			Transaction Authority Markup Language
**XAPIA**			X.400 API Association
**XAUI**			10 Gigabit Attachment Unit Interface (Ethernet)
**XAUTH**			Extended Authentification (IT-Sicherheit)
**XBM**				X-Bitmap
**XBSA**			X/Open Backup Service API (Speichernetze)
**XC**				Cross Copy
**XCBC**			XOR Cipher Block Chaining
**XCCDF**			Extensible Configuration Checklist Description Format
**XDCS**			X/Open Distributed Computing Structure
**XDF**				Xrm Decrease Factor (ATM, B-ISDN)
**XDM**				Extended Data Management (Weitverkehrsnetze)
**XDMCP**			X-Display Manager Control Protocol (Protokolle)
**XDR**				External Data Representation
**XGM**				Cross Gain Modulation (Optische Netze, SDH, Sonet)
**XGMII**			10 Gigabit Medium Independent Interface (Ethernet)
**XGXS**			XGMII Extender Sublayer (Ethernet)
**XHTML**			Extensible Hypertext Markup Language
**XI**				X.25 SNA Interconnect (DEC/SNA)
**XLINK**			Extended Local Informatik-Netz Karlsruhe (Offene NW-Konzepte)
**XLR**				Extra Long Range
**XMI**				XML Metadata Interchange
**XML**				Extended Markup Language
**XML**				Extensible Markup Language
**XMP**				X/Open Management Protocol (Protokolle)
**XNS**				Xerox Network System --> Datenübertragungs- und Routing-Protokolle (Xerox) (Protokolle)
**XON/XOFF**			Ein/Aus --> Steuersignal
**XOT**				X.25 over TCP/IP (Cisco) (Internetworking)
**XPM**				Cross Phase Modulation (Optische Netze, SDH, Sonet)
**XREF**			Cross Reference Table --> Querverweisliste
**XRF**				Extended Recovery Facility
**XRN**				Expandable Resilient Networking (Offene NW-Konzepte)
**XSD**				XML Schema Definition
**XSL**				Extensible Stylesheet Language
**XSL**				Extensible Stylesheet Language  
**XSLT**			Extensible Stylesheet Language Transformation 
**XSS**				Cross-site Scripting
**XT**				Crosstalk --> Übersprechen (Verkabelung)
**XTA**				Cross Talk Attenuation (Verkabelung)
**XTAL**			Crystal --> Quarz
**XTC**				External Transmit Clock
**XTI**				Extended Transport Layer Interface
**XTP**				Xypress Transfer Protocol (Protokolle)
**Xrm**				ABR-Service Parameter (ATM, B-ISDN)
**YP**				Yellow Pages --> Verzeichnisdienst-Protokoll (Sun) (Protokolle)
**Z-DCN**			Zentrales Data Communication Network
**Z-Netz**			Zerberus-Netz
**ZBTSI**			Zero Byte Time Slot Interchange --> Leitungscodierungsverfahren (T1)
**ZF**				Zwischenfrequenz
**ZFe**				Zentrum für Fernmeldebetrieb (Gremien, Organisationen)
**ZGR**				Zugangsrechner (Netzkomponenten)
**ZGS**				Zeichengabesystem (ISDN)
**ZH**				Zero Halogen --> Halogenfreie Mischung (Verkabelung)
**ZIG**				Zählimpulsgeber (Weitverkehrsnetze)
**ZIP**				Zone Information Protocol (AppleTalk) (Protokolle)
**ZM**				Zeitmultiplex (DK-Übertragungstechniken)
**ZN**				Zertifikatnehmer (IT-Sicherheit)
**ZS**				Zeichensynchronisation (DK-Übertragungstechniken)
**ZSB**				Zweiseitenbandmodulation (Modulation)
**ZSI**				Zentralstelle für die Sicherheit in der Informationstechnik (Gremien, Organisationen)
**ZSL**				Zero Slot LAN (Lokale Netze)
**ZT**				Zentrum für Telekommunikation (Gremien, Organisationen)
**ZV**				Zeitvielfach
**ZVEI**			Zentralverband der elektrotechnischen Industrie (Gremien, Organisationen)
**ZVSt**			Zentralvermittlungsstelle (Weitverkehrsnetze)
**ZWR**				Zwischenregenerator
**ZZF**				Zentrale Zulassungsstelle für Geräte der Telekommunikation (Gremien, Organisationen)
**ZZG**				Zentralkanalzeichengabe (ISDN, SS#7)
**ZZK**				Zentraler Zeichengabekanal (ISDN, SS#7)
**Zell-PE**			Zell-Polyäthylen (Verkabelung)
**ZfCH**			Zentralstelle für das Chiffrierwesen (Gremien, Organisationen)
**ZfM**				Zentralamt für Mobilfunk (Gremien, Organisationen)
**a/b-Schnittstelle**		Zweidrähtige Schnittstelle zum analogen Fernsprechnetz
**a/b-Wandler**			Einrichtung zum Anschluß von analogen Endgeräten an das ISDN
**bBKVrSt**			Benutzerseitige BK-Verstärkerstelle
**bit/sec**			Bits pro Sekunde --> Maßeinheit für Informationsgeschwindigkeit
**bps**				Bits Per Second --> Bits pro Sekunde --> Maßeinheit für die Übertragungsgeschwindigkeit
**dB**				Decibel --> Logarithmische Maßeinheit
**dBm**				Dezibel Milliwatt bzw. Dezibel Millivolt
**dpi**				Dots Per Inch --> Auflösungsmaß
**ebXML**			E-Business XML (Online-Dienste)
**fps**				Frames per Second (Übertragungsgeschwindigkeit)
**iSCSI**			Small Computer System Interface over IP
**iServer**			The Vattenfall Enterprise Architecture Management portal
**isoEnet**			Isochronous Ethernet (ISLAN16-T, IVDLAN oder IVD)
**k**				kilo --> Zahlensysteme (Präfix für Tausend)
**kHz**				Kilohertz --> Tausend Schwingungen pro Sekunde
**kbit/s**			Kilobits per Second --> Kilobit pro Sekunde (Übertragungsgeschwindigkeit)
**kbps**			Kilobits per Second --> Kilobit pro Sekunde (Übertragungsgeschwindigkeit)
**m**				Milli --> Präfix für Tausendstel (Zahlensysteme)
**n**				Nano --> Präfix für Milliardstel (Zahlensysteme)
**n.e.f.**			Nicht entflammbar (Verkabelung)
**nömL**			Nicht öffentlicher mobiler Landfunk (Mobilfunknetze)
**pRDM**			Physical mode raw device mapping, presents a LUN directly to a VM. (VMware)
**ppm**				Parts per Million (= 10-6)
**routed**			Route Daemon
**rsh**				Remote Shell
**rwho**			Remote Who (Protokolle)
**s.e.f.**			Schwer entflammbar (Verkabelung)
**s.v.**			Selbstverlöschend (Verkabelung)
**self-FEXT**			Fernnebensprechen gleichartiger Systeme
**self-NEXT**			Nahnebensprechen gleichartiger Systeme
**vBNS**			Very Highspeed Backbone Network Service
**vCLI**			vSphere Command Line Interface, allows tasks to be run against hosts and vCenter Server. (VMware)
**vCSA**			Virtual appliance edition of vCenter Server. (VMware)
**vCSA**			vCenter Server Appliance (VMware)
**vCenter Linked Mode**		A way of pooling vCenter Servers, typically across geographies. (VMware)
**vCenter Server Heartbeat**	Will keep the vCenter Server available in the event a host fails which is running vCenter. (VMware)
**vCenter Server**		Server application that runs vSphere. (VMware)
**vCloud Automation Center**	IT service delivery through policy and portals, get familiar with vCAC. (VMware)
**vCloud Director**		Application to pool vCenter environments and enable self-deployment of VMs. (VMware)
**vDS**				vNetwork Distributed Switch, an enhanced version of the virtual switch. (VMware)
**vMotion**			A VM migration technique. (VMware)
**vRDM**			Virtual mode raw device mapping, encapsulates a path to a LUN specifically for one VM in a VMDK. (VMware)
**vSphere Client**		Administrative interface of vCenter Server. (VMware)
**vSphere DRS**			Distributed Resource Scheduler, service that manages performance of VMs. (VMware)
**vSphere HA**			High Availability, will restart a VM on another host if it fails. (VMware)
**vSphere SDRS**		Storage DRS, manages free space and datastore latency for VMs in pools. (VMware)
**vSphere Web Client**		Web-based administrative interface of vCenter Server. (VMware)
**vSwitch**			A virtual switch, places VMs on a physical network. (VMware)
**vdx**				Vollduplex
**vswif**			Virtual Switch Interface (VMware)
**xDSL**			Digital Subscriber Line vom Typ x (Anschlussnetze)
**ynq**				Yes no quit
**ÖML**				Öffentlicher Mobiler Landfunk (Mobilfunknetze)
**ÖN**				Öffentliches Netz (Weitverkehrsnetze)
**ÖbL**				Öffentlicher bewegter Landfunk
**ÜN**				Übertragungsnetz
**ÜP**				Übergabepunkt
**ÜSFu**			Übersee-Sendefunkstelle
**üBKVrSt**			Übergeordnete BK-Verstärkerstelle